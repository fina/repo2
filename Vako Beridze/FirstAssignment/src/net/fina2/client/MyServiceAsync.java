package net.fina2.client;

import java.util.ArrayList;

import net.fina2.shared.Person;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MyServiceAsync {

	void selectAll(AsyncCallback<ArrayList<Person>> callback);

	void getPerson(String id, AsyncCallback<Person> callback);

	void insert(Person person, AsyncCallback<Person> callback);

	void update(Person person, AsyncCallback<Person> callback);

	void delete(Person person, AsyncCallback<Boolean> callback);

}
