package net.fina2.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;

public class FirstAssignment implements EntryPoint {

	public void onModuleLoad() {
		MyServiceAsync myService = GWT.create(MyService.class);
		HandlerManager eventBus = new HandlerManager(null);
		AppController appViewer = new AppController(myService, eventBus);
		appViewer.go(RootPanel.get("Container"));
	}
}
