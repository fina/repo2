package net.fina2.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditPersonEventHandler extends EventHandler {
	
	void onEditPerson(EditPersonEvent event);

}
