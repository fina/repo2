package net.fina2.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DeletePersonEventHandler extends EventHandler  {
	void onDeletePerson(DeletePersonEvent event);
}
