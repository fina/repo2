package net.fina2.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class DeletePersonEvent extends GwtEvent<DeletePersonEventHandler> {

	public static Type<DeletePersonEventHandler> TYPE = new Type<DeletePersonEventHandler>();
	private final String id;
	
	public DeletePersonEvent(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DeletePersonEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeletePersonEventHandler handler) {
		handler.onDeletePerson(this);
	}
}
