package net.fina2.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditPersonEvent extends GwtEvent<EditPersonEventHandler> {

	public static Type<EditPersonEventHandler> TYPE = new Type<EditPersonEventHandler>();
	private final String id;
	
	public EditPersonEvent(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}


	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditPersonEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EditPersonEventHandler handler) {
		handler.onEditPerson(this);
	}

}
