package net.fina2.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface PersonUpdatedEventHandler extends EventHandler {

	void onPersonUpdated(PersonUpdatedEvent event);
}
