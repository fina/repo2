package net.fina2.client.event;

import net.fina2.shared.Person;

import com.google.gwt.event.shared.GwtEvent;

public class PersonUpdatedEvent extends GwtEvent<PersonUpdatedEventHandler> {

	public static Type<PersonUpdatedEventHandler> TYPE = new Type<PersonUpdatedEventHandler>();
	private final Person updatedPerson;

	public PersonUpdatedEvent(Person updatedPerson) {
		this.updatedPerson = updatedPerson;
	}

	public Person getUpdatedPerson() {
		return updatedPerson;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<PersonUpdatedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(PersonUpdatedEventHandler handler) {
		handler.onPersonUpdated(this);

	}

}
