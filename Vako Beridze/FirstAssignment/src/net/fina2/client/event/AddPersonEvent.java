package net.fina2.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddPersonEvent extends GwtEvent<AddPersonEventHandler> {
	
	public static Type<AddPersonEventHandler> TYPE = new Type<AddPersonEventHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AddPersonEventHandler> getAssociatedType() {
		// TODO Auto-generated method stub
		return TYPE;
	}

	@Override
	protected void dispatch(AddPersonEventHandler handler) {
		handler.onAddPerson(this);
	}

}
