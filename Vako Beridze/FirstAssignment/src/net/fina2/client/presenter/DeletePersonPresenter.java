package net.fina2.client.presenter;

import net.fina2.client.MyServiceAsync;
import net.fina2.client.event.PersonUpdatedEvent;
import net.fina2.shared.Person;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class DeletePersonPresenter implements Presenter {

	public interface Display {
		HasClickHandlers getYesButton();

		HasClickHandlers getNoButton();

		Widget asWidget();
	}

	private Person person;
	private final MyServiceAsync myService;
	private final HandlerManager eventBus;
	private final Display display;

	public DeletePersonPresenter(MyServiceAsync myService,
			HandlerManager eventBus, Display display, String personId) {
		this.myService = myService;
		this.eventBus = eventBus;
		this.display = display;
		bind();

		this.myService.getPerson(personId, new AsyncCallback<Person>() {

			@Override
			public void onSuccess(Person result) {
				person = result;
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error retrieving person! while deleting");
			}
		});
	}

	private void bind() {
		this.display.getYesButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				doDelete();
			}
		});

		this.display.getNoButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				((DialogBox) display.asWidget()).hide();
			}
		});
	}

	protected void doDelete() {
		myService.delete(person, new AsyncCallback<Boolean>() {

			@Override
			public void onSuccess(Boolean result) {
				if (result) {
					eventBus.fireEvent(new PersonUpdatedEvent(null));
					((DialogBox) display.asWidget()).hide();
				} else {
					Window.alert("error while deleting Person !");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("error while deleting Person !");
			}
		});
	}

	@Override
	public void go(HasWidgets container) {
		((DialogBox) display.asWidget()).center();
	}

}
