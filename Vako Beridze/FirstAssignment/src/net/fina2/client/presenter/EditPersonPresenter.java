package net.fina2.client.presenter;

import net.fina2.client.MyServiceAsync;
import net.fina2.client.event.PersonUpdatedEvent;
import net.fina2.shared.Person;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class EditPersonPresenter implements Presenter {

	public interface Display {
		HasClickHandlers getSaveButton();

		HasClickHandlers getCancelButton();

		HasClickHandlers getErrorLabel();

		HasValue<String> getFirstName();

		HasValue<String> getLastName();

		HasValue<String> getEmailAddress();

		Widget asWidget();
	}

	private Person person;
	private final MyServiceAsync myService;
	private final HandlerManager eventBus;
	private final Display display;

	public EditPersonPresenter(MyServiceAsync myService,
			HandlerManager eventBus, Display display) {
		this.person = new Person();
		this.myService = myService;
		this.eventBus = eventBus;
		this.display = display;
		bind();
	}

	public EditPersonPresenter(MyServiceAsync myService,
			HandlerManager eventBus, Display display, String personId) {

		this.myService = myService;
		this.eventBus = eventBus;
		this.display = display;
		bind();

		this.myService.getPerson(personId, new AsyncCallback<Person>() {

			@Override
			public void onSuccess(Person result) {
				person = result;
				EditPersonPresenter.this.display.getFirstName().setValue(
						person.firstName);
				EditPersonPresenter.this.display.getLastName().setValue(
						person.lastName);
				EditPersonPresenter.this.display.getEmailAddress().setValue(
						person.email);
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error retrieving person");
			}
		});

	}

	private void bind() {

		this.display.getSaveButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (!display.getFirstName().getValue().equals("")
						&& !display.getLastName().getValue().equals("")
						&& !display.getEmailAddress().getValue().equals("")) {

					// ((Label) display.getErrorLabel()).setVisible(false);
					doSave();
				} else {
					((Label) display.getErrorLabel())
							.setStyleName("serverResponseLabelError");
					((Label) display.getErrorLabel()).setVisible(true);
				}
			}
		});

		this.display.getCancelButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				((DialogBox) display.asWidget()).hide();
			}
		});
	}

	private void doSave() {
		person.setFirstName(display.getFirstName().getValue());
		person.setLastName(display.getLastName().getValue());
		person.setEmail(display.getEmailAddress().getValue());

		if (person.getId() == 0) {
			myService.insert(person, new AsyncCallback<Person>() {

				@Override
				public void onSuccess(Person result) {
					// Window.alert("Person added ");
					((DialogBox) display.asWidget()).hide();
					eventBus.fireEvent(new PersonUpdatedEvent(result));
				}

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Error adding Person");
				}
			});
		} else {

			myService.update(person, new AsyncCallback<Person>() {

				@Override
				public void onSuccess(Person result) {

					//Window.alert("Person updated Event");
					((DialogBox) display.asWidget()).hide();
					eventBus.fireEvent(new PersonUpdatedEvent(result));
				}

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Error updating Person");
				}
			});
		}
	}

	@Override
	public void go(HasWidgets container) {
		((DialogBox) display.asWidget()).center();
	}

}
