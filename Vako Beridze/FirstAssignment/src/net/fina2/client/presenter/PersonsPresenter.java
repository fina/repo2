package net.fina2.client.presenter;

import java.util.ArrayList;
import java.util.List;

import net.fina2.client.MyServiceAsync;
import net.fina2.client.event.AddPersonEvent;
import net.fina2.client.event.DeletePersonEvent;
import net.fina2.client.event.EditPersonEvent;
import net.fina2.shared.Person;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class PersonsPresenter  implements Presenter {

	public interface Display {
		
		HasClickHandlers getAddButton();
		HasClickHandlers getUpdateButton();
		HasClickHandlers getDeleteButton();
		HasClickHandlers getList();
		void setData(List<Person> data);
		Person getClickedPerson();
//		List<Integer> getSelectedRows();
		Widget asWidget();
		
	}

	private final MyServiceAsync myService;
	private final HandlerManager eventBus;
	private final Display display;

	public PersonsPresenter(MyServiceAsync myService, HandlerManager eventBus,	Display view) {
		this.myService = myService;
		this.eventBus = eventBus;
		this.display = view;
	}

	public void bind() {

		display.getAddButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new AddPersonEvent());
			}
		});
		
		display.getUpdateButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new EditPersonEvent(String.valueOf(display.getClickedPerson().getId())));
			}
		});
		
		display.getDeleteButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				//Window.alert("deleting person with id: "+String.valueOf(display.getClickedPerson().getId()));
				eventBus.fireEvent(new DeletePersonEvent(String.valueOf(display.getClickedPerson().getId())));
			}
		});

	}

	@Override
	public void go(HasWidgets container) {
		
		bind();
		container.clear();
		container.add(display.asWidget());
		fetchContactDetails();
	}

	private void fetchContactDetails() {
		
		myService.selectAll(new AsyncCallback<ArrayList<Person>>() {
			
			@Override
			public void onSuccess(ArrayList<Person> result) {
				display.setData(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching persons");
			}
		});
	}

}
