package net.fina2.client;

import net.fina2.client.event.AddPersonEvent;
import net.fina2.client.event.AddPersonEventHandler;
import net.fina2.client.event.DeletePersonEvent;
import net.fina2.client.event.DeletePersonEventHandler;
import net.fina2.client.event.EditPersonEvent;
import net.fina2.client.event.EditPersonEventHandler;
import net.fina2.client.event.PersonUpdatedEvent;
import net.fina2.client.event.PersonUpdatedEventHandler;
import net.fina2.client.presenter.DeletePersonPresenter;
import net.fina2.client.presenter.EditPersonPresenter;
import net.fina2.client.presenter.PersonsPresenter;
import net.fina2.client.presenter.Presenter;
import net.fina2.client.view.DeletePersonView;
import net.fina2.client.view.EditPersonView;
import net.fina2.client.view.PersonsView;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;

public class AppController implements Presenter {

	private final MyServiceAsync myService;
	private final HandlerManager eventBus;
	private HasWidgets container;

	public AppController(MyServiceAsync myService, HandlerManager eventBus) {
		this.myService = myService;
		this.eventBus = eventBus;
		bind();
	}

	private void bind() {

		eventBus.addHandler(AddPersonEvent.TYPE, new AddPersonEventHandler() {

			@Override
			public void onAddPerson(AddPersonEvent addPersonEvent) {
				doAddNewContact();
			}
		});

		eventBus.addHandler(EditPersonEvent.TYPE, new EditPersonEventHandler() {

			@Override
			public void onEditPerson(EditPersonEvent event) {
				doEditPerson(event.getId());
			}
		});

		eventBus.addHandler(PersonUpdatedEvent.TYPE,
				new PersonUpdatedEventHandler() {

					@Override
					public void onPersonUpdated(PersonUpdatedEvent event) {
						doUpdateTable();
					}
				});

		eventBus.addHandler(DeletePersonEvent.TYPE,
				new DeletePersonEventHandler() {

					@Override
					public void onDeletePerson(DeletePersonEvent event) {
						doDeletePerson(event.getId());
					}
				});
	}

	protected void doAddNewContact() {
		new EditPersonPresenter(myService, eventBus, new EditPersonView())
				.go(container);
	}

	protected void doEditPerson(String id) {
		new EditPersonPresenter(myService, eventBus, new EditPersonView(), id)
				.go(container);
	}

	protected void doDeletePerson(String id) {
		new DeletePersonPresenter(myService, eventBus, new DeletePersonView(),
				id).go(container);
	}

	protected void doUpdateTable() {
		new PersonsPresenter(myService, eventBus, new PersonsView())
				.go(container);
	}

	@Override
	public void go(HasWidgets container) {
		this.container = container;
		doUpdateTable();
	}

}
