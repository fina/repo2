package net.fina2.client;

import java.util.ArrayList;

import net.fina2.shared.Person;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("myService")
public interface MyService extends RemoteService {
	
	ArrayList<Person> selectAll();
	Person insert(Person person);
	Person update(Person person);
	boolean delete(Person person);
	Person getPerson(String id);

}
