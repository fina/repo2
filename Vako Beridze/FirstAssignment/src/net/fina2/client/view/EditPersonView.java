package net.fina2.client.view;

import net.fina2.client.presenter.EditPersonPresenter;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class EditPersonView extends DialogBox implements EditPersonPresenter.Display {

	private final TextBox firstName;
	private final TextBox lastName;
	private final TextBox emailAddress;
	private final Button saveButton;
	private final Button cancelButton;
	private final Label errorLabel;

	public EditPersonView() {

		VerticalPanel panel = new VerticalPanel();
		firstName = new TextBox();
		lastName = new TextBox();
		emailAddress = new TextBox();
		saveButton = new Button("save");
		cancelButton = new Button("cancel");
		errorLabel = new Label("fill all fields");
		errorLabel.setVisible(false);
		setUpDialogBox(panel);
		setWidget(panel);
	}

	private void setUpDialogBox(VerticalPanel panel) {

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.add(errorLabel);
		vPanel.add(firstName);
		vPanel.add(lastName);
		vPanel.add(emailAddress);

		FlowPanel flowPanel = new FlowPanel();
		flowPanel.add(saveButton);
		flowPanel.add(cancelButton);
		
		panel.add(vPanel);
		panel.add(flowPanel);
	}

	@Override
	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	@Override
	public HasValue<String> getFirstName() {
		return firstName;
	}

	@Override
	public HasValue<String> getLastName() {
		return lastName;
	}

	@Override
	public HasValue<String> getEmailAddress() {
		return emailAddress;
	}
	
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public HasClickHandlers getErrorLabel() {
		return errorLabel;
	}
}
