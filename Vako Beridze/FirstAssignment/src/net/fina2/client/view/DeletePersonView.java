package net.fina2.client.view;

import net.fina2.client.presenter.*;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DeletePersonView extends DialogBox implements
		DeletePersonPresenter.Display {

	private final Button yesButton;
	private final Button noButton;
	private final Label questionLabel;

	public DeletePersonView() {
		VerticalPanel vPanel = new VerticalPanel();
		FlowPanel fPanel = new FlowPanel();
		questionLabel = new Label("are you sure?");
		yesButton = new Button("yes");
		noButton = new Button("no");
		fPanel.add(yesButton);
		fPanel.add(noButton);
		
		vPanel.add(questionLabel);
		vPanel.add(fPanel);
		setWidget(vPanel);
	}

	@Override
	public HasClickHandlers getYesButton() {
		return yesButton;
	}

	@Override
	public HasClickHandlers getNoButton() {
		return noButton;
	}
	
	@Override
	public Widget asWidget() {
		return this;
	}

}
