package net.fina2.client.view;

import java.util.List;

import net.fina2.shared.Person;
import net.fina2.client.presenter.PersonsPresenter;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class PersonsView extends Composite implements PersonsPresenter.Display {

	private final Button addButton;
	private final Button updateButton;
	private final Button deleteButton;
	private CellTable<Person> personsTable;

	public PersonsView() {
		HorizontalPanel hPanel = new HorizontalPanel();
		addButton = new Button("add");
		addButton.addStyleName("Button");
		updateButton = new Button("update");
		updateButton.addStyleName("Button");
		updateButton.setEnabled(false);
		deleteButton = new Button("delete");
		deleteButton.addStyleName("Button");
		deleteButton.setEnabled(false);
		personsTable = new CellTable<Person>();
		setUpCellTable(personsTable);

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.add(addButton);
		vPanel.add(updateButton);
		vPanel.add(deleteButton);
		hPanel.add(vPanel);

		DecoratorPanel contentTableDecorator = new DecoratorPanel();
		contentTableDecorator.add(personsTable);
		hPanel.add(contentTableDecorator);
		initWidget(hPanel);
	}

	private void setUpCellTable(CellTable<Person> table) {

		TextColumn<Person> idColumn = new TextColumn<Person>() {
			@Override
			public String getValue(Person person) {
				return String.valueOf(person.id);
			}
		};
		table.addColumn(idColumn, "id");

		TextColumn<Person> firstNameColumn = new TextColumn<Person>() {
			@Override
			public String getValue(Person person) {
				return person.firstName;
			}
		};
		table.addColumn(firstNameColumn, "firstName");

		TextColumn<Person> lastNameColumn = new TextColumn<Person>() {
			@Override
			public String getValue(Person person) {
				return person.lastName;
			}
		};
		table.addColumn(lastNameColumn, "lastName");

		TextColumn<Person> emailColumn = new TextColumn<Person>() {
			@Override
			public String getValue(Person person) {
				return person.email;
			}
		};
		table.addColumn(emailColumn, "email");

		// Add a selection model to handle user selection.
		final SingleSelectionModel<Person> selectionModel = new SingleSelectionModel<Person>();
		table.setSelectionModel(selectionModel);
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						Person selected = selectionModel.getSelectedObject();
						if (selected != null) {
							updateButton.setEnabled(true);
							deleteButton.setEnabled(true);
						} else {
							updateButton.setEnabled(false);
							deleteButton.setEnabled(false);
						}
					}
				});

	}

	@Override
	public HasClickHandlers getAddButton() {
		return addButton;
	}

	@Override
	public HasClickHandlers getUpdateButton() {
		return updateButton;
	}

	@Override
	public HasClickHandlers getDeleteButton() {
		return deleteButton;
	}

	@Override
	public HasClickHandlers getList() {
		return (HasClickHandlers) personsTable;
	}

	@Override
	public void setData(List<Person> data) {

		personsTable.setRowCount(data.size(), true);
		personsTable.setRowData(data);
	}

	@Override
	public Person getClickedPerson() {
		@SuppressWarnings("unchecked")
		SingleSelectionModel<Person> selectionModel = (SingleSelectionModel<Person>) personsTable.getSelectionModel();
		Person p = selectionModel.getSelectedObject();
		if (p == null)
			Window.alert("p is null");
		return p;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

}
