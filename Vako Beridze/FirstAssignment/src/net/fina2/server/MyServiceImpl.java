package net.fina2.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import net.fina2.client.MyService;
import net.fina2.shared.Person;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class MyServiceImpl extends RemoteServiceServlet implements MyService {

	@Override
	public ArrayList<Person> selectAll() {

		ArrayList<Person> persons = new ArrayList<Person>();

		// The name of the file to open.
		String fileName = "data.txt";

		// This will reference one line at a time
		String line = null;

		try {
			
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			Person person;
			while ((line = bufferedReader.readLine()) != null) {
				person = new Person();
				String[] tmp = line.split(";");
				person.setId(Long.valueOf(tmp[0]));
				person.setFirstName(tmp[1]);
				person.setLastName(tmp[2]);
				person.setEmail(tmp[3]);
				persons.add(person);
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}
		return persons;
	}

	@Override
	public Person insert(Person person) throws IllegalArgumentException {

		ArrayList<Person> persons = selectAll();
		long lastId = -1;
		for (Person pers : persons) {
			lastId = pers.getId();
		}
//		lastId = persons.get(persons.indexOf(persons.size()-1)).getId();
		person.setId(lastId + 1);
		persons.add(person);

		// The name of the file to open.
		String fileName = "data.txt";

		try {
			// Assume default encoding.
			FileWriter fileWriter = new FileWriter(fileName);

			// Always wrap FileWriter in BufferedWriter.
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Note that write() does not automatically
			// append a newline character.
			for (Person p : persons) {
				bufferedWriter.write(String.valueOf(p.getId()) + ";"
						+ p.getFirstName() + ";" + p.getLastName() + ";"
						+ p.getEmail());
				bufferedWriter.newLine();
			}
			System.out.println("writing file");

			// Always close files.
			bufferedWriter.close();
		} catch (IOException ex) {
			System.out.println("Error writing to file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}

		return person;
	}

	@Override
	public Person update(Person person) throws IllegalArgumentException {

		// The name of the file to open.
		String fileName = "data.txt";

		// This will reference one line at a time
		String line = null;

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			Person p = new Person();
			while ((line = bufferedReader.readLine()) != null) {
				String[] tmp = line.split(";");
				p.setId(Long.valueOf(tmp[0]));
				if (p.getId() == person.id) {
					p.setFirstName(person.getFirstName());
					p.setLastName(person.getLastName());
					p.setEmail(person.getEmail());
					break;
				}
			}
			delete(person);
			ArrayList<Person> persons = selectAll();
			persons.add(p);
			writeAll(persons);

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
		}
		return person;
	}

	@Override
	public Person getPerson(String id) throws IllegalArgumentException {
		// The name of the file to open.
		String fileName = "data.txt";

		// This will reference one line at a time
		String line = null;

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			Person p = new Person();
			while ((line = bufferedReader.readLine()) != null) {
				String[] tmp = line.split(";");
				p.setId(Long.valueOf(tmp[0]));
				if (p.getId() == Long.valueOf(id)) {
					p.setFirstName(tmp[1]);
					p.setLastName(tmp[2]);
					p.setEmail(tmp[3]);
					break;
				}
			}
			// Always close files.
			bufferedReader.close();
			return p;
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
		}
		return null;
	}

	@Override
	public boolean delete(Person person) {

		int index = -1;
		ArrayList<Person> persons = selectAll();
		for (Person p : persons) {
			if (p.getId() == person.getId()) {
				index = persons.indexOf(p);
				break;
			}
		}
		if (index > -1) {
			if (persons.remove(index) != null) {
				System.out.println("person removed with id:" + person.getId());
				writeAll(persons);
				return true;
			} else
				System.out.println("person was not removed with id:"
						+ person.getId());
		} else {
			System.out.println("cannot find person with id:" + person.getId());
		}
		return false;

	}

	private void writeAll(ArrayList<Person> persons) {
		// The name of the file to open.
		String fileName = "data.txt";

		try {
			// Assume default encoding.
			FileWriter fileWriter = new FileWriter(fileName);

			// Always wrap FileWriter in BufferedWriter.
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Note that write() does not automatically
			// append a newline character.
			for (Person p : persons) {
				bufferedWriter.write(String.valueOf(p.getId()) + ";"
						+ p.getFirstName() + ";" + p.getLastName() + ";"
						+ p.getEmail());
				bufferedWriter.newLine();
			}
			System.out.println("writing file");

			// Always close files.
			bufferedWriter.close();
		} catch (IOException ex) {
			System.out.println("Error writing to file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}

	}

}
