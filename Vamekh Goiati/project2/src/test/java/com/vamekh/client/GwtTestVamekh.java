package com.vamekh.client;

import java.util.ArrayList;

import org.junit.Test;

import com.vamekh.server.TemplateServiceImpl;
import com.vamekh.shared.FieldVerifier;
import com.vamekh.shared.Schedule;
import com.vamekh.shared.TemplateDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.junit.tools.GWTTestSuite;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

/**
 * GWT JUnit <b>integration</b> tests must extend GWTTestCase.
 * Using <code>"GwtTest*"</code> naming pattern exclude them from running with
 * surefire during the test phase.
 * 
 * If you run the tests using the Maven command line, you will have to 
 * navigate with your browser to a specific url given by Maven. 
 * See http://mojo.codehaus.org/gwt-maven-plugin/user-guide/testing.html 
 * for details.
 */
public class GwtTestVamekh extends GWTTestSuite {

  /**
   * Must refer to a valid module that sources this class.
   */
  public String getModuleName() {
    return "com.vamekh.VamekhJUnit";
  }

  /**
   * Tests the FieldVerifier.
   */
  @Test
  public void testAddTemplate(){
	  TemplateServiceImpl rpcImpl = new TemplateServiceImpl();
	  TemplateDTO tmp1 = new TemplateDTO( 1, "002", "desc1", Schedule.DAILY);
	  assert(rpcImpl.codeIsUnique(tmp1.getCode(), tmp1.getId()) == false);
	  
  }
  
  

  

}
