package com.vamekh.client;

import com.vamekh.shared.FieldVerifier;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Templates implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		TemplateServiceAsync rpcService = GWT.create(TemplateService.class);
		HandlerManager eventBus = new HandlerManager(null);
		AppController appViewer = new AppController(rpcService, eventBus);
		appViewer.go(RootPanel.get());
	}
}
