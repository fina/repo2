package com.vamekh.client.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vamekh.client.MyListBox;
import com.vamekh.client.presenter.EditTemplatePresenter;
import com.vamekh.shared.Schedule;

public class EditTemplateView extends Composite implements
		EditTemplatePresenter.Display {

	private final TextBox code;
	private final TextBox description;
	private final MyListBox schedule;
	private final FlexTable detailsTable;
	private final Button saveButton;
	private final Button cancelButton;

	public EditTemplateView() {
		DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
		contentDetailsDecorator.setWidth("18em");
		initWidget(contentDetailsDecorator);

		VerticalPanel contentDetailsPanel = new VerticalPanel();
		contentDetailsPanel.setWidth("100%");

		detailsTable = new FlexTable();
		detailsTable.setCellSpacing(0);
		detailsTable.setWidth("100%");
		code = new TextBox();
		description = new TextBox();
		schedule = new MyListBox();
		initDetailsTable();
		contentDetailsPanel.add(detailsTable);

		HorizontalPanel menuPanel = new HorizontalPanel();
		saveButton = new Button("Save");
		cancelButton = new Button("Cancel");
		menuPanel.add(saveButton);
		menuPanel.add(cancelButton);
		contentDetailsPanel.add(menuPanel);
		contentDetailsDecorator.add(contentDetailsPanel);
	}

	private void initDetailsTable() {
		detailsTable.setWidget(0, 0, new Label("Code:"));
		detailsTable.setWidget(0, 1, code);
		detailsTable.setWidget(1, 0, new Label("Description:"));
		detailsTable.setWidget(1, 1, description);
		detailsTable.setWidget(2, 0, new Label("Schedule"));
		detailsTable.setWidget(2, 1, schedule);
		for (Schedule sc : Schedule.values()) {
			schedule.addItem(sc.toString());
		}
		code.setFocus(true);
	}

	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	public HasValue<String> getCode() {
		return code;
	}

	public HasValue<String> getDescription() {
		return description;
	}

	public HasValue<Schedule> getSchedule() {
		return schedule;
	}

	public Widget asWidget() {
		return this;
	}

}
