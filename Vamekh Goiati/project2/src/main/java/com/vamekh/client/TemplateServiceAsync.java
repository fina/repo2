package com.vamekh.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.vamekh.shared.TemplateDTO;

public interface TemplateServiceAsync {

	void addTemplate(TemplateDTO templateDTO,
			AsyncCallback<TemplateDTO> callback);

	void deleteTemplate(int id, AsyncCallback<Boolean> callback);

	void deleteTemplates(ArrayList<Integer> ids,
			AsyncCallback<ArrayList<TemplateDTO>> callback);

	void getTemplates(AsyncCallback<ArrayList<TemplateDTO>> callback);

	void getTemplate(int id, AsyncCallback<TemplateDTO> callback);

	void updateTemplate(TemplateDTO templateDTO,
			AsyncCallback<TemplateDTO> callback);

	void codeIsUnique(String code, int id, AsyncCallback<Boolean> callback);

}
