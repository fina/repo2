package com.vamekh.client.presenter;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.vamekh.client.TemplateServiceAsync;
import com.vamekh.client.event.AddTemplateEvent;
import com.vamekh.client.event.EditTemplateEvent;
import com.vamekh.shared.TemplateDTO;

public class TemplatesPresenter implements Presenter{

	private List<TemplateDTO> templates;
	
	public interface Display {
		HasClickHandlers getAddButton();
	    HasClickHandlers getDeleteButton();
	    HasClickHandlers getList();
	    void setData(List<TemplateDTO> data);
	    int getClickedRow(ClickEvent event);
	    List<Integer> getSelectedRows();
	    Widget asWidget();
	}
	
	private final TemplateServiceAsync rpcService;
	private final HandlerManager eventBus;
	private final Display display;
	
	public TemplatesPresenter(TemplateServiceAsync rpcService, HandlerManager eventBus, Display display){
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = display;
	}
	
	public void bind(){
		
		display.getAddButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new AddTemplateEvent());
			}
		});
		
		display.getDeleteButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				deleteSelectedTemplates();
			}
		});
		
		display.getList().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				int selectedRow = display.getClickedRow(event);
				
				if(selectedRow >= 0){
					int id = templates.get(selectedRow).getId();
					eventBus.fireEvent(new EditTemplateEvent(id));
				}
			}
		});
	
	}
	
	private void deleteSelectedTemplates() {
		
		List<Integer> selectedRows = display.getSelectedRows();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for(int i = 0; i < selectedRows.size(); i++){
			ids.add(templates.get(selectedRows.get(i)).getId());		
		}
		
		rpcService.deleteTemplates(ids, new AsyncCallback<ArrayList<TemplateDTO>>() {
			
			public void onSuccess(ArrayList<TemplateDTO> result) {
				templates = result;
				display.setData(templates);
			}
			
			public void onFailure(Throwable caught) {
				Window.alert("Error deleting selected templates");
			}
		});
		
	}
	
	private void fetchTemplates(){
		rpcService.getTemplates(new AsyncCallback<ArrayList<TemplateDTO>>() {

			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				Window.alert("Error fetching contact details");
			}

			public void onSuccess(ArrayList<TemplateDTO> result) {
				templates = result;
				display.setData(templates);
			}
		});
	}

	public void setTemplates(List<TemplateDTO> templateDTOs){
		this.templates = templateDTOs;
	}
	
	public TemplateDTO getTemplate(int index){
		return templates.get(index);
	}
	
	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
		fetchTemplates();
	}

}
