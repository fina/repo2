package com.vamekh.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.vamekh.shared.TemplateDTO;

@RemoteServiceRelativePath("templateService")
public interface TemplateService extends RemoteService{
	
	TemplateDTO addTemplate(TemplateDTO templateDTO);
	
	boolean deleteTemplate(int id);
	
	ArrayList<TemplateDTO> deleteTemplates(ArrayList<Integer> ids);
	
	ArrayList<TemplateDTO> getTemplates();
	
	TemplateDTO getTemplate(int id);
	
	TemplateDTO updateTemplate(TemplateDTO templateDTO);
	
	boolean codeIsUnique(String code, int id);
	
}
