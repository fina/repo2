package com.vamekh.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vamekh.client.event.AddTemplateEvent;
import com.vamekh.client.event.AddTemplateEventHandler;
import com.vamekh.client.event.EditTemplateCancelledEvent;
import com.vamekh.client.event.EditTemplateCancelledEventHandler;
import com.vamekh.client.event.EditTemplateEvent;
import com.vamekh.client.event.EditTemplateEventHandler;
import com.vamekh.client.event.TemplateUpdatedEvent;
import com.vamekh.client.event.TemplateUpdatedEventHandler;
import com.vamekh.client.presenter.EditTemplatePresenter;
import com.vamekh.client.presenter.Presenter;
import com.vamekh.client.presenter.TemplatesPresenter;
import com.vamekh.client.view.EditTemplateView;
import com.vamekh.client.view.TemplatesView;

public class AppController implements Presenter, ValueChangeHandler<String> {

	private final HandlerManager eventBus;
	private final TemplateServiceAsync rpcService;
	private HasWidgets container;

	public AppController(TemplateServiceAsync rpcService,
			HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.rpcService = rpcService;
		bind();
	}

	private void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(AddTemplateEvent.TYPE,
				new AddTemplateEventHandler() {

					public void onAddTemplate(AddTemplateEvent event) {
						doAddNewTemplate();
					}
				});

		eventBus.addHandler(EditTemplateEvent.TYPE,
				new EditTemplateEventHandler() {

					public void onEditTemplate(EditTemplateEvent event) {
						doEditTemplate(event.getId());
					}
				});

		eventBus.addHandler(EditTemplateCancelledEvent.TYPE,
				new EditTemplateCancelledEventHandler() {

					public void onEditTemplateCancelled(
							EditTemplateCancelledEvent event) {
						doEditTemplateCancelled();
					}
				});

		eventBus.addHandler(TemplateUpdatedEvent.TYPE,
				new TemplateUpdatedEventHandler() {

					public void onTemplateUpdated(TemplateUpdatedEvent event) {
						doTemplateUpdated();
					}
				});

	}

	private void doTemplateUpdated() {
		History.newItem("list");
	}

	private void doEditTemplateCancelled() {
		History.newItem("list");
	}

	private void doEditTemplate(int id) {
		History.newItem("edit", false);
		Presenter presenter = new EditTemplatePresenter(rpcService, eventBus,
				new EditTemplateView(), id);
		presenter.go(container);
	}

	private void doAddNewTemplate() {
		History.newItem("add");
	}

	public void onValueChange(ValueChangeEvent<String> event) {

		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;

			if (token.equals("list")) {
				presenter = new TemplatesPresenter(rpcService, eventBus,
						new TemplatesView());
			} else if (token.equals("add")) {
				presenter = new EditTemplatePresenter(rpcService, eventBus,
						new EditTemplateView());
			} else if (token.equals("edit")) {
				presenter = new EditTemplatePresenter(rpcService, eventBus,
						new EditTemplateView());
			}

			if (presenter != null) {
				presenter.go(container);
			}
		}
	}

	public void go(final HasWidgets container) {
		this.container = container;

		if ("".equals(History.getToken())) {
			History.newItem("list");
		} else {
			History.fireCurrentHistoryState();
		}
	}

}
