package com.vamekh.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vamekh.client.presenter.TemplatesPresenter;
import com.vamekh.shared.TemplateDTO;

public class TemplatesView extends Composite implements
		TemplatesPresenter.Display {

	private final Button addButton;
	private final Button deleteButton;
	private FlexTable templatesTable;
	private final FlexTable contentTable;

	public TemplatesView() {

		DecoratorPanel contentTableDecorator = new DecoratorPanel();
		initWidget(contentTableDecorator);
		contentTableDecorator.setWidth("100%");
		contentTableDecorator.setWidth("18em");

		contentTable = new FlexTable();
		contentTable.setWidth("100%");
		contentTable.getCellFormatter().setWidth(0, 0, "100%");
		contentTable.getFlexCellFormatter().setVerticalAlignment(0, 0,
				DockPanel.ALIGN_TOP);

		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.setBorderWidth(0);
		buttonPanel.setSpacing(0);
		buttonPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		addButton = new Button("Add");
		buttonPanel.add(addButton);
		deleteButton = new Button("Delete");
		buttonPanel.add(deleteButton);

		contentTable.setWidget(0, 0, buttonPanel);

		templatesTable = new FlexTable();
		templatesTable.setCellSpacing(0);
		templatesTable.setCellPadding(0);
		templatesTable.setWidth("100%");
		templatesTable.getColumnFormatter().setWidth(0, "15px");
		contentTable.setWidget(1, 0, templatesTable);

		contentTableDecorator.add(contentTable);

	}

	public HasClickHandlers getAddButton() {
		return addButton;
	}

	public HasClickHandlers getDeleteButton() {
		return deleteButton;
	}

	public HasClickHandlers getList() {
		return templatesTable;
	}

	public void setData(List<TemplateDTO> data) {
		templatesTable.removeAllRows();

		for (int i = 0; i < data.size(); ++i) {
			templatesTable.setWidget(i, 0, new CheckBox());
			templatesTable.setText(i, 1, data.get(i).getCode());
			templatesTable.setText(i, 2, data.get(i).getDescription());
			templatesTable.setText(i, 3, data.get(i).getSchedule().toString());
		}
	}

	public int getClickedRow(ClickEvent event) {
		int selectedRow = -1;
		HTMLTable.Cell cell = templatesTable.getCellForEvent(event);

		if (cell != null) {
			if (cell.getCellIndex() > 0) {
				selectedRow = cell.getRowIndex();
			}
		}

		return selectedRow;
	}

	public List<Integer> getSelectedRows() {
		List<Integer> selectedRows = new ArrayList<Integer>();

		for (int i = 0; i < templatesTable.getRowCount(); i++) {
			CheckBox checkBox = (CheckBox) templatesTable.getWidget(i, 0);
			if (checkBox.getValue()) {
				selectedRows.add(i);
			}
		}

		return selectedRows;
	}

	public Widget asWidget() {
		return this;
	}

}
