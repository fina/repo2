package com.vamekh.server;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.hibernate.Session;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.vamekh.client.TemplateService;
import com.vamekh.shared.Template;
import com.vamekh.shared.TemplateDTO;

public class TemplateServiceImpl extends RemoteServiceServlet implements TemplateService{

	private static final long serialVersionUID = 1L;

	private EntityManager em;
	
	public TemplateDTO addTemplate(TemplateDTO templateDTO) {
		
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		Template template = new Template(templateDTO);
		em.persist(template);
		em.getTransaction().commit();
		
		return templateDTO;
		
	}

	public boolean deleteTemplate(int id) {
	
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Template.class, id));
		em.getTransaction().commit();
		
		return true;
		
	}

	public ArrayList<TemplateDTO> deleteTemplates(ArrayList<Integer> ids) {
		
		for(int id : ids){
			deleteTemplate(id);
		}
		
		return getTemplates();
		
	}

	public ArrayList<TemplateDTO> getTemplates() {
		
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		ArrayList<Template> templates = new ArrayList<Template>(em.createQuery("SELECT e FROM Template e", Template.class).getResultList());
		ArrayList<TemplateDTO> templateDTOs = new ArrayList<TemplateDTO>( templates != null ? templates.size() : 0);
		if(templates != null){
			for(Template template : templates){
				templateDTOs.add(createTemplateDTO(template));
			}
		}
		em.getTransaction().commit();
		
		return templateDTOs;
		
	}

	public TemplateDTO getTemplate(int id) {
		
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		Template template = (Template) em.find(Template.class, id);
		TemplateDTO templateDTO = createTemplateDTO(template);
		em.getTransaction().commit();
		
		return templateDTO;
		
	}

	public TemplateDTO updateTemplate(TemplateDTO templateDTO) {
		
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		Template template = em.find(Template.class, templateDTO.getId());
		template.setCode(templateDTO.getCode());
		template.setDescription(templateDTO.getDescription());
		template.setSchedule(templateDTO.getSchedule());
		em.getTransaction().commit();
		
		return templateDTO;
		
	}
	
	public boolean codeIsUnique(String code, int id){
		
		em = JpaUtil.getEntityManager().createEntityManager();
		em.getTransaction().begin();
		ArrayList<Template> templates = new ArrayList<Template>(em.createQuery("SELECT e FROM Template e WHERE e.code = :tmpCode AND id != :tmpId", Template.class).setParameter("tmpCode", code).setParameter("tmpId", id).getResultList());
		em.getTransaction().commit();
		
		return templates.size() == 0;
		
	}
	
	private TemplateDTO createTemplateDTO(Template template){
		return new TemplateDTO(template.getId(), template.getCode(), template.getDescription(), template.getSchedule());
	}

}
