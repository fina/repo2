package com.myproject.institutions.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.jasper.compiler.ServletWriter;

import com.google.appengine.api.appidentity.AppIdentityServicePb.SigningService.Streams;
import com.myproject.institutions.client.FIManager;
import com.myproject.institutions.shared.Institution;
import com.myproject.institutions.shared.Institutions;

@WebServlet("upload")
public class FileUploadServlet extends HttpServlet {

	private static final String UPLOAD_DIRECTORY = "/uploaded";
	private InstitutionManager FIManager = new InstitutionManager();
	
	private XMLImportExportImpl XMLManager = new XMLImportExportImpl();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/xml");
		response.setHeader("Content-disposition", "attachment; filename=FinancialInstitutions.xml");
		
		Writer outputStream = response.getWriter();
		
		ArrayList<Institution> institutions = FIManager.getInstitutions();
		Institutions institutionList = new Institutions();
		institutionList.setInstitutions(institutions);
		
		try {
				
				/*
				JAXBContext jaxbContextTypes = JAXBContext.newInstance(InstitutionTypes.class);
			    Marshaller jaxbMarshallerTypes = jaxbContextTypes.createMarshaller();
			    
			   jaxbMarshallerTypes.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			     
			    jaxbMarshallerTypes.marshal(institutionTypeList, fw);
				
				*/
			JAXBContext jaxbContext = JAXBContext.newInstance(Institutions.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    
		    
			jaxbMarshaller.marshal(institutionList, outputStream);
			    
		} catch (JAXBException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		
		
	
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FileItemFactory factory = new DiskFileItemFactory();
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			// Parse the request
			List items = upload.parseRequest(request);
			
			// Process the uploaded items
			Iterator iter = items.iterator();

			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				// handling a normal form-field
				if (!item.isFormField()) {
					String fileName = item.getName();
					if (fileName != null) {
						fileName = FilenameUtils.getName(fileName);
					}
					String contentType = item.getContentType();
					if (!contentType.equals("text/xml")) {
						throw new IllegalArgumentException();
					}

					File uploadedFile = new File(getServletContext().getRealPath(UPLOAD_DIRECTORY), fileName);
					
						item.write(uploadedFile);
						response.setStatus(HttpServletResponse.SC_CREATED);
						response.getWriter().print(
								"The file was created successfully.");
						response.flushBuffer();
					if(request.getParameter("mode").equals("import")){
						XMLManager.XMLImport(uploadedFile.getAbsolutePath());
					} else {
						XMLManager.XMLExport(uploadedFile.getAbsolutePath());
					}

				}
			}
		} catch (Exception e) {
			System.out.print("File Upload Failed!" + e.getMessage());
		}
	}
}
