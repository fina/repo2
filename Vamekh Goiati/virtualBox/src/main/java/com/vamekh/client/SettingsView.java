/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vamekh.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.ListView;
import com.sencha.gxt.widget.core.client.Slider;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.container.CardLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer.HBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.Field;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.IntegerField;
import com.sencha.gxt.widget.core.client.form.IntegerSpinnerField;
import com.sencha.gxt.widget.core.client.form.ListField;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.tree.Tree;
import java.util.Arrays;

/**
 *
 * @author vamekh
 */
public class SettingsView extends Window {
    
    private enum ShareOptions {
        
        DISABLED("Disabled"), HOST_TO_GUEST("Host to Guest"), GUEST_TO_HOST("Guest to Host"), BIDIRECTIONAL("Bidirectional");
        
        private String value;
        
        private ShareOptions(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    
    private enum Chipset {
        
        PIIX3, ICH9;
        
    };
    
    private enum IDEType {
        
        PIIX3, PIIX4, ICH6;
        
    };
    
    private enum PointingDevice {
        
        PS2_MOUSE("PS/2 Mouse"), USB_TABLET("USB Tablet"), USB_MULTITOUCH_TABLET("USB Multi-Touch Tablet");
        
        private String value;
        
        private PointingDevice(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    
    private enum HostAudio {
        
        WIN_DIRECT_SOUND("Windows DirectSound"), NULL_AUDIO("Null Audio Driver");
        
        private String value;
        
        private HostAudio(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    
    private enum AudioController {
        
        INTEL_AUDIO("Intel HD Audio"), ICH_AC97("ICH AC97"), SOUNDBLASTER_16("SoundBlaster 16");
        
        private String value;
        
        private AudioController(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    
    private enum NetworkAttachType {
        
        NOT_ATTACHED("Not Attached"), NAT("NAT"), NAT_NETWORK("NAT Network"), BRIDGED_ADAPTER("Bridged Adapter"), INTERNAL_NETWORK("Internal Network"), HOST_ONLY("Host-only Adapter"), GENERIC("Generic Adapter");
        
        private String value;
        
        private NetworkAttachType(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    
    private enum NetworkAdapterType {
        
        PCNETII("PCnet-FAST II (Am79C970A)"), PCNETIII("PCnet-FAST III (Am79C973)"), INTEL_PRO_MT_DESKTOP("Intel PRO/1000 MT Desktop (82540EM)"), INTEL_PRO_T_SERVER("Intel PRO/1000 T Server (82543GC)"), INTEL_PRO_MT_SERVER("Intel PRO/1000 MT Server (82545GC)"), PARAVIRTUALIZED("Paravirtualized Network (virtio-net)");
        
        private String value;
        
        private NetworkAdapterType(String value){
            this.value = value;
        }
        
        
        public String getValue(){
            return value;
        }
       
        @Override
        public String toString(){
            return value;
        }
        
    };
    

    private static final String WINDOW_NAME = " - Settings";
    private static final int WIDTH = 600;
    private static final int HEIGHT = 500;
    private static final String collapsedImage = "images/collapsed.png";
    private static final String expandedImage = "images/expanded.png";
    private VirtualMachine vm;
    private BorderLayoutContainer borderLayout;
    private CardLayoutContainer centerLayout;
    private VBoxLayoutContainer buttonContainer;
    private VerticalLayoutContainer centerContainer;
    private ToggleButton generalButton;
    private ToggleButton systemButton;
    private ToggleButton displayButton;
    private ToggleButton storageButton;
    private ToggleButton audioButton;
    private ToggleButton networkButton;
    private ToggleGroup buttonGroup;
    private HorizontalPanel headingPanel;
    private ContentPanel generalPanel;
    private ContentPanel systemPanel;
    private ContentPanel displayPanel;
    private ContentPanel storagePanel;
    private ContentPanel audioPanel;
    private ContentPanel networkPanel;
    private TabPanel generalTabPanel;
    private TabPanel systemTabPanel;
    private TabPanel displayTabPanel;
    private ComboBox<OperatingSystem> osChooser;
    private OperatingSystem osCurr;
    private ComboBox<ShareOptions> clipBoardChooser;
    private ComboBox<ShareOptions> dragndropChooser;
    private ComboBox<Chipset> chipsetChooser;
    private ComboBox<PointingDevice> pdChooser;
    private ShareOptions clipBoard;
    private ShareOptions dragndrop;
    private Chipset chipset;
    private PointingDevice pd;
    private Slider memorySlider;
    private IntegerSpinnerField memorySpinner;
    private String memory;
    private Slider procSlider;
    private IntegerSpinnerField procSpinner;
    private int procN;
    private Slider execSlider;
    private IntegerSpinnerField execSpinner;
    private int execPercentage;
    private int videoMemory;
    private Slider videoMemSlider;
    private IntegerSpinnerField videoMemSpinner;
    private int monitorCount;
    private Slider monitorCountSlider;
    private IntegerSpinnerField monitorCountSpinner;
    private CheckBox enableServer;
    private FieldLabel portLabel;
    private FieldLabel authTimeoutLabel;
    private FieldLabel extendedFeaturesLabel;
    private IDEType currIdeType;
    private ComboBox<IDEType> typeChooser;
    private CheckBox enableAudio;
    private FieldLabel hostAudioLabel;
    private FieldLabel audioControllerLabel;
    private ComboBox<HostAudio> hostAudioChooser;
    private ComboBox<AudioController> controllerChooser;
    private HostAudio currHostAudio;
    private AudioController currAudioController;
    private Image windowStateImage;
    private ComboBox<NetworkAttachType> attachTypeChooser;
    private NetworkAttachType currAttachType;
    private ComboBox<NetworkAdapterType> netAdapterTypeChooser;
    private NetworkAdapterType currAdapterType;
    private CheckBox enableNetwork;
    private boolean advancedTabVisible;
    private VerticalLayoutContainer networkTabPanel;
    private HBoxLayoutContainer advancedPanelButton;
    private FieldLabel attachTypeLabel;
    private FieldLabel adapterTypeLabel;
    private VerticalLayoutContainer advancedFieldsLayout;
    private HBoxLayoutContainer bottomButtons;
    private TextButton saveButton;
    private TextButton cancelButton;
    
    
    public SettingsView(VirtualMachine vm) {
        
        this.vm = vm;
        setHeadingText(vm.getName() + WINDOW_NAME);
        com.google.gwt.user.client.Window.alert(vm.getName());
        setWidth(WIDTH);
        setHeight(HEIGHT);
        borderLayout = new BorderLayoutContainer();
        setWidget(borderLayout);
        centerLayout = new CardLayoutContainer();
        centerContainer = new VerticalLayoutContainer();
        buttonContainer = new VBoxLayoutContainer();
        generalButton = new ToggleButton("General");
        systemButton = new ToggleButton("System");
        displayButton = new ToggleButton("Display");
        storageButton = new ToggleButton("Storage");
        audioButton = new ToggleButton("Audio");
        networkButton = new ToggleButton("Network");
        buttonGroup = new ToggleGroup();
        headingPanel = new HorizontalPanel();
        generalPanel = new ContentPanel();
        systemPanel = new ContentPanel();
        displayPanel = new ContentPanel();
        storagePanel = new ContentPanel();
        audioPanel = new ContentPanel();
        networkPanel = new ContentPanel();
        generalTabPanel = new TabPanel();
        systemTabPanel = new TabPanel();
        displayTabPanel = new TabPanel();
        windowStateImage = new Image();
        bottomButtons = new HBoxLayoutContainer();
        bottomButtons.setPadding(new Padding(5));
        bottomButtons.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        bottomButtons.setPack(BoxLayoutPack.END);
        saveButton = new TextButton("Save");
        cancelButton = new TextButton("Cancel");
        
        
        initGui();
    }

    private void initGui() {
        
        buttonContainer.add(generalButton);
        buttonContainer.add(systemButton);
        buttonContainer.add(displayButton);
        buttonContainer.add(storageButton);
        buttonContainer.add(audioButton);
        buttonContainer.add(networkButton);
        buttonContainer.setBorders(true);
        buttonContainer.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
        buttonContainer.setPixelSize(120, 400);
        borderLayout.setWestWidget(buttonContainer);
        
        buttonGroup.add(generalButton);
        buttonGroup.add(systemButton);
        buttonGroup.add(displayButton);
        buttonGroup.add(storageButton);
        buttonGroup.add(audioButton);
        buttonGroup.add(networkButton);
        
        ToggleButtonClickHandler myClickHandler = new ToggleButtonClickHandler();
        
        generalButton.addSelectHandler(myClickHandler);
        systemButton.addSelectHandler(myClickHandler);
        displayButton.addSelectHandler(myClickHandler);
        storageButton.addSelectHandler(myClickHandler);
        audioButton.addSelectHandler(myClickHandler);
        networkButton.addSelectHandler(myClickHandler);
        
        initGeneralPanel();
        initSystemPanel();
        initDisplayPanel();
        initStoragePanel();
        initAudioPanel();
        initNetworkPanel();
        
        generalPanel.setHeadingText("General");
        systemPanel.setHeadingText("System");
        displayPanel.setHeadingText("Display");
        storagePanel.setHeadingText("Storage");
        audioPanel.setHeadingText("Audio");
        networkPanel.setHeadingText("Network");
        
        centerLayout.add(generalPanel, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        centerLayout.add(systemPanel);
        centerLayout.add(displayPanel);
        centerLayout.add(storagePanel);
        centerLayout.add(audioPanel);
        centerLayout.add(networkPanel);
        
        saveButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                hide();
            }
        });
        
        cancelButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                hide();
            }
        });
        
        bottomButtons.add(saveButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        bottomButtons.add(cancelButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        
        centerLayout.setActiveWidget(generalPanel);
        
        centerContainer.add(centerLayout, new VerticalLayoutData(1, 0.7));
        centerContainer.add(bottomButtons, new VerticalLayoutData(1, 0.3));
        
        borderLayout.setCenterWidget(centerContainer);
        
    }

    private void initGeneralPanel() {
        
        VerticalLayoutContainer basicTabContainer = new VerticalLayoutContainer();
        TextField vmName = new TextField();
        vmName.setValue(vm.getName());
        basicTabContainer.add(new FieldLabel(vmName, "Name"));
        
        ListStore<OperatingSystem> osStore = new ListStore<OperatingSystem>(new ModelKeyProvider<OperatingSystem>() {

            public String getKey(OperatingSystem item) {
                return "" + item.toString();
            }

        });

        osStore.addAll(Arrays.asList(OperatingSystem.values()));
        osCurr = osStore.get(0);
        osChooser = new ComboBox<OperatingSystem>(osStore, new LabelProvider<OperatingSystem>() {

            public String getLabel(OperatingSystem item) {
                return item.toString();
            }

        });

        osChooser.setEditable(false);
        osChooser.setForceSelection(true);
        osChooser.setTypeAhead(true);
        osChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        osChooser.setValue(osCurr);
        osChooser.addSelectionHandler(new SelectionHandler<OperatingSystem>() {

            public void onSelection(SelectionEvent<OperatingSystem> event) {
                osCurr = event.getSelectedItem();
            }
        });
        
        basicTabContainer.add(new FieldLabel(osChooser, "Operating System"));
        
        
        generalTabPanel.add(basicTabContainer, "Basic");
        
        VerticalLayoutContainer advancedTabContainer = new VerticalLayoutContainer();
        
        ListStore<ShareOptions> shareOptionsList = new ListStore<ShareOptions>(new ModelKeyProvider<ShareOptions>(){

            public String getKey(ShareOptions item) {
                return item.toString();
            }
            
        });
        
        shareOptionsList.addAll(Arrays.asList(ShareOptions.values()));
        clipBoard = shareOptionsList.get(0);
        clipBoardChooser = new ComboBox<ShareOptions>(shareOptionsList, new LabelProvider<ShareOptions> () {

            public String getLabel(ShareOptions item) {
                return item.toString();
            }
            
        });
        
        clipBoardChooser.setEditable(false);
        clipBoardChooser.setForceSelection(true);
        clipBoardChooser.setTypeAhead(true);
        clipBoardChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        clipBoardChooser.setValue(clipBoard);
        clipBoardChooser.addSelectionHandler(new SelectionHandler<ShareOptions>() {

            public void onSelection(SelectionEvent<ShareOptions> event) {
                clipBoard = event.getSelectedItem();
            }
        });
        
        dragndrop = shareOptionsList.get(0);
        dragndropChooser = new ComboBox<ShareOptions>(shareOptionsList, new LabelProvider<ShareOptions> () {

            public String getLabel(ShareOptions item) {
                return item.toString();
            }
            
        });
        
        dragndropChooser.setEditable(false);
        dragndropChooser.setForceSelection(true);
        dragndropChooser.setTypeAhead(true);
        dragndropChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        dragndropChooser.setValue(dragndrop);
        dragndropChooser.addSelectionHandler(new SelectionHandler<ShareOptions>() {

            public void onSelection(SelectionEvent<ShareOptions> event) {
                dragndrop = event.getSelectedItem();
            }
        });
        
        CheckBox removableMedia = new CheckBox();
        removableMedia.setBoxLabel("Remember Runtime Changes");
        removableMedia.setValue(true);
        
        CheckBox topScreen = new CheckBox();
        topScreen.setBoxLabel("Show at Top of Screen");
        topScreen.setValue(false);
        
        advancedTabContainer.add(new FieldLabel(clipBoardChooser, "Shared Clipboard"));
        advancedTabContainer.add(new FieldLabel(dragndropChooser, "Drag\'n\'drop"));
        advancedTabContainer.add(new FieldLabel(removableMedia, "Removable Media"));
        advancedTabContainer.add(new FieldLabel(topScreen));
        generalTabPanel.add(advancedTabContainer, "Advanced");
        
        generalPanel.add(generalTabPanel);
        
    }

    private void initSystemPanel() {
        
        VerticalLayoutContainer motherboardTabContainer = new VerticalLayoutContainer();
        VerticalLayoutContainer processorTabContainer = new VerticalLayoutContainer();
        
        ListStore<Chipset> chipsetStore = new ListStore<Chipset>(new ModelKeyProvider<Chipset>(){

            public String getKey(Chipset item) {
                return item.toString();
            }
            
        });
        chipsetStore.addAll(Arrays.asList(Chipset.values()));
        
        ListStore<PointingDevice> pdStore = new ListStore<PointingDevice>(new ModelKeyProvider<PointingDevice>(){

            public String getKey(PointingDevice item) {
                return item.toString();
            }
            
        });
        pdStore.addAll(Arrays.asList(PointingDevice.values()));
        
        
        memorySlider = new Slider();
        memorySlider.setMinValue(0);
        memorySlider.setMaxValue(4096);
        memorySlider.setIncrement(1);
        memorySlider.setWidth(150);
        
        memorySpinner = new IntegerSpinnerField();
        memorySpinner.setMinValue(0);
        memorySpinner.setMaxValue(4096);
        memorySpinner.setIncrement(1);
        memorySpinner.setWidth(60);
        
        procSlider = new Slider();
        procSlider.setMinValue(1);
        procSlider.setMaxValue(4);
        procSlider.setIncrement(1);
        procSlider.setWidth(150);
        
        procSpinner = new IntegerSpinnerField();
        procSpinner.setMinValue(1);
        procSpinner.setMaxValue(4);
        procSpinner.setIncrement(1);
        procSpinner.setWidth(50);
        
        execSlider = new Slider();
        execSlider.setMinValue(1);
        execSlider.setMaxValue(100);
        execSlider.setIncrement(1);
        execSlider.setWidth(150);
        
        execSpinner = new IntegerSpinnerField();
        execSpinner.setMinValue(1);
        execSpinner.setMaxValue(100);
        execSpinner.setIncrement(1);
        execSpinner.setWidth(60);
        
        memorySlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                memory = event.getValue().toString();
                memorySpinner.setValue(event.getValue());
                memorySlider.redraw();
            }
        });
        
        memorySpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                memory = event.getValue().toString();
                memorySlider.setValue(event.getValue());
            }
        });
        
        procSlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                procN = event.getValue();
                procSpinner.setValue(event.getValue());
                procSlider.redraw();
            }
        });
        
        procSpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                procN = event.getValue();
                procSlider.setValue(event.getValue());
            }
        });
        
        execSlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                execPercentage = event.getValue();
                execSpinner.setValue(event.getValue());
                execSlider.redraw();
            }
        });
        
        execSpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                execPercentage = event.getValue();
                execSlider.setValue(event.getValue());
            }
        });
        
        HBoxLayoutContainer fieldContainer = new HBoxLayoutContainer();
        HBoxLayoutContainer procContainer = new HBoxLayoutContainer();
        HBoxLayoutContainer execContainer = new HBoxLayoutContainer();
        BoxLayoutContainer.BoxLayoutData layoutData = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 2, 0, 5));
        fieldContainer.add(memorySlider, layoutData);
        fieldContainer.add(memorySpinner, layoutData);
        fieldContainer.add(new HTML("MB"), layoutData);
        fieldContainer.setPadding(new Padding(5));
        fieldContainer.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.MIDDLE);
        fieldContainer.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        procContainer.add(procSlider, layoutData);
        procContainer.add(procSpinner, layoutData);
        procContainer.setPadding(new Padding(5));
        procContainer.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.MIDDLE);
        procContainer.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        execContainer.add(execSlider, layoutData);
        execContainer.add(execSpinner, layoutData);
        execContainer.setPadding(new Padding(5));
        execContainer.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.MIDDLE);
        execContainer.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        
        
        memorySlider.setValue(Integer.parseInt(vm.getMemory()));
        memorySpinner.setValue(Integer.parseInt(vm.getMemory()));
        procSlider.setValue(1);
        procSpinner.setValue(1);
        execSlider.setValue(100);
        execSpinner.setValue(100);
        
        motherboardTabContainer.add(new FieldLabel(fieldContainer, "Base Memory"));
        
        
        chipset = chipsetStore.get(0);
        chipsetChooser = new ComboBox<Chipset>(chipsetStore, new LabelProvider<Chipset>() {

            public String getLabel(Chipset item) {
                return item.toString();
            }

        });

        chipsetChooser.setEditable(false);
        chipsetChooser.setForceSelection(true);
        chipsetChooser.setTypeAhead(true);
        chipsetChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        chipsetChooser.setValue(chipset);
        chipsetChooser.addSelectionHandler(new SelectionHandler<Chipset>() {

            public void onSelection(SelectionEvent<Chipset> event) {
                chipset = event.getSelectedItem();
            }
        });
        
        pd = pdStore.get(0);
        pdChooser = new ComboBox<PointingDevice>(pdStore, new LabelProvider<PointingDevice>() {

            public String getLabel(PointingDevice item) {
                return item.toString();
            }

        });

        pdChooser.setEditable(false);
        pdChooser.setForceSelection(true);
        pdChooser.setTypeAhead(true);
        pdChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        pdChooser.setValue(pd);
        pdChooser.addSelectionHandler(new SelectionHandler<PointingDevice>() {

            public void onSelection(SelectionEvent<PointingDevice> event) {
                pd = event.getSelectedItem();
            }
        });
        
        motherboardTabContainer.add(new FieldLabel(chipsetChooser, "Chipset"));
        motherboardTabContainer.add(new FieldLabel(pdChooser, "Pointing Device"));
        
        systemTabPanel.add(motherboardTabContainer, "Motherboard");
        
        processorTabContainer.add(new FieldLabel(procContainer, "Processor(s)"));
        processorTabContainer.add(new FieldLabel(execContainer, "Execution Cap"));
        
        systemTabPanel.add(processorTabContainer, "Processor");
        
        systemPanel.add(systemTabPanel);
        
       
        
    }

    private void initDisplayPanel() {
        
        VerticalLayoutContainer videoTabContainer = new VerticalLayoutContainer();
        HBoxLayoutContainer videoMemoryContainer = new HBoxLayoutContainer();
        HBoxLayoutContainer monitorInfoContainer = new HBoxLayoutContainer();
        BoxLayoutContainer.BoxLayoutData layoutData = new BoxLayoutContainer.BoxLayoutData(new Margins(0, 2, 0, 5));
        
        
        videoMemSlider = new Slider();
        videoMemSlider.setMinValue(1);
        videoMemSlider.setMaxValue(128);
        videoMemSlider.setIncrement(1);
        videoMemSlider.setValue(Integer.parseInt((vm.getVideoMemory().equals("")) ? "0" : vm.getVideoMemory()));
        videoMemSlider.setWidth(150);
        
        videoMemSpinner = new IntegerSpinnerField();
        videoMemSpinner.setMinValue(1);
        videoMemSpinner.setMaxValue(128);
        videoMemSpinner.setIncrement(1);
        videoMemSpinner.setValue(Integer.parseInt((vm.getVideoMemory().equals("")) ? "0" : vm.getVideoMemory()));
        videoMemSpinner.setWidth(50);
        
        monitorCountSlider = new Slider();
        monitorCountSlider.setMinValue(1);
        monitorCountSlider.setMaxValue(8);
        monitorCountSlider.setIncrement(1);
        monitorCountSlider.setWidth(150);
        
        monitorCountSpinner = new IntegerSpinnerField();
        monitorCountSpinner.setMinValue(1);
        monitorCountSpinner.setMaxValue(8);
        monitorCountSpinner.setIncrement(1);
        monitorCountSpinner.setWidth(50);
        
        videoMemSlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                videoMemory = event.getValue();
                videoMemSpinner.setValue(videoMemory);
                videoMemSlider.redraw();
            }
        });
        
        videoMemSpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                videoMemory = event.getValue();
                videoMemSlider.setValue(videoMemory);
            }
        });
        
        monitorCountSlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                monitorCount = event.getValue();
                monitorCountSpinner.setValue(monitorCount);
                monitorCountSlider.redraw();
            }
        });
        
        monitorCountSpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                monitorCount = event.getValue();
                monitorCountSlider.setValue(monitorCount);
            }
        });
        
        CheckBox acceleration = new CheckBox();
        acceleration.setBoxLabel("Enable 3D Acceleration");
        
        videoMemoryContainer.add(videoMemSlider, layoutData);
        videoMemoryContainer.add(videoMemSpinner, layoutData);
        monitorInfoContainer.add(monitorCountSlider, layoutData);
        monitorInfoContainer.add(monitorCountSpinner, layoutData);
        
        videoMemoryContainer.setPadding(new Padding(5));
        videoMemoryContainer.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.MIDDLE);
        videoMemoryContainer.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        monitorInfoContainer.setPadding(new Padding(5));
        monitorInfoContainer.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.MIDDLE);
        monitorInfoContainer.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        
        videoTabContainer.add(new FieldLabel(videoMemoryContainer, "Video Memory"));
        videoTabContainer.add(new FieldLabel(monitorInfoContainer, "Monitor Count"));
        videoTabContainer.add(new FieldLabel(acceleration, "Extended Features"));
        
        displayTabPanel.add(videoTabContainer, "Video");
        
        VerticalLayoutContainer remoteDisplayTabContainer = new VerticalLayoutContainer();
        
        IntegerField serverPortField = new IntegerField();
        IntegerField authTimeoutField = new IntegerField();
        
        CheckBox allowMultiConnections = new CheckBox();
        allowMultiConnections.setBoxLabel("Allow Multiple Connections");
        
        enableServer = new CheckBox();
        enableServer.setBoxLabel("Enable Server");
        
        enableServer.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setServerFieldAvailability(event.getValue());
            }
        });
        
        portLabel = new FieldLabel(serverPortField, "Server Port");
        authTimeoutLabel = new FieldLabel(authTimeoutField, "Authentication Timeout");
        extendedFeaturesLabel = new FieldLabel(allowMultiConnections, "Extended Features");
        
        setServerFieldAvailability(false);
        
        remoteDisplayTabContainer.add(enableServer);
        remoteDisplayTabContainer.add(portLabel);
        remoteDisplayTabContainer.add(authTimeoutLabel);
        remoteDisplayTabContainer.add(extendedFeaturesLabel);
        
        displayTabPanel.add(remoteDisplayTabContainer, "Remote Display");
        
        displayPanel.add(displayTabPanel);
    }

    private void initStoragePanel() {
        
        storagePanel.setPixelSize(480, 400);
        VerticalLayoutContainer storageTabContainer = new VerticalLayoutContainer();
        ContentPanel leftPanel = new ContentPanel();
        leftPanel.setHeadingText("Storage Tree");
        
        ContentPanel rightPanel = new ContentPanel();
        rightPanel.setHeadingText("Attributes");
        
        TreeStore<String> storageList = new TreeStore<String>(new ModelKeyProvider<String>() {

            public String getKey(String item) {
                return item;
            }
        });
        
        storageList.add("Controller: IDE");
        storageList.add("Controller: IDE", vm.getName() + ".vdi");
        storageList.add("Controller: IDE", "empty");
        
        Tree<String, String> ideTree = new Tree<String, String>(storageList, new ValueProvider<String, String>() {

            public String getValue(String object) {
                return object;
            }

            public void setValue(String object, String value) {
            }

            public String getPath() {
                return "name";
            }
        });
        
        
        leftPanel.add(ideTree);
        
        ideTree.expandAll();
        
        VerticalLayoutContainer rightPanelFields = new VerticalLayoutContainer();
        TextField controllerName = new TextField();
        
        ListStore<IDEType> typeStore = new ListStore<IDEType>(new ModelKeyProvider<IDEType>() {

            public String getKey(IDEType item) {
                return item.toString();
            }

        });

        typeStore.addAll(Arrays.asList(IDEType.values()));
        currIdeType = typeStore.get(0);
        
        typeChooser = new ComboBox<IDEType>(typeStore, new LabelProvider<IDEType>() {

            public String getLabel(IDEType item) {
                return item.toString();
            }
        });
        
        typeChooser.setEditable(false);
        typeChooser.setForceSelection(true);
        typeChooser.setTypeAhead(true);
        typeChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        typeChooser.setValue(currIdeType);
        typeChooser.addSelectionHandler(new SelectionHandler<IDEType>() {

            public void onSelection(SelectionEvent<IDEType> event) {
                currIdeType = event.getSelectedItem();
            }
        });
        
        rightPanelFields.add(new FieldLabel(controllerName, "Name"));
        rightPanelFields.add(new FieldLabel(typeChooser, "Type"));
        
        rightPanel.add(rightPanelFields);
        
        
        storageTabContainer.add(leftPanel, new VerticalLayoutData(1, .5));
        storageTabContainer.add(rightPanel, new VerticalLayoutData(1, .5));
        
        storagePanel.add(storageTabContainer);
        
    }

    private void initAudioPanel() {
        
        VerticalLayoutContainer audioTabContainer = new VerticalLayoutContainer();
        
        ListStore<HostAudio> hostAudioStore = new ListStore<HostAudio>(new ModelKeyProvider<HostAudio>() {

            public String getKey(HostAudio item) {
                return item.getValue();
            }
        });
        
        hostAudioStore.addAll(Arrays.asList(HostAudio.values()));
        currHostAudio = hostAudioStore.get(0);
        
        ListStore<AudioController> controllerStore = new ListStore<AudioController>(new ModelKeyProvider<AudioController>() {

            public String getKey(AudioController item) {
                return item.getValue();
            }
        });
       
        controllerStore.addAll(Arrays.asList(AudioController.values()));
        currAudioController = controllerStore.get(0);
        
        hostAudioChooser = new ComboBox<HostAudio>(hostAudioStore, new LabelProvider<HostAudio>() {

            public String getLabel(HostAudio item) {
                return item.getValue();
            }
        });
        
        hostAudioChooser.setEditable(false);
        hostAudioChooser.setForceSelection(true);
        hostAudioChooser.setTypeAhead(true);
        hostAudioChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        hostAudioChooser.setValue(currHostAudio);
        hostAudioChooser.addSelectionHandler(new SelectionHandler<HostAudio>() {

            public void onSelection(SelectionEvent<HostAudio> event) {
                currHostAudio = event.getSelectedItem();
            }
        });
        
        hostAudioChooser.setValue(currHostAudio);
        
        controllerChooser = new ComboBox<AudioController>(controllerStore, new LabelProvider<AudioController>() {

            public String getLabel(AudioController item) {
                return item.getValue();
            }
        });
        
        controllerChooser.setEditable(false);
        controllerChooser.setForceSelection(true);
        controllerChooser.setTypeAhead(true);
        controllerChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        controllerChooser.setValue(currAudioController);
        controllerChooser.addSelectionHandler(new SelectionHandler<AudioController>() {

            public void onSelection(SelectionEvent<AudioController> event) {
                currAudioController = event.getSelectedItem();
            }
        });
        
        controllerChooser.setValue(currAudioController);
            
        hostAudioLabel = new FieldLabel(hostAudioChooser, "Host Audio Driver");
        audioControllerLabel = new FieldLabel(controllerChooser, "Audio Controller");
        
        
        enableAudio = new CheckBox();
        enableAudio.setBoxLabel("Enable Audio");
        
        enableAudio.setValue(true);
        setAudioFieldAvailability(true);
        
        enableAudio.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setAudioFieldAvailability(event.getValue());
            }
        });
        
        audioTabContainer.add(enableAudio);
        audioTabContainer.add(hostAudioLabel);
        audioTabContainer.add(audioControllerLabel);
        
        audioPanel.add(audioTabContainer);
        
    }

    private void initNetworkPanel() {
        
        enableNetwork = new CheckBox();
        enableNetwork.setBoxLabel("Enable Network");
        enableNetwork.setValue(true);
        advancedTabVisible = false;
        
        enableNetwork.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setNetworkFieldsAvailability(event.getValue());
            }
        });
        
        networkTabPanel = new VerticalLayoutContainer();
        advancedPanelButton = new HBoxLayoutContainer();
        BoxLayoutData layoutData = new BoxLayoutData(new Margins(0, 5, 0, 0));
        advancedFieldsLayout = new VerticalLayoutContainer();
        TextField macAddress = new TextField();
        
        
        ListStore<NetworkAttachType> attachTypeStore = new ListStore<NetworkAttachType>(new ModelKeyProvider<NetworkAttachType>() {

            public String getKey(NetworkAttachType item) {
                return item.getValue();
            }
        });
        
        attachTypeStore.addAll(Arrays.asList(NetworkAttachType.values()));
        currAttachType = attachTypeStore.get(0);
        
        attachTypeChooser = new ComboBox<NetworkAttachType>(attachTypeStore, new LabelProvider<NetworkAttachType>() {

            public String getLabel(NetworkAttachType item) {
                return item.getValue();
            }
        });
        
        attachTypeChooser.setEditable(false);
        attachTypeChooser.setForceSelection(true);
        attachTypeChooser.setTypeAhead(true);
        attachTypeChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        attachTypeChooser.setValue(currAttachType);
        attachTypeChooser.addSelectionHandler(new SelectionHandler<NetworkAttachType>() {

            public void onSelection(SelectionEvent<NetworkAttachType> event) {
                currAttachType = event.getSelectedItem();
            }
        });
        
        attachTypeChooser.setValue(currAttachType);
        
        ListStore<NetworkAdapterType> adapterTypeStore = new ListStore<NetworkAdapterType>(new ModelKeyProvider<NetworkAdapterType>() {

            public String getKey(NetworkAdapterType item) {
                return item.getValue();
            }
        });
        
        adapterTypeStore.addAll(Arrays.asList(NetworkAdapterType.values()));
        currAdapterType = adapterTypeStore.get(0);
        
       netAdapterTypeChooser = new ComboBox<NetworkAdapterType>(adapterTypeStore, new LabelProvider<NetworkAdapterType>() {

            public String getLabel(NetworkAdapterType item) {
                return item.getValue();
            }
        });
        
        netAdapterTypeChooser.setEditable(false);
        netAdapterTypeChooser.setForceSelection(true);
        netAdapterTypeChooser.setTypeAhead(true);
        netAdapterTypeChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        netAdapterTypeChooser.setValue(currAdapterType);
        netAdapterTypeChooser.addSelectionHandler(new SelectionHandler<NetworkAdapterType>() {

            public void onSelection(SelectionEvent<NetworkAdapterType> event) {
                currAdapterType = event.getSelectedItem();
            }
        });
        
        netAdapterTypeChooser.setValue(currAdapterType);
       
        windowStateImage.setUrl(collapsedImage);
        
        windowStateImage.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                if(advancedTabVisible){
                    windowStateImage.setUrl(collapsedImage);
                    advancedTabVisible = false;
                } else {
                    windowStateImage.setUrl(expandedImage);
                    advancedTabVisible = true;
                }
                
                advancedFieldsLayout.setVisible(advancedTabVisible);
                advancedPanelButton.forceLayout();
                networkTabPanel.forceLayout();
                
            }
        });
        
        advancedPanelButton.add(windowStateImage, layoutData);
        advancedPanelButton.add(new HTML("Advanced"), layoutData);
        advancedPanelButton.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);
        advancedPanelButton.setPack(BoxLayoutPack.CENTER);
        
        advancedFieldsLayout.add(new FieldLabel(netAdapterTypeChooser, "Adapter Type"));
        advancedFieldsLayout.add(new FieldLabel(macAddress, "MAC Address"));
        
        attachTypeLabel = new FieldLabel(attachTypeChooser, "Attached to");
        
        networkTabPanel.add(enableNetwork);
        networkTabPanel.add(attachTypeLabel);
        networkTabPanel.add(advancedPanelButton);
        networkTabPanel.add(advancedFieldsLayout);
        advancedFieldsLayout.setVisible(false);
        networkPanel.add(networkTabPanel);
        
        setNetworkFieldsAvailability(true);
        
    }
    
    private class ToggleButtonClickHandler implements SelectHandler{

        public void onSelect(SelectEvent event) {
            
            ToggleButton currButton = (ToggleButton) event.getSource();
            int index = buttonContainer.getWidgetIndex(currButton);
            
            centerLayout.setActiveWidget(centerLayout.getWidget(index));
            
        }

    }
    
    private void setServerFieldAvailability(boolean checkBoxValue){
    
        portLabel.setEnabled(checkBoxValue);
        authTimeoutLabel.setEnabled(checkBoxValue);
        extendedFeaturesLabel.setEnabled(checkBoxValue);
        
    }
    
    private void setAudioFieldAvailability(boolean checkBoxValue){
        
        hostAudioLabel.setEnabled(checkBoxValue);
        audioControllerLabel.setEnabled(checkBoxValue);
    
    }
    
    private void setNetworkFieldsAvailability(boolean checkBoxValue){
        attachTypeLabel.setEnabled(checkBoxValue);
        advancedPanelButton.setEnabled(checkBoxValue);
        advancedFieldsLayout.setEnabled(checkBoxValue);
    }
    
}
