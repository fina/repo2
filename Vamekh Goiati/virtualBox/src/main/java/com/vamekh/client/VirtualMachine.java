/*
 * @author : vamekh goiati
 * 
 * 
 */
package com.vamekh.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Image;

/**
 *
 * @author vamekh
 */
public class VirtualMachine {
    
    private int id = 0;
    private String name = "";
    private String os = "";
    private String memory = "";
    private String bootOrder = "";
    private String videoMemory = "";
    private String acceleration = "";
    private String videoCapture = "";
    private String idePrimary = "";
    private String hdCapacity = "";
    private String ideSecondary = "";
    private String audioDriver = "";
    private String audioController = "";
    private String networkAdapter = "";
    private String description = "";
    private Image image = new Image("images/blank.png");
    private SafeUri imageUri = UriUtils.fromString(image.getUrl());

    public VirtualMachine() {

    }

    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getBootOrder() {
        return bootOrder;
    }

    public void setBootOrder(String bootOrder) {
        this.bootOrder = bootOrder;
    }

    public String getVideoMemory() {
        return videoMemory;
    }

    public void setVideoMemory(String videoMemory) {
        this.videoMemory = videoMemory;
    }

    public String getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(String acceleration) {
        this.acceleration = acceleration;
    }

    public String getVideoCapture() {
        return videoCapture;
    }

    public void setVideoCapture(String videoCapture) {
        this.videoCapture = videoCapture;
    }

    public String getIdePrimary() {
        return idePrimary;
    }

    public void setIdePrimary(String idePrimary) {
        this.idePrimary = idePrimary;
    }

    public String getIdeSecondary() {
        return ideSecondary;
    }

    public void setIdeSecondary(String ideSecondary) {
        this.ideSecondary = ideSecondary;
    }

    public String getAudioDriver() {
        return audioDriver;
    }

    public void setAudioDriver(String audioDriver) {
        this.audioDriver = audioDriver;
    }

    public String getAudioController() {
        return audioController;
    }

    public void setAudioController(String audioController) {
        this.audioController = audioController;
    }

    public String getNetworkAdapter() {
        return networkAdapter;
    }

    public void setNetworkAdapter(String networkAdapter) {
        this.networkAdapter = networkAdapter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getHdCapacity() {
        return hdCapacity;
    }

    public void setHdCapacity(String hdCapacity) {
        this.hdCapacity = hdCapacity;
    }
    
    public Image getImage(){
        return image;
    }
    
    public void setImage(Image image){
        this.image = image;
        this.imageUri = UriUtils.fromString(image.getUrl());
    }
    
    public SafeUri getImageUri(){
        return imageUri;
    }
    
    public void setImageUri(SafeUri imageUri){
        this.imageUri = imageUri;
    }
}
