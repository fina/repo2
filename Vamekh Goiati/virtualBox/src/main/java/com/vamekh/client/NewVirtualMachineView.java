/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vamekh.client;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.theme.base.client.field.FieldLabelDefaultAppearance;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Slider;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.container.CardLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer.HBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent.HideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.DoubleField;
import com.sencha.gxt.widget.core.client.form.DoubleSpinnerField;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.IntegerSpinnerField;
import com.sencha.gxt.widget.core.client.form.Radio;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.Validator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxNumberValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinNumberValidator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;
import java.util.Arrays;
import java.util.Iterator;

/**
 *
 * @author vamekh
 */
public class NewVirtualMachineView extends Window {

    private static final String WINDOW_NAME = "Create Virtual Machine";
    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private TextButton nextButton;
    private TextButton cancelButton;
    private TextButton backButton;
    private TextButton createButton;
    private BorderLayoutContainer borderLayout;
    private CardLayoutContainer centerLayout;
    private HBoxLayoutContainer buttonBar;
    private ContentPanel firstPage;
    private VerticalLayoutContainer firstPageContainer;
    private ContentPanel secondPage;
    private VerticalLayoutContainer secondPageContainer;
    private ContentPanel thirdPage;
    private VerticalLayoutContainer thirdPageContainer;
    private String name;
    private String os;
    private String memory;
    private int currentPage = 0;
    private TextField nameField;
    private ComboBox<OperatingSystem> osChooser;
    private OperatingSystem osCurr;
    private Slider memorySlider;
    private VerticalPanel sliderPanel;
    private IntegerSpinnerField memorySpinner;
    private ToggleGroup hdTypeGroup;
    private Slider hdSizeSlider;
    private DoubleField hdSizeField;
    VirtualMachine vm;
    
    public NewVirtualMachineView() {

        setHeadingText(WINDOW_NAME);
        setWidth(WIDTH);
        setHeight(HEIGHT);
        borderLayout = new BorderLayoutContainer();
        buttonBar = new HBoxLayoutContainer();
        buttonBar.setPadding(new Padding(5));
        buttonBar.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        buttonBar.setPack(BoxLayoutPack.END);
        centerLayout = new CardLayoutContainer();
        nextButton = new TextButton("Next");
        cancelButton = new TextButton("Cancel");
        backButton = new TextButton("Back");
        createButton = new TextButton("Create");
        backButton.setEnabled(false);
        firstPage = new ContentPanel();
        firstPageContainer = new VerticalLayoutContainer();
        secondPage = new ContentPanel();
        secondPageContainer = new VerticalLayoutContainer();
        thirdPage = new ContentPanel();

        setWidget(borderLayout);

        initGui();
        
    }

    private void initGui() {

        initFirstPage();
        initSecondPage();
        initThirdPage();
        
        centerLayout.setActiveWidget(centerLayout.getWidget(currentPage));

        nextButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                
                backButton.setEnabled(true);
                currentPage++;
                if(currentPage == 2){
                    nextButton.setEnabled(false);
                    createButton.setEnabled(true);
                }

                centerLayout.setActiveWidget(centerLayout.getWidget(currentPage));
                
            }

        });
        
        createButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                createNewVM();
                hide();
            }
        });

        cancelButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                hide();
            }
        });
        
        backButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                currentPage --;
                nextButton.setEnabled(true);
                createButton.setEnabled(false);
                if(currentPage == 0)backButton.setEnabled(false);
                centerLayout.setActiveWidget(centerLayout.getWidget(currentPage));
            }
        });

        buttonBar.add(backButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        buttonBar.add(nextButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        buttonBar.add(createButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        createButton.setEnabled(false);
        buttonBar.add(cancelButton, new BoxLayoutData(new Margins(0, 5, 0, 0)));
        borderLayout.setCenterWidget(centerLayout);
        borderLayout.setSouthWidget(buttonBar);

    }

    private void initFirstPage() {

        firstPage.setHeadingText("Name and operating sysem");
        VBoxLayoutContainer fieldContainer = new VBoxLayoutContainer();
        nameField = new TextField();
        nameField.addValidator(new RegExValidator("[A-Za-z0-9 \\s]+",
                "Name contains invalid characters"));
        nameField.addValidator(new MinLengthValidator(1));
        nameField.addValidator(new MaxLengthValidator(255));
        nameField.setAutoValidate(true);

        ListStore<OperatingSystem> osStore = new ListStore<OperatingSystem>(new ModelKeyProvider<OperatingSystem>() {

            public String getKey(OperatingSystem item) {
                return "" + item.toString();
            }

        });

        osStore.addAll(Arrays.asList(OperatingSystem.values()));
        osCurr = osStore.get(0);
        osChooser = new ComboBox<OperatingSystem>(osStore, new LabelProvider<OperatingSystem>() {

            public String getLabel(OperatingSystem item) {
                return item.toString();
            }

        });

        osChooser.setEditable(false);
        osChooser.setForceSelection(true);
        osChooser.setTypeAhead(true);
        osChooser.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        osChooser.setValue(osCurr);
        osChooser.addSelectionHandler(new SelectionHandler<OperatingSystem>() {

            public void onSelection(SelectionEvent<OperatingSystem> event) {
                osCurr = event.getSelectedItem();
            }
        });

        BoxLayoutData layoutData = new BoxLayoutData(new Margins(0, 0, 5, 0));
        fieldContainer.add(new FieldLabel(nameField, "Name"), layoutData);
        fieldContainer.add(new FieldLabel(osChooser, "Operating System"), layoutData);
        
        fieldContainer.setPadding(new Padding(5));
        fieldContainer.setVBoxLayoutAlign(VBoxLayoutAlign.CENTER);
        fieldContainer.setPack(BoxLayoutPack.CENTER);
        
        HTML text = new HTML("<div><p>&nbsp;Enter system name and type</p><br><br></div>");
        firstPageContainer.add(text);
        firstPageContainer.add(fieldContainer);
        
        VBoxLayoutContainer tmpContainer = new VBoxLayoutContainer();
        tmpContainer.add(firstPageContainer, layoutData);
        tmpContainer.setPadding(new Padding(5));
        tmpContainer.setVBoxLayoutAlign(VBoxLayoutAlign.CENTER);
        tmpContainer.setPack(BoxLayoutPack.CENTER);
        firstPage.add(tmpContainer);
        centerLayout.add(firstPage);
        
    }

    private void initSecondPage() {
        
        secondPage.setHeadingText("Memory size");
        HBoxLayoutContainer fieldContainer = new HBoxLayoutContainer();
        BoxLayoutData layoutData = new BoxLayoutData(new Margins(0, 2, 0, 5));
        
        memorySlider = new Slider();
        memorySlider.setMinValue(0);
        memorySlider.setMaxValue(4096);
        memorySlider.setIncrement(1);
        sliderPanel = new VerticalPanel();
        
        memorySpinner = new IntegerSpinnerField();
        memorySpinner.setMinValue(0);
        memorySpinner.setMaxValue(4096);
        memorySpinner.setIncrement(1);
        
        memorySlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                memory = event.getValue().toString();
                memorySpinner.setValue(event.getValue());
                memorySlider.redraw();
            }
        });
        
        memorySpinner.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                memory = event.getValue().toString();
                memorySlider.setValue(event.getValue());
            }
        });
        
        fieldContainer.add(memorySlider, layoutData);
        fieldContainer.add(memorySpinner, layoutData);
        fieldContainer.add(new HTML("MB"), layoutData);
        fieldContainer.setPadding(new Padding(5));
        fieldContainer.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        fieldContainer.setPack(BoxLayoutPack.CENTER);
        
        
        HTML text = new HTML("<div><p>&nbsp;Select the amount of memory for virtual machine</p><br><br></div>");
        secondPageContainer.add(text);
        secondPageContainer.add(fieldContainer);
        
        HBoxLayoutContainer tmpContainer = new HBoxLayoutContainer();
        tmpContainer.add(secondPageContainer, layoutData);
        tmpContainer.setPadding(new Padding(5));
        tmpContainer.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        tmpContainer.setPack(BoxLayoutPack.CENTER);
        secondPage.add(tmpContainer);
        centerLayout.add(secondPage);
        
        
    }

    private void initThirdPage() {
        
        thirdPage.setHeadingText("Hard drive file type");
        VBoxLayoutContainer fieldContainer = new VBoxLayoutContainer();
        HBoxLayoutContainer sliderContainer = new HBoxLayoutContainer();
        VerticalPanel radioContainer = new VerticalPanel();
        BoxLayoutData layoutData = new BoxLayoutData(new Margins(0, 0, 5, 0));
        BoxLayoutData hBoxLayoutData = new BoxLayoutData(new Margins(0, 2, 0, 5));
        
        HTML text = new HTML("<div><p>&nbsp;Select hard disk file type</p><br></div>");
        HTML hdSizeTxt = new HTML("<div><p><br><br>&nbsp;Select hard disk capacity</p></div>");
        
        hdTypeGroup = new ToggleGroup();
        Radio vdi = new Radio();
        vdi.setBoxLabel("VDI");
        
        Radio vmdk = new Radio();
        vmdk.setBoxLabel("VMDK");
        
        Radio vhd = new Radio();
        vhd.setBoxLabel("VHD");
        
        Radio hdd = new Radio();
        hdd.setBoxLabel("HDD");
        
        hdTypeGroup.add(vdi);
        hdTypeGroup.add(vmdk);
        hdTypeGroup.add(vhd);
        hdTypeGroup.add(hdd);
        
        radioContainer.add(vdi);
        radioContainer.add(vmdk);
        radioContainer.add(vhd);
        radioContainer.add(hdd);
        
        fieldContainer.add(text, layoutData);
        fieldContainer.add(radioContainer, layoutData);
        fieldContainer.add(hdSizeTxt, layoutData);
        
        HTML sizeTxt = new HTML("GB");
        
        hdSizeSlider = new Slider();
        hdSizeSlider.setMinValue(0);
        hdSizeSlider.setMaxValue(512);
        hdSizeSlider.setIncrement(1);
        
        hdSizeSlider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

            public void onValueChange(ValueChangeEvent<Integer> event) {
                int currSize = event.getValue();
                hdSizeField.setValue(Double.parseDouble(Integer.toString(currSize)));
                hdSizeSlider.redraw();
            }
        });
        
        
        
        hdSizeField = new DoubleField();
        hdSizeField.addValidator(new MinNumberValidator<Double>(0.0));
        hdSizeField.addValidator(new MaxNumberValidator<Double>(512.0));
        hdSizeField.setAutoValidate(true);
        
        hdSizeField.addValueChangeHandler(new ValueChangeHandler<Double>() {

            public void onValueChange(ValueChangeEvent<Double> event) {
                if(hdSizeField.validate()){
                    double currValue = event.getValue();
                    hdSizeSlider.setValue((int)currValue);
                }
            }
        });
        
        sliderContainer.add(hdSizeSlider, hBoxLayoutData);
        sliderContainer.add(hdSizeField, hBoxLayoutData);
        sliderContainer.add(sizeTxt, hBoxLayoutData);
        
        fieldContainer.add(sliderContainer, layoutData);
        
        fieldContainer.setPadding(new Padding(5));
        fieldContainer.setVBoxLayoutAlign(VBoxLayoutAlign.CENTER);
        fieldContainer.setPack(BoxLayoutPack.CENTER);
        
        
        sliderContainer.setPadding(new Padding(5));
        sliderContainer.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        sliderContainer.setPack(BoxLayoutPack.CENTER);
        
        thirdPage.add(fieldContainer);
        
        centerLayout.add(thirdPage);
        
    }

    private void createNewVM() {
        
        vm = new VirtualMachine();
        vm.setName(nameField.getValue());
        vm.setOs(osCurr.toString());
        vm.setMemory(memorySpinner.getValue().toString());
        Iterator it = hdTypeGroup.iterator();
        
        while(it.hasNext()){
            Radio tmp = (Radio)it.next();
            if(tmp.getValue()){
                vm.setIdePrimary(tmp.getBoxLabel());
            }
        }
        vm.setHdCapacity(hdSizeField.getText());

    }
    
    public VirtualMachine getCreatedVirtualMachine(){
        return vm;
    }
    
}
