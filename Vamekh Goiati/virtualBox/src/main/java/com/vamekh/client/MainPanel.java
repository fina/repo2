
package com.vamekh.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.XTemplates;
import com.sencha.gxt.core.client.XTemplates.XTemplate;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Portlet;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.CenterLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.PortalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.BeforeHideEvent;
import com.sencha.gxt.widget.core.client.event.EnableEvent;
import com.sencha.gxt.widget.core.client.event.EnableEvent.EnableHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuBar;
import com.sencha.gxt.widget.core.client.menu.MenuBarItem;
import com.sencha.gxt.widget.core.client.menu.MenuItem;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vamekh goiati
 */
public class MainPanel implements IsWidget{
    
    interface InfoTemplates extends XTemplates {
        
        @XTemplate("<p>Name: {name}</p><br><p>Operating System: {os}</p><br>")
        SafeHtml getGeneralInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>Base Memory: {memory} MB</p><br><p>Boot Order: {bootOrder}</p><br></div>")
        SafeHtml getSystemInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>Video Memory: {videoMemory} GB</p><br><p>Acceleration: {acceleration}</p><br><p>Video Capture: {videoCapture}</p><br></div>")
        SafeHtml getDisplayInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>IDE primary: {idePrimary} GB</p><br><p>IDE secondary: {ideSecondary}</p><br></div>")
        SafeHtml getStorageInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>Host Driver: {audioDriver}</p><br><p>Controller: {audioController}</p><br></div>")
        SafeHtml getAudioInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>Adapter: {networkAdapter}</p><br></div>")
        SafeHtml getNetworkInfo(VirtualMachine vm);
        
        @XTemplate("<div><p>{description} <br> </p></div>")
        SafeHtml getDescriptionInfo(VirtualMachine vm);
        
        @XTemplate("<div><img width=\"{width}\" height=\"{height}\" src=\"{vm.imageUri}\"></div>")
        SafeHtml getImageInfo(VirtualMachine vm, int width, int height);

    }

    private ContentPanel mainPanel;
    private BorderLayoutContainer borderLayout;
    private VBoxLayoutContainer buttonContainer;
    private VerticalLayoutContainer mainToolbarContainer;
    private ButtonBar buttonBar;
    private MenuBar menuBar;
    private HorizontalLayoutContainer topContainer;
    private VerticalLayoutContainer topLeftContainer;
    private VerticalLayoutContainer topRightContainer;
    private VerticalLayoutContainer bottomContainer;
    private TextButton newButton;
    private TextButton settingsButton;
    private TextButton startButton;
    private TextButton discardButton;
    private HTML generalHTML;
    private HTML systemHTML;
    private HTML displayHTML;
    private HTML storageHTML;
    private HTML audioHTML;
    private HTML networkHTML;
    private HTML descriptionHTML;
    private HTML imageHTML;
    private ToggleGroup vmButtonGroup;
    private List<VirtualMachine> vMachines;
    private VirtualMachine vmtmp;
    private ScrollPanel scroll;
    private VerticalLayoutContainer centerContainer;
    private ContentPanel preview;
    private VirtualMachine createdMachine;
    private VirtualMachine currVm;
    InfoTemplates infoTmp;
    NewVirtualMachineView newVMWindow;
    SettingsView settingsWindow;
    private ToggleButton selectedButton;
    
    public Widget asWidget() {
        
        infoTmp = GWT.create(InfoTemplates.class);
        
        mainPanel = new ContentPanel();
        mainPanel.setHeadingText("Virtual Box");
        mainPanel.setPixelSize(600, 600);
        
        borderLayout = new BorderLayoutContainer();
        mainPanel.setWidget(borderLayout);
        
        scroll = new ScrollPanel();
        
        vmtmp = new VirtualMachine();

        vMachines = new ArrayList<VirtualMachine>();
        
        
        initDisplayHTML();
        initGui();
        updateDisplay(vmtmp);
        
        return mainPanel;
    }

    private void initGui() {
        
        Menu menu = new Menu();
        MenuItem vmManager = new MenuItem("Virtual Media Manager...");
        menu.add(vmManager);
        
        Menu machine = new Menu();
        MenuItem newVM = new MenuItem("New...");
        
        newVM.addSelectionHandler(new SelectionHandler<Item>() {

            public void onSelection(SelectionEvent<Item> event) {
                newVMWindow = new NewVirtualMachineView();
                newVMWindow.addBeforeHideHandler(new BeforeHideEvent.BeforeHideHandler() {

                    public void onBeforeHide(BeforeHideEvent event) {
                        createdMachine = newVMWindow.getCreatedVirtualMachine();
                        if(createdMachine != null){
                            ToggleButton testOS = new ToggleButton(createdMachine.getName());
                            testOS.setId("" + vMachines.size());
                            vMachines.add(createdMachine);
                            vmButtonGroup.add(testOS);
                            buttonContainer.add(testOS);
                            testOS.addSelectHandler(new toggleButtonClickHandler());
                            buttonContainer.forceLayout();
                        }
                    }
                });
                newVMWindow.show();
            }
        });
        
        machine.add(newVM);
        
        Menu help = new Menu();
        MenuItem about = new MenuItem("About...");
        help.add(about);
        
        menuBar = new MenuBar();
        menuBar.add(new MenuBarItem("File", menu));
        menuBar.add(new MenuBarItem("Machine", machine));
        menuBar.add(new MenuBarItem("Help", help));
        
        newButton = new TextButton("New");
        newButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                newVMWindow = new NewVirtualMachineView();
                newVMWindow.addBeforeHideHandler(new BeforeHideEvent.BeforeHideHandler() {

                    public void onBeforeHide(BeforeHideEvent event) {
                        createdMachine = newVMWindow.getCreatedVirtualMachine();
                        if(createdMachine != null){
                            ToggleButton testOS = new ToggleButton(createdMachine.getName());
                            testOS.setId("" + vMachines.size());
                            vMachines.add(createdMachine);
                            vmButtonGroup.add(testOS);
                            buttonContainer.add(testOS);
                            testOS.addSelectHandler(new toggleButtonClickHandler());
                            buttonContainer.forceLayout();
                        }
                    }
                });
                newVMWindow.show();
            }
        });
        
        settingsButton = new TextButton("Settings");
        
        settingsButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                settingsWindow = new SettingsView(currVm);
                settingsWindow.show();
            }
        });
        
        startButton = new TextButton("Start");
        discardButton = new TextButton("Discard");
        
        discardButton.addSelectHandler(new SelectHandler() {

            public void onSelect(SelectEvent event) {
                buttonContainer.remove(selectedButton);
                VirtualMachine vm = new VirtualMachine();
                updateDisplay(vm);
            }
        });
        
        buttonBar = new ButtonBar();
        buttonBar.setBorders(true);
        
        buttonBar.add(newButton);
        buttonBar.add(settingsButton);
        buttonBar.add(startButton);
        buttonBar.add(discardButton);
        
        mainToolbarContainer = new VerticalLayoutContainer();
        
        mainToolbarContainer.add(menuBar);
        mainToolbarContainer.add(buttonBar);
        
        borderLayout.setNorthWidget(mainToolbarContainer);
        
        buttonContainer = new VBoxLayoutContainer();
        buttonContainer.setPadding(new Padding(5));
        buttonContainer.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
        vmButtonGroup = new ToggleGroup();
        
        for(int i = 0; i < vMachines.size(); i++){
            ToggleButton testOS = new ToggleButton(vMachines.get(i).getName());
            testOS.setId("" + i);
            vmButtonGroup.add(testOS);
            buttonContainer.add(testOS);
            testOS.addSelectHandler(new toggleButtonClickHandler());
        }
        
        VirtualMachine testVm = new VirtualMachine();
        testVm.setName("Test");
        vMachines.add(testVm);
        ToggleButton testOS = new ToggleButton("Test OS");
        testOS.setId("0");
        vmButtonGroup.add(testOS);
        buttonContainer.add(testOS);
        testOS.addSelectHandler(new toggleButtonClickHandler());
        
        borderLayout.setWestWidget(buttonContainer);
        
        topContainer = new HorizontalLayoutContainer();
        centerContainer = new VerticalLayoutContainer();
        topLeftContainer = new VerticalLayoutContainer();
        topRightContainer = new VerticalLayoutContainer();
        bottomContainer = new VerticalLayoutContainer();
        
        ContentPanel general = new ContentPanel();
        general.setHeadingText("General");
        general.setCollapsible(true);
        VerticalLayoutContainer generalContainer = new VerticalLayoutContainer();
        generalContainer.add(generalHTML);
        generalContainer.forceLayout();
        general.add(generalContainer);
        
        ContentPanel system = new ContentPanel();
        system.setHeadingText("System");
        system.setCollapsible(true);
        VerticalLayoutContainer systemContainer = new VerticalLayoutContainer();
        systemContainer.add(systemHTML);
        systemContainer.forceLayout();
        system.add(systemContainer);
        
        topLeftContainer.add(general, new VerticalLayoutData(1, .5));
        topLeftContainer.add(system, new VerticalLayoutData(1, .5));
        
        preview = new ContentPanel();
        preview.setHeadingText("Preview");
        preview.setCollapsible(true);
        VerticalLayoutContainer previewContainer = new VerticalLayoutContainer();
        previewContainer.add(imageHTML, new VerticalLayoutData(1, 1));
        previewContainer.forceLayout();
        preview.add(previewContainer);
        
        topRightContainer.add(preview, new VerticalLayoutData(1, 1));
        
        ContentPanel display = new ContentPanel();
        display.setHeadingText("Display");
        display.setCollapsible(true);
        VerticalLayoutContainer displayContainer = new VerticalLayoutContainer();
        displayContainer.add(displayHTML, new VerticalLayoutData(1, 1));
        displayContainer.forceLayout();
        display.add(displayContainer);
        
        ContentPanel storage = new ContentPanel();
        storage.setHeadingText("Storage");
        storage.setCollapsible(true);
        VerticalLayoutContainer storageContainer = new VerticalLayoutContainer();
        storageContainer.add(storageHTML, new VerticalLayoutData(1, 1));
        storageContainer.forceLayout();
        storage.add(storageContainer);
        
        ContentPanel audio = new ContentPanel();
        audio.setHeadingText("Audio");
        audio.setCollapsible(true);
        VerticalLayoutContainer audioContainer = new VerticalLayoutContainer();
        audioContainer.add(audioHTML, new VerticalLayoutData(1, 1));
        audioContainer.forceLayout();
        audio.add(audioContainer);
        
        ContentPanel network = new ContentPanel();
        network.setHeadingText("Network");
        network.setCollapsible(true);
        VerticalLayoutContainer networkContainer = new VerticalLayoutContainer();
        networkContainer.add(networkHTML, new VerticalLayoutData(1, 1));
        networkContainer.forceLayout();
        network.add(networkContainer);
        
        ContentPanel description = new ContentPanel();
        description.setHeadingText("Description");
        description.setCollapsible(true);
        VerticalLayoutContainer descriptionContainer = new VerticalLayoutContainer();
        descriptionContainer.add(descriptionHTML, new VerticalLayoutData(1, 1));
        descriptionContainer.forceLayout();
        description.add(descriptionContainer);
        
        bottomContainer.add(display);
        bottomContainer.add(storage);
        bottomContainer.add(audio);
        bottomContainer.add(network);
        bottomContainer.add(description);
        
        topContainer.add(topLeftContainer, new HorizontalLayoutData(.72, 1));
        topContainer.add(topRightContainer, new HorizontalLayoutData(.28, 1));
        
        centerContainer.add(topContainer, new VerticalLayoutData(1, .4));
        centerContainer.add(bottomContainer, new VerticalLayoutData(1, .6));
        
        scroll.add(centerContainer);
        borderLayout.setCenterWidget(scroll);
    
    }
 
    private class toggleButtonClickHandler implements SelectHandler{

        public void onSelect(SelectEvent event) {
            selectedButton = (ToggleButton) event.getSource();
            currVm = new VirtualMachine();
            if(Integer.parseInt(selectedButton.getId()) == 0){
                currVm.setId(Integer.parseInt(selectedButton.getId()));
                currVm.setName("OpenSuse OS");
                currVm.setOs("Linux");
                currVm.setMemory("2048");
                currVm.setVideoMemory("2");
                currVm.setAudioDriver("IDT Hight Def audio");
                currVm.setNetworkAdapter("Wi-Fi");
                currVm.setDescription("descriptioni tuto perfecto");
                currVm.setImage(new Image("images/test.jpg"));
            } else {
                currVm = vMachines.get(Integer.parseInt(selectedButton.getId()));
            }
            updateDisplay(currVm);
        }

    }
    
    private void initDisplayHTML(){
    
        if(generalHTML == null)generalHTML = new HTML();
        if(systemHTML == null)systemHTML = new HTML();
        if(displayHTML == null)displayHTML = new HTML();
        if(storageHTML == null)storageHTML = new HTML();
        if(audioHTML == null)audioHTML = new HTML();
        if(networkHTML == null)networkHTML = new HTML();
        if(descriptionHTML == null)descriptionHTML = new HTML();
        if(imageHTML == null)imageHTML = new HTML();
        
    }
    
    private void updateDisplay(VirtualMachine vm){
       
        generalHTML.setHTML(infoTmp.getGeneralInfo(vm));
        systemHTML.setHTML(infoTmp.getSystemInfo(vm));
        displayHTML.setHTML(infoTmp.getDisplayInfo(vm));
        storageHTML.setHTML(infoTmp.getStorageInfo(vm));
        audioHTML.setHTML(infoTmp.getAudioInfo(vm));
        networkHTML.setHTML(infoTmp.getNetworkInfo(vm));
        descriptionHTML.setHTML(infoTmp.getDescriptionInfo(vm));
        imageHTML.setHTML(infoTmp.getImageInfo(vm, preview.getOffsetWidth(), preview.getOffsetHeight()));
        
    }
    
}
