
package com.fina.misha.client;

import com.fina.misha.client.center.CenterPanel;
import com.fina.misha.client.center.ConversationMode;
import com.fina.misha.client.west.WestComponent;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Skype implements EntryPoint {

    @Override
    public void onModuleLoad() {
        // jer unda iyos menu

        HorizontalPanel body = new HorizontalPanel();
        body.setBorderWidth(1);

        final CenterPanel center = new CenterPanel(RootLayoutPanel.get().getOffsetWidth() - 310, 600);
        WestComponent west = new WestComponent(300, 600, center);
        body.add(west.getWestPabel());
        body.add(center);

        VerticalPanel main = new VerticalPanel();
        main.setPixelSize(RootLayoutPanel.get().getOffsetWidth(), RootLayoutPanel.get().getOffsetHeight());
        main.add(body);

        RootLayoutPanel.get().add(main);
    }
}
