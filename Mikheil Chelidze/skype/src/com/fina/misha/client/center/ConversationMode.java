
package com.fina.misha.client.center;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ConversationMode {

	private int width;
	private int heigth;
	private Map<String, String> conversation = new LinkedHashMap<String, String> ();

	public ConversationMode () {
		this (600, 600);
	}

	public ConversationMode (int width, int heigth) {
		this.width = width;
		this.heigth = heigth;
	}

	public ConversationMode (int width, int heigth, Map<String, String> conversation) {
		this (width, heigth);
		this.conversation = conversation;
	}

	private Panel getDecoratedConversation (String key, String Value) {
		VerticalPanel vp = new VerticalPanel ();
		HorizontalPanel hp = new HorizontalPanel ();

		vp.setWidth (this.width + "px");
		hp.setWidth ("100%");

		hp.add (new HTML ("&nbsp&nbsp&nbsp&nbsp"));
		Label valueLabel = new Label (Value);
		hp.add (valueLabel);
		valueLabel.setWidth ("100%");

		hp.setCellWidth (hp.getWidget (0), "10%");
		hp.setCellWidth (hp.getWidget (1), "90%");

		vp.add (new HTML (key));
		vp.add (hp);
		vp.setCellHorizontalAlignment (vp.getWidget (0), HasHorizontalAlignment.ALIGN_LEFT);

		return vp;
	}

	public VerticalPanel show () {
		// FIXME: must delete

		VerticalPanel vPanel = new VerticalPanel ();
		Set<String> keys = conversation.keySet ();
		for (String key : keys)
			{
				vPanel.add (getDecoratedConversation (key, conversation.get (key)));
			}
		return vPanel;
	}
}
