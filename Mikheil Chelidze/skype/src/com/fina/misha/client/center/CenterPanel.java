
package com.fina.misha.client.center;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

public class CenterPanel extends LayoutPanel {
	
	private int width;
	private int heigth;
	private Map <String,String> conversation = new LinkedHashMap<String,String> ();
	
	public CenterPanel(int width, int heigth){
		this.width = width;
		this.heigth = heigth;
		this.setPixelSize (this.width, this.heigth);
		
	}
	
	public void addConversationMode(Map <String,String> conversation){
		this.conversation = conversation;
		ConversationMode cMode = new ConversationMode (width-20, heigth, this.conversation);
		this.clear ();
		ScrollPanel scrollPanel = new ScrollPanel ();
		scrollPanel.setHeight ("80%");
		scrollPanel.add (cMode.show ());
		this.add (scrollPanel);
	}
}
