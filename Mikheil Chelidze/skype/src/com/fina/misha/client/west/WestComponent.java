
package com.fina.misha.client.west;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class WestComponent {

	private int width;// px
	private int height;
	private Panel centerPanel;

	private VerticalPanel westMainVerticalPanel = new VerticalPanel ();
	private StackPanel communicationStackPanel = new StackPanel ();
	private Button aboutMeButton = new Button ();
	private Button addUserButton = new Button ();

	public WestComponent (int width,int height, Panel centerPanel) {
		this.width = width;
		this.height = height;
		this.centerPanel = centerPanel;
	}
	public WestComponent (int height,Panel centerPabel) {
		this(250,height,centerPabel);
	}

	public Panel getWestPabel () {
		initialWestMainVerticalPanel ();
		return this.westMainVerticalPanel;
	}

	private void initialWestMainVerticalPanel () {
		initialCommunicationStackPanel ();
		initAboutMeButton ();
		initilalAddUserButton ();
		westMainVerticalPanel.setWidth (width + "px");

		westMainVerticalPanel.add (aboutMeButton);
		westMainVerticalPanel.add (communicationStackPanel);
		westMainVerticalPanel.add (addUserButton);
	}

	private void initialCommunicationStackPanel () {
		int stackPanelHeight = (int) (this.height * 0.8);
		int stackPanelBodyHeight = stackPanelHeight - (2 * 32) - 10;

		ScrollPanel contacts = new ScrollPanel ();
		contacts.setHeight (stackPanelBodyHeight + "px");
		contacts.add (Contacts.getContactsPanel (this.width - 26,centerPanel));
		contacts.setAlwaysShowScrollBars (true);

		communicationStackPanel.setPixelSize (this.width, stackPanelHeight);
		communicationStackPanel.add (Phone.getPhone (Math.min (this.width, 250)-26), "call phones", false);
		communicationStackPanel.add (contacts, "contacts");
		communicationStackPanel.showStack (1);
	}

	private void initAboutMeButton () {
		aboutMeButton.setPixelSize (this.width, (int) (this.height * 0.1 - 2));
		aboutMeButton.setText ("'Users Name'  |  profile");
	}

	private void initilalAddUserButton () {
		addUserButton.setPixelSize (this.width, (int) (this.height * 0.1 - 2));
		addUserButton.setText ("add friend");
	}
}
