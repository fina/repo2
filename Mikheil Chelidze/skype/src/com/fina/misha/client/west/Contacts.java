
package com.fina.misha.client.west;


import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fina.misha.client.center.CenterPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

class Contacts {

	private TabPanel tabs = new TabPanel ();
	private VerticalPanel contactsVerticalPanel = new VerticalPanel ();
	private VerticalPanel conversationVericalPanel = new VerticalPanel ();
	private Panel centerPanel;

	private int width;

	private Contacts (int width, Panel centerPanel) {
		this.width = width;
		this.centerPanel = centerPanel;
	}

	// TODO must del
	private Button[] btns = new Button[0];

	private void initTabs () {
		initContactsVerticalPanel ();
		initConversationVerticalPanel ();

		String contacts = "contacts";
		String Conversations = "recent";

		tabs.add (contactsVerticalPanel, contacts);
		tabs.add (conversationVericalPanel, Conversations);

		tabs.selectTab (0);
		tabs.setWidth (this.width + "px");
	}

	private Button[] buttonContacts () {
		// TODO: must delete
		Button[] btns = new Button[28];
		LoadConversation load = new LoadConversation (centerPanel);
		for (int i = 0; i < btns.length; i++)
			{
				btns[i] = new Button ("firsname_" + i + "&nbsp&nbsp&nbsp lastname_" + i);
				btns[i].addClickHandler (load);
				btns[i].setWidth ("100%");
			}
		return btns;
	}

	private void initContactsVerticalPanel () {
		// FIXME: must load from server or local
		btns = buttonContacts ();
		for (int i = 0; i < btns.length; i++)
			{
				contactsVerticalPanel.add (btns[i]);
				contactsVerticalPanel.setCellHorizontalAlignment (btns[i], HasHorizontalAlignment.ALIGN_RIGHT);
			}

	}

	private void initConversationVerticalPanel () {
		// FIXME: must load from server or local
		for (int i = 0; i < btns.length / 5; i++)
			{
				int rand = Random.nextInt (btns.length - 1);
				if (conversationVericalPanel.getWidgetCount () < 1)
					{
						conversationVericalPanel.add (btns[rand].asWidget ());
						conversationVericalPanel.setCellHorizontalAlignment (btns[rand], HasHorizontalAlignment.ALIGN_RIGHT);
						continue;
					}
				if (conversationVericalPanel.getWidgetIndex (btns[rand]) < 0)
					{
						conversationVericalPanel.add (btns[rand]);
						conversationVericalPanel.setCellHorizontalAlignment (btns[rand], HasHorizontalAlignment.ALIGN_RIGHT);
					}
				else
					{
						i--;
					}
			}
	}

	public static TabPanel getContactsPanel (int width, Panel centerPanel) {
		Contacts c = new Contacts (width, centerPanel);
		c.initTabs ();
		return c.tabs;
	}

}



class LoadConversation implements ClickHandler {

	private Panel centerPanel;

	public LoadConversation (Panel centerPanel) {
		this.centerPanel = centerPanel;
	}

	private static String time () {
		Date date = new Date ();
		DateTimeFormat timeFormat = DateTimeFormat.getFormat ("dd.MM.yy  HH:mm:SS");
		return timeFormat.format (date);
	}

	@Override
	public void onClick (ClickEvent event) {
		Map<String, String> conversation = new LinkedHashMap<String, String> ();
		for (int i = 0; i < 15; i++)
			{
				conversation
						.put ("conert_" + i + " <br /> " + time (), " some big conversation text=[123456789]  index = " + i);
			}

		this.centerPanel.clear ();
		((CenterPanel) this.centerPanel).addConversationMode (conversation);
	}
}
