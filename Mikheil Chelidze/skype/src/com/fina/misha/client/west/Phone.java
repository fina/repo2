
package com.fina.misha.client.west;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.TextBox;

class Phone {

	private int width;

	FlexTable table = new FlexTable ();
	TextBox numberTextBox = new TextBox ();

	ClickHandler numberButtonClick = new ClickHandler () {

		@Override
		public void onClick (ClickEvent event) {
			Button b = (Button) event.getSource ();
			numberTextBox.setText (numberTextBox.getText () + b.getText ());
		}
	};

	public Phone (int width) {
		this.width = width;
	}

	public static FlexTable getPhone (int width) {
		Phone p = new Phone (width);
		p.initFlexTable ();
		return p.table;
	}

	private void initFlexTable () {
		Integer buttonWidth = this.width / 3;
		Integer buttonHeight = (buttonWidth * 4) / 5;

		table.setWidth (width + "px");
		numberTextBox.setWidth (width + "px");

		table.setWidget (0, 0, numberTextBox);
		FlexCellFormatter formatter = table.getFlexCellFormatter ();
		formatter.setColSpan (0, 0, 3);

		// add buttons
		for (int row = 1; row < 4; row++)
			{
				for (int column = 0; column < 3; column++)
					{
						Button btn = new Button (Integer.toString (3 * (row - 1) + (column + 1)));
						btn.addClickHandler (numberButtonClick);
						btn.setPixelSize (buttonWidth, buttonHeight);
						table.setWidget (row, column, btn);
					}
			}
		int row = table.getRowCount ();
		for (int col = 0; col < 3; col++)
			{
				String[] arr = new String[] { "*", "0", "#" };
				Button btn = new Button (arr[col]);
				btn.addClickHandler (numberButtonClick);
				btn.setPixelSize (buttonWidth, buttonHeight);
				table.setWidget (row, col, btn);
			}
		Button backspace = new Button ("\u21a4");
		backspace.setTitle ("back space");
		backspace.setPixelSize (buttonWidth, buttonHeight);
		backspace.addClickHandler (new ClickHandler () {

			@Override
			public void onClick (ClickEvent event) {
				String number = numberTextBox.getText ().trim ();
				if (number.isEmpty ()) { return; }
				numberTextBox.setText (number.substring (0, number.length () - 1));
			}
		});

		Button call = new Button ("CALL");
		call.setPixelSize (buttonWidth * 2 + table.getCellPadding (), buttonHeight);
		call.addClickHandler (new ClickHandler () {

			@Override
			public void onClick (ClickEvent event) {
				if (numberTextBox.getText ().trim ().isEmpty ())
					{
						Window.alert ("enter number");
					}
				else
					Window.alert ("Call with [" + numberTextBox.getText ().trim () + "]  FAILURED!\nBecause it doesn't works :D");
				numberTextBox.setText ("");
			}
		});
		row = table.getRowCount ();
		table.setWidget (row, 0, backspace);
		table.setWidget (row, 1, call);
		formatter.setColSpan (row, 1, 2);
		formatter.setHorizontalAlignment (row, 1, HasHorizontalAlignment.ALIGN_CENTER);

	}
}
