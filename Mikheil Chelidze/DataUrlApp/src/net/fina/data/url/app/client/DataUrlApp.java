
package net.fina.data.url.app.client;

import org.apache.tools.ant.taskdefs.Unpack;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class DataUrlApp implements EntryPoint {

	private VerticalPanel vPanel = new VerticalPanel ();
	private TextArea input = new TextArea ();
	private Button generateDataUrl = new Button ("generate download url");
	private Anchor dataURL = new Anchor (); 
	
	private ClickHandler clickEvent = new ClickHandler() {
		
		@Override
		public void onClick (ClickEvent event) {
			String fileContent = input.getText ();
			fileContent = Base64.encode (fileContent);
			dataURL.setHTML ("download.xml");
			dataURL.setHref ("data:application/xml;charset=utf-8;base64," + fileContent);
			dataURL.setTarget ("_blanc");
		}
	};
	
	
	public void onModuleLoad () {		
		Panel mainPanel = RootLayoutPanel.get ();
		vPanel.setSize ("100%","100%");
		
		vPanel.add (input);
		vPanel.add(dataURL);
		vPanel.add(generateDataUrl);
		
		vPanel.setCellHeight (input, "80%");
		vPanel.setCellHeight (dataURL, "9%");
		vPanel.setCellHeight (generateDataUrl, "9%");
		
		vPanel.setCellHorizontalAlignment (input, HasHorizontalAlignment.ALIGN_CENTER);
		vPanel.setCellHorizontalAlignment (dataURL, HasHorizontalAlignment.ALIGN_CENTER);
		vPanel.setCellVerticalAlignment (dataURL, HasVerticalAlignment.ALIGN_MIDDLE);
		
		input.setSize ("98%","100%");
		generateDataUrl.addClickHandler (clickEvent);
		generateDataUrl.setSize ("100%","100%");
		dataURL.setSize ("100%","100%");
		
		
		mainPanel.add (vPanel);
		
		
	}
}
