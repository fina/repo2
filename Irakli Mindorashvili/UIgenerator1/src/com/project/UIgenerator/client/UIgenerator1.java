package com.project.UIgenerator.client ;



import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class UIgenerator1 implements EntryPoint {

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	
	   VerticalPanel vert = new VerticalPanel();
	   HorizontalPanel addPanel = new HorizontalPanel();
	   TextBox TextBox = new TextBox();
	   Button GenerateButton = new Button("generate");
	   final HTML serverResponseLabel = new HTML();
	   

	  /**
	   * Entry point method.
	   */
	  public void onModuleLoad() {
         
		  vert.setPixelSize(450, 500);
		  vert.setVisible(true); 
		  vert.add(addPanel);
		  addPanel.setPixelSize(300, 200);
		  addPanel.setVisible(true);
		  addPanel.add(GenerateButton);
		  addPanel.add(TextBox);
		  TextBox.setFocus(true);
		  RootPanel.get().add(vert);
		  
		  GenerateButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
            greetingService.greetServer("str", new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					TextBox.setText(result);
					
					
					
					
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
				
				
				
				
				
			}
		});
		  
		 

	  }
	
	  
	  
	  

	}