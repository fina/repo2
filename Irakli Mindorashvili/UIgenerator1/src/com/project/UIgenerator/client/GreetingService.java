package com.project.UIgenerator.client;

import com.google.gwt.user.client.rpc.RemoteService;

public interface  GreetingService extends RemoteService {
	
	String greetServer(String name) throws IllegalArgumentException;
	

}
