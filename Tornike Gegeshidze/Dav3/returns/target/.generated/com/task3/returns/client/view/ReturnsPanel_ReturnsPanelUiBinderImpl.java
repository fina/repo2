package com.task3.returns.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class ReturnsPanel_ReturnsPanelUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.task3.returns.client.view.ReturnsPanel>, com.task3.returns.client.view.ReturnsPanel.ReturnsPanelUiBinder {

  interface Template extends SafeHtmlTemplates {
    @Template("FINANTIAL INSTITUTE")
    SafeHtml html1();
     
    @Template("FINANTIAL TYPE")
    SafeHtml html2();
     
    @Template("TEMPLATE")
    SafeHtml html3();
     
    @Template("RETURNS")
    SafeHtml html4();
     
  }

  Template template = GWT.create(Template.class);


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.task3.returns.client.view.ReturnsPanel owner) {


    return new Widgets(owner).get_tabPanel();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.task3.returns.client.view.ReturnsPanel owner;


    public Widgets(final com.task3.returns.client.view.ReturnsPanel owner) {
      this.owner = owner;
      build_style();  // generated css resource must be always created. Type: GENERATED_CSS. Precedence: 1
    }

    SafeHtml template_html1() {
      return template.html1();
    }
    SafeHtml template_html2() {
      return template.html2();
    }
    SafeHtml template_html3() {
      return template.html3();
    }
    SafeHtml template_html4() {
      return template.html4();
    }

    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 1 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle) GWT.create(com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle.class);
      // Setup section.


      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for style called 2 times. Type: GENERATED_CSS. Build precedence: 1.
     */
    private com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style style;
    private com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style get_style() {
      return style;
    }
    private com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style build_style() {
      // Creation section.
      style = get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay().style();
      // Setup section.
      style.ensureInjected();


      return style;
    }

    /**
     * Getter for tabPanel called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private com.google.gwt.user.client.ui.TabLayoutPanel get_tabPanel() {
      return build_tabPanel();
    }
    private com.google.gwt.user.client.ui.TabLayoutPanel build_tabPanel() {
      // Creation section.
      final com.google.gwt.user.client.ui.TabLayoutPanel tabPanel = new com.google.gwt.user.client.ui.TabLayoutPanel(60, com.google.gwt.dom.client.Style.Unit.PX);
      // Setup section.
      tabPanel.add(get_f_VerticalPanel1(), template_html1().asString(), true);
      tabPanel.add(get_f_VerticalPanel17(), template_html2().asString(), true);
      tabPanel.add(get_f_VerticalPanel24(), template_html3().asString(), true);
      tabPanel.add(get_f_VerticalPanel33(), template_html4().asString(), true);
      tabPanel.addStyleName("" + get_style().tabPanelExample1() + "");
      tabPanel.setHeight("600px");
      tabPanel.setWidth("475px");


      this.owner.tabPanel = tabPanel;

      return tabPanel;
    }

    /**
     * Getter for f_VerticalPanel1 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.VerticalPanel get_f_VerticalPanel1() {
      return build_f_VerticalPanel1();
    }
    private com.google.gwt.user.client.ui.VerticalPanel build_f_VerticalPanel1() {
      // Creation section.
      final com.google.gwt.user.client.ui.VerticalPanel f_VerticalPanel1 = (com.google.gwt.user.client.ui.VerticalPanel) GWT.create(com.google.gwt.user.client.ui.VerticalPanel.class);
      // Setup section.
      f_VerticalPanel1.add(get_f_HorizontalPanel2());
      f_VerticalPanel1.add(get_f_HorizontalPanel4());
      f_VerticalPanel1.add(get_f_HorizontalPanel6());
      f_VerticalPanel1.add(get_f_HorizontalPanel8());
      f_VerticalPanel1.add(get_f_HorizontalPanel10());
      f_VerticalPanel1.add(get_f_HorizontalPanel12());
      f_VerticalPanel1.add(get_f_HorizontalPanel14());
      f_VerticalPanel1.add(get_f_HorizontalPanel16());
      f_VerticalPanel1.add(get_FI_Flextable());


      return f_VerticalPanel1;
    }

    /**
     * Getter for f_HorizontalPanel2 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel2() {
      return build_f_HorizontalPanel2();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel2() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel2 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel2.add(get_f_Label3());
      f_HorizontalPanel2.add(get_FI_Code());


      return f_HorizontalPanel2;
    }

    /**
     * Getter for f_Label3 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label3() {
      return build_f_Label3();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label3() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label3 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label3.setText("FI Code");


      return f_Label3;
    }

    /**
     * Getter for FI_Code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Code() {
      return build_FI_Code();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Code = FI_Code;

      return FI_Code;
    }

    /**
     * Getter for f_HorizontalPanel4 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel4() {
      return build_f_HorizontalPanel4();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel4() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel4 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel4.add(get_f_Label5());
      f_HorizontalPanel4.add(get_FI_Type_code());


      return f_HorizontalPanel4;
    }

    /**
     * Getter for f_Label5 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label5() {
      return build_f_Label5();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label5() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label5 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label5.setText("Type Code");


      return f_Label5;
    }

    /**
     * Getter for FI_Type_code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Type_code() {
      return build_FI_Type_code();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Type_code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Type_code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Type_code = FI_Type_code;

      return FI_Type_code;
    }

    /**
     * Getter for f_HorizontalPanel6 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel6() {
      return build_f_HorizontalPanel6();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel6() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel6 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel6.add(get_f_Label7());
      f_HorizontalPanel6.add(get_FI_Name());


      return f_HorizontalPanel6;
    }

    /**
     * Getter for f_Label7 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label7() {
      return build_f_Label7();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label7() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label7 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label7.setText("FI Name");


      return f_Label7;
    }

    /**
     * Getter for FI_Name called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Name() {
      return build_FI_Name();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Name() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Name = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Name = FI_Name;

      return FI_Name;
    }

    /**
     * Getter for f_HorizontalPanel8 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel8() {
      return build_f_HorizontalPanel8();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel8() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel8 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel8.add(get_f_Label9());
      f_HorizontalPanel8.add(get_FI_Address());


      return f_HorizontalPanel8;
    }

    /**
     * Getter for f_Label9 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label9() {
      return build_f_Label9();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label9() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label9 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label9.setText("FI Address");


      return f_Label9;
    }

    /**
     * Getter for FI_Address called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Address() {
      return build_FI_Address();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Address() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Address = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Address = FI_Address;

      return FI_Address;
    }

    /**
     * Getter for f_HorizontalPanel10 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel10() {
      return build_f_HorizontalPanel10();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel10() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel10 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel10.add(get_f_Label11());
      f_HorizontalPanel10.add(get_FI_Phone());


      return f_HorizontalPanel10;
    }

    /**
     * Getter for f_Label11 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label11() {
      return build_f_Label11();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label11() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label11 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label11.setText("FI Phone");


      return f_Label11;
    }

    /**
     * Getter for FI_Phone called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Phone() {
      return build_FI_Phone();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Phone() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Phone = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Phone = FI_Phone;

      return FI_Phone;
    }

    /**
     * Getter for f_HorizontalPanel12 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel12() {
      return build_f_HorizontalPanel12();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel12() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel12 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel12.add(get_f_Label13());
      f_HorizontalPanel12.add(get_FI_Mail());


      return f_HorizontalPanel12;
    }

    /**
     * Getter for f_Label13 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label13() {
      return build_f_Label13();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label13() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label13 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label13.setText("FI Mail");


      return f_Label13;
    }

    /**
     * Getter for FI_Mail called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Mail() {
      return build_FI_Mail();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Mail() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Mail = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Mail = FI_Mail;

      return FI_Mail;
    }

    /**
     * Getter for f_HorizontalPanel14 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel14() {
      return build_f_HorizontalPanel14();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel14() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel14 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel14.add(get_f_Label15());
      f_HorizontalPanel14.add(get_FI_Type());


      return f_HorizontalPanel14;
    }

    /**
     * Getter for f_Label15 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label15() {
      return build_f_Label15();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label15() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label15 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label15.setText("FI Phone");


      return f_Label15;
    }

    /**
     * Getter for FI_Type called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_FI_Type() {
      return build_FI_Type();
    }
    private com.google.gwt.user.client.ui.TextBox build_FI_Type() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox FI_Type = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.FI_Type = FI_Type;

      return FI_Type;
    }

    /**
     * Getter for f_HorizontalPanel16 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel16() {
      return build_f_HorizontalPanel16();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel16() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel16 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel16.add(get_Add_Fi_button());
      f_HorizontalPanel16.add(get_delete_Fi_button());


      return f_HorizontalPanel16;
    }

    /**
     * Getter for Add_Fi_button called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_Add_Fi_button() {
      return build_Add_Fi_button();
    }
    private com.google.gwt.user.client.ui.Button build_Add_Fi_button() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button Add_Fi_button = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      Add_Fi_button.setText("add");


      this.owner.Add_Fi_button = Add_Fi_button;

      return Add_Fi_button;
    }

    /**
     * Getter for delete_Fi_button called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_delete_Fi_button() {
      return build_delete_Fi_button();
    }
    private com.google.gwt.user.client.ui.Button build_delete_Fi_button() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button delete_Fi_button = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      delete_Fi_button.setText("delete");


      this.owner.delete_Fi_button = delete_Fi_button;

      return delete_Fi_button;
    }

    /**
     * Getter for FI_Flextable called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.FlexTable get_FI_Flextable() {
      return build_FI_Flextable();
    }
    private com.google.gwt.user.client.ui.FlexTable build_FI_Flextable() {
      // Creation section.
      final com.google.gwt.user.client.ui.FlexTable FI_Flextable = (com.google.gwt.user.client.ui.FlexTable) GWT.create(com.google.gwt.user.client.ui.FlexTable.class);
      // Setup section.
      FI_Flextable.addStyleName("" + get_style().contactsViewContactsFlexTable() + "");


      this.owner.FI_Flextable = FI_Flextable;

      return FI_Flextable;
    }

    /**
     * Getter for f_VerticalPanel17 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.VerticalPanel get_f_VerticalPanel17() {
      return build_f_VerticalPanel17();
    }
    private com.google.gwt.user.client.ui.VerticalPanel build_f_VerticalPanel17() {
      // Creation section.
      final com.google.gwt.user.client.ui.VerticalPanel f_VerticalPanel17 = (com.google.gwt.user.client.ui.VerticalPanel) GWT.create(com.google.gwt.user.client.ui.VerticalPanel.class);
      // Setup section.
      f_VerticalPanel17.add(get_f_HorizontalPanel18());
      f_VerticalPanel17.add(get_f_HorizontalPanel20());
      f_VerticalPanel17.add(get_f_HorizontalPanel22());
      f_VerticalPanel17.add(get_f_HorizontalPanel23());


      return f_VerticalPanel17;
    }

    /**
     * Getter for f_HorizontalPanel18 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel18() {
      return build_f_HorizontalPanel18();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel18() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel18 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel18.add(get_f_Label19());
      f_HorizontalPanel18.add(get_Type_Code());


      return f_HorizontalPanel18;
    }

    /**
     * Getter for f_Label19 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label19() {
      return build_f_Label19();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label19() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label19 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label19.setText("Type Code");


      return f_Label19;
    }

    /**
     * Getter for Type_Code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Type_Code() {
      return build_Type_Code();
    }
    private com.google.gwt.user.client.ui.TextBox build_Type_Code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Type_Code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Type_Code = Type_Code;

      return Type_Code;
    }

    /**
     * Getter for f_HorizontalPanel20 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel20() {
      return build_f_HorizontalPanel20();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel20() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel20 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel20.add(get_f_Label21());
      f_HorizontalPanel20.add(get_Type_Name());


      return f_HorizontalPanel20;
    }

    /**
     * Getter for f_Label21 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label21() {
      return build_f_Label21();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label21() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label21 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label21.setText("Type name");


      return f_Label21;
    }

    /**
     * Getter for Type_Name called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Type_Name() {
      return build_Type_Name();
    }
    private com.google.gwt.user.client.ui.TextBox build_Type_Name() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Type_Name = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Type_Name = Type_Name;

      return Type_Name;
    }

    /**
     * Getter for f_HorizontalPanel22 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel22() {
      return build_f_HorizontalPanel22();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel22() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel22 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel22.add(get_Add_Type());
      f_HorizontalPanel22.add(get_Delete_type());


      return f_HorizontalPanel22;
    }

    /**
     * Getter for Add_Type called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_Add_Type() {
      return build_Add_Type();
    }
    private com.google.gwt.user.client.ui.Button build_Add_Type() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button Add_Type = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      Add_Type.setText("Add");


      this.owner.Add_Type = Add_Type;

      return Add_Type;
    }

    /**
     * Getter for Delete_type called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_Delete_type() {
      return build_Delete_type();
    }
    private com.google.gwt.user.client.ui.Button build_Delete_type() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button Delete_type = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      Delete_type.setText("Delete");


      this.owner.Delete_type = Delete_type;

      return Delete_type;
    }

    /**
     * Getter for f_HorizontalPanel23 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel23() {
      return build_f_HorizontalPanel23();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel23() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel23 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel23.add(get_Fi_Type_Flextable());


      return f_HorizontalPanel23;
    }

    /**
     * Getter for Fi_Type_Flextable called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.FlexTable get_Fi_Type_Flextable() {
      return build_Fi_Type_Flextable();
    }
    private com.google.gwt.user.client.ui.FlexTable build_Fi_Type_Flextable() {
      // Creation section.
      final com.google.gwt.user.client.ui.FlexTable Fi_Type_Flextable = (com.google.gwt.user.client.ui.FlexTable) GWT.create(com.google.gwt.user.client.ui.FlexTable.class);
      // Setup section.


      this.owner.Fi_Type_Flextable = Fi_Type_Flextable;

      return Fi_Type_Flextable;
    }

    /**
     * Getter for f_VerticalPanel24 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.VerticalPanel get_f_VerticalPanel24() {
      return build_f_VerticalPanel24();
    }
    private com.google.gwt.user.client.ui.VerticalPanel build_f_VerticalPanel24() {
      // Creation section.
      final com.google.gwt.user.client.ui.VerticalPanel f_VerticalPanel24 = (com.google.gwt.user.client.ui.VerticalPanel) GWT.create(com.google.gwt.user.client.ui.VerticalPanel.class);
      // Setup section.
      f_VerticalPanel24.add(get_f_HorizontalPanel25());
      f_VerticalPanel24.add(get_f_HorizontalPanel27());
      f_VerticalPanel24.add(get_f_HorizontalPanel29());
      f_VerticalPanel24.add(get_f_HorizontalPanel31());
      f_VerticalPanel24.add(get_f_HorizontalPanel32());


      return f_VerticalPanel24;
    }

    /**
     * Getter for f_HorizontalPanel25 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel25() {
      return build_f_HorizontalPanel25();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel25() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel25 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel25.add(get_f_Label26());
      f_HorizontalPanel25.add(get_Template_Code());


      return f_HorizontalPanel25;
    }

    /**
     * Getter for f_Label26 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label26() {
      return build_f_Label26();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label26() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label26 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label26.setText("Template Code");


      return f_Label26;
    }

    /**
     * Getter for Template_Code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Template_Code() {
      return build_Template_Code();
    }
    private com.google.gwt.user.client.ui.TextBox build_Template_Code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Template_Code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Template_Code = Template_Code;

      return Template_Code;
    }

    /**
     * Getter for f_HorizontalPanel27 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel27() {
      return build_f_HorizontalPanel27();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel27() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel27 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel27.add(get_f_Label28());
      f_HorizontalPanel27.add(get_Template_Name());


      return f_HorizontalPanel27;
    }

    /**
     * Getter for f_Label28 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label28() {
      return build_f_Label28();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label28() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label28 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label28.setText("Template Name");


      return f_Label28;
    }

    /**
     * Getter for Template_Name called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Template_Name() {
      return build_Template_Name();
    }
    private com.google.gwt.user.client.ui.TextBox build_Template_Name() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Template_Name = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Template_Name = Template_Name;

      return Template_Name;
    }

    /**
     * Getter for f_HorizontalPanel29 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel29() {
      return build_f_HorizontalPanel29();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel29() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel29 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel29.add(get_f_Label30());
      f_HorizontalPanel29.add(get_Template_Transaction());


      return f_HorizontalPanel29;
    }

    /**
     * Getter for f_Label30 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label30() {
      return build_f_Label30();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label30() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label30 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label30.setText("Transaction :");


      return f_Label30;
    }

    /**
     * Getter for Template_Transaction called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.ListBox get_Template_Transaction() {
      return build_Template_Transaction();
    }
    private com.google.gwt.user.client.ui.ListBox build_Template_Transaction() {
      // Creation section.
      final com.google.gwt.user.client.ui.ListBox Template_Transaction = (com.google.gwt.user.client.ui.ListBox) GWT.create(com.google.gwt.user.client.ui.ListBox.class);
      // Setup section.
      Template_Transaction.addItem("Weekly", "1");
      Template_Transaction.addItem("Monthly", "2");
      Template_Transaction.addItem("Yearly", "3");


      this.owner.Template_Transaction = Template_Transaction;

      return Template_Transaction;
    }

    /**
     * Getter for f_HorizontalPanel31 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel31() {
      return build_f_HorizontalPanel31();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel31() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel31 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel31.add(get_addTemplateButton());
      f_HorizontalPanel31.add(get_deleteTemplateButton());


      return f_HorizontalPanel31;
    }

    /**
     * Getter for addTemplateButton called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_addTemplateButton() {
      return build_addTemplateButton();
    }
    private com.google.gwt.user.client.ui.Button build_addTemplateButton() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button addTemplateButton = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      addTemplateButton.setText("add");


      this.owner.addTemplateButton = addTemplateButton;

      return addTemplateButton;
    }

    /**
     * Getter for deleteTemplateButton called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_deleteTemplateButton() {
      return build_deleteTemplateButton();
    }
    private com.google.gwt.user.client.ui.Button build_deleteTemplateButton() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button deleteTemplateButton = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      deleteTemplateButton.setText("delete");


      this.owner.deleteTemplateButton = deleteTemplateButton;

      return deleteTemplateButton;
    }

    /**
     * Getter for f_HorizontalPanel32 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel32() {
      return build_f_HorizontalPanel32();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel32() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel32 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel32.add(get_templateTable());


      return f_HorizontalPanel32;
    }

    /**
     * Getter for templateTable called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.FlexTable get_templateTable() {
      return build_templateTable();
    }
    private com.google.gwt.user.client.ui.FlexTable build_templateTable() {
      // Creation section.
      final com.google.gwt.user.client.ui.FlexTable templateTable = (com.google.gwt.user.client.ui.FlexTable) GWT.create(com.google.gwt.user.client.ui.FlexTable.class);
      // Setup section.


      this.owner.templateTable = templateTable;

      return templateTable;
    }

    /**
     * Getter for f_VerticalPanel33 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.VerticalPanel get_f_VerticalPanel33() {
      return build_f_VerticalPanel33();
    }
    private com.google.gwt.user.client.ui.VerticalPanel build_f_VerticalPanel33() {
      // Creation section.
      final com.google.gwt.user.client.ui.VerticalPanel f_VerticalPanel33 = (com.google.gwt.user.client.ui.VerticalPanel) GWT.create(com.google.gwt.user.client.ui.VerticalPanel.class);
      // Setup section.
      f_VerticalPanel33.add(get_f_HorizontalPanel34());
      f_VerticalPanel33.add(get_f_HorizontalPanel36());
      f_VerticalPanel33.add(get_f_HorizontalPanel38());
      f_VerticalPanel33.add(get_f_HorizontalPanel39());


      return f_VerticalPanel33;
    }

    /**
     * Getter for f_HorizontalPanel34 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel34() {
      return build_f_HorizontalPanel34();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel34() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel34 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel34.add(get_f_Label35());
      f_HorizontalPanel34.add(get_Return_temp_code());


      return f_HorizontalPanel34;
    }

    /**
     * Getter for f_Label35 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label35() {
      return build_f_Label35();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label35() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label35 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label35.setText("Template code");


      return f_Label35;
    }

    /**
     * Getter for Return_temp_code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Return_temp_code() {
      return build_Return_temp_code();
    }
    private com.google.gwt.user.client.ui.TextBox build_Return_temp_code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Return_temp_code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Return_temp_code = Return_temp_code;

      return Return_temp_code;
    }

    /**
     * Getter for f_HorizontalPanel36 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel36() {
      return build_f_HorizontalPanel36();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel36() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel36 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel36.add(get_f_Label37());
      f_HorizontalPanel36.add(get_Return_fi_code());


      return f_HorizontalPanel36;
    }

    /**
     * Getter for f_Label37 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Label get_f_Label37() {
      return build_f_Label37();
    }
    private com.google.gwt.user.client.ui.Label build_f_Label37() {
      // Creation section.
      final com.google.gwt.user.client.ui.Label f_Label37 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
      // Setup section.
      f_Label37.setText("FI code");


      return f_Label37;
    }

    /**
     * Getter for Return_fi_code called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.TextBox get_Return_fi_code() {
      return build_Return_fi_code();
    }
    private com.google.gwt.user.client.ui.TextBox build_Return_fi_code() {
      // Creation section.
      final com.google.gwt.user.client.ui.TextBox Return_fi_code = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
      // Setup section.


      this.owner.Return_fi_code = Return_fi_code;

      return Return_fi_code;
    }

    /**
     * Getter for f_HorizontalPanel38 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel38() {
      return build_f_HorizontalPanel38();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel38() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel38 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel38.add(get_addReturnButton());
      f_HorizontalPanel38.add(get_deleteReturnButton());


      return f_HorizontalPanel38;
    }

    /**
     * Getter for addReturnButton called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_addReturnButton() {
      return build_addReturnButton();
    }
    private com.google.gwt.user.client.ui.Button build_addReturnButton() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button addReturnButton = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      addReturnButton.setText("add");


      this.owner.addReturnButton = addReturnButton;

      return addReturnButton;
    }

    /**
     * Getter for deleteReturnButton called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.Button get_deleteReturnButton() {
      return build_deleteReturnButton();
    }
    private com.google.gwt.user.client.ui.Button build_deleteReturnButton() {
      // Creation section.
      final com.google.gwt.user.client.ui.Button deleteReturnButton = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
      // Setup section.
      deleteReturnButton.setText("delete");


      this.owner.deleteReturnButton = deleteReturnButton;

      return deleteReturnButton;
    }

    /**
     * Getter for f_HorizontalPanel39 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private com.google.gwt.user.client.ui.HorizontalPanel get_f_HorizontalPanel39() {
      return build_f_HorizontalPanel39();
    }
    private com.google.gwt.user.client.ui.HorizontalPanel build_f_HorizontalPanel39() {
      // Creation section.
      final com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel39 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
      // Setup section.
      f_HorizontalPanel39.add(get_returnTable());


      return f_HorizontalPanel39;
    }

    /**
     * Getter for returnTable called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private com.google.gwt.user.client.ui.FlexTable get_returnTable() {
      return build_returnTable();
    }
    private com.google.gwt.user.client.ui.FlexTable build_returnTable() {
      // Creation section.
      final com.google.gwt.user.client.ui.FlexTable returnTable = (com.google.gwt.user.client.ui.FlexTable) GWT.create(com.google.gwt.user.client.ui.FlexTable.class);
      // Setup section.


      this.owner.returnTable = returnTable;

      return returnTable;
    }
  }
}
