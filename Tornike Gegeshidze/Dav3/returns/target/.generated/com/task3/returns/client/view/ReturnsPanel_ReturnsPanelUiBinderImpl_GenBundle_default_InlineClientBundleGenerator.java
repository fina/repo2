package com.task3.returns.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle_default_InlineClientBundleGenerator implements com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle {
  private static ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle_default_InlineClientBundleGenerator _instance0 = new ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle_default_InlineClientBundleGenerator();
  private void styleInitializer() {
    style = new com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GOIYV2KFI{font-weight:" + ("bold")  + ";}.GOIYV2KGI{margin:" + ("10px")  + ";}.GOIYV2KEI{margin:" + ("2px"+ " " +"0"+ " " +"2px"+ " " +"0")  + ";}");
      }
      public java.lang.String contactsViewContactsFlexTable() {
        return "GOIYV2KEI";
      }
      public java.lang.String important() {
        return "GOIYV2KFI";
      }
      public java.lang.String tabPanelExample1() {
        return "GOIYV2KGI";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style get() {
      return style;
    }
  }
  public com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle::style()();
    }
    return null;
  }-*/;
}
