package com.task3.returns.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Returns_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.task3.returns.client.Returnskey getReturnskey(com.task3.returns.client.Returns instance) /*-{
    return instance.@com.task3.returns.client.Returns::returnskey;
  }-*/;
  
  private static native void setReturnskey(com.task3.returns.client.Returns instance, com.task3.returns.client.Returnskey value) 
  /*-{
    instance.@com.task3.returns.client.Returns::returnskey = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.client.Returns instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setReturnskey(instance, (com.task3.returns.client.Returnskey) streamReader.readObject());
    
  }
  
  public static com.task3.returns.client.Returns instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.client.Returns();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.client.Returns instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeObject(getReturnskey(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.client.Returns_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.client.Returns_FieldSerializer.deserialize(reader, (com.task3.returns.client.Returns)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.client.Returns_FieldSerializer.serialize(writer, (com.task3.returns.client.Returns)object);
  }
  
}
