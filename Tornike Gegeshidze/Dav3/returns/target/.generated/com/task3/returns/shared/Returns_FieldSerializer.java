package com.task3.returns.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Returns_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFinantialCode(com.task3.returns.shared.Returns instance) /*-{
    return instance.@com.task3.returns.shared.Returns::finantialCode;
  }-*/;
  
  private static native void setFinantialCode(com.task3.returns.shared.Returns instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Returns::finantialCode = value;
  }-*/;
  
  private static native java.lang.String getTemplateCode(com.task3.returns.shared.Returns instance) /*-{
    return instance.@com.task3.returns.shared.Returns::templateCode;
  }-*/;
  
  private static native void setTemplateCode(com.task3.returns.shared.Returns instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Returns::templateCode = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.shared.Returns instance) throws SerializationException {
    setFinantialCode(instance, streamReader.readString());
    setTemplateCode(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.shared.Returns instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.shared.Returns();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.shared.Returns instance) throws SerializationException {
    streamWriter.writeString(getFinantialCode(instance));
    streamWriter.writeString(getTemplateCode(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.shared.Returns_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.shared.Returns_FieldSerializer.deserialize(reader, (com.task3.returns.shared.Returns)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.shared.Returns_FieldSerializer.serialize(writer, (com.task3.returns.shared.Returns)object);
  }
  
}
