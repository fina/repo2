package com.task3.returns.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Finantial_institute_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFI_Address(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_Address;
  }-*/;
  
  private static native void setFI_Address(com.task3.returns.client.Finantial_institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_Address = value;
  }-*/;
  
  private static native java.lang.Long getFI_ID(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_ID;
  }-*/;
  
  private static native void setFI_ID(com.task3.returns.client.Finantial_institute instance, java.lang.Long value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_ID = value;
  }-*/;
  
  private static native java.lang.String getFI_Mail(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_Mail;
  }-*/;
  
  private static native void setFI_Mail(com.task3.returns.client.Finantial_institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_Mail = value;
  }-*/;
  
  private static native java.lang.String getFI_Name(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_Name;
  }-*/;
  
  private static native void setFI_Name(com.task3.returns.client.Finantial_institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_Name = value;
  }-*/;
  
  private static native java.lang.String getFI_Phone(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_Phone;
  }-*/;
  
  private static native void setFI_Phone(com.task3.returns.client.Finantial_institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_Phone = value;
  }-*/;
  
  private static native java.lang.String getFI_code(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::FI_code;
  }-*/;
  
  private static native void setFI_code(com.task3.returns.client.Finantial_institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::FI_code = value;
  }-*/;
  
  private static native com.task3.returns.client.Instityte_type getTute(com.task3.returns.client.Finantial_institute instance) /*-{
    return instance.@com.task3.returns.client.Finantial_institute::tute;
  }-*/;
  
  private static native void setTute(com.task3.returns.client.Finantial_institute instance, com.task3.returns.client.Instityte_type value) 
  /*-{
    instance.@com.task3.returns.client.Finantial_institute::tute = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.client.Finantial_institute instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setFI_Address(instance, streamReader.readString());
    setFI_ID(instance, (java.lang.Long) streamReader.readObject());
    setFI_Mail(instance, streamReader.readString());
    setFI_Name(instance, streamReader.readString());
    setFI_Phone(instance, streamReader.readString());
    setFI_code(instance, streamReader.readString());
    setTute(instance, (com.task3.returns.client.Instityte_type) streamReader.readObject());
    
  }
  
  public static com.task3.returns.client.Finantial_institute instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.client.Finantial_institute();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.client.Finantial_institute instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeString(getFI_Address(instance));
    streamWriter.writeObject(getFI_ID(instance));
    streamWriter.writeString(getFI_Mail(instance));
    streamWriter.writeString(getFI_Name(instance));
    streamWriter.writeString(getFI_Phone(instance));
    streamWriter.writeString(getFI_code(instance));
    streamWriter.writeObject(getTute(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.client.Finantial_institute_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.client.Finantial_institute_FieldSerializer.deserialize(reader, (com.task3.returns.client.Finantial_institute)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.client.Finantial_institute_FieldSerializer.serialize(writer, (com.task3.returns.client.Finantial_institute)object);
  }
  
}
