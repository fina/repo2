package com.task3.returns.client.view;

import com.google.gwt.resources.client.CssResource;

public interface ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style extends CssResource {
  String important();
  String contactsViewContactsFlexTable();
  String tabPanelExample1();
}
