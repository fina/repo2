package com.task3.returns.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Template_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getTemplate_Code(com.task3.returns.shared.Template instance) /*-{
    return instance.@com.task3.returns.shared.Template::Template_Code;
  }-*/;
  
  private static native void setTemplate_Code(com.task3.returns.shared.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Template::Template_Code = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getTemplate_id(com.task3.returns.shared.Template instance) /*-{
    return instance.@com.task3.returns.shared.Template::Template_id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setTemplate_id(com.task3.returns.shared.Template instance, long value) 
  /*-{
    instance.@com.task3.returns.shared.Template::Template_id = value;
  }-*/;
  
  private static native java.lang.String getTemplate_name(com.task3.returns.shared.Template instance) /*-{
    return instance.@com.task3.returns.shared.Template::Template_name;
  }-*/;
  
  private static native void setTemplate_name(com.task3.returns.shared.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Template::Template_name = value;
  }-*/;
  
  private static native java.lang.String getTemplate_schedule(com.task3.returns.shared.Template instance) /*-{
    return instance.@com.task3.returns.shared.Template::Template_schedule;
  }-*/;
  
  private static native void setTemplate_schedule(com.task3.returns.shared.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Template::Template_schedule = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.shared.Template instance) throws SerializationException {
    setTemplate_Code(instance, streamReader.readString());
    setTemplate_id(instance, streamReader.readLong());
    setTemplate_name(instance, streamReader.readString());
    setTemplate_schedule(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.shared.Template instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.shared.Template();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.shared.Template instance) throws SerializationException {
    streamWriter.writeString(getTemplate_Code(instance));
    streamWriter.writeLong(getTemplate_id(instance));
    streamWriter.writeString(getTemplate_name(instance));
    streamWriter.writeString(getTemplate_schedule(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.shared.Template_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.shared.Template_FieldSerializer.deserialize(reader, (com.task3.returns.shared.Template)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.shared.Template_FieldSerializer.serialize(writer, (com.task3.returns.shared.Template)object);
  }
  
}
