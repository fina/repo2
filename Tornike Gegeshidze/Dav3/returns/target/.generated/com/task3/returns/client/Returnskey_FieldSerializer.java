package com.task3.returns.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Returnskey_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getRet_FI_ID(com.task3.returns.client.Returnskey instance) /*-{
    return instance.@com.task3.returns.client.Returnskey::Ret_FI_ID;
  }-*/;
  
  private static native void setRet_FI_ID(com.task3.returns.client.Returnskey instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Returnskey::Ret_FI_ID = value;
  }-*/;
  
  private static native java.lang.String getRet_Template_ID(com.task3.returns.client.Returnskey instance) /*-{
    return instance.@com.task3.returns.client.Returnskey::Ret_Template_ID;
  }-*/;
  
  private static native void setRet_Template_ID(com.task3.returns.client.Returnskey instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Returnskey::Ret_Template_ID = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.client.Returnskey instance) throws SerializationException {
    setRet_FI_ID(instance, streamReader.readString());
    setRet_Template_ID(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.client.Returnskey instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.client.Returnskey();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.client.Returnskey instance) throws SerializationException {
    streamWriter.writeString(getRet_FI_ID(instance));
    streamWriter.writeString(getRet_Template_ID(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.client.Returnskey_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.client.Returnskey_FieldSerializer.deserialize(reader, (com.task3.returns.client.Returnskey)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.client.Returnskey_FieldSerializer.serialize(writer, (com.task3.returns.client.Returnskey)object);
  }
  
}
