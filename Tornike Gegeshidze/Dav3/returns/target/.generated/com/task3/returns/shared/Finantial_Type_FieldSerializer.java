package com.task3.returns.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Finantial_Type_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getType_code(com.task3.returns.shared.Finantial_Type instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Type::Type_code;
  }-*/;
  
  private static native void setType_code(com.task3.returns.shared.Finantial_Type instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Type::Type_code = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getType_id(com.task3.returns.shared.Finantial_Type instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Type::Type_id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setType_id(com.task3.returns.shared.Finantial_Type instance, long value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Type::Type_id = value;
  }-*/;
  
  private static native java.lang.String getType_name(com.task3.returns.shared.Finantial_Type instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Type::Type_name;
  }-*/;
  
  private static native void setType_name(com.task3.returns.shared.Finantial_Type instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Type::Type_name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.shared.Finantial_Type instance) throws SerializationException {
    setType_code(instance, streamReader.readString());
    setType_id(instance, streamReader.readLong());
    setType_name(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.shared.Finantial_Type instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.shared.Finantial_Type();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.shared.Finantial_Type instance) throws SerializationException {
    streamWriter.writeString(getType_code(instance));
    streamWriter.writeLong(getType_id(instance));
    streamWriter.writeString(getType_name(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.shared.Finantial_Type_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.shared.Finantial_Type_FieldSerializer.deserialize(reader, (com.task3.returns.shared.Finantial_Type)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.shared.Finantial_Type_FieldSerializer.serialize(writer, (com.task3.returns.shared.Finantial_Type)object);
  }
  
}
