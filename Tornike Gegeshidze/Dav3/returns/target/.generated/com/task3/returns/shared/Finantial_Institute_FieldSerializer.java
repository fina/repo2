package com.task3.returns.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Finantial_Institute_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFi_Adrress(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_Adrress;
  }-*/;
  
  private static native void setFi_Adrress(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_Adrress = value;
  }-*/;
  
  private static native java.lang.String getFi_Code(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_Code;
  }-*/;
  
  private static native void setFi_Code(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_Code = value;
  }-*/;
  
  private static native java.lang.String getFi_Name(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_Name;
  }-*/;
  
  private static native void setFi_Name(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_Name = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getFi_id(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setFi_id(com.task3.returns.shared.Finantial_Institute instance, long value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_id = value;
  }-*/;
  
  private static native java.lang.String getFi_mail(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_mail;
  }-*/;
  
  private static native void setFi_mail(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_mail = value;
  }-*/;
  
  private static native java.lang.String getFi_phone(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_phone;
  }-*/;
  
  private static native void setFi_phone(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_phone = value;
  }-*/;
  
  private static native java.lang.String getFi_type(com.task3.returns.shared.Finantial_Institute instance) /*-{
    return instance.@com.task3.returns.shared.Finantial_Institute::Fi_type;
  }-*/;
  
  private static native void setFi_type(com.task3.returns.shared.Finantial_Institute instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.shared.Finantial_Institute::Fi_type = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.shared.Finantial_Institute instance) throws SerializationException {
    setFi_Adrress(instance, streamReader.readString());
    setFi_Code(instance, streamReader.readString());
    setFi_Name(instance, streamReader.readString());
    setFi_id(instance, streamReader.readLong());
    setFi_mail(instance, streamReader.readString());
    setFi_phone(instance, streamReader.readString());
    setFi_type(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.shared.Finantial_Institute instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.shared.Finantial_Institute();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.shared.Finantial_Institute instance) throws SerializationException {
    streamWriter.writeString(getFi_Adrress(instance));
    streamWriter.writeString(getFi_Code(instance));
    streamWriter.writeString(getFi_Name(instance));
    streamWriter.writeLong(getFi_id(instance));
    streamWriter.writeString(getFi_mail(instance));
    streamWriter.writeString(getFi_phone(instance));
    streamWriter.writeString(getFi_type(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.shared.Finantial_Institute_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.shared.Finantial_Institute_FieldSerializer.deserialize(reader, (com.task3.returns.shared.Finantial_Institute)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.shared.Finantial_Institute_FieldSerializer.serialize(writer, (com.task3.returns.shared.Finantial_Institute)object);
  }
  
}
