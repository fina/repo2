package com.task3.returns.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Instityte_type_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getType_Code(com.task3.returns.client.Instityte_type instance) /*-{
    return instance.@com.task3.returns.client.Instityte_type::Type_Code;
  }-*/;
  
  private static native void setType_Code(com.task3.returns.client.Instityte_type instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Instityte_type::Type_Code = value;
  }-*/;
  
  private static native java.lang.Long getType_ID(com.task3.returns.client.Instityte_type instance) /*-{
    return instance.@com.task3.returns.client.Instityte_type::Type_ID;
  }-*/;
  
  private static native void setType_ID(com.task3.returns.client.Instityte_type instance, java.lang.Long value) 
  /*-{
    instance.@com.task3.returns.client.Instityte_type::Type_ID = value;
  }-*/;
  
  private static native java.lang.String getType_Name(com.task3.returns.client.Instityte_type instance) /*-{
    return instance.@com.task3.returns.client.Instityte_type::Type_Name;
  }-*/;
  
  private static native void setType_Name(com.task3.returns.client.Instityte_type instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Instityte_type::Type_Name = value;
  }-*/;
  
  private static native java.util.List getIns_types(com.task3.returns.client.Instityte_type instance) /*-{
    return instance.@com.task3.returns.client.Instityte_type::ins_types;
  }-*/;
  
  private static native void setIns_types(com.task3.returns.client.Instityte_type instance, java.util.List value) 
  /*-{
    instance.@com.task3.returns.client.Instityte_type::ins_types = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.client.Instityte_type instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setType_Code(instance, streamReader.readString());
    setType_ID(instance, (java.lang.Long) streamReader.readObject());
    setType_Name(instance, streamReader.readString());
    setIns_types(instance, (java.util.List) streamReader.readObject());
    
  }
  
  public static com.task3.returns.client.Instityte_type instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.client.Instityte_type();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.client.Instityte_type instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeString(getType_Code(instance));
    streamWriter.writeObject(getType_ID(instance));
    streamWriter.writeString(getType_Name(instance));
    streamWriter.writeObject(getIns_types(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.client.Instityte_type_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.client.Instityte_type_FieldSerializer.deserialize(reader, (com.task3.returns.client.Instityte_type)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.client.Instityte_type_FieldSerializer.serialize(writer, (com.task3.returns.client.Instityte_type)object);
  }
  
}
