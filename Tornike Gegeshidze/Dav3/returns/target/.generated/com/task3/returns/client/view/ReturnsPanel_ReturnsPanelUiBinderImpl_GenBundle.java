package com.task3.returns.client.view;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.CssResource.Import;

public interface ReturnsPanel_ReturnsPanelUiBinderImpl_GenBundle extends ClientBundle {
  @Source("uibinder:com.task3.returns.client.view.ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style.css")
  ReturnsPanel_ReturnsPanelUiBinderImpl_GenCss_style style();

}
