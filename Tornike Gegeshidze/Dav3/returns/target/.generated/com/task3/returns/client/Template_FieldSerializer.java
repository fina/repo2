package com.task3.returns.client;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Template_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getID(com.task3.returns.client.Template instance) /*-{
    return instance.@com.task3.returns.client.Template::ID;
  }-*/;
  
  private static native void setID(com.task3.returns.client.Template instance, java.lang.Long value) 
  /*-{
    instance.@com.task3.returns.client.Template::ID = value;
  }-*/;
  
  private static native java.lang.String getT_Code(com.task3.returns.client.Template instance) /*-{
    return instance.@com.task3.returns.client.Template::T_Code;
  }-*/;
  
  private static native void setT_Code(com.task3.returns.client.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Template::T_Code = value;
  }-*/;
  
  private static native java.lang.String getT_Name(com.task3.returns.client.Template instance) /*-{
    return instance.@com.task3.returns.client.Template::T_Name;
  }-*/;
  
  private static native void setT_Name(com.task3.returns.client.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Template::T_Name = value;
  }-*/;
  
  private static native java.lang.String getT_Schedule(com.task3.returns.client.Template instance) /*-{
    return instance.@com.task3.returns.client.Template::T_Schedule;
  }-*/;
  
  private static native void setT_Schedule(com.task3.returns.client.Template instance, java.lang.String value) 
  /*-{
    instance.@com.task3.returns.client.Template::T_Schedule = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task3.returns.client.Template instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setID(instance, (java.lang.Long) streamReader.readObject());
    setT_Code(instance, streamReader.readString());
    setT_Name(instance, streamReader.readString());
    setT_Schedule(instance, streamReader.readString());
    
  }
  
  public static com.task3.returns.client.Template instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task3.returns.client.Template();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task3.returns.client.Template instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeObject(getID(instance));
    streamWriter.writeString(getT_Code(instance));
    streamWriter.writeString(getT_Name(instance));
    streamWriter.writeString(getT_Schedule(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task3.returns.client.Template_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task3.returns.client.Template_FieldSerializer.deserialize(reader, (com.task3.returns.client.Template)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task3.returns.client.Template_FieldSerializer.serialize(writer, (com.task3.returns.client.Template)object);
  }
  
}
