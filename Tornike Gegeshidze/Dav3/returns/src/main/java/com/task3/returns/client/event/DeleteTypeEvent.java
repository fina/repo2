package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class DeleteTypeEvent extends GwtEvent<DeleteTypeEventHandler> {

    public static Type<DeleteTypeEventHandler> TYPE=new Type<DeleteTypeEventHandler>();
    @Override
    public Type<DeleteTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteTypeEventHandler handler) {
        handler.onDeleteType(this);
    }
}
