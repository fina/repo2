package com.task3.returns.shared;

import java.io.Serializable;

/**
 * Created by tornike on 13-Jan-15.
 */
public class Finantial_Type implements Serializable {
    private long typeId;
    private String typeCode;
    private String typeName;


    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
