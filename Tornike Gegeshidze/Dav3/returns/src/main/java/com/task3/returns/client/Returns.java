package com.task3.returns.client;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by tornike on 26-Jan-15.
 */
@Entity
public class Returns implements Serializable {


    @EmbeddedId
    private Returnskey returnskey;


    public Returnskey getReturnskey() {
        return returnskey;
    }

    public void setReturnskey(Returnskey returnskey) {
        this.returnskey = returnskey;
    }
}
