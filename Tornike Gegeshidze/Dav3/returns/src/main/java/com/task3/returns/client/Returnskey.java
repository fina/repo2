package com.task3.returns.client;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by tornike on 04-Feb-15.
 */
@Embeddable
public class Returnskey implements Serializable {
    @Column
    private String retTemplateID;
    @Column
    private String retFiID;

    public String getRetTemplateID() {
        return retTemplateID;
    }

    public void setRetTemplateID(String retTemplateID) {
        this.retTemplateID = retTemplateID;
    }

    public String getRetFIID() {
        return retFiID;
    }

    public void setRetFIID(String retFIID) {
        this.retFiID = retFIID;
    }
}
