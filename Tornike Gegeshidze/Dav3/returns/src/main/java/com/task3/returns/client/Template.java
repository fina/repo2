package com.task3.returns.client;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tornike on 27-Dec-14.
 */
@Entity
public class Template implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;
    @Column(unique = true)
    private String tCode;
    private String tName;
    private String tSchedule;

   /* @ManyToMany(mappedBy = "Fi_Template")
    private Collection<Finantial_institute> fi_institutes;
    */

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getTCode() {
        return tCode;
    }

    public void setTCode(String tCode) {
        this.tCode = tCode;
    }

    public String getTName() {
        return tName;
    }

    public void setTName(String tName) {
        this.tName = tName;
    }

    public String getTSchedule() {
        return tSchedule;
    }

    public void setTSchedule(String tSchedule) {
        this.tSchedule = tSchedule;
    }
}
