package com.task3.returns.client;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by tornike on 27-Dec-14.
 */
@Entity
public class instityteType implements Serializable {

    @Id
    @GeneratedValue
    private Long typeID;
    @Column(unique = true)
    private String typeCode;

    private String Type_Name;

    @OneToMany(mappedBy = "tute", cascade = CascadeType.PERSIST)
    private List<Finantial_institute> ins_types;


    public instityteType() {
    }

    public Long getTypeID() {
        return typeID;
    }

    public void setTypeID(Long typeID) {
        this.typeID = typeID;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return Type_Name;
    }

    public void setTypeName(String typeName) {
        Type_Name = typeName;
    }
}
