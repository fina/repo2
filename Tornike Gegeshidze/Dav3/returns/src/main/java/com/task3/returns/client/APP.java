package com.task3.returns.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;


public class APP implements EntryPoint {

    public void onModuleLoad() {


        ReturnServiceAsync rpcservice = GWT.create(ReturnService.class);
        HandlerManager eventbus = new HandlerManager(null);
        AppController app = new AppController(rpcservice, eventbus);
        app.go(RootPanel.get());

    }
}