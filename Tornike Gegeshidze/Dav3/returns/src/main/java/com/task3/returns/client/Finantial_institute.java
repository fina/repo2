package com.task3.returns.client;

import com.google.gwt.user.client.rpc.IsSerializable;

import javax.persistence.*;

/**
 * Created by tornike on 27-Dec-14.
 */
@Entity
public class Finantial_institute implements IsSerializable {
    @Id
    @GeneratedValue
    private Long fiID;

    @Column(unique = true)
    private String fiCode;
    private String fiAddress;
    private String fiName;
    private String fiMail;
    private String fiPhone;

    @ManyToOne
    private instityteType tute;

    /*
        @ManyToMany
        @JoinTable
        public Collection<Template> Fi_Template;

    */
    public instityteType getInstityteTypes() {
        return getTute();
    }

    public void setInstityteTypes(instityteType instityteType) {
        this.setTute(instityteType);
    }

    public Long getFiID() {
        return fiID;
    }

    public void setFiID(Long fiID) {
        this.fiID = fiID;
    }

    public String getFiCode() {
        return fiCode;
    }

    public void setFiCode(String FI_code) {
        this.fiCode = FI_code;
    }

    public String getFiAddress() {
        return fiAddress;
    }

    public void setFiAddress(String fiAddress) {
        this.fiAddress = fiAddress;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }

    public String getFiMail() {
        return fiMail;
    }

    public void setFiMail(String fiMail) {
        this.fiMail = fiMail;
    }

    public String getFiPhone() {
        return fiPhone;
    }

    public void setFiPhone(String fiPhone) {
        this.fiPhone = fiPhone;
    }


    public instityteType getTute() {
        return tute;
    }

    public void setTute(instityteType tute) {
        this.tute = tute;
    }
}
