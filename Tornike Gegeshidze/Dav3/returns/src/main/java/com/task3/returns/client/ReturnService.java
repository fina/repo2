package com.task3.returns.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task3.returns.shared.Finantial_Institute;
import com.task3.returns.shared.Finantial_Type;

import java.util.List;

/**
 * Created by tornike on 08-Jan-15.
 */
@RemoteServiceRelativePath("contactService")
public interface ReturnService extends RemoteService {
    public void doConnect();
    public void addFi(Finantial_Institute finantial_institute);
    public void deleteFi(String typecode);
    public void addFiType(instityteType instityteType);
    public instityteType returnType(String code);
    public void addTemplate(Template template);
    public void addReturn(Returns returns);
    public void deleteType(String type);
    public void deleteTemplate(String temp);
    public void deleteReturns(Returns returns);
    public List<Finantial_Institute> loadFi();
    public List<Finantial_Type> loadType();
    public List<com.task3.returns.shared.Template> loadTemplate();
    public List<com.task3.returns.shared.Returns> loadReturn();

}
