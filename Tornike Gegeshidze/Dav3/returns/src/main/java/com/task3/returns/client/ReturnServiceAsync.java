package com.task3.returns.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.task3.returns.shared.Finantial_Institute;
import com.task3.returns.shared.Finantial_Type;

import java.util.List;

public interface ReturnServiceAsync {
    void doConnect(AsyncCallback<Void> async);
    void deleteFi(String typecode, AsyncCallback<Void> async);
    void addFi(Finantial_Institute finantial_institute, AsyncCallback<Void> async);
    void addFiType(instityteType instityteType, AsyncCallback<Void> async);
    void returnType(String code, AsyncCallback<instityteType> async);
    void addTemplate(Template template, AsyncCallback<Void> async);
    void addReturn(Returns returns, AsyncCallback<Void> async);
    void deleteType(String type, AsyncCallback<Void> async);
    void deleteTemplate(String temp, AsyncCallback<Void> async);
    void deleteReturns(Returns returns, AsyncCallback<Void> async);
    void loadFi(AsyncCallback<List<Finantial_Institute>> async);
    void loadType(AsyncCallback<List<Finantial_Type>> async);
    void loadTemplate(AsyncCallback<List<com.task3.returns.shared.Template>> async);
    void loadReturn(AsyncCallback<List<com.task3.returns.shared.Returns>> async);
}
