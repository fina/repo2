package com.task3.returns.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task3.returns.client.*;
import com.task3.returns.shared.Finantial_Institute;
import com.task3.returns.shared.Finantial_Type;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tornike on 08-Jan-15.
 */
@SuppressWarnings("serial")
public class ReturnServiceImpl extends RemoteServiceServlet implements ReturnService {



    public void doConnect()
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("SELECT x FROM Template x");
        em.getTransaction().commit();
        em.close();


    }

    public void addFi(Finantial_Institute finantial_institute)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        Finantial_institute fi = new Finantial_institute();
        fi.setFiMail(finantial_institute.getFiMail());
        fi.setFiName(finantial_institute.getFiName());
        fi.setFiPhone(finantial_institute.getFiPhone());
        fi.setFiCode(finantial_institute.getFiCode());
        fi.setFiAddress(finantial_institute.getFiAdrress());
        String type = finantial_institute.getFiType();
        instityteType in_type = returnType(type);
        fi.setTute(in_type);
        em.getTransaction().begin();

        if (typeExist(finantial_institute.getFiType()))
        {
            em.persist(fi);
            em.getTransaction().commit();
        }
        for (Finantial_institute f : em.createQuery("select x from Finantial_institute x", Finantial_institute.class).getResultList())
        {
            System.out.println(f.getTute().getTypeCode());
        }
        em.close();
    }


    public void deleteFi(String typecode)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("delete from Finantial_institute x where x.fiCode=:fc");
        q.setParameter("fc", typecode);
        q.executeUpdate();
        em.getTransaction().commit();
        em.close();

    }

    public void addFiType(instityteType instityteType)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(instityteType);
        em.getTransaction().commit();
        em.close();
    }

    public instityteType returnType(String code)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("select x from instityteType x where x.typeCode=:tc");
        q.setParameter("tc", code);
        instityteType type = (instityteType) q.getSingleResult();
        em.getTransaction().commit();
        em.close();
        return type;
    }

    public void addTemplate(Template template)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(template);
        em.getTransaction().commit();
        em.close();
    }

    public void addReturn(Returns returns)
    {
        if (fiCodeExist(returns.getReturnskey().getRetFIID()) == true && templateCodeExist(returns.getReturnskey().getRetTemplateID()) == true)
        {
            EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
            EntityManager em=emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(returns);
            em.getTransaction().commit();
            em.close();
        } else
            {
            throw new RuntimeException();
            }
    }

    public void deleteType(String type)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("delete  from instityteType x where  x.typeCode=:tc");
        q.setParameter("tc", type);
        q.executeUpdate();
        em.getTransaction().commit();
        em.close();

    }

    public void deleteTemplate(String temp
    ) {
        if (!templateInReturnsExist(temp))
        {
            EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
            EntityManager em=emf.createEntityManager();
            em.getTransaction().begin();
            Query q = em.createQuery("delete  from Template x where x.tCode=:tc");
            q.setParameter("tc", temp);
            q.executeUpdate();
            em.getTransaction().commit();
            em.close();
        } else
        {
            throw new RuntimeException();
        }


    }

    public void deleteReturns(Returns returns)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();

        Returns returns1 = em.find(Returns.class, returns.getReturnskey());
        em.remove(returns1);
        //  em.refresh(returns);
        em.getTransaction().commit();
        em.close();

    }

    public List<Finantial_Institute> loadFi()
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        List<Finantial_Institute> tempFi;
        em.getTransaction().begin();
        Query query = em.createQuery("select c from Finantial_institute c");
        List<Finantial_institute> fi = query.getResultList();
        em.getTransaction().commit();
        tempFi = getFiList(fi);
        em.close();
        return tempFi;
    }

    public List<Finantial_Type> loadType()
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        List<Finantial_Type> tempType;
        em.getTransaction().begin();
        Query q = em.createQuery("SELECT  c from instityteType c");
        List<instityteType> type = q.getResultList();
        em.getTransaction().commit();
        tempType = getTypes(type);
        em.close();

        return tempType;


    }

    public List<com.task3.returns.shared.Template> loadTemplate()
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        List<com.task3.returns.shared.Template> templates;
        em.getTransaction().begin();
        Query q = em.createQuery("select  c from Template c");
        List<Template> temps = q.getResultList();
        em.getTransaction().commit();
        templates = getTemplate(temps);
        em.close();


        return templates;
    }

    public List<com.task3.returns.shared.Returns> loadReturn()
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        List<com.task3.returns.shared.Returns> returnses;
        em.getTransaction().begin();
        Query q = em.createQuery("select c from Returns c");
        List<Returns> tempreturns = q.getResultList();
        em.getTransaction().commit();
        returnses = getReturs(tempreturns);
        em.close();
        return returnses;
    }

    private List<com.task3.returns.shared.Returns> getReturs(List<Returns> tempreturns)
    {
        ArrayList<com.task3.returns.shared.Returns> returnses = new ArrayList<com.task3.returns.shared.Returns>();
        com.task3.returns.shared.Returns returns = null;
        for (int i = 0; i < tempreturns.size(); i++)
        {
            returns = new com.task3.returns.shared.Returns();
            returns.setFinantialCode(tempreturns.get(i).getReturnskey().getRetFIID());
            returns.setTemplateCode(tempreturns.get(i).getReturnskey().getRetTemplateID());
            returnses.add(returns);
        }

        return returnses;
    }

    private List<com.task3.returns.shared.Template> getTemplate(List<Template> temps)
    {
        ArrayList<com.task3.returns.shared.Template> templates = new ArrayList<com.task3.returns.shared.Template>();
        com.task3.returns.shared.Template template = null;
        for (int i = 0; i < temps.size(); i++)
        {
            template = new com.task3.returns.shared.Template();
            template.setTemplateCode(temps.get(i).getTCode());
            template.setTemplateName(temps.get(i).getTName());
            templates.add(template);
        }
        return templates;

    }

    private List<Finantial_Type> getTypes(List<instityteType> type)
    {
        ArrayList<Finantial_Type> types = new ArrayList<Finantial_Type>();
        Finantial_Type tempType = null;
        for (int i = 0; i < type.size(); i++)
        {
            tempType = new Finantial_Type();
            tempType.setTypeCode(type.get(i).getTypeCode());
            tempType.setTypeName(type.get(i).getTypeName());
            types.add(tempType);
        }

        return types;

    }

    boolean typeExist(String type)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        boolean exists = 0L < em.createQuery("select count(x) from instityteType x where x.typeCode=:tc", Long.class).setParameter("tc", type).getSingleResult();
        em.getTransaction().commit();
        em.close();

        return exists;
    }

    boolean fiCodeExist(String ficode)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Boolean exists = 0l < em.createQuery("select count(x) from Finantial_institute x where x.fiCode=:cd", Long.class).setParameter("cd", ficode).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return exists;

    }

    boolean templateCodeExist(String Temcode)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Boolean exists = 0l < em.createQuery("select  count (x) from  Template  x where  x.tCode=:tc", Long.class).setParameter("tc", Temcode).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return exists;
    }

    boolean templateInReturnsExist(String retCode)
    {
        EntityManagerFactory emf =EmfProvider.getInstance().getEntityManagerFactory();
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        boolean exists = 0L < em.createQuery("select  count (x) from Returns  x where  x.returnskey.retTemplateID=:rtc", Long.class).setParameter("rtc", retCode).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return exists;
    }

    private List<Finantial_Institute> getFiList(List<Finantial_institute> fiList)
    {
        ArrayList<Finantial_Institute> fi = new ArrayList<Finantial_Institute>();
        Finantial_Institute tempFi = null;
        for (int i = 0; i < fiList.size(); i++)
        {
            tempFi = new Finantial_Institute();
            tempFi.setFiCode(fiList.get(i).getFiCode());
            tempFi.setFiName(fiList.get(i).getFiName());

            fi.add(tempFi);

        }
        return fi;
    }

}
