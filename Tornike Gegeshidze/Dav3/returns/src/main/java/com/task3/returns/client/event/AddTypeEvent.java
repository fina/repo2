package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class AddTypeEvent extends GwtEvent<AddTypeEventHandler> {
    public static Type<AddTypeEventHandler> TYPE = new Type<AddTypeEventHandler>();
    @Override
    public Type<AddTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddTypeEventHandler handler) {
        handler.onAddType(this);

    }
}
