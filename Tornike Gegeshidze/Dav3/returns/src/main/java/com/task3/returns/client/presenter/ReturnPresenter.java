package com.task3.returns.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.task3.returns.client.instityteType;
import com.task3.returns.client.ReturnServiceAsync;
import com.task3.returns.client.Returns;
import com.task3.returns.client.Template;
import com.task3.returns.client.event.*;
import com.task3.returns.shared.Finantial_Institute;
import com.task3.returns.shared.Finantial_Type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tornike on 13-Jan-15.
 */
public class ReturnPresenter implements Presenter {

    public interface Display {
        HasClickHandlers getFiAddButton();
        HasClickHandlers getFiDeleteButton();
        HasClickHandlers getTypeAddButton();
        HasClickHandlers getTypeDeleteButton();
        HasClickHandlers getTemplateAddButton();
        HasClickHandlers getTemplateDeleteButton();
        HasClickHandlers getReturnAddButton();
        HasClickHandlers getReturnDeleteButton();
        HasValue<String> getFiCode();
        HasValue<String> getTypeCode();
        Finantial_Institute getFi();
        instityteType getType();
        Template getTemplate();
        Returns getReturns();
        void AddFinantialType();
        List<String> deleteFinantialTypes();
        List<String> deleteFinantialInstitute();
        List<String> deleteTemplate();
        List<Returns> deleteReturn();
        void addFinantialInstitute();
        void addTemplateInstitute();
        void addReturns();
        void loadFii(List<Finantial_Institute> res);
        void loadTypes(List<Finantial_Type> types);
        void loadTemplate(List<com.task3.returns.shared.Template> templates);
        void loadReturns(List<com.task3.returns.shared.Returns> returnses);
        void clearFi();
        Boolean checkFiCode();
        Boolean checkTypeCode();
        Boolean checkTemplateCode();
        Widget asWidget();

    }

    private final ReturnServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display view;

    public ReturnPresenter(ReturnServiceAsync rpcService, HandlerManager eventBus, Display view) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.view = view;

    }

    public void go(HasWidgets container)
    {
        bind();
        container.clear();
        container.add(view.asWidget());
        loadData();

    }

    public void loadData() {
        rpcService.loadFi(new AsyncCallback<List<Finantial_Institute>>()
        {
            public void onFailure(Throwable caught)
            {
                caught.getMessage();
            }

            public void onSuccess(List<Finantial_Institute> result)
            {

                view.loadFii(result);
            }
        });
        rpcService.loadType(new AsyncCallback<List<Finantial_Type>>() {
            public void onFailure(Throwable caught) {

            }

            public void onSuccess(List<Finantial_Type> result) {
                view.loadTypes(result);
            }
        });
        rpcService.loadTemplate(new AsyncCallback<List<com.task3.returns.shared.Template>>() {
            public void onFailure(Throwable caught) {

            }

            public void onSuccess(List<com.task3.returns.shared.Template> result) {
                view.loadTemplate(result);
            }
        });
        rpcService.loadReturn(new AsyncCallback<List<com.task3.returns.shared.Returns>>() {
            public void onFailure(Throwable caught) {


            }

            public void onSuccess(List<com.task3.returns.shared.Returns> result) {
                view.loadReturns(result);
            }
        });
    }


    public void bind() {
        view.getFiAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addNewFi();
                eventBus.fireEvent(new AddFiEvent());
            }
        });
        view.getFiDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteThisFi();
                eventBus.fireEvent(new DeleteFiEvent());
            }
        });
        view.getTypeAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addNewType();
                eventBus.fireEvent(new AddTypeEvent());
            }
        });
        view.getTypeDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteType();
                eventBus.fireEvent(new DeleteTypeEvent());
            }
        });
        view.getTemplateAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addNewTemplate();
                eventBus.fireEvent(new AddTemplateEvent());
            }
        });
        view.getTemplateDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteTemplate();
                eventBus.fireEvent(new DeleteTemplateEvent());
            }
        });
        view.getReturnAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addNewReturn();
                eventBus.fireEvent(new AddReturnEvent());
            }
        });
        view.getReturnDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteReturn();
                eventBus.fireEvent(new DeleteReturnEvent());
            }
        });
    }

    private void deleteReturn() {
        List<Returns> returnses = view.deleteReturn();
        for (int i = 0; i < returnses.size(); i++)
        {
            rpcService.deleteReturns(returnses.get(i), new AsyncCallback<Void>()
            {
                public void onFailure(Throwable caught) {
                    caught.getMessage();
                }

                public void onSuccess(Void result)
                {
                    Window.alert("return deleted");

                }
            });
        }


    }

    private void addNewReturn() {
        Returns returns = view.getReturns();
        rpcService.addReturn(returns, new AsyncCallback<Void>()
        {
            public void onFailure(Throwable caught)
            {

                Window.alert("returns exists");
                caught.getMessage();
            }

            public void onSuccess(Void result)
            {
                view.addReturns();
                Window.alert("Return added");
            }
        });

    }

    private void deleteTemplate() {
        List<String> templates = view.deleteTemplate();
        for (int i = 0; i < templates.size(); i++)
        {
            rpcService.deleteTemplate(templates.get(i), new AsyncCallback<Void>()
            {
                public void onFailure(Throwable caught)
                {

                    Window.alert("template is used");
                    caught.getMessage();
                }

                public void onSuccess(Void result) {
                    Window.alert("template deleted");
                }
            });
        }

    }

    private void addNewTemplate()
    {
        Template template = view.getTemplate();
        if(view.checkTemplateCode())
        {
            rpcService.addTemplate(template, new AsyncCallback<Void>()
            {
                public void onFailure(Throwable caught)
                {
                    Window.alert("temp code exists");
                    caught.getMessage();
                }

                public void onSuccess(Void result)
                {
                    view.addTemplateInstitute();
                    Window.alert("template added");
                }
            });
        }
        else
        {
            Window.alert("Template code length must be less than 12 symbol");
        }
    }

    private void deleteType() {
        List<String> typecodes = new ArrayList<String>();
        typecodes = view.deleteFinantialTypes();
        for (int i = 0; i < typecodes.size(); i++) {
            rpcService.deleteType(typecodes.get(i), new AsyncCallback<Void>() {
                public void onFailure(Throwable caught) {
                    Window.alert("type code is used");
                    GWT.log(caught.getMessage(), caught);
                }

                public void onSuccess(Void result) {
                    Window.alert("type/types deleted");
                }
            });
        }


    }


    private void addNewType()
    {
        instityteType type = new instityteType();
        type = view.getType();
        if(view.checkTypeCode())
            {
                rpcService.addFiType(type, new AsyncCallback<Void>()
                {
                    public void onFailure(Throwable caught) {
                    Window.alert("type exists");
                    caught.getMessage();
                }

                public void onSuccess(Void result) {
                view.AddFinantialType();
            }
                });
            }
        else
        {
            Window.alert("Type code length must be less than 12 symbol");
        }
    }

    private void addNewFi() {
        Finantial_Institute fi = new Finantial_Institute();
        fi = view.getFi();

        if(view.checkFiCode())
        {
            rpcService.addFi(fi, new AsyncCallback<Void>() {
                public void onFailure(Throwable caught) {

                    Window.alert("check input");
                    view.clearFi();
                }

                public void onSuccess(Void result) {
                    view.addFinantialInstitute();

                    Window.alert("added new fi");
                }
            });
        }
        else
        {
            Window.alert("Fi code length must be less than 12 symbol");
        }
    }

    private void deleteThisFi() {
        List<String> ficode = new ArrayList<String>();
        ficode = view.deleteFinantialInstitute();
        for (int i = 0; i < ficode.size(); i++)
        {
            rpcService.deleteFi(ficode.get(i), new AsyncCallback<Void>()
            {
                public void onFailure(Throwable caught) {
                    caught.getMessage();
                }

                public void onSuccess(Void result) {
                    Window.alert("fi deleted");
                }
            });

        }
    }
}
