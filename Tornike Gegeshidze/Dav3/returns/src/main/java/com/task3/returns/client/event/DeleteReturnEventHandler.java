package com.task3.returns.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by tornike on 08-Mar-15.
 */
public interface DeleteReturnEventHandler extends EventHandler {
    public  void onDeleteReturn(DeleteReturnEvent event);
}
