package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class AddReturnEvent extends GwtEvent<AddReturnEventHandler> {
    public static Type<AddReturnEventHandler> TYPE = new Type<AddReturnEventHandler>();
    @Override
    public Type<AddReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddReturnEventHandler handler) {
        handler.onAddReturn(this);

    }
}
