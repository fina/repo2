package com.task3.returns.server;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by tornike on 08-Mar-15.
 */
public class EmfProvider {
    private static final String DB_PU = "persistenceUnit";
    public static final boolean debug=true;
    private EmfProvider(){}
    private EntityManagerFactory emf;
    public   static final   EmfProvider emfProvider=new EmfProvider();
    public static EmfProvider getInstance(){ return  emfProvider;}
    public EntityManagerFactory getEntityManagerFactory()
    {
        if(emf==null)
        {
            this.emf= Persistence.createEntityManagerFactory(DB_PU);
        }
        return emf;
    }
}
