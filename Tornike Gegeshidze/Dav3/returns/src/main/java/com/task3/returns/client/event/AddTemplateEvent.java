package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class AddTemplateEvent extends GwtEvent<AddTemplateEventHandler> {
    public static Type<AddTemplateEventHandler> TYPE = new Type<AddTemplateEventHandler>();
    @Override
    public Type<AddTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddTemplateEventHandler handler) {
        handler.onAddTemplate(this);
    }
}
