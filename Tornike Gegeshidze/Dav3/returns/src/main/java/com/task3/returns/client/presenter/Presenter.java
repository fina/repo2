package com.task3.returns.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Created by tornike on 13-Jan-15.
 */
public abstract interface Presenter {
    public void go(final HasWidgets container);
}
