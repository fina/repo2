package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 13-Jan-15.
 */
public class DeleteFiEvent extends GwtEvent<DeleteFiEventHandler> {
    public static Type<DeleteFiEventHandler> TYPE = new Type<DeleteFiEventHandler>();

    @Override
    public Type<DeleteFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteFiEventHandler handler) {

        handler.onDeleteFi(this);

    }


}
