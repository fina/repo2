package com.task3.returns.shared;

import java.io.Serializable;

/**
 * Created by tornike on 13-Jan-15.
 */
public class Template implements Serializable {
    private long templateId;
    private String templateName;
    private String templateCode;
    private String templateSchedule;

    public long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateSchedule() {
        return templateSchedule;
    }

    public void setTemplateSchedule(String templateSchedule) {
        this.templateSchedule = templateSchedule;
    }
}
