package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class DeleteReturnEvent extends GwtEvent<DeleteReturnEventHandler> {
    public static Type<DeleteReturnEventHandler> TYPE=new Type<DeleteReturnEventHandler>();

    @Override
    public Type<DeleteReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteReturnEventHandler handler) {
        handler.onDeleteReturn(this);

    }
}
