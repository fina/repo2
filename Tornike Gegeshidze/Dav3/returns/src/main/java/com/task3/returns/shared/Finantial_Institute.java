package com.task3.returns.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Created by tornike on 13-Jan-15.
 */
public class Finantial_Institute implements IsSerializable {

    private long fiId;
    private String fiCode;
    private String fiAdrress;
    private String fiName;
    private String fiMail;
    private String fiPhone;
    private String fiType;

    public long getFiId() {
        return fiId;
    }

    public void setFiId(long fiId) {
        this.fiId = fiId;
    }

    public String getFiCode() {
        return fiCode;
    }

    public void setFiCode(String fiCode) {
        this.fiCode = fiCode;
    }

    public String getFiAdrress() {
        return fiAdrress;
    }

    public void setFiAdrress(String fiAdrress) {
        this.fiAdrress = fiAdrress;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }

    public String getFiMail() {
        return fiMail;
    }

    public void setFiMail(String fiMail) {
        this.fiMail = fiMail;
    }

    public String getFiPhone() {
        return fiPhone;
    }

    public void setFiPhone(String fiPhone) {
        this.fiPhone = fiPhone;
    }

    public String getFiType() {
        return fiType;
    }

    public void setFiType(String fiType) {
        this.fiType = fiType;
    }
}
