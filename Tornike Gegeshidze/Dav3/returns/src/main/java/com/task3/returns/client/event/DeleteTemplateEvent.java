package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 08-Mar-15.
 */
public class DeleteTemplateEvent extends GwtEvent<DeleteTemplateEventHandler> {
    public static Type<DeleteTemplateEventHandler> TYPE = new Type<DeleteTemplateEventHandler>();

    @Override
    public Type<DeleteTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteTemplateEventHandler handler) {
        handler.onDeleteTemplate(this);
    }
}
