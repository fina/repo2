package com.task3.returns.shared;

import java.io.Serializable;

/**
 * Created by tornike on 30-Jan-15.
 */
public class Returns implements Serializable {
    private String finantialCode;
    private String templateCode;

    public Returns(String fc, String tc) {
        this.setFinantialCode(fc);
        this.setTemplateCode(tc);
    }

    public Returns() {
    }

    public String getFinantialCode() {
        return finantialCode;
    }

    public void setFinantialCode(String finantialCode) {
        this.finantialCode = finantialCode;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }
}
