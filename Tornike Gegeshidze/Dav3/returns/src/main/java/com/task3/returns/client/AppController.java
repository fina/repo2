package com.task3.returns.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.task3.returns.client.event.*;
import com.task3.returns.client.presenter.Presenter;
import com.task3.returns.client.presenter.ReturnPresenter;
import com.task3.returns.client.view.ReturnsPanel;

/**
 * Created by tornike on 13-Jan-15.
 */
public class AppController implements Presenter, ValueChangeHandler<String> {
    private final HandlerManager eventBus;
    private final ReturnServiceAsync rpcService;
    private HasWidgets container;

    public AppController(ReturnServiceAsync rpcService, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.rpcService = rpcService;
        bind();
    }

    public void bind()
    {
        History.addValueChangeHandler(this);
        eventBus.addHandler(AddFiEvent.TYPE, new AddFiEventHandler() {
            public void onAddFi(AddFiEvent event) {
                doAdd();
            }
        });
        eventBus.addHandler(DeleteFiEvent.TYPE, new DeleteFiEventHandler() {
            public void onDeleteFi(DeleteFiEvent event) {
                doDelete();
            }
        });
        eventBus.addHandler(AddTypeEvent.TYPE, new AddTypeEventHandler() {
            public void onAddType(AddTypeEvent event) {
                doDelete();
            }
        });
        eventBus.addHandler(DeleteTypeEvent.TYPE, new DeleteTypeEventHandler() {
            public void onDeleteType(DeleteTypeEvent event) {
                doDelete();
            }
        });
        eventBus.addHandler(AddTemplateEvent.TYPE, new AddTemplateEventHandler() {
            public void onAddTemplate(AddTemplateEvent event) {
                doAdd();
            }
        });
        eventBus.addHandler(DeleteTemplateEvent.TYPE, new DeleteTemplateEventHandler() {
            public void onDeleteTemplate(DeleteTemplateEvent event) {
                doDelete();
            }
        });
        eventBus.addHandler(AddReturnEvent.TYPE, new AddReturnEventHandler() {
            public void onAddReturn(AddReturnEvent event) {
                doAdd();
            }
        });
        eventBus.addHandler(DeleteReturnEvent.TYPE, new DeleteReturnEventHandler() {
            public void onDeleteReturn(DeleteReturnEvent event) {
                doDelete();
            }
        });

    }



    private void doDelete() {
        History.newItem("Delete");
    }

    private void doAdd() {
        History.newItem("Add");
    }

    public void go(HasWidgets container) {
        this.container = container;
        if ("".equals(History.getToken())) {
            History.newItem("list");
        } else {
            History.fireCurrentHistoryState();
        }

    }

    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        if (token != null) {
            Presenter presenter = null;

            if (token.equals("list")) {
                presenter = new ReturnPresenter(rpcService, eventBus, new ReturnsPanel());
            } else if (token.equals("add")) {
                presenter = new ReturnPresenter(rpcService, eventBus, new ReturnsPanel());
            } else if (token.equals("Delete")) {
                presenter = new ReturnPresenter(rpcService, eventBus, new ReturnsPanel());
            }

            if (presenter != null) {
                presenter.go(container);
            }
        }
    }

}

