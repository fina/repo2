package com.task3.returns.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by tornike on 13-Jan-15.
 */
public class AddFiEvent extends GwtEvent<AddFiEventHandler> {
    public static Type<AddFiEventHandler> TYPE = new Type<AddFiEventHandler>();

    @Override
    public Type<AddFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddFiEventHandler handler) {
        handler.onAddFi(this);

    }
}
