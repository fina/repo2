package com.task3.returns.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by tornike on 13-Jan-15.
 */
public interface DeleteFiEventHandler extends EventHandler {
    void onDeleteFi(DeleteFiEvent event);
}
