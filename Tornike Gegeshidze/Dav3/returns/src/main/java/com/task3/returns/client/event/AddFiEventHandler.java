package com.task3.returns.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by tornike on 13-Jan-15.
 */
public interface AddFiEventHandler extends EventHandler {
    void onAddFi(AddFiEvent event);
}
