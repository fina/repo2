package com.task3.returns.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.task3.returns.client.instityteType;
import com.task3.returns.client.Returns;
import com.task3.returns.client.Returnskey;
import com.task3.returns.client.Template;
import com.task3.returns.client.presenter.ReturnPresenter;
import com.task3.returns.shared.Finantial_Institute;
import com.task3.returns.shared.Finantial_Type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tornike on 09-Jan-15.
 */
public class ReturnsPanel extends Composite implements ReturnPresenter.Display {

    public HasClickHandlers getFiAddButton() {
        return addFiButton;
    }

    public HasClickHandlers getFiDeleteButton() {
        return deleteFiButton;
    }

    public HasClickHandlers getTypeAddButton() {
        return addType;
    }

    public HasClickHandlers getTypeDeleteButton() {
        return deleteType;
    }

    public HasClickHandlers getTemplateAddButton() {
        return addTemplateButton;
    }

    public HasClickHandlers getTemplateDeleteButton() {
        return deleteTemplateButton;
    }

    public HasClickHandlers getReturnAddButton() {
        return addReturnButton;
    }

    public HasClickHandlers getReturnDeleteButton() {
        return deleteReturnButton;
    }

    public HasValue<String> getFiCode() {
        return fiCode;
    }

    public HasValue<String> getTypeCode() {
        return typeCode;
    }

    public Finantial_Institute getFi() {
        Finantial_Institute fi = new Finantial_Institute();
        fi.setFiCode(fiCode.getValue());
        fi.setFiAdrress(fiAddress.getValue());
        fi.setFiMail(fiMail.getValue());
        fi.setFiName(fiName.getValue());
        fi.setFiPhone(fiPhone.getValue());
        fi.setFiType(fiTypeCode.getValue());
        return fi;
    }

    public instityteType getType() {
        instityteType type = new instityteType();
        type.setTypeCode(typeCode.getValue());
        type.setTypeName(typeName.getValue());
        return type;
    }

    public Template getTemplate() {
        Template template = new Template();
        template.setTCode(templateCode.getValue());
        template.setTName(templateName.getValue());
        template.setTSchedule(templateTransaction.getItemText(templateTransaction.getSelectedIndex()));
        return template;
    }

    public Returns getReturns() {
        Returns returns = new Returns();
        Returnskey returnskey = new Returnskey();
        returnskey.setRetTemplateID(returnTempCode.getValue());
        returnskey.setRetFIID(returnFiCode.getValue());
        returns.setReturnskey(returnskey);
        return returns;

    }

    public void AddFinantialType() {
        int index = fiTypeFlextable.getRowCount();
        fiTypeFlextable.setWidget(index, 0, new CheckBox());
        fiTypeFlextable.setText(index, 1, typeCode.getValue());
        fiTypeFlextable.setText(index, 2, typeName.getValue());
        typeCode.setText("");
        typeName.setText("");

    }

    public List<String> deleteFinantialTypes() {
        List<String> selectedrow = new ArrayList<String>();
        for (int i = fiTypeFlextable.getRowCount() - 1; i > 0; i--) {
            CheckBox check = (CheckBox) fiTypeFlextable.getWidget(i, 0);
            if (check.getValue()) {
                selectedrow.add(fiTypeFlextable.getHTML(i, 1).toString());
                fiTypeFlextable.removeRow(i);


            }

        }
        return selectedrow;


    }

    public List<String> deleteFinantialInstitute() {
        List<String> fi = new ArrayList<String>();
        for (int i = fiFlextable.getRowCount() - 1; i > 0; i--) {
            CheckBox check = (CheckBox) fiFlextable.getWidget(i, 0);
            if (check.getValue()) {
                fi.add(fiFlextable.getHTML(i, 1).toString());
                fiFlextable.removeRow(i);
            }
        }

        return fi;
    }

    public List<String> deleteTemplate() {
        List<String> templates = new ArrayList<String>();
        for (int i = templateTable.getRowCount() - 1; i > 0; i--) {
            CheckBox checkBox = (CheckBox) templateTable.getWidget(i, 0);
            if (checkBox.getValue()) {
                templates.add(templateTable.getHTML(i, 1).toString());
                templateTable.removeRow(i);
            }


        }
        return templates;
    }

    public List<Returns> deleteReturn() {
        List<Returns> returns = new ArrayList<Returns>();
        int index = returnTable.getRowCount() - 1;


        for (int i = index; i > 0; i--) {
            CheckBox checkBox = (CheckBox) returnTable.getWidget(i, 0);
            Returnskey returnskey = null;
            Returns tempReturns = null;
            if (checkBox.getValue()) {
                returnskey = new Returnskey();
                tempReturns = new Returns();
                returnskey.setRetFIID(returnTable.getHTML(i, 2));
                returnskey.setRetTemplateID(returnTable.getHTML(i, 1));
                tempReturns.setReturnskey(returnskey);
                returns.add(tempReturns);
                returnTable.removeRow(i);
                index = returnTable.getRowCount() - 1;

            }
        }
        return returns;
    }

    public void addFinantialInstitute() {
        int fiIndex = fiFlextable.getRowCount();
        fiFlextable.setWidget(fiIndex, 0, new CheckBox());
        fiFlextable.setText(fiIndex, 1, fiCode.getValue());
        fiFlextable.setText(fiIndex, 2, fiName.getValue());

    }

    public void addTemplateInstitute() {
        int temindex = templateTable.getRowCount();
        templateTable.setWidget(temindex, 0, new CheckBox());
        templateTable.setText(temindex, 1, templateCode.getValue());
        templateTable.setText(temindex, 2, templateName.getValue());

    }

    public void addReturns() {
        int index = returnTable.getRowCount();
        returnTable.setWidget(index, 0, new CheckBox());
        returnTable.setText(index, 2, returnFiCode.getValue());
        returnTable.setText(index, 1, returnTempCode.getValue());
    }

    public void loadFii(List<Finantial_Institute> results) {
        for (int i = 0; i < results.size(); i++) {
            int index = fiFlextable.getRowCount();
            fiFlextable.setText(index, 1, results.get(i).getFiCode());
            fiFlextable.setText(index, 2, results.get(i).getFiName());
            fiFlextable.setWidget(index, 0, new CheckBox());

        }
    }

    public void loadTypes(List<Finantial_Type> types) {
        for (int i = 0; i < types.size(); i++) {
            int index = fiTypeFlextable.getRowCount();
            fiTypeFlextable.setWidget(index, 0, new CheckBox());
            fiTypeFlextable.setText(index, 1, types.get(i).getTypeCode());
            fiTypeFlextable.setText(index, 2, types.get(i).getTypeName());
        }
    }

    public void loadTemplate(List<com.task3.returns.shared.Template> templates) {
        for (int i = 0; i < templates.size(); i++) {
            int index = templateTable.getRowCount();
            templateTable.setWidget(index, 0, new CheckBox());
            templateTable.setText(index, 1, templates.get(i).getTemplateCode());
            templateTable.setText(index, 2, templates.get(i).getTemplateName());
        }
    }

    public void loadReturns(List<com.task3.returns.shared.Returns> returnses) {
        for (int i = 0; i < returnses.size(); i++) {
            int index = returnTable.getRowCount();
            returnTable.setWidget(index, 0, new CheckBox());
            returnTable.setText(index, 1, returnses.get(i).getTemplateCode());
            returnTable.setText(index, 2, returnses.get(i).getFinantialCode());
        }
    }

    public void clearFi() {
        fiCode.setText("");
        fiTypeCode.setText("");
    }

    public Boolean checkFiCode() {
        if(fiCode.getValue().length()<=12)
        {
            return true;
        }
        return false;
    }

    public Boolean checkTypeCode() {
        if(typeCode.getValue().length()<=12)
        {
            return  true;
        }
        return false;
    }

    public Boolean checkTemplateCode() {
        if(templateCode.getValue().length()<=12)
        {
         return true;
        }
        return false;
    }


    interface ReturnsPanelUiBinder extends UiBinder<Widget, ReturnsPanel> {
    }

    private static ReturnsPanelUiBinder ourUiBinder = GWT.create(ReturnsPanelUiBinder.class);

    //<editor-fold desc="uiField">

    @UiField
    TabLayoutPanel tabPanel;
    @UiField
    FlexTable fiFlextable;
    @UiField
    TextBox fiCode;
    @UiField
    TextBox fiTypeCode;
    @UiField
    TextBox fiName;
    @UiField
    TextBox fiAddress;
    @UiField
    TextBox fiPhone;
    @UiField
    TextBox fiMail;
    @UiField
    TextBox fiType;
    @UiField
    TextBox typeCode;
    @UiField
    TextBox typeName;
    @UiField
    TextBox templateCode;
    @UiField
    TextBox templateName;
    @UiField
    ListBox templateTransaction;
    @UiField
    Button addFiButton;
    @UiField
    Button deleteFiButton;
    @UiField
    Button addType;
    @UiField
    Button deleteType;
    @UiField
    TextBox returnTempCode;
    @UiField
    TextBox returnFiCode;
    @UiField
    Button addTemplateButton;
    @UiField
    Button deleteTemplateButton;
    @UiField
    Button addReturnButton;
    @UiField
    Button deleteReturnButton;
    @UiField
    FlexTable fiTypeFlextable;
    @UiField
    FlexTable templateTable;
    @UiField
    FlexTable returnTable;

    //</editor-fold>

    public ReturnsPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        tabPanel.selectTab(0);
        fiFlextable.setText(0, 0, "  ");
        fiFlextable.setText(0, 1, "Fi code");
        fiFlextable.setText(0, 2, "Fi Name");
        fiTypeFlextable.setText(0, 1, "Type Code");
        fiTypeFlextable.setText(0, 2, "Type Name");
        fiFlextable.setText(0, 3, "Date");
        templateTable.setText(0, 0, " ");
        templateTable.setText(0, 1, "Temp code");
        templateTable.setText(0, 2, "temp name");
        returnTable.setText(0, 0, "  ");
        returnTable.setText(0, 1, "template code");
        returnTable.setText(0, 2, "fi code");


    }
}