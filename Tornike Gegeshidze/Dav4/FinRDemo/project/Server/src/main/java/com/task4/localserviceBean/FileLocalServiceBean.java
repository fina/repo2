//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localserviceBean;

import com.task4.entity.FileStore;
import com.task4.entity.FinInstitute;
import com.task4.entity.ReturnStore;
import com.task4.entity.Template;
import com.task4.localservice.FiLocalService;
import com.task4.localservice.FileLocalService;
import com.task4.localservice.TemplateLocalService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;

@Stateless
@Local({FileLocalService.class})
public class FileLocalServiceBean implements FileLocalService {
    private static Logger logger = Logger.getLogger(FileLocalServiceBean.class);
    @PersistenceContext(
            name = "task4"
    )
    private EntityManager em;
    @EJB
    FiLocalService fiLocalService;
    @EJB
    TemplateLocalService templateLocalService;

    public FileLocalServiceBean() {
    }

    public boolean checkFiCode(String code) {
        ArrayList institutions = new ArrayList(this.em.createQuery("SELECT e FROM FinInstitute e WHERE e.code = :code", FinInstitute.class).setParameter("code", code).getResultList());
        return institutions.size() == 1;
    }

    public boolean checkTemplateCode(String code) {
        ArrayList templates = new ArrayList(this.em.createQuery("SELECT e FROM Template e WHERE e.code = :code", Template.class).setParameter("code", code).getResultList());
        return templates.size() == 1;
    }

    public boolean checkForReturn(String instCode, String templCode) {
        FinInstitute finInstitute = this.fiLocalService.getFiByCode(instCode);
        Template template = this.templateLocalService.getTemplateByCode(templCode);
        ArrayList returns = new ArrayList(this.em.createQuery("SELECT e FROM ReturnStore e WHERE e.institution = :institution AND e.template = :template", ReturnStore.class).setParameter("institution", finInstitute).setParameter("template", template).getResultList());
        return returns.size() > 0;
    }

    public void addFile(FileStore fileStore) {
        this.em.merge(fileStore);
    }

    public List<FileStore> getFiles() {
        ArrayList files = new ArrayList(this.em.createQuery("SELECT e FROM FileStore e", FileStore.class).getResultList());
        return files;
    }

    public boolean isFileUnique(String fileName) {
        ArrayList files = new ArrayList(this.em.createQuery("SELECT e FROM FileStore e WHERE e.fileName = :fileName", FileStore.class).setParameter("fileName", fileName).getResultList());
        return files.size() <= 0;
    }

    public void deleteFile(int id) {
        this.em.remove(this.em.find(FileStore.class, Integer.valueOf(id)));
    }

    public FileStore getFile(int id) {
        return (FileStore)this.em.find(FileStore.class, Integer.valueOf(id));
    }

}
