//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localservice;

import com.task4.entity.ReturnStore;
import java.util.List;

public interface ReturnLocalService {
    void addReturn(ReturnStore var1);
    List<ReturnStore> loadReturn();
    void deleteReturn(int var1);
    boolean  returnIsUsed(ReturnStore returnStore);
}
