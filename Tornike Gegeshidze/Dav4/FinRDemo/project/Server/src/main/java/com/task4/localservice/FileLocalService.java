//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localservice;

import com.task4.entity.FileStore;
import java.util.List;

public interface FileLocalService {
    boolean checkFiCode(String var1);

    boolean checkTemplateCode(String var1);

    boolean checkForReturn(String var1, String var2);

    void addFile(FileStore var1);

    List<FileStore> getFiles();

    boolean isFileUnique(String var1);

    void deleteFile(int var1);

    FileStore getFile(int var1);
}
