//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.entity;

import com.task4.entity.FinInstitute;
import com.task4.entity.Template;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ReturnStore implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    @JoinColumn(
            name = "FI_ID"
    )
    private FinInstitute institution;
    @ManyToOne
    @JoinColumn(
            name = "TEMPLATE_ID"
    )
    private Template template;

    public ReturnStore() {
    }

    public ReturnStore(int id, FinInstitute finInstitute, Template template) {
        this.setId(id);
        this.setInstitution(finInstitute);
        this.setTemplate(template);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FinInstitute getInstitution() {
        return this.institution;
    }

    public void setInstitution(FinInstitute institution) {
        this.institution = institution;
    }

    public Template getTemplate() {
        return this.template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }
}
