//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localserviceBean;

import com.task4.entity.FileStore;
import com.task4.entity.ReturnStore;
import com.task4.localservice.ReturnLocalService;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Stateless
@Local({ReturnLocalService.class})
public class ReturnLocalServiceBean implements ReturnLocalService {
    private static Logger logger = Logger.getLogger(ReturnLocalServiceBean.class);
    @PersistenceContext(
            name = "task4"
    )
    private EntityManager em;

    public ReturnLocalServiceBean() {
    }

    public void addReturn(ReturnStore aReturnStore) {
        this.em.merge(aReturnStore);
    }

    public List<ReturnStore> loadReturn() {
        List returnStores = null;

        try {
            returnStores = this.em.createQuery("select f from  ReturnStore  f").getResultList();
            return returnStores;
        } catch (Exception var3) {
            logger.log(Level.ERROR, var3.getMessage());
            throw new RuntimeException("error loading templates");
        }
    }

    public void deleteReturn(int id) {
        this.em.remove(this.em.find(ReturnStore.class, Integer.valueOf(id)));
    }
    public boolean returnIsUsed(ReturnStore ret) {
        ArrayList<FileStore> files = new ArrayList<FileStore>(em.createQuery("SELECT e FROM FileStore e WHERE e.finInstitute = :institution AND e.template = :template", FileStore.class).setParameter("template", ret.getTemplate()).setParameter("institution", ret.getInstitution()).getResultList());
        return files.size() > 0;

    }
}
