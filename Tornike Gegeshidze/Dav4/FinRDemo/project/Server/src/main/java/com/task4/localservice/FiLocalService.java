//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localservice;

import com.task4.entity.FinInstitute;
import java.sql.SQLException;
import java.util.List;

public interface FiLocalService {
    void saveFi(FinInstitute var1) throws SQLException;

    boolean fiExists(String var1);

    List<FinInstitute> loadFies();

    void deleteFi(int var1) throws Exception;

    FinInstitute getFiByCode(String var1);

    FinInstitute getInstitutionByCode(String var1);
    public boolean institutionIsUsed(FinInstitute finInstitute);
}
