//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localservice;

import com.task4.entity.Template;
import java.sql.SQLException;
import java.util.List;

public interface TemplateLocalService {
    void addTemplate(Template var1) throws SQLException;
    List<Template> loadTemplate();
    void deleteTemplate(int var1);
    Template getTemplateByCode(String var1);
    boolean templateIsUsed(Template template);
}
