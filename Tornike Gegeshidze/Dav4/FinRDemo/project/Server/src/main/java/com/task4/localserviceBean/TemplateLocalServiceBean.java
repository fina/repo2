//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localserviceBean;

import com.task4.entity.FileStore;
import com.task4.entity.ReturnStore;
import com.task4.entity.Template;
import com.task4.localservice.TemplateLocalService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Stateless
@Local({TemplateLocalService.class})
public class TemplateLocalServiceBean implements TemplateLocalService {
    private static Logger logger = Logger.getLogger(TemplateLocalServiceBean.class);
    @PersistenceContext(
            name = "task4"
    )
    private EntityManager em;

    public TemplateLocalServiceBean() {
    }

    public void addTemplate(Template template) throws SQLException {
        if(template.getId() != 0) {
            this.em.merge(template);
        } else {
            if(this.templateExists(template.getCode())) {
                logger.log(Level.ERROR, "template already code exists");
                throw new SQLException("template code  used");
            }

            this.em.merge(template);
        }

    }

    public List<Template> loadTemplate() {
        List templatesList = null;

        try {
            templatesList = this.em.createQuery("select f from Template  f ").getResultList();
            return templatesList;
        } catch (Exception var3) {
            logger.log(Level.ERROR, var3.getMessage());
            throw new RuntimeException("error loading templates");
        }
    }

    public void deleteTemplate(int id) {
        this.em.remove(this.em.find(Template.class, Integer.valueOf(id)));
    }

    public boolean templateExists(String code) {
        List resultList = this.em.createQuery("select f from Template f where f.code=:code").setParameter("code", code).getResultList();
        return resultList.size() > 0;
    }

    public Template getTemplateByCode(String code) {
        Template template = (Template)this.em.createQuery("SELECT e FROM Template e WHERE e.code = :code", Template.class).setParameter("code", code).getSingleResult();
        return template;
    }
    public boolean templateIsUsed(Template template) {
        ArrayList<ReturnStore> returns = new ArrayList<ReturnStore>(em.createQuery("SELECT e FROM ReturnStore e where e.template = :template", ReturnStore.class).setParameter("template", template).getResultList());
        ArrayList<FileStore> files = new ArrayList<FileStore>(em.createQuery("SELECT e FROM FileStore e WHERE e.template = :template", FileStore.class).setParameter("template", template).getResultList());
        return (returns.size() > 0 || files.size() > 0);
    }

}
