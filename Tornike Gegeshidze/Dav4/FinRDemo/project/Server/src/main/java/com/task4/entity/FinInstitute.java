//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.entity;

import com.task4.entity.FinType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class FinInstitute implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String code;
    private String name;
    private String address;
    private String phone;
    private Date regDate;
    @ManyToOne
    @JoinColumn(
            name = "FinTypeId"
    )
    private FinType finType;

    public FinInstitute() {
    }

    public FinInstitute(int id, String code, String name, String address, String phone, Date regDate) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.address = address;
        this.phone = phone;
        this.regDate = regDate;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegDate() {
        return this.regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public FinType getFinType() {
        return this.finType;
    }

    public void setFinType(FinType finType) {
        this.finType = finType;
    }
}
