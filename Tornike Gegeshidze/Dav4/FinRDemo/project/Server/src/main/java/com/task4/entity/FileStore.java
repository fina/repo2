//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.entity;

import com.task4.entity.FinInstitute;
import com.task4.entity.Template;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class FileStore implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String fileName;
    @ManyToOne
    @JoinColumn(
            name = "FIN_ID"
    )
    private FinInstitute finInstitute;
    @ManyToOne
    @JoinColumn(
            name = "Temp_ID"
    )
    private Template template;
    Date uploadDate;
    String scheduleTime;
    @Lob
    private byte[] fileContent;

    public FileStore(String fileName, FinInstitute finInstitute, Template template, Date uploadDate, String scheduleTime, byte[] fileContent) {
        this.fileName = fileName;
        this.finInstitute = finInstitute;
        this.template = template;
        this.uploadDate = uploadDate;
        this.scheduleTime = scheduleTime;
        this.fileContent = fileContent;
    }

    public FileStore() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FinInstitute getFinInstitute() {
        return this.finInstitute;
    }

    public void setFinInstitute(FinInstitute finInstitute) {
        this.finInstitute = finInstitute;
    }

    public Template getTemplate() {
        return this.template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Date getUploadDate() {
        return this.uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getScheduleTime() {
        return this.scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public byte[] getFileContent() {
        return this.fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }
}
