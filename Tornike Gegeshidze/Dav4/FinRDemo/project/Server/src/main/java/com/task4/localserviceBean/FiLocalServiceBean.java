//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localserviceBean;

import com.task4.entity.FileStore;
import com.task4.entity.FinInstitute;
import com.task4.entity.ReturnStore;
import com.task4.localservice.FiLocalService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Stateless
@Local({FiLocalService.class})
public class FiLocalServiceBean implements FiLocalService {
    private static Logger logger = Logger.getLogger(FiLocalServiceBean.class);
    @PersistenceContext(
            name = "task4"
    )
    private EntityManager em;

    public FiLocalServiceBean() {
    }

    public void saveFi(FinInstitute finInstitute) throws SQLException {
        this.em.merge(finInstitute);
        logger.log(Level.INFO, "added " + finInstitute.getName());
    }

    public boolean fiExists(String fiCode) {
        List resultList = this.em.createQuery("select f from FinInstitute f where f.code=:code").setParameter("code", fiCode).getResultList();
        return resultList.size() > 0;
    }

    public List<FinInstitute> loadFies() {
        List fies = null;

        try {
            fies = this.em.createQuery("select f from FinInstitute  f ").getResultList();
        } catch (Exception var3) {
            logger.log(Level.ERROR, "daloadeba warumateblad damtavrda", var3);
        }

        return fies;
    }

    public void deleteFi(int id) throws Exception {
            this.em.remove(this.em.find(FinInstitute.class, Integer.valueOf(id)));
    }

    public FinInstitute getFiByCode(String code) {
        FinInstitute institution = (FinInstitute)this.em.createQuery("SELECT e FROM FinInstitute e WHERE e.code = :code", FinInstitute.class).setParameter("code", code).getSingleResult();
        return institution;
    }

    public FinInstitute getInstitutionByCode(String code) {
        FinInstitute institution = (FinInstitute)this.em.createQuery("SELECT e FROM FinInstitute e WHERE e.code = :code", FinInstitute.class).setParameter("code", code).getSingleResult();
        return institution;
    }
    public boolean institutionIsUsed(FinInstitute inst) {
        ArrayList<ReturnStore> returns = new ArrayList<ReturnStore>(em.createQuery("SELECT e FROM ReturnStore e where e.institution = :institution", ReturnStore.class).setParameter("institution", inst).getResultList());
        ArrayList<FileStore> files = new ArrayList<FileStore>(em.createQuery("SELECT e FROM FileStore e WHERE e.finInstitute = :institution", FileStore.class).setParameter("institution", inst).getResultList());
        return (returns.size() > 0 || files.size() > 0);

    }
}
