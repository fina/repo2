//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localserviceBean;

import com.task4.entity.FinInstitute;
import com.task4.entity.FinType;
import com.task4.localservice.FiTypeLocalService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Stateless
@Local({FiTypeLocalService.class})
public class FiTypeLocalServiceBean implements FiTypeLocalService {
    private static Logger logger = Logger.getLogger(FiTypeLocalServiceBean.class);
    @PersistenceContext(
            name = "task4"
    )
    private EntityManager em;

    public FiTypeLocalServiceBean() {
    }

    public void addFiType(FinType finType) throws SQLException {
        if(this.fiTypeExists(finType.getCode())) {
            throw new SQLException("code in not unique!");
        } else {
            this.em.merge(finType);
            logger.log(Level.INFO, "added " + finType.getName());
        }
    }

    public boolean fiTypeExists(String typeCode) {
        List resultList = this.em.createQuery("select f from FinType f where f.code=:code").setParameter("code", typeCode).getResultList();
        return resultList.size() > 0;
    }

    public List<FinType> loadFiTpe() {
        List finTypes = null;

        try {
            finTypes = this.em.createQuery("select f from FinType  f ").getResultList();
        } catch (Exception var3) {
            logger.log(Level.ERROR, "ragacams gaasxa", var3);
        }

        return finTypes;
    }

    public void deleteFiType(int id) throws Exception {

        em.remove(this.em.find(FinType.class, id));
    }


    public boolean typeIsUsed(FinType type) {

        ArrayList<FinInstitute> institutions = new ArrayList<FinInstitute>(em.createQuery("SELECT e FROM FinInstitute e WHERE e.finType = :type", FinInstitute.class).setParameter("type", type).getResultList());
        return institutions.size() > 0;
    }

}
