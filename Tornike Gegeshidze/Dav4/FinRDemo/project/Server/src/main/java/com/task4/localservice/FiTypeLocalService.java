//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.localservice;

import com.task4.entity.FinType;
import java.sql.SQLException;
import java.util.List;

public interface FiTypeLocalService {
    void addFiType(FinType var1) throws SQLException;
    boolean fiTypeExists(String var1);
    List<FinType> loadFiTpe();
    void deleteFiType(int var1) throws Exception;
    boolean typeIsUsed(FinType finType);
}
