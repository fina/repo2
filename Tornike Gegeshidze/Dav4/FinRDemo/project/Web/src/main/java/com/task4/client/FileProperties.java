//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.task4.shared.FileDTO;
import java.util.Date;

public interface FileProperties extends PropertyAccess<FileDTO> {
    ValueProvider<FileDTO, Integer> id();

    ValueProvider<FileDTO, String> fileName();

    ValueProvider<FileDTO, Date> uploadDate();
}
