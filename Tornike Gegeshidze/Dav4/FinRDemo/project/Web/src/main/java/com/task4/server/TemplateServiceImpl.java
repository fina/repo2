//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task4.client.service.TemplateService;
import com.task4.entity.Template;
import com.task4.localservice.TemplateLocalService;
import com.task4.shared.ConverterClass;
import com.task4.shared.ScheduleDTO;
import com.task4.shared.TemplateDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;

public class TemplateServiceImpl extends RemoteServiceServlet implements TemplateService {
    @EJB
    TemplateLocalService templateLocalService;

    public TemplateServiceImpl() {
    }

    public void addTemplate(TemplateDTO templateDTO) {
        try {
            this.templateLocalService.addTemplate(ConverterClass.getInstance().convertTemplateDTOToEntity(templateDTO));
        } catch (SQLException var3) {
            throw new RuntimeException(var3.getMessage());
        }
    }

    public List<TemplateDTO> loadTemplate() {
        ArrayList templateDTOList = new ArrayList();
        List templates = this.templateLocalService.loadTemplate();
        Iterator var3 = templates.iterator();

        while (var3.hasNext()) {
            Template template = (Template) var3.next();
            templateDTOList.add(ConverterClass.getInstance().convertTemplateEntityToDto(template));
        }

        return templateDTOList;
    }

    public void deleteTemplate(TemplateDTO templateDTO) {
        if (!templateLocalService.templateIsUsed(ConverterClass.getInstance().convertTemplateDTOToEntity(templateDTO))) {
            this.templateLocalService.deleteTemplate(templateDTO.getId());
        }
    else
    {
        throw new RuntimeException();
    }

}

}
