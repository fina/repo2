//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.task4.client.presenter.FinTypePresenter.Display;
import com.task4.shared.FinTypeDTO;

public class FiTypeRegisterForm extends Composite implements Display {
    private Window window;
    TextButton saveFiTypeButton;
    TextButton cancelFiTypeButton;
    TextField fiTypeCode;
    TextField fiTypeName;

    public FiTypeRegisterForm() {
        this.initComponent();
    }

    private void initComponent() {
        this.window = new Window();
        this.initWidget(this.window);
        this.window.setPixelSize(500, 300);
        this.window.setModal(false);
        this.window.setBlinkModal(true);
        this.window.setHeadingText("Fi Registration Form");
        this.window.setShadow(false);
        this.fiTypeCode = new TextField();
        this.fiTypeCode.setAllowBlank(false);
        this.fiTypeName = new TextField();
        this.fiTypeName.setAllowBlank(false);
        VBoxLayoutContainer c = new VBoxLayoutContainer();
        c.setPadding(new Padding(5));
        c.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCHMAX);
        c.add(new FieldLabel(this.fiTypeCode, "FI type code"), new BoxLayoutData(new Margins(0, 0, 5, 0)));
        c.add(new FieldLabel(this.fiTypeName, "FI type name"), new BoxLayoutData(new Margins(0, 0, 90, 0)));
        FieldSet fieldSet = new FieldSet();
        fieldSet.setHeadingText("User Information");
        fieldSet.setCollapsible(true);
        fieldSet.add(c);
        FramedPanel fiTypeRegisterPanel = new FramedPanel();
        fiTypeRegisterPanel.setLayoutData(new BorderLayoutContainer());
        fiTypeRegisterPanel.setHeadingText("");
        fiTypeRegisterPanel.setBodyStyle("background: none; padding: 10px");
        fiTypeRegisterPanel.setWidth(350);
        fiTypeRegisterPanel.add(fieldSet);
        this.saveFiTypeButton = new TextButton("Save");
        this.cancelFiTypeButton = new TextButton("Cancel");
        fiTypeRegisterPanel.addButton(this.saveFiTypeButton);
        fiTypeRegisterPanel.addButton(this.cancelFiTypeButton);
        this.window.add(fiTypeRegisterPanel);
    }

    public Widget asWidget() {
        return this;
    }

    public HasSelectHandlers getAddTypeButton() {
        return this.saveFiTypeButton;
    }

    public HasSelectHandlers getcancelTypeButton() {
        return this.cancelFiTypeButton;
    }

    public FinTypeDTO getFiType() {
        FinTypeDTO tmpFinTypeDTO = new FinTypeDTO();
        tmpFinTypeDTO.setCode((String)this.fiTypeCode.getCurrentValue());
        tmpFinTypeDTO.setName((String)this.fiTypeName.getCurrentValue());
        return tmpFinTypeDTO;
    }

    public void clearInputs() {
        this.fiTypeCode.setText("");
        this.fiTypeName.setText("");
    }

    public void updateSelectedFiType(FinTypeDTO finTypeDTO) {
        this.fiTypeName.setText(finTypeDTO.getName());
        this.fiTypeCode.setText(finTypeDTO.getCode());
    }
}
