//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.CancelFiTypeEvent;
import com.task4.client.Event.SaveFiTypeEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.FinTypeServiceAsync;
import com.task4.shared.FinTypeDTO;

public class FinTypePresenter implements Presenter {
    private FinTypePresenter.Display display;
    private HandlerManager eventBus;
    private FinTypeServiceAsync rpcService;
    FinTypeDTO finTypeDTO;
    private DialogHideEvent.DialogHideHandler hideHandler = new DialogHideEvent.DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };

    public FinTypePresenter(FinTypeServiceAsync rpcService, HandlerManager eventBus, FinTypePresenter.Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        this.bind();
    }

    public FinTypePresenter(FinTypeServiceAsync rpcService, HandlerManager eventBus, FinTypePresenter.Display display, FinTypeDTO finTypeDTO) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        this.finTypeDTO = finTypeDTO;
        this.display.updateSelectedFiType(finTypeDTO);
        this.bind();
    }

    private void bind() {
        this.display.getAddTypeButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent selectEvent) {
                FinTypeDTO fiType = FinTypePresenter.this.display.getFiType();
                if(FinTypePresenter.this.finTypeDTO != null) {
                    FinTypePresenter.this.finTypeDTO.setName(fiType.getName());
                    FinTypePresenter.this.finTypeDTO.setCode(fiType.getCode());
                    FinTypePresenter.this.saveFiType(FinTypePresenter.this.finTypeDTO);
                    FinTypePresenter.this.eventBus.fireEvent(new SaveFiTypeEvent());
                } else {
                    FinTypePresenter.this.saveFiType(fiType);
                    FinTypePresenter.this.eventBus.fireEvent(new SaveFiTypeEvent());
                }

            }
        });
        this.display.getcancelTypeButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FinTypePresenter.this.finTypeDTO = null;
                FinTypePresenter.this.eventBus.fireEvent(new CancelFiTypeEvent());
            }
        });
    }

    private void saveFiType(FinTypeDTO finTypeDTO) {

        rpcService.addFiType(finTypeDTO, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Error deleting Type");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Fi type Saved");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }
        });

    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(this.display.asWidget());
    }

    public interface Display {
        Widget asWidget();

        HasSelectHandlers getAddTypeButton();

        HasSelectHandlers getcancelTypeButton();

        FinTypeDTO getFiType();

        void clearInputs();

        void updateSelectedFiType(FinTypeDTO var1);
    }
}
