//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.task4.client.Event.AddFiEvent;
import com.task4.client.Event.AddFiEventHandler;
import com.task4.client.Event.AddFiTypeEvent;
import com.task4.client.Event.AddFiTypeEventHandler;
import com.task4.client.Event.AddFileEvent;
import com.task4.client.Event.AddFileEventHandler;
import com.task4.client.Event.AddReturnEvent;
import com.task4.client.Event.AddReturnEventHandler;
import com.task4.client.Event.AddTemplateEvent;
import com.task4.client.Event.AddTemplateEventHandler;
import com.task4.client.Event.CancelFiEvent;
import com.task4.client.Event.CancelFiEventHandler;
import com.task4.client.Event.CancelFiTypeEvent;
import com.task4.client.Event.CancelFiTypeEventHandler;
import com.task4.client.Event.CancelFileEvent;
import com.task4.client.Event.CancelFileEventHandler;
import com.task4.client.Event.CancelReturnEvent;
import com.task4.client.Event.CancelReturnEventHandler;
import com.task4.client.Event.CancelTemplateEvent;
import com.task4.client.Event.CancelTemplateEventHandler;
import com.task4.client.Event.DeleteFiEvent;
import com.task4.client.Event.DeleteFiEventHandler;
import com.task4.client.Event.DeleteFiTypeEvent;
import com.task4.client.Event.DeleteFiTypeEventHandler;
import com.task4.client.Event.DeleteFileEvent;
import com.task4.client.Event.DeleteFileEventHandler;
import com.task4.client.Event.DeleteReturnEvent;
import com.task4.client.Event.DeleteReturnEventHandler;
import com.task4.client.Event.DeleteTemplateEvent;
import com.task4.client.Event.DeleteTemplateEventHandler;
import com.task4.client.Event.SaveFiEvent;
import com.task4.client.Event.SaveFiEventHandler;
import com.task4.client.Event.SaveFiTypeEvent;
import com.task4.client.Event.SaveFiTypeEventHandler;
import com.task4.client.Event.SaveFileEvent;
import com.task4.client.Event.SaveFileEventHandler;
import com.task4.client.Event.SaveReturnEvent;
import com.task4.client.Event.SaveReturnEventHandler;
import com.task4.client.Event.SaveTemplateEvent;
import com.task4.client.Event.SaveTemplateEventHandler;
import com.task4.client.Event.UpdateFiEvent;
import com.task4.client.Event.UpdateFiEventHandler;
import com.task4.client.Event.UpdateFiTypeEvent;
import com.task4.client.Event.UpdateFiTypeEventHandler;
import com.task4.client.Event.UpdateReturnEvent;
import com.task4.client.Event.UpdateReturnEventHandler;
import com.task4.client.Event.UpdateTemplateEvent;
import com.task4.client.Event.UpdateTemplateEventHandler;
import com.task4.client.presenter.FiPresenter;
import com.task4.client.presenter.FilePresenter;
import com.task4.client.presenter.FinTypePresenter;
import com.task4.client.presenter.MainPresenter;
import com.task4.client.presenter.Presenter;
import com.task4.client.presenter.ReturnPresenter;
import com.task4.client.presenter.TemplatePresenter;
import com.task4.client.service.FileServiceAsync;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.FinTypeServiceAsync;
import com.task4.client.service.ReturnServiceAsync;
import com.task4.client.service.TemplateServiceAsync;
import com.task4.client.view.FiRegisterForm;
import com.task4.client.view.FiTypeRegisterForm;
import com.task4.client.view.FileUploadForm;
import com.task4.client.view.MainPanel;
import com.task4.client.view.ReturnRegisterForm;
import com.task4.client.view.TemplateRegisterForm;

public class AppController implements Presenter, ValueChangeHandler<String> {
    FinServiceAsync finServiceAsync;
    FinTypeServiceAsync finTypeServiceAsync;
    TemplateServiceAsync templateServiceAsync;
    ReturnServiceAsync returnServiceAsync;
    FileServiceAsync fileServiceAsync;
    private HandlerManager eventBus;
    private HasWidgets container;
    Presenter presenter = null;

    public AppController(HandlerManager eventBus, FinTypeServiceAsync typeServiceAsync, FinServiceAsync finServiceAsync, TemplateServiceAsync templateServiceAsync, ReturnServiceAsync returnServiceAsync, FileServiceAsync fileServiceAsync) {
        this.finTypeServiceAsync = typeServiceAsync;
        this.finServiceAsync = finServiceAsync;
        this.templateServiceAsync = templateServiceAsync;
        this.returnServiceAsync = returnServiceAsync;
        this.fileServiceAsync = fileServiceAsync;
        this.eventBus = eventBus;
        this.bind();
    }

    public void bind() {
        History.addValueChangeHandler(this);
        this.eventBus.addHandler(AddFiTypeEvent.TYPE, new AddFiTypeEventHandler() {
            public void onFiTypeAdd(AddFiTypeEvent fiEvent) {
                History.newItem("Add");
            }
        });
        this.eventBus.addHandler(SaveFiTypeEvent.TYPE, new SaveFiTypeEventHandler() {
            public void onFiTypeSave(SaveFiTypeEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(DeleteFiTypeEvent.TYPE, new DeleteFiTypeEventHandler() {
            public void onFiTypeDelete(DeleteFiTypeEvent fiEvent) {
                History.newItem("list", false);
            }
        });
        this.eventBus.addHandler(UpdateFiTypeEvent.TYPE, new UpdateFiTypeEventHandler() {
            public void onFiTypeUpdate(UpdateFiTypeEvent fiEvent) {
                History.newItem("update");
                AppController.this.presenter = new FinTypePresenter(AppController.this.finTypeServiceAsync, AppController.this.eventBus, new FiTypeRegisterForm(), fiEvent.getFinTypeDTO());
                AppController.this.presenter.go(AppController.this.container);
            }
        });
        this.eventBus.addHandler(CancelFiTypeEvent.TYPE, new CancelFiTypeEventHandler() {
            public void onFiTypeCancel(CancelFiTypeEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(AddFiEvent.TYPE, new AddFiEventHandler() {
            public void onFiTypeAdd(AddFiEvent fiEvent) {
                History.newItem("AddFi");
            }
        });
        this.eventBus.addHandler(SaveFiEvent.TYPE, new SaveFiEventHandler() {
            public void onFiTypeSave(SaveFiEvent saveFiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(CancelFiEvent.TYPE, new CancelFiEventHandler() {
            public void onFiCancel(CancelFiEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(DeleteFiEvent.TYPE, new DeleteFiEventHandler() {
            public void onFiDelete(DeleteFiEvent fiEvent) {
                History.newItem("list", false);
            }
        });
        this.eventBus.addHandler(UpdateFiEvent.TYPE, new UpdateFiEventHandler() {
            public void onFiUpdate(UpdateFiEvent fiEvent) {
                History.newItem("update");
                AppController.this.presenter = new FiPresenter(AppController.this.finServiceAsync, AppController.this.finTypeServiceAsync, AppController.this.eventBus, new FiRegisterForm(), fiEvent.getFinInstituteDTO());
                AppController.this.presenter.go(AppController.this.container);
            }
        });
        this.eventBus.addHandler(AddTemplateEvent.TYPE, new AddTemplateEventHandler() {
            public void onTemplateAdd(AddTemplateEvent fiEvent) {
                History.newItem("AddTemplate");
            }
        });
        this.eventBus.addHandler(SaveTemplateEvent.TYPE, new SaveTemplateEventHandler() {
            public void onTemplateSave(SaveTemplateEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(CancelTemplateEvent.TYPE, new CancelTemplateEventHandler() {
            public void onTemplateCancel(CancelTemplateEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(DeleteTemplateEvent.TYPE, new DeleteTemplateEventHandler() {
            public void onTemplateDelete(DeleteTemplateEvent fiEvent) {
                History.newItem("list", false);
            }
        });
        this.eventBus.addHandler(UpdateTemplateEvent.TYPE, new UpdateTemplateEventHandler() {
            public void onTemplateUpdate(UpdateTemplateEvent fiEvent) {
                History.newItem("update");
                AppController.this.presenter = new TemplatePresenter(AppController.this.templateServiceAsync, AppController.this.eventBus, new TemplateRegisterForm(), fiEvent.getTemplateDTO());
                AppController.this.presenter.go(AppController.this.container);
            }
        });
        this.eventBus.addHandler(AddReturnEvent.TYPE, new AddReturnEventHandler() {
            public void onReturnAdd(AddReturnEvent fiEvent) {
                History.newItem("AddReturn");
            }
        });
        this.eventBus.addHandler(CancelReturnEvent.TYPE, new CancelReturnEventHandler() {
            public void onReturnCancel(CancelReturnEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(SaveReturnEvent.TYPE, new SaveReturnEventHandler() {
            public void onReturnSave(SaveReturnEvent saveFiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(UpdateReturnEvent.TYPE, new UpdateReturnEventHandler() {
            public void onReturnUpdate(UpdateReturnEvent fiEvent) {
                History.newItem("update");
                AppController.this.presenter = new ReturnPresenter(AppController.this.returnServiceAsync, AppController.this.finServiceAsync, AppController.this.templateServiceAsync, AppController.this.eventBus, new ReturnRegisterForm(), fiEvent.getReturnDTO());
                AppController.this.presenter.go(AppController.this.container);
            }
        });
        this.eventBus.addHandler(DeleteReturnEvent.TYPE, new DeleteReturnEventHandler() {
            public void onReturnDelete(DeleteReturnEvent fiEvent) {
                History.newItem("list", false);
            }
        });
        this.eventBus.addHandler(AddFileEvent.TYPE, new AddFileEventHandler() {
            public void onFileAdd(AddFileEvent fiEvent) {
                History.newItem("UploadFile");
            }
        });
        this.eventBus.addHandler(CancelFileEvent.TYPE, new CancelFileEventHandler() {
            public void onFileCancel(CancelFileEvent fiEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(SaveFileEvent.TYPE, new SaveFileEventHandler() {
            public void onFileSave(SaveFileEvent saveFileEvent) {
                History.newItem("list");
            }
        });
        this.eventBus.addHandler(DeleteFileEvent.TYPE, new DeleteFileEventHandler() {
            public void onFileDelete(DeleteFileEvent fiEvent) {
                History.newItem("list", false);
            }
        });
    }

    public void go(HasWidgets container) {
        this.container = container;
        if("".equals(History.getToken())) {
            History.newItem("list");
        } else {
            History.fireCurrentHistoryState();
        }

    }

    public void onValueChange(ValueChangeEvent<String> valueChangeEvent) {
        String token = (String)valueChangeEvent.getValue();
        if(token != null) {
            Object presenter = null;
            if(token.equals("list")) {
                presenter = new MainPresenter(this.finTypeServiceAsync, this.finServiceAsync, this.templateServiceAsync, this.returnServiceAsync, this.fileServiceAsync, this.eventBus, new MainPanel());
                ((Presenter)presenter).go(this.container);
            }

            if(token.equals("Add")) {
                presenter = new FinTypePresenter(this.finTypeServiceAsync, this.eventBus, new FiTypeRegisterForm());
                ((Presenter)presenter).go(this.container);
            }

            if(token.equals("AddFi")) {
                presenter = new FiPresenter(this.finServiceAsync, this.finTypeServiceAsync, this.eventBus, new FiRegisterForm());
                ((Presenter)presenter).go(this.container);
            }

            if(token.equals("AddTemplate")) {
                presenter = new TemplatePresenter(this.templateServiceAsync, this.eventBus, new TemplateRegisterForm());
                ((Presenter)presenter).go(this.container);
            }

            if(token.equals("AddReturn")) {
                presenter = new ReturnPresenter(this.returnServiceAsync, this.finServiceAsync, this.templateServiceAsync, this.eventBus, new ReturnRegisterForm());
                ((Presenter)presenter).go(this.container);
            }

            if(token.equals("UploadFile")) {
                presenter = new FilePresenter(this.fileServiceAsync, this.finServiceAsync, this.templateServiceAsync, this.eventBus, new FileUploadForm());
                ((Presenter)presenter).go(this.container);
            }

            if(presenter != null) {
                ((Presenter)presenter).go(this.container);
            }
        }

    }
}
