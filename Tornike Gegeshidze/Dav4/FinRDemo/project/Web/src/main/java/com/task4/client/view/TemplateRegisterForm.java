//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.task4.client.TemplateProperties;
import com.task4.client.presenter.TemplatePresenter.Display;
import com.task4.shared.ScheduleDTO;
import com.task4.shared.TemplateDTO;

public class TemplateRegisterForm extends Composite implements Display {
    private Window window;
    private TextButton saveTemplateButton;
    private TextButton cancelTemplateButton;
    private TextField templateCode;
    private TextField templateName;
    private ComboBox<ScheduleDTO> scheduleDTOComboBox;
    private ListStore<ScheduleDTO> templateDTOListStore;

    public TemplateRegisterForm() {
        this.initComponent();
    }

    private void initComponent() {
        this.window = new Window();
        this.initWidget(this.window);
        this.window.setPixelSize(500, 300);
        this.window.setModal(false);
        this.window.setBlinkModal(true);
        this.window.setHeadingText("Template Registration Form");
        this.window.setShadow(false);
        this.templateCode = new TextField();
        this.templateCode.setAllowBlank(false);
        this.templateName = new TextField();
        this.templateName.setAllowBlank(false);
        TemplateProperties templateProperties = (TemplateProperties)GWT.create(TemplateProperties.class);
        this.templateDTOListStore = new ListStore<>(new ModelKeyProvider<ScheduleDTO>() {
            @Override
            public String getKey(ScheduleDTO item) {
                return item.toString();
            }
        });
        ScheduleDTO[] c = ScheduleDTO.values();
        int fieldSet = c.length;

        for(int templateRegisterPanel = 0; templateRegisterPanel < fieldSet; ++templateRegisterPanel) {
            ScheduleDTO scheduleDTO = c[templateRegisterPanel];
            this.templateDTOListStore.add(scheduleDTO);
        }

        scheduleDTOComboBox = new ComboBox(templateDTOListStore, new LabelProvider() {
            @Override
            public String getLabel(Object item) {
                return item.toString();
            }
        });
        VBoxLayoutContainer vBoxLayoutContainer = new VBoxLayoutContainer();
        vBoxLayoutContainer.setPadding(new Padding(5));
        vBoxLayoutContainer.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCHMAX);
        vBoxLayoutContainer.add(new FieldLabel(this.templateCode, "Template code"), new BoxLayoutData(new Margins(0, 0, 5, 0)));
        vBoxLayoutContainer.add(new FieldLabel(this.templateName, "Template name"), new BoxLayoutData(new Margins(0, 0, 5, 0)));
        vBoxLayoutContainer.add(new FieldLabel(this.scheduleDTOComboBox, "Schedule"), new BoxLayoutData(new Margins(0, 0, 90, 0)));
        FieldSet fieldSet1 = new FieldSet();
        fieldSet1.setHeadingText("Template Information");
        fieldSet1.setCollapsible(true);
        fieldSet1.add(vBoxLayoutContainer);
        FramedPanel var8 = new FramedPanel();
        var8.setLayoutData(new BorderLayoutContainer());
        var8.setHeadingText("");
        var8.setBodyStyle("background: none; padding: 10px");
        var8.setWidth(350);
        var8.add(fieldSet1);
        this.saveTemplateButton = new TextButton("Save");
        this.cancelTemplateButton = new TextButton("Cancel");
        var8.addButton(this.saveTemplateButton);
        var8.addButton(this.cancelTemplateButton);
        this.window.add(var8);
    }

    public Widget asWidget() {
        return this;
    }

    public HasSelectHandlers getTemplateAddButton() {
        return this.saveTemplateButton;
    }

    public HasSelectHandlers getcancelTemplateButton() {
        return this.cancelTemplateButton;
    }

    public void updateSelectedTemplate(TemplateDTO dto) {
        this.templateCode.setValue(dto.getCode());
        this.templateName.setValue(dto.getName());
        this.scheduleDTOComboBox.setValue(dto.getSchedule());
    }

    public TemplateDTO getTemplateDTO() {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setName((String)this.templateName.getCurrentValue());
        templateDTO.setCode((String)this.templateCode.getCurrentValue());
        templateDTO.setSchedule((ScheduleDTO)this.scheduleDTOComboBox.getCurrentValue());
        return templateDTO;
    }

    public void clearInputs() {
        this.templateCode.setText("");
        this.templateName.setText("");
        this.scheduleDTOComboBox.setValue(this.scheduleDTOComboBox.getOriginalValue());
    }
}
