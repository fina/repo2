//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.AddFiEvent;
import com.task4.client.Event.AddFiTypeEvent;
import com.task4.client.Event.AddFileEvent;
import com.task4.client.Event.AddReturnEvent;
import com.task4.client.Event.AddTemplateEvent;
import com.task4.client.Event.DeleteFiEvent;
import com.task4.client.Event.DeleteFiTypeEvent;
import com.task4.client.Event.DeleteFileEvent;
import com.task4.client.Event.DeleteReturnEvent;
import com.task4.client.Event.DeleteTemplateEvent;
import com.task4.client.Event.UpdateFiEvent;
import com.task4.client.Event.UpdateFiTypeEvent;
import com.task4.client.Event.UpdateReturnEvent;
import com.task4.client.Event.UpdateTemplateEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.FileServiceAsync;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.FinTypeServiceAsync;
import com.task4.client.service.ReturnServiceAsync;
import com.task4.client.service.TemplateServiceAsync;
import com.task4.shared.FileDTO;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import com.task4.shared.ReturnDTO;
import com.task4.shared.TemplateDTO;
import java.util.List;

public class MainPresenter implements Presenter {
    private MainPresenter.Display display;
    private HandlerManager eventBus;
    private FinTypeServiceAsync finTypeServiceAsync;
    private FinServiceAsync finServiceAsync;
    private TemplateServiceAsync templateServiceAsync;
    private ReturnServiceAsync returnServiceAsync;
    private FileServiceAsync fileServiceAsync;
    private DialogHideHandler hideHandler = new DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };

    public MainPresenter(FinTypeServiceAsync typeServiceAsync, FinServiceAsync finServiceAsync, TemplateServiceAsync templateServiceAsync, ReturnServiceAsync returnServiceAsync, FileServiceAsync fileServiceAsync, HandlerManager eventBus, MainPresenter.Display display) {
        this.eventBus = eventBus;
        this.display = display;
        this.finTypeServiceAsync = typeServiceAsync;
        this.finServiceAsync = finServiceAsync;
        this.templateServiceAsync = templateServiceAsync;
        this.returnServiceAsync = returnServiceAsync;
        this.fileServiceAsync = fileServiceAsync;
        this.bind();
    }

    private void bind() {
        this.refresh();
        this.display.getFiTypeAddButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent selectEvent) {
                MainPresenter.this.eventBus.fireEvent(new AddFiTypeEvent());
            }
        });
        this.display.getFiTypeDeleteButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.deleteFiType(display.getSelectedDTO());
                MainPresenter.this.eventBus.fireEvent(new DeleteFiTypeEvent());
            }
        });
        this.display.getFiTypeUpdateButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FinTypeDTO finTypeDTO = MainPresenter.this.display.getFiType();
                if(!finTypeDTO.equals((Object)null)) {
                    MainPresenter.this.eventBus.fireEvent(new UpdateFiTypeEvent(finTypeDTO));
                } else {
                    AlertMessageBox messageBox = new AlertMessageBox("Alert", "nothing was selected");
                    messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                    messageBox.show();
                }

            }
        });
        this.display.getFiAddButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.eventBus.fireEvent(new AddFiEvent());
            }
        });
        this.display.getFiDeleteButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.deleteFi(MainPresenter.this.display.getSeceltedFi());
                MainPresenter.this.eventBus.fireEvent(new DeleteFiEvent());
            }
        });
        this.display.getFiUpdateButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FinInstituteDTO tmpInstituteDTO = MainPresenter.this.display.getSeceltedFi();
                if(!tmpInstituteDTO.equals((Object)null)) {
                    MainPresenter.this.eventBus.fireEvent(new UpdateFiEvent(tmpInstituteDTO));
                } else {
                    AlertMessageBox messageBox = new AlertMessageBox("Alert", "nothing was selected");
                    messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                    messageBox.show();
                }

            }
        });
        this.display.getTemplateAddButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.eventBus.fireEvent(new AddTemplateEvent());
            }
        });
        this.display.getTemplateUpdateButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                TemplateDTO templateDTO = MainPresenter.this.display.getSelectedTemplate();
                if(!templateDTO.equals((Object)null)) {
                    MainPresenter.this.eventBus.fireEvent(new UpdateTemplateEvent(templateDTO));
                } else {
                    AlertMessageBox messageBox = new AlertMessageBox("Alert", "nothing was selected");
                    messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                    messageBox.show();
                }

            }
        });
        this.display.getTemplateDeleteButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.deleteTemplate(MainPresenter.this.display.getSelectedTemplate());
                MainPresenter.this.eventBus.fireEvent(new DeleteTemplateEvent());
            }
        });
        this.display.getReturnAddButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.eventBus.fireEvent(new AddReturnEvent());
            }
        });
        this.display.getReturnUpdateButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                ReturnDTO returnDTO = MainPresenter.this.display.getSelectedReturn();
                if(!returnDTO.equals((Object)null)) {
                    MainPresenter.this.eventBus.fireEvent(new UpdateReturnEvent(returnDTO));
                } else {
                    AlertMessageBox messageBox = new AlertMessageBox("Alert", "nothing was selected");
                    messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                    messageBox.show();
                }

            }
        });
        this.display.getReturnDeleteButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.deleteReturn(MainPresenter.this.display.getSelectedReturn());
                MainPresenter.this.eventBus.fireEvent(new DeleteReturnEvent());
            }
        });
        this.display.getFileUploadButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.eventBus.fireEvent(new AddFileEvent());
            }
        });
        this.display.getFileUploadDeleteButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.deleteFile(MainPresenter.this.display.getSelectedFileDTO());
                MainPresenter.this.eventBus.fireEvent(new DeleteFileEvent());
            }
        });
        this.display.getFileUploadDownloadButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                MainPresenter.this.downloadSelectedFile(MainPresenter.this.display.getSelectedFileDTO());
            }
        });
    }

    private void downloadSelectedFile(FileDTO selectedFileDTO) {
        if(selectedFileDTO == null) {
            AlertMessageBox baseUrl = new AlertMessageBox("Alert", "select file to download");
            baseUrl.addDialogHideHandler(this.hideHandler);
            baseUrl.show();
        } else {
            String baseUrl1 = GWT.getModuleBaseURL();
            String url = baseUrl1 + "FileDownloadServlet" + "?id=" + selectedFileDTO.getId();
            Window.open(url, "", "menubar=no,location=yes,resizable=yes,scrollbars=yes,status=no,dependent=truetoolbar=true");
        }

    }

    private void deleteFile(FileDTO selectedFileDTO) {

        fileServiceAsync.deleteFile(selectedFileDTO.getId(), new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Error deleting File");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "File deleted");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
                refresh();
            }
        });

    }

    private void deleteReturn(ReturnDTO returnDTO) {
        returnServiceAsync.deleteReturn(returnDTO, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Return is used");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Return deleted");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
                refresh();
            }
        });

    }

    private void deleteTemplate(TemplateDTO selectedTemplate) {
        templateServiceAsync.deleteTemplate(selectedTemplate, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Template is used");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Template deleted");
                messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                messageBox.show();
                refresh();
            }
        });
    }

    private void deleteFi(FinInstituteDTO finInstituteDTO) {
        finServiceAsync.deleteSeletedFi(finInstituteDTO, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Fi is used");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Fi Type deleted");
                messageBox.addDialogHideHandler(MainPresenter.this.hideHandler);
                messageBox.show();
                refresh();
            }
        });

    }

    private void refresh() {
        finTypeServiceAsync.loadFiType(new AsyncCallback<List<FinTypeDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(List<FinTypeDTO> result) {
                display.loadFiType(result);
            }
        });
        finServiceAsync.loadFies(new AsyncCallback<List<FinInstituteDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "error loading fies");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(List<FinInstituteDTO> result) {
                display.loadFies(result);
            }
        });
        templateServiceAsync.loadTemplate(new AsyncCallback<List<TemplateDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "error loading Templates");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(List<TemplateDTO> result) {
                display.loadTemplates(result);
            }
        });
       returnServiceAsync.loadReturns(new AsyncCallback<List<ReturnDTO>>() {
           @Override
           public void onFailure(Throwable caught) {
               AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
               messageBox.addDialogHideHandler(hideHandler);
               messageBox.show();
           }

           @Override
           public void onSuccess(List<ReturnDTO> result) {
               display.loadReturns(result);
           }
       });
        fileServiceAsync.loadFiles(new AsyncCallback<List<FileDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(List<FileDTO> result) {
                display.loadFiles(result);
            }
        });
    }

    private void deleteFiType(FinTypeDTO finTypeDTO) {
        finTypeServiceAsync.deleteFiType(finTypeDTO, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Type is used");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", "Fi Type deleted");
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
                refresh();
            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(this.display.asWidget());
    }

    public interface Display {
        Widget asWidget();

        HasSelectHandlers getFiTypeAddButton();

        HasSelectHandlers getFiTypeDeleteButton();

        HasSelectHandlers getFiTypeUpdateButton();

        HasSelectHandlers getFiAddButton();

        HasSelectHandlers getFiDeleteButton();

        HasSelectHandlers getFiUpdateButton();

        HasSelectHandlers getTemplateAddButton();

        HasSelectHandlers getTemplateDeleteButton();

        HasSelectHandlers getTemplateUpdateButton();

        HasSelectHandlers getReturnAddButton();

        HasSelectHandlers getReturnDeleteButton();

        HasSelectHandlers getReturnUpdateButton();

        HasSelectHandlers getFileUploadDeleteButton();

        HasSelectHandlers getFileUploadDownloadButton();

        HasSelectHandlers getFileUploadButton();

        void loadFiType(List var1);

        void loadFies(List var1);

        void loadTemplates(List var1);

        void loadReturns(List var1);

        void loadFiles(List var1);

        FinTypeDTO getFiType();

        FinInstituteDTO getSeceltedFi();

        TemplateDTO getSelectedTemplate();

        ReturnDTO getSelectedReturn();

        FileDTO getSelectedFileDTO();

        FinTypeDTO getSelectedDTO();
    }
}
