//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.CancelFileEvent;
import com.task4.client.Event.SaveFileEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.FileServiceAsync;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.TemplateServiceAsync;
import com.task4.shared.FiNotFoundException;
import com.task4.shared.ReturnNotFoundException;
import com.task4.shared.TemplateNotFoundException;

public class FilePresenter implements Presenter {
    private FilePresenter.Display display;
    private HandlerManager eventBus;
    private FileServiceAsync fileServiceAsync;
    private FinServiceAsync finServiceAsync;
    private TemplateServiceAsync templateServiceAsync;
    private DialogHideHandler hideHandler = new DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };

    public FilePresenter(FileServiceAsync rpcService, FinServiceAsync finServiceAsync, TemplateServiceAsync templateServiceAsync, HandlerManager eventBus, FilePresenter.Display display) {
        this.fileServiceAsync = rpcService;
        this.eventBus = eventBus;
        this.finServiceAsync = finServiceAsync;
        this.templateServiceAsync = templateServiceAsync;
        this.display = display;
        this.bind();
    }

    private void bind() {
        this.display.getcancelFileUploadButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FilePresenter.this.eventBus.fireEvent(new CancelFileEvent());
            }
        });
        this.display.getuploadFileUploadButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                String fileFullPath = FilePresenter.this.display.getFileUploadField().getValue();
                String fileFullName = fileFullPath.contains("/")?fileFullPath.substring(fileFullPath.lastIndexOf(47) + 1):fileFullPath.substring(fileFullPath.lastIndexOf(92) + 1);
                String regex = "[A-Za-z0-9]+\\.[A-Za-z0-9]+\\.[0-9]{8}\\.xml";
                if(fileFullName.matches(regex)) {

                    fileServiceAsync.checkFileByName(fileFullName, new AsyncCallback<Boolean>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            AlertMessageBox messageBox;
                            if(caught instanceof FiNotFoundException) {
                                messageBox = new AlertMessageBox("Alert", "Fi wasnt found");
                                messageBox.addDialogHideHandler(hideHandler);
                                messageBox.show();
                            }

                            if(caught instanceof TemplateNotFoundException) {
                                messageBox = new AlertMessageBox("Alert", "Template wasbt found");
                                messageBox.addDialogHideHandler(hideHandler);
                                messageBox.show();
                            }

                            if(caught instanceof ReturnNotFoundException) {
                                messageBox = new AlertMessageBox("Alert", "return wasnt found");
                                messageBox.addDialogHideHandler(hideHandler);
                                messageBox.show();
                            }
                        }

                        @Override
                        public void onSuccess(Boolean result) {
                            if(result.booleanValue()) {
                                FilePresenter.this.display.getFormPanel().submit();
                                FilePresenter.this.eventBus.fireEvent(new SaveFileEvent());
                            } else {
                                AlertMessageBox messageBox = new AlertMessageBox("Alert", " unknown Error occured");
                                messageBox.addDialogHideHandler(hideHandler);
                                messageBox.show();
                            }
                        }
                    });

                } else {
                    AlertMessageBox messageBox = new AlertMessageBox("Alert", "File name: \"" + fileFullName + "\" does not match patern ([fi Code].[Template Code].[Schedule Time].xml)");
                    messageBox.addDialogHideHandler(hideHandler);
                    messageBox.show();
                }

            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(this.display.asWidget());
    }

    public interface Display {
        Widget asWidget();

        HasSelectHandlers getuploadFileUploadButton();

        HasSelectHandlers getcancelFileUploadButton();

        FileUploadField getFileUploadField();

        FormPanel getFormPanel();
    }
}
