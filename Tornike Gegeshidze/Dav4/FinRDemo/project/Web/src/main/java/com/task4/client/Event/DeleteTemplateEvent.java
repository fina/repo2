//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.DeleteTemplateEventHandler;

public class DeleteTemplateEvent extends GwtEvent<DeleteTemplateEventHandler> {
    public static final Type<DeleteTemplateEventHandler> TYPE = new Type();

    public DeleteTemplateEvent() {
    }

    public Type<DeleteTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteTemplateEventHandler handler) {
        handler.onTemplateDelete(this);
    }
}