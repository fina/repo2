//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import java.io.Serializable;

public class FinTypeDTO implements Serializable {
    private int id;
    private String code;
    private String name;

    public FinTypeDTO() {
    }

    public FinTypeDTO(int id, String name, String code) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
