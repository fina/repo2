//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task4.client.service.FinService;
import com.task4.entity.FinInstitute;
import com.task4.entity.FinType;
import com.task4.localservice.FiLocalService;
import com.task4.shared.ConverterClass;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;

public class FinServiceImpl extends RemoteServiceServlet implements FinService {
    @EJB
    private FiLocalService fiLocalService;

    public FinServiceImpl() {
    }

    public boolean addFi(FinInstituteDTO finInstituteDTO) {
        FinInstitute finInstitute = ConverterClass.getInstance().convertFinDtoToEntity(finInstituteDTO);

        try {
            this.fiLocalService.saveFi(finInstitute);
            return false;
        } catch (SQLException var4) {
            var4.printStackTrace();
            throw new RuntimeException("code isnt unicue");
        }
    }

    public List<FinInstituteDTO> loadFies() {
        ArrayList finInstituteDTOs = new ArrayList();
        List finInstitutes = null;
        finInstitutes = this.fiLocalService.loadFies();
        Iterator var3 = finInstitutes.iterator();

        while(var3.hasNext()) {
            FinInstitute finInstitute = (FinInstitute)var3.next();
            finInstituteDTOs.add(ConverterClass.getInstance().convertFinEntityToDto(finInstitute));
        }

        return finInstituteDTOs;
    }

    public void deleteSeletedFi(FinInstituteDTO finInstituteDTO) throws Exception {
        if(!fiLocalService.institutionIsUsed(ConverterClass.getInstance().convertFinDtoToEntity(finInstituteDTO))){
            fiLocalService.deleteFi(finInstituteDTO.getId());
        }
        else{
            throw new RuntimeException();
        }
    }


}

