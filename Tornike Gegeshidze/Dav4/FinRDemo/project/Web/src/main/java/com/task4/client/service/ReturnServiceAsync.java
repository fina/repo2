//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.task4.shared.ReturnDTO;
import java.util.List;

public interface ReturnServiceAsync {
    void saveReturn(ReturnDTO var1, AsyncCallback<Void> var2);

    void loadReturns(AsyncCallback<List<ReturnDTO>> var1);

    void deleteReturn(ReturnDTO returnDTO, AsyncCallback<Void> var2);
}
