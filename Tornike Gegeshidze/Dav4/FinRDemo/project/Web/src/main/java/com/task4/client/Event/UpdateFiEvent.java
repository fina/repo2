//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.UpdateFiEventHandler;
import com.task4.shared.FinInstituteDTO;

public class UpdateFiEvent extends GwtEvent<UpdateFiEventHandler> {
    public static final Type<UpdateFiEventHandler> TYPE = new Type();
    private final FinInstituteDTO finInstituteDTO;

    public UpdateFiEvent(FinInstituteDTO typeDTO) {
        this.finInstituteDTO = typeDTO;
    }

    public Type<UpdateFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateFiEventHandler updateFiEventHandler) {
        updateFiEventHandler.onFiUpdate(this);
    }

    public FinInstituteDTO getFinInstituteDTO() {
        return this.finInstituteDTO;
    }
}
