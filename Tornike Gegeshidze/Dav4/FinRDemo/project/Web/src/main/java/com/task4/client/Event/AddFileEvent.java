
package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.AddFileEventHandler;

public class AddFileEvent extends GwtEvent<AddFileEventHandler> {
    public static final Type<AddFileEventHandler> TYPE = new Type();

    public AddFileEvent() {
    }

    public Type<AddFileEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(AddFileEventHandler addFileEventHandler) {
        addFileEventHandler.onFileAdd(this);
    }
}
