//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.AddFiEventHandler;

public class AddFiEvent extends GwtEvent<AddFiEventHandler> {
    public static final Type<AddFiEventHandler> TYPE = new Type();

    public AddFiEvent() {
    }

    public Type<AddFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(AddFiEventHandler addFiEventHandler) {
        addFiEventHandler.onFiTypeAdd(this);
    }
}
