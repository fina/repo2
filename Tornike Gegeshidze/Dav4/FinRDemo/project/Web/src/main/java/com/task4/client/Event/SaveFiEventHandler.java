//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.SaveFiEvent;

public interface SaveFiEventHandler extends EventHandler {
    void onFiTypeSave(SaveFiEvent var1);
}
