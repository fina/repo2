//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.CancelReturnEvent;
import com.task4.client.Event.SaveReturnEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.ReturnServiceAsync;
import com.task4.client.service.TemplateServiceAsync;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.ReturnDTO;
import com.task4.shared.TemplateDTO;
import java.util.List;

public class ReturnPresenter implements Presenter {
    private ReturnPresenter.Display display;
    private HandlerManager eventBus;
    private ReturnServiceAsync rpcService;
    private FinServiceAsync finServiceAsync;
    private TemplateServiceAsync templateServiceAsync;
    private DialogHideHandler hideHandler = new DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };
    ReturnDTO returnDTO;

    public ReturnPresenter(ReturnServiceAsync rpcService, FinServiceAsync finServiceAsync, TemplateServiceAsync templateServiceAsync, HandlerManager eventBus, ReturnPresenter.Display display) {
        this.rpcService = rpcService;
        this.finServiceAsync = finServiceAsync;
        this.templateServiceAsync = templateServiceAsync;
        this.eventBus = eventBus;
        this.display = display;
        this.bind();
    }

    public ReturnPresenter(ReturnServiceAsync rpcService, FinServiceAsync finServiceAsync, TemplateServiceAsync templateServiceAsync, HandlerManager eventBus, ReturnPresenter.Display display, ReturnDTO returnDTO) {
        this.rpcService = rpcService;
        this.finServiceAsync = finServiceAsync;
        this.templateServiceAsync = templateServiceAsync;
        this.eventBus = eventBus;
        this.display = display;
        this.returnDTO = returnDTO;
        this.display.updateSelectedReturn(returnDTO);
        this.bind();
    }

    private void bind() {
        finServiceAsync.loadFies(new AsyncCallback<List<FinInstituteDTO>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(List<FinInstituteDTO> result) {
                display.loadFiComobox(result);
            }
        });
        templateServiceAsync.loadTemplate(new AsyncCallback<List<TemplateDTO>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(List<TemplateDTO> result) {
                display.loadTemplateComboBox(result);
            }
        });
        this.display.getReturnSaveButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                ReturnDTO dto = ReturnPresenter.this.display.getReturnDto();
                if(ReturnPresenter.this.returnDTO != null) {
                    ReturnPresenter.this.returnDTO.setTemplateDTO(dto.getTemplateDTO());
                    ReturnPresenter.this.returnDTO.setFinInstituteDTO(dto.getFinInstituteDTO());
                    ReturnPresenter.this.saveReturn(ReturnPresenter.this.returnDTO);
                    ReturnPresenter.this.eventBus.fireEvent(new SaveReturnEvent());
                } else {
                    ReturnPresenter.this.saveReturn(dto);
                    ReturnPresenter.this.eventBus.fireEvent(new SaveReturnEvent());
                }

            }
        });
        this.display.getReturnCancelButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                ReturnPresenter.this.returnDTO = null;
                ReturnPresenter.this.eventBus.fireEvent(new CancelReturnEvent());
            }
        });
    }

    private void saveReturn(ReturnDTO returnDTO) {
        rpcService.saveReturn(returnDTO, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                display.cleanInputs();
            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(this.display.asWidget());
    }

    public interface Display {
        Widget asWidget();

        HasSelectHandlers getReturnSaveButton();

        HasSelectHandlers getReturnCancelButton();

        ReturnDTO getReturnDto();

        void cleanInputs();

        void updateSelectedReturn(ReturnDTO var1);

        void loadFiComobox(List<FinInstituteDTO> var1);

        void loadTemplateComboBox(List<TemplateDTO> var1);
    }
}
