//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.task4.client.FiProperties;
import com.task4.client.FiTypeProperties;
import com.task4.client.FileProperties;
import com.task4.client.ReturnProperties;
import com.task4.client.TemplateProperties;
import com.task4.client.presenter.MainPresenter.Display;
import com.task4.shared.FileDTO;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import com.task4.shared.ReturnDTO;
import com.task4.shared.TemplateDTO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class MainPanel extends Composite implements Display {
    TabPanel mainPanel;
    FramedPanel fiFramedPanel;
    FramedPanel fiTypeFramedPanel;
    FramedPanel templateFramePanel;
    FramedPanel returnFramedPanel;
    FramedPanel fileUploadFramedPanel;
    TextButton fiTypeDeleteButon;
    TextButton fiTypeAddButton;
    TextButton fiTypeUpdateButton;
    ListStore<FinInstituteDTO> finInstituteDTOListStore;
    ListStore<FinTypeDTO> listStore;
    ListStore<TemplateDTO> templateDTOListStore;
    ListStore<ReturnDTO> returnDTOListStore;
    ListStore<FileDTO> fileDTOListStore;
    Grid<FinInstituteDTO> finInstituteDTOGrid;
    Grid<FinTypeDTO> typeDTOGrid;
    Grid<TemplateDTO> templateDTOGrid;
    Grid<ReturnDTO> returnDTOGrid;
    Grid<FileDTO> fileDTOGrid;
    TextButton fiAddButton;
    TextButton fiDeleteButton;
    TextButton fiUpdateButton;
    TextButton templateAddButton;
    TextButton templateDeleteButton;
    TextButton templateUpdateButton;
    TextButton returnAddButton;
    TextButton returnDeleteButton;
    TextButton returnUpdateButton;
    TextButton fileDeleteButton;
    TextButton fileDownloadButton;
    TextButton fileUploadButton;

    public MainPanel() {
        this.initComponent();
        this.fiView();
        this.fiTypeView();
        this.templateView();
        this.returnView();
        this.fileUploadView();
    }

    private void fileUploadView() {
        this.fileUploadFramedPanel = new FramedPanel();
        this.fileUploadFramedPanel.setHeadingText("File");
        this.fileUploadFramedPanel.setBodyStyle("background: none; padding: 15px");
        ContentPanel fileUploadContentPanel = new ContentPanel();
        fileUploadContentPanel.setHeadingText("File List");
        fileUploadContentPanel.setHeight(250);
        fileUploadContentPanel.setButtonAlign(BoxLayoutPack.END);
        this.fileDownloadButton = new TextButton("Download");
        this.fileDeleteButton = new TextButton("Delete");
        this.fileUploadButton = new TextButton("Upload File");
        fileUploadContentPanel.addButton(this.fileDownloadButton);
        fileUploadContentPanel.addButton(this.fileDeleteButton);
        fileUploadContentPanel.addButton(this.fileUploadButton);
        FileProperties fileProperties = (FileProperties)GWT.create(FileProperties.class);
        ArrayList columnConfigs = new ArrayList();
        columnConfigs.add(new ColumnConfig(fileProperties.fileName(), 100, "File Name"));
        columnConfigs.add(new ColumnConfig(fileProperties.uploadDate(), 100, "Upload Date"));
        ColumnModel columnModel = new ColumnModel(columnConfigs);
        this.fileDTOListStore = new ListStore<>(new ModelKeyProvider<FileDTO>() {
            @Override
            public String getKey(FileDTO item) {
                return item.getId()+"";
            }
        });
        this.fileDTOGrid = new Grid(this.fileDTOListStore, columnModel);
        fileUploadContentPanel.add(this.fileDTOGrid);
        this.fileUploadFramedPanel.add(fileUploadContentPanel);
        this.mainPanel.add(this.fileUploadFramedPanel, "File");
    }

    private void returnView() {
        this.returnFramedPanel = new FramedPanel();
        this.returnFramedPanel.setHeadingText("Returns");
        this.returnFramedPanel.setBodyStyle("background: none; padding: 15px");
        ContentPanel returnContentPanel = new ContentPanel();
        returnContentPanel.setHeadingText("Returns List");
        returnContentPanel.setHeight(250);
        returnContentPanel.setButtonAlign(BoxLayoutPack.END);
        this.returnAddButton = new TextButton("Add");
        this.returnDeleteButton = new TextButton("Delete");
        this.returnUpdateButton = new TextButton("Update");
        returnContentPanel.addButton(this.returnAddButton);
        returnContentPanel.addButton(this.returnUpdateButton);
        returnContentPanel.addButton(this.returnDeleteButton);
        ReturnProperties returnProperties = (ReturnProperties)GWT.create(ReturnProperties.class);
        ArrayList columnConfigs = new ArrayList();
        columnConfigs.add(new ColumnConfig(returnProperties.finInstituteString(), 100, "FI"));
        columnConfigs.add(new ColumnConfig(returnProperties.templateString(), 100, "template"));
        ColumnModel columnModel = new ColumnModel(columnConfigs);
        this.returnDTOListStore = new ListStore<>(new ModelKeyProvider<ReturnDTO>() {
            @Override
            public String getKey(ReturnDTO item) {
                return item.getId()+"";
            }
        });
        this.returnDTOGrid = new Grid(this.returnDTOListStore, columnModel);
        returnContentPanel.add(this.returnDTOGrid);
        this.returnFramedPanel.add(returnContentPanel);
        this.mainPanel.add(this.returnFramedPanel, "Return");
    }

    private void fiTypeView() {
        this.fiTypeFramedPanel = new FramedPanel();
        this.fiTypeFramedPanel.setHeadingText("Fi Type");
        this.fiTypeFramedPanel.setBodyStyle("background: none; padding: 10px");
        this.fiTypeDeleteButon = new TextButton("Delete");
        this.fiTypeAddButton = new TextButton("Add FI");
        this.fiTypeUpdateButton = new TextButton("update");
        new ContentPanel();
        ContentPanel panel = new ContentPanel();
        panel.setHeadingText("FI Type List");
        panel.setHeight(250);
        panel.setButtonAlign(BoxLayoutPack.END);
        panel.addButton(this.fiTypeAddButton);
        panel.addButton(this.fiTypeDeleteButon);
        panel.addButton(this.fiTypeUpdateButton);
        FiTypeProperties fiTypeProperties = (FiTypeProperties)GWT.create(FiTypeProperties.class);
        ArrayList columnConfigList = new ArrayList();
        columnConfigList.add(new ColumnConfig(fiTypeProperties.name(), 100, "name"));
        columnConfigList.add(new ColumnConfig(fiTypeProperties.code(), 100, "code"));
        ColumnModel cm = new ColumnModel(columnConfigList);
        this.listStore = new ListStore<>(new ModelKeyProvider<FinTypeDTO>() {
            @Override
            public String getKey(FinTypeDTO item) {
                return item.getId()+"";
            }
        });
        this.typeDTOGrid = new Grid(this.listStore, cm);
        panel.add(this.typeDTOGrid);
        this.fiTypeFramedPanel.add(panel);
        this.mainPanel.add(this.fiTypeFramedPanel, "Type");
    }

    private void fiView() {
        this.fiFramedPanel = new FramedPanel();
        this.fiFramedPanel.setHeadingText("Fi registration form");
        this.fiFramedPanel.setBodyStyle("background: none; padding: 15px");
        ContentPanel fiContentPanel = new ContentPanel();
        fiContentPanel.setHeadingText("FI List");
        fiContentPanel.setHeight(250);
        fiContentPanel.setButtonAlign(BoxLayoutPack.END);
        this.fiAddButton = new TextButton("Add");
        this.fiDeleteButton = new TextButton("Delete");
        this.fiUpdateButton = new TextButton("Update");
        fiContentPanel.addButton(this.fiAddButton);
        fiContentPanel.addButton(this.fiUpdateButton);
        fiContentPanel.addButton(this.fiDeleteButton);
        FiProperties fiProperties = (FiProperties)GWT.create(FiProperties.class);
        ArrayList columnConfigs = new ArrayList();
        columnConfigs.add(new ColumnConfig(fiProperties.name(), 100, "name"));
        columnConfigs.add(new ColumnConfig(fiProperties.code(), 100, "code"));
        columnConfigs.add(new ColumnConfig(fiProperties.address(), 100, "address"));
        columnConfigs.add(new ColumnConfig(fiProperties.phone(), 100, "phone"));
        columnConfigs.add(new ColumnConfig(fiProperties.regDate(), 100, "date"));
        ColumnModel cm = new ColumnModel(columnConfigs);
        this.finInstituteDTOListStore = new ListStore<>(new ModelKeyProvider<FinInstituteDTO>() {
            @Override
            public String getKey(FinInstituteDTO item) {
                return item.getId()+"";
            }
        });
        this.finInstituteDTOGrid = new Grid(this.finInstituteDTOListStore, cm);
        fiContentPanel.add(this.finInstituteDTOGrid);
        this.fiFramedPanel.add(fiContentPanel);
        this.mainPanel.add(this.fiFramedPanel, "FI");
    }

    private void templateView() {
        this.templateFramePanel = new FramedPanel();
        this.templateFramePanel.setHeadingText("Template registration form");
        this.templateFramePanel.setBodyStyle("background: none; padding: 15px");
        ContentPanel templateContentPanel = new ContentPanel();
        templateContentPanel.setHeadingText("Template List");
        templateContentPanel.setHeight(250);
        templateContentPanel.setButtonAlign(BoxLayoutPack.END);
        this.templateAddButton = new TextButton("Add");
        this.templateDeleteButton = new TextButton("Delete");
        this.templateUpdateButton = new TextButton("Update");
        templateContentPanel.addButton(this.templateAddButton);
        templateContentPanel.addButton(this.templateUpdateButton);
        templateContentPanel.addButton(this.templateDeleteButton);
        TemplateProperties templateProperties = (TemplateProperties)GWT.create(TemplateProperties.class);
        ArrayList columnConfigs = new ArrayList();
        columnConfigs.add(new ColumnConfig(templateProperties.name(), 100, "name"));
        columnConfigs.add(new ColumnConfig(templateProperties.code(), 100, "code"));
        columnConfigs.add(new ColumnConfig(templateProperties.schedule(), 100, "Schedule"));
        ColumnModel cm = new ColumnModel(columnConfigs);
        this.templateDTOListStore = new ListStore<>(new ModelKeyProvider<TemplateDTO>() {
            @Override
            public String getKey(TemplateDTO item) {
                return item.getId()+"";
            }
        });
        this.templateDTOGrid = new Grid(this.templateDTOListStore, cm);
        templateContentPanel.add(this.templateDTOGrid);
        this.templateFramePanel.add(templateContentPanel);
        this.mainPanel.add(this.templateFramePanel, "Template");
    }

    public void initComponent() {
        this.mainPanel = new TabPanel();
        this.mainPanel.setWidth("50%");
        this.mainPanel.setHeight("100%");
        this.initWidget(this.mainPanel);
    }

    public HasSelectHandlers getFiTypeAddButton() {
        return this.fiTypeAddButton;
    }

    public HasSelectHandlers getFiTypeDeleteButton() {
        return this.fiTypeDeleteButon;
    }

    public HasSelectHandlers getFiTypeUpdateButton() {
        return this.fiTypeUpdateButton;
    }

    public HasSelectHandlers getFiAddButton() {
        return this.fiAddButton;
    }

    public HasSelectHandlers getFiDeleteButton() {
        return this.fiDeleteButton;
    }

    public HasSelectHandlers getFiUpdateButton() {
        return this.fiUpdateButton;
    }

    public HasSelectHandlers getTemplateAddButton() {
        return this.templateAddButton;
    }

    public HasSelectHandlers getTemplateDeleteButton() {
        return this.templateDeleteButton;
    }

    public HasSelectHandlers getTemplateUpdateButton() {
        return this.templateUpdateButton;
    }

    public HasSelectHandlers getReturnAddButton() {
        return this.returnAddButton;
    }

    public HasSelectHandlers getReturnDeleteButton() {
        return this.returnDeleteButton;
    }

    public HasSelectHandlers getReturnUpdateButton() {
        return this.returnUpdateButton;
    }

    public HasSelectHandlers getFileUploadDeleteButton() {
        return this.fileDeleteButton;
    }

    public HasSelectHandlers getFileUploadDownloadButton() {
        return this.fileDownloadButton;
    }

    public HasSelectHandlers getFileUploadButton() {
        return this.fileUploadButton;
    }

    public void loadFiType(List typeList) {
        this.listStore.clear();
        Iterator var2 = typeList.iterator();

        while(var2.hasNext()) {
            Object finType = var2.next();
            this.listStore.add((FinTypeDTO)finType);
        }

    }

    public void loadFies(List fiList) {
        this.finInstituteDTOListStore.clear();
        Iterator var2 = fiList.iterator();

        while(var2.hasNext()) {
            Object fi = var2.next();
            this.finInstituteDTOListStore.add((FinInstituteDTO)fi);
        }

    }

    public void loadTemplates(List templateList) {
        this.templateDTOListStore.clear();
        Iterator var2 = templateList.iterator();

        while(var2.hasNext()) {
            Object temp = var2.next();
            this.templateDTOListStore.add((TemplateDTO)temp);
        }

    }

    public void loadReturns(List returnList) {
        this.returnDTOListStore.clear();
        Iterator var2 = returnList.iterator();

        while(var2.hasNext()) {
            Object o = var2.next();
            this.returnDTOListStore.add((ReturnDTO)o);
        }

    }

    public void loadFiles(List fileList) {
        this.fileDTOListStore.clear();
        Iterator var2 = fileList.iterator();

        while(var2.hasNext()) {
            Object o = var2.next();
            this.fileDTOListStore.add((FileDTO)o);
        }

    }

    public int getFiId() {
        int id = ((FinTypeDTO)this.typeDTOGrid.getSelectionModel().getSelectedItem()).getId();
        return id;
    }

    public FinTypeDTO getFiType() {
        return (FinTypeDTO)this.typeDTOGrid.getSelectionModel().getSelectedItem();
    }

    public FinInstituteDTO getSeceltedFi() {
        return (FinInstituteDTO)this.finInstituteDTOGrid.getSelectionModel().getSelectedItem();
    }

    public TemplateDTO getSelectedTemplate() {
        return (TemplateDTO)this.templateDTOGrid.getSelectionModel().getSelectedItem();
    }

    public ReturnDTO getSelectedReturn() {
        return (ReturnDTO)this.returnDTOGrid.getSelectionModel().getSelectedItem();
    }

    public FileDTO getSelectedFileDTO() {
        return (FileDTO)this.fileDTOGrid.getSelectionModel().getSelectedItem();
    }

    @Override
    public FinTypeDTO getSelectedDTO() {
        return typeDTOGrid.getSelectionModel().getSelectedItem();
    }

    public Widget asWidget() {
        return this;
    }
}
