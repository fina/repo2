package com.task4.client;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.task4.shared.FinTypeDTO;

public interface FiTypeProperties extends PropertyAccess<FinTypeDTO> {
    @Path("code")
    LabelProvider<FinTypeDTO> codeLabel();

    ValueProvider<FinTypeDTO, Integer> id();

    ValueProvider<FinTypeDTO, String> code();

    ValueProvider<FinTypeDTO, String> name();
}
