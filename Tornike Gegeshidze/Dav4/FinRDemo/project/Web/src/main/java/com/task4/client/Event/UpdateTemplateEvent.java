//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.UpdateTemplateEventHandler;
import com.task4.shared.TemplateDTO;

public class UpdateTemplateEvent extends GwtEvent<UpdateTemplateEventHandler> {
    public static final Type<UpdateTemplateEventHandler> TYPE = new Type();
    private final TemplateDTO templateDTO;

    public UpdateTemplateEvent(TemplateDTO typeDTO) {
        this.templateDTO = typeDTO;
    }

    public Type<UpdateTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateTemplateEventHandler updateTemplateEventHandler) {
        updateTemplateEventHandler.onTemplateUpdate(this);
    }

    public TemplateDTO getTemplateDTO() {
        return this.templateDTO;
    }
}
