//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.task4.shared.ScheduleDTO;
import com.task4.shared.TemplateDTO;

public interface TemplateProperties extends PropertyAccess<TemplateDTO> {
    @Path("code")
    LabelProvider<TemplateDTO> codeLabel();

    ValueProvider<TemplateDTO, Integer> id();

    ValueProvider<TemplateDTO, String> code();

    ValueProvider<TemplateDTO, String> name();

    ValueProvider<TemplateDTO, ScheduleDTO> schedule();
}
