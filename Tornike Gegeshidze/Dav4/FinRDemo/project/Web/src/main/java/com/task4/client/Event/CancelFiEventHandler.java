package com.task4.client.Event;

/**
 * Created by tornike on 02-Sep-15.
 */
import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.CancelFiEvent;

public interface CancelFiEventHandler extends EventHandler {
    void onFiCancel(CancelFiEvent var1);
}
