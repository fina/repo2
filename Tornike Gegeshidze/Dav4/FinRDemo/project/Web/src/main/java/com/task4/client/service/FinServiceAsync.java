//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.task4.shared.FinInstituteDTO;
import java.util.List;

public interface FinServiceAsync {
    void addFi(FinInstituteDTO var1, AsyncCallback<Boolean> var2);

    void loadFies(AsyncCallback<List<FinInstituteDTO>> var1);

    void deleteSeletedFi(FinInstituteDTO finInstituteDTO, AsyncCallback<Void> var2);
}
