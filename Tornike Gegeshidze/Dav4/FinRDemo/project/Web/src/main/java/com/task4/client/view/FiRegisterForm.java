//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.dom.ScrollSupport.ScrollMode;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.ParseErrorEvent;
import com.sencha.gxt.widget.core.client.event.ParseErrorEvent.ParseErrorHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.MinDateValidator;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.FiTypeProperties;
import com.task4.client.presenter.FiPresenter.Display;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import java.util.Date;
import java.util.List;

public class FiRegisterForm extends Composite implements Display {
    private TextButton saveFiButton;
    private TextButton cancelFiButton;
    private TextButton updateFiButton;
    private TextField fiCode;
    private TextField fiName;
    private TextField fiAddress;
    private TextField fiMail;
    private TextField fiPhoneNumber;
    DateField fiRegdate;
    ComboBox<FinTypeDTO> finTypeDTOComboBox;
    private ListStore<FinTypeDTO> finInstituteDTOListStore;
    private Window mainWindow;

    public FiRegisterForm() {
        this.initComponent();
    }

    private void initComponent() {
        this.mainWindow = new Window();
        this.initWidget(this.mainWindow);
        this.mainWindow.setHeadingText("Fi");
        this.mainWindow.setPixelSize(600, 500);
        this.mainWindow.setShadow(false);
        this.saveFiButton = new TextButton("Save");
        this.cancelFiButton = new TextButton("cancel");
        this.updateFiButton = new TextButton("Update");
        this.fiCode = new TextField();
        this.fiCode.setAllowBlank(false);
        this.fiAddress = new TextField();
        this.fiMail = new TextField();
        this.fiName = new TextField();
        this.fiName.setAllowBlank(false);
        this.fiPhoneNumber = new TextField();
        FiTypeProperties fiProperties = (FiTypeProperties)GWT.create(FiTypeProperties.class);
        this.finInstituteDTOListStore = new ListStore<>(new ModelKeyProvider<FinTypeDTO>() {
            @Override
            public String getKey(FinTypeDTO item) {
                return item.getId()+"";
            }
        });
        this.finTypeDTOComboBox = new ComboBox(this.finInstituteDTOListStore, fiProperties.codeLabel());
        this.fiRegdate = new DateField();
        this.fiRegdate.addValidator(new MinDateValidator(new Date()));
        this.fiRegdate.addParseErrorHandler(new ParseErrorHandler() {
            public void onParseError(ParseErrorEvent event) {
                Info.display("Parse Error", event.getErrorValue() + " could not be parsed as a date");
            }
        });
        VerticalLayoutContainer vlc = new VerticalLayoutContainer();
        vlc.getScrollSupport().setScrollMode(ScrollMode.AUTO);
        vlc.add(new FieldLabel(this.fiCode, "Fi code"), new VerticalLayoutData(1.0D, -1.0D));
        vlc.add(new FieldLabel(this.fiName, "Fi Name"), new VerticalLayoutData(1.0D, -1.0D));
        vlc.add(new FieldLabel(this.finTypeDTOComboBox, "Fi Type"), new VerticalLayoutData(1.0D, -1.0D));
        vlc.add(new FieldLabel(this.fiAddress, "Address"), new VerticalLayoutData(1.0D, -1.0D));
        vlc.add(new FieldLabel(this.fiPhoneNumber, "Phone"), new VerticalLayoutData(1.0D, -1.0D));
        vlc.add(new FieldLabel(this.fiRegdate, "Date"), new VerticalLayoutData(1.0D, -1.0D));
        FieldSet fieldSet = new FieldSet();
        fieldSet.setHeadingText("User Information");
        fieldSet.setCollapsible(true);
        fieldSet.add(vlc);
        FramedPanel fiRegisterPanel = new FramedPanel();
        fiRegisterPanel.setLayoutData(new BorderLayoutContainer());
        fiRegisterPanel.setHeadingText("");
        fiRegisterPanel.setBodyStyle("background: none; padding: 10px");
        fiRegisterPanel.setWidth(350);
        fiRegisterPanel.add(fieldSet);
        fiRegisterPanel.addButton(this.saveFiButton);
        fiRegisterPanel.addButton(this.updateFiButton);
        fiRegisterPanel.addButton(this.cancelFiButton);
        this.mainWindow.add(fiRegisterPanel);
    }

    public Widget asWidget() {
        return this;
    }

    public void loadFiTypeComboBox(List<FinTypeDTO> finTypeDTOList) {
        this.finInstituteDTOListStore.addAll(finTypeDTOList);
    }

    public HasSelectHandlers getFiSaveButton() {
        return this.saveFiButton;
    }

    public HasSelectHandlers getFiCancelButton() {
        return this.cancelFiButton;
    }

    public FinInstituteDTO getFiDTO() {
        FinInstituteDTO tmpFinInstituteDTO = new FinInstituteDTO();
        tmpFinInstituteDTO.setCode((String)this.fiCode.getValue());
        tmpFinInstituteDTO.setName((String)this.fiName.getValue());
        tmpFinInstituteDTO.setAddress((String)this.fiAddress.getValue());
        tmpFinInstituteDTO.setPhone((String)this.fiPhoneNumber.getValue());
        tmpFinInstituteDTO.setRegDate((Date)this.fiRegdate.getValue());
        tmpFinInstituteDTO.setTypeDTO((FinTypeDTO)this.finTypeDTOComboBox.getCurrentValue());
        return tmpFinInstituteDTO;
    }

    public void clearInputs() {
        this.fiMail.setText("");
        this.fiAddress.setText("");
        this.fiName.setText("");
        this.fiPhoneNumber.setText("");
        this.fiCode.setText("");
        this.fiRegdate.reset();
    }

    public void updateSelectedFi(FinInstituteDTO finInstituteDTO) {
        this.fiCode.setValue(finInstituteDTO.getCode());
        this.fiRegdate.setValue(finInstituteDTO.getRegDate());
        this.fiPhoneNumber.setValue(finInstituteDTO.getPhone());
        this.fiAddress.setValue(finInstituteDTO.getAddress());
        this.finTypeDTOComboBox.setValue(finInstituteDTO.getTypeDTO());
        this.fiName.setValue(finInstituteDTO.getName());
    }
}
