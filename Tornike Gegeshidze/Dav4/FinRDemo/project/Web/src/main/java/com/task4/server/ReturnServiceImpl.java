//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task4.client.service.ReturnService;
import com.task4.entity.ReturnStore;
import com.task4.localservice.ReturnLocalService;
import com.task4.shared.ConverterClass;
import com.task4.shared.ReturnDTO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;

public class ReturnServiceImpl extends RemoteServiceServlet implements ReturnService {
    @EJB
    ReturnLocalService returnLocalService;

    public ReturnServiceImpl() {
    }

    public void saveReturn(ReturnDTO returnDTO) {
        this.returnLocalService.addReturn(ConverterClass.getInstance().convertReturnDTOToEntity(returnDTO));
    }

    public List<ReturnDTO> loadReturns() {
        ArrayList returnDTOArrayList = new ArrayList();
        List returnStores = this.returnLocalService.loadReturn();
        Iterator var3 = returnStores.iterator();

        while(var3.hasNext()) {
            ReturnStore template = (ReturnStore)var3.next();
            returnDTOArrayList.add(ConverterClass.getInstance().convertReturnEntityToDto(template));
        }
        return returnDTOArrayList;
    }

    public void deleteReturn(ReturnDTO returnDTO) throws Exception{
        if(!returnLocalService.returnIsUsed(ConverterClass.getInstance().convertReturnDTOToEntity(returnDTO))){
        this.returnLocalService.deleteReturn(returnDTO.getId());}
        else{
            throw new RuntimeException();
        }
    }


}
