//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.FormPanel.Encoding;
import com.sencha.gxt.widget.core.client.form.FormPanel.Method;
import com.task4.client.presenter.FilePresenter.Display;

public class FileUploadForm extends Composite implements Display {
    private Window window;
    private TextButton uploadFileUploadButton;
    private TextButton cancelFileUploadButton;
    private FileUploadField fileUploadField;
    private FormPanel form;

    public FileUploadForm() {
        this.initComponent();
    }

    private void initComponent() {
        this.fileUploadField = new FileUploadField();
        this.window = new Window();
        this.initWidget(this.window);
        this.window.setPixelSize(500, 300);
        this.window.setModal(false);
        this.window.setBlinkModal(true);
        this.window.setHeadingText("Template Registration Form");
        this.window.setShadow(false);
        this.fileUploadField.setName("Upload File");
        this.fileUploadField.setAllowBlank(false);
        this.fileUploadField.getElement().setPropertyString("Acccept", ".xml");
        this.uploadFileUploadButton = new TextButton("upload");
        this.cancelFileUploadButton = new TextButton("cancel");
        ButtonBar buttonBar = new ButtonBar();
        buttonBar.add(this.uploadFileUploadButton);
        buttonBar.add(this.cancelFileUploadButton);
        VerticalLayoutContainer vlc = new VerticalLayoutContainer();
        vlc.add(new FieldLabel(this.fileUploadField, "File"), new VerticalLayoutData(-18.0D, -1.0D));
        vlc.add(new FieldLabel(buttonBar, "Company"), new VerticalLayoutData(-18.0D, -1.0D));
        this.form = new FormPanel();
        this.form.setAction(GWT.getModuleBaseURL() + "FilesUploadServlet");
        this.form.setEncoding(Encoding.MULTIPART);
        this.form.setMethod(Method.POST);
        this.form.add(vlc, new MarginData(10));
        this.window.add(this.form);
    }

    public Widget asWidget() {
        return this;
    }

    public HasSelectHandlers getuploadFileUploadButton() {
        return this.uploadFileUploadButton;
    }

    public HasSelectHandlers getcancelFileUploadButton() {
        return this.cancelFileUploadButton;
    }

    public FileUploadField getFileUploadField() {
        return this.fileUploadField;
    }

    public FormPanel getFormPanel() {
        return this.form;
    }
}
