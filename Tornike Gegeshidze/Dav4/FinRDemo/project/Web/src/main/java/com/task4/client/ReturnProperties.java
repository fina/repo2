//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.task4.shared.ReturnDTO;

public interface ReturnProperties extends PropertyAccess<ReturnDTO> {
    ValueProvider<ReturnDTO, Integer> id();

    ValueProvider<ReturnDTO, String> finInstituteString();

    ValueProvider<ReturnDTO, String> templateString();
}
