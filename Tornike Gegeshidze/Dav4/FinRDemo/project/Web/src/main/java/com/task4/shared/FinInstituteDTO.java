//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import com.task4.shared.FinTypeDTO;
import java.io.Serializable;
import java.util.Date;

public class FinInstituteDTO implements Serializable {
    private int id;
    private String code;
    private String name;
    private String address;
    private String phone;
    private FinTypeDTO typeDTO;
    private Date regDate;

    public FinInstituteDTO() {
    }

    public FinInstituteDTO(int id, String code, String name, String address, String phone, FinTypeDTO typeDTO, Date date) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.typeDTO = typeDTO;
        this.regDate = date;
    }

    public FinInstituteDTO(int i, String test, String test1, String test2, String test3) {
        this.id = i;
        this.code = test;
        this.address = test1;
        this.name = test2;
        this.phone = test3;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public FinTypeDTO getTypeDTO() {
        return this.typeDTO;
    }

    public void setTypeDTO(FinTypeDTO typeDTO) {
        this.typeDTO = typeDTO;
    }

    public Date getRegDate() {
        return this.regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
}
