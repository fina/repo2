//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

public class TemplateNotFoundException extends Exception {
    public TemplateNotFoundException() {
        super("Template wasnt found");
    }
}
