//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.UpdateReturnEventHandler;
import com.task4.shared.ReturnDTO;

public class UpdateReturnEvent extends GwtEvent<UpdateReturnEventHandler> {
    public static final Type<UpdateReturnEventHandler> TYPE = new Type();
    private final ReturnDTO returnDTO;

    public UpdateReturnEvent(ReturnDTO returnDTO) {
        this.returnDTO = returnDTO;
    }

    public Type<UpdateReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateReturnEventHandler updateFiEventHandler) {
        updateFiEventHandler.onReturnUpdate(this);
    }

    public ReturnDTO getReturnDTO() {
        return this.returnDTO;
    }
}
