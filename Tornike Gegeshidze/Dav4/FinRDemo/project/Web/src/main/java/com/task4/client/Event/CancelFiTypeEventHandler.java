//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.CancelFiTypeEvent;

public interface CancelFiTypeEventHandler extends EventHandler {
    void onFiTypeCancel(CancelFiTypeEvent var1);
}
