//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.CancelFiEvent;
import com.task4.client.Event.SaveFiEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.FinTypeServiceAsync;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import java.util.List;

public class FiPresenter implements Presenter {
    private FiPresenter.Display display;
    private HandlerManager eventBus;
    private FinTypeServiceAsync finTypeServiceAsync;
    private FinServiceAsync finServiceAsync;
    private FinInstituteDTO finInstituteDTO;
    private DialogHideHandler hideHandler = new DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };

    public FiPresenter(FinServiceAsync finServiceAsync, FinTypeServiceAsync typeServiceAsync, HandlerManager eventBus, FiPresenter.Display display) {
        this.finServiceAsync = finServiceAsync;
        this.finTypeServiceAsync = typeServiceAsync;
        this.eventBus = eventBus;
        this.display = display;
        this.bind();
    }

    public FiPresenter(FinServiceAsync finServiceAsync, FinTypeServiceAsync typeServiceAsync, HandlerManager eventBus, FiPresenter.Display display, FinInstituteDTO finInstituteDTO) {
        this.finServiceAsync = finServiceAsync;
        this.finTypeServiceAsync = typeServiceAsync;
        this.eventBus = eventBus;
        this.display = display;
        this.finInstituteDTO = finInstituteDTO;
        display.updateSelectedFi(finInstituteDTO);
        this.bind();
    }

    private void bind() {
        finTypeServiceAsync.loadFiType(new AsyncCallback<List<FinTypeDTO>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(List<FinTypeDTO> result) {
                display.loadFiTypeComboBox(result);
            }
        });
        this.display.getFiSaveButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FinInstituteDTO fi = FiPresenter.this.display.getFiDTO();
                if(FiPresenter.this.finInstituteDTO != null) {
                    FiPresenter.this.finInstituteDTO.setName(fi.getName());
                    FiPresenter.this.finInstituteDTO.setAddress(fi.getAddress());
                    FiPresenter.this.finInstituteDTO.setPhone(fi.getPhone());
                    FiPresenter.this.finInstituteDTO.setCode(fi.getCode());
                    FiPresenter.this.finInstituteDTO.setTypeDTO(fi.getTypeDTO());
                    FiPresenter.this.finInstituteDTO.setRegDate(fi.getRegDate());
                    FiPresenter.this.savefi(FiPresenter.this.finInstituteDTO);
                    FiPresenter.this.eventBus.fireEvent(new SaveFiEvent());
                } else {
                    FiPresenter.this.savefi(fi);
                    FiPresenter.this.eventBus.fireEvent(new SaveFiEvent());
                }

            }
        });
        this.display.getFiCancelButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                FiPresenter.this.finInstituteDTO = null;
                FiPresenter.this.eventBus.fireEvent(new CancelFiEvent());
            }
        });
    }

    private void savefi(FinInstituteDTO finInstituteDTO) {
        finServiceAsync.addFi(finInstituteDTO, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
                messageBox.addDialogHideHandler(hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Boolean result) {
                display.clearInputs();
            }
        });
    }

    public void go(HasWidgets widget) {
        widget.clear();
        widget.add(this.display.asWidget());
    }

    public interface Display {
        Widget asWidget();

        void loadFiTypeComboBox(List<FinTypeDTO> var1);

        HasSelectHandlers getFiSaveButton();

        HasSelectHandlers getFiCancelButton();

        FinInstituteDTO getFiDTO();

        void clearInputs();

        void updateSelectedFi(FinInstituteDTO var1);
    }
}
