//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task4.shared.FinTypeDTO;
import java.util.List;

@RemoteServiceRelativePath("finTypeService")
public interface FinTypeService extends RemoteService {
    void addFiType(FinTypeDTO var1);

    List<FinTypeDTO> loadFiType();

    void deleteFiType(FinTypeDTO finTypeDTO) throws Exception;
}
