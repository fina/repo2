//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import com.task4.shared.FinInstituteDTO;
import com.task4.shared.TemplateDTO;
import java.io.Serializable;

public class ReturnDTO implements Serializable {
    private int id;
    private FinInstituteDTO finInstituteDTO;
    private TemplateDTO templateDTO;
    private String finInstituteString;
    private String templateString;

    public ReturnDTO() {
    }

    public ReturnDTO(int id, FinInstituteDTO dto, TemplateDTO templateDTO) {
        this.setId(id);
        this.setFinInstituteDTO(dto);
        this.setTemplateDTO(templateDTO);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FinInstituteDTO getFinInstituteDTO() {
        return this.finInstituteDTO;
    }

    public void setFinInstituteDTO(FinInstituteDTO finInstituteDTO) {
        this.finInstituteDTO = finInstituteDTO;
    }

    public TemplateDTO getTemplateDTO() {
        return this.templateDTO;
    }

    public void setTemplateDTO(TemplateDTO templateDTO) {
        this.templateDTO = templateDTO;
    }

    public String getFinInstituteString() {
        return this.getFinInstituteDTO().getCode();
    }

    public String getTemplateString() {
        return this.getTemplateDTO().getCode();
    }
}
