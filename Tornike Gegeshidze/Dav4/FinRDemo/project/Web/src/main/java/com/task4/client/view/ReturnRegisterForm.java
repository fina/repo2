//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.task4.client.FiProperties;
import com.task4.client.TemplateProperties;
import com.task4.client.presenter.ReturnPresenter.Display;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.ReturnDTO;
import com.task4.shared.TemplateDTO;
import java.util.List;

public class ReturnRegisterForm extends Composite implements Display {
    private Window window;
    TextButton saveReturnButton;
    TextButton cancelReturnButton;
    ComboBox<FinInstituteDTO> finInstituteDTOComboBox;
    ListStore<FinInstituteDTO> finInstituteDTOListStore;
    ComboBox<TemplateDTO> templateDTOComboBox;
    ListStore<TemplateDTO> templateDTOListStore;

    public ReturnRegisterForm() {
        this.initComponent();
    }

    private void initComponent() {
        this.window = new Window();
        this.initWidget(this.window);
        this.window.setPixelSize(500, 300);
        this.window.setModal(false);
        this.window.setBlinkModal(true);
        this.window.setHeadingText("Return Registration Form");
        this.window.setShadow(false);
        FiProperties fiProperties = (FiProperties)GWT.create(FiProperties.class);
        TemplateProperties templateProperties = (TemplateProperties)GWT.create(TemplateProperties.class);
        this.finInstituteDTOListStore = new ListStore<>(new ModelKeyProvider<FinInstituteDTO>() {
            @Override
            public String getKey(FinInstituteDTO item) {
                return item.getId()+"";
            }
        });
        this.templateDTOListStore = new ListStore<>(new ModelKeyProvider<TemplateDTO>() {
            @Override
            public String getKey(TemplateDTO item) {
                return item.getId()+"";
            }
        });
        this.finInstituteDTOComboBox = new ComboBox(this.finInstituteDTOListStore, fiProperties.codeLabel());
        this.templateDTOComboBox = new ComboBox(this.templateDTOListStore, templateProperties.codeLabel());
        VBoxLayoutContainer c = new VBoxLayoutContainer();
        c.setPadding(new Padding(5));
        c.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCHMAX);
        c.add(new FieldLabel(this.finInstituteDTOComboBox, "Select Fi"), new BoxLayoutData(new Margins(0, 0, 5, 0)));
        c.add(new FieldLabel(this.templateDTOComboBox, "select template"), new BoxLayoutData(new Margins(0, 0, 90, 0)));
        FieldSet fieldSet = new FieldSet();
        fieldSet.setHeadingText("User Information");
        fieldSet.setCollapsible(true);
        fieldSet.add(c);
        FramedPanel returnRegisterPanel = new FramedPanel();
        returnRegisterPanel.setLayoutData(new BorderLayoutContainer());
        returnRegisterPanel.setHeadingText("");
        returnRegisterPanel.setBodyStyle("background: none; padding: 10px");
        returnRegisterPanel.setWidth(350);
        returnRegisterPanel.add(fieldSet);
        this.saveReturnButton = new TextButton("Save");
        this.cancelReturnButton = new TextButton("Cancel");
        returnRegisterPanel.addButton(this.saveReturnButton);
        returnRegisterPanel.addButton(this.cancelReturnButton);
        this.window.add(returnRegisterPanel);
    }

    public Widget asWidget() {
        return this;
    }

    public HasSelectHandlers getReturnSaveButton() {
        return this.saveReturnButton;
    }

    public HasSelectHandlers getReturnCancelButton() {
        return this.cancelReturnButton;
    }

    public ReturnDTO getReturnDto() {
        ReturnDTO dto = new ReturnDTO();
        dto.setFinInstituteDTO((FinInstituteDTO)this.finInstituteDTOComboBox.getCurrentValue());
        dto.setTemplateDTO((TemplateDTO)this.templateDTOComboBox.getCurrentValue());
        return dto;
    }

    public void cleanInputs() {
        this.finInstituteDTOComboBox.setValue(this.finInstituteDTOComboBox.getOriginalValue());
        this.templateDTOComboBox.setValue(this.templateDTOComboBox.getOriginalValue());
    }

    public void updateSelectedReturn(ReturnDTO returnDTO) {
        this.templateDTOComboBox.setValue(returnDTO.getTemplateDTO());
        this.finInstituteDTOComboBox.setValue(returnDTO.getFinInstituteDTO());
    }

    public void loadFiComobox(List<FinInstituteDTO> finInstituteDTOs) {
        this.finInstituteDTOListStore.addAll(finInstituteDTOs);
    }

    public void loadTemplateComboBox(List<TemplateDTO> templateDTOs) {
        this.templateDTOListStore.addAll(templateDTOs);
    }
}
