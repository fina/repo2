//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.DeleteFileEventHandler;

public class DeleteFileEvent extends GwtEvent<DeleteFileEventHandler> {
    public static final Type<DeleteFileEventHandler> TYPE = new Type();

    public DeleteFileEvent() {
    }

    public Type<DeleteFileEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteFileEventHandler handler) {
        handler.onFileDelete(this);
    }
}
