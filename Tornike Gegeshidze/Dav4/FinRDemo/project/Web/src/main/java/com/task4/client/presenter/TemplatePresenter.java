//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent.HasSelectHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.task4.client.Event.CancelTemplateEvent;
import com.task4.client.Event.SaveTemplateEvent;
import com.task4.client.presenter.Presenter;
import com.task4.client.service.TemplateServiceAsync;
import com.task4.shared.TemplateDTO;

public class TemplatePresenter implements Presenter {
    private TemplatePresenter.Display display;
    private HandlerManager eventBus;
    private TemplateServiceAsync rpcService;
    private TemplateDTO templateDTO;
    private DialogHideHandler hideHandler = new DialogHideHandler() {
        public void onDialogHide(DialogHideEvent event) {
            String message = Format.substitute("The \'{0}\' button was pressed", event.getHideButton());
            Info.display("MessageBox", message);
        }
    };

    public TemplatePresenter(TemplateServiceAsync rpcService, HandlerManager eventBus, TemplatePresenter.Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        this.bind();
    }

    public TemplatePresenter(TemplateServiceAsync rpcService, HandlerManager eventBus, TemplatePresenter.Display display, TemplateDTO templateDTO) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        this.templateDTO = templateDTO;
        display.updateSelectedTemplate(templateDTO);
        this.bind();
    }

    private void bind() {
        this.display.getTemplateAddButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                TemplateDTO dto = TemplatePresenter.this.display.getTemplateDTO();
                if(TemplatePresenter.this.templateDTO != null) {
                    TemplatePresenter.this.templateDTO.setName(dto.getName());
                    TemplatePresenter.this.templateDTO.setCode(dto.getCode());
                    TemplatePresenter.this.templateDTO.setSchedule(dto.getSchedule());
                    TemplatePresenter.this.saveTemplate(TemplatePresenter.this.templateDTO);
                    TemplatePresenter.this.eventBus.fireEvent(new SaveTemplateEvent());
                } else {
                    TemplatePresenter.this.saveTemplate(dto);
                    TemplatePresenter.this.eventBus.fireEvent(new SaveTemplateEvent());
                }

            }
        });
        this.display.getcancelTemplateButton().addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                TemplatePresenter.this.templateDTO = null;
                TemplatePresenter.this.eventBus.fireEvent(new CancelTemplateEvent());
            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(this.display.asWidget());
    }

    private void saveTemplate(TemplateDTO dto) {
        rpcService.addTemplate(dto, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                AlertMessageBox messageBox = new AlertMessageBox("Alert", caught.getMessage());
                messageBox.addDialogHideHandler(TemplatePresenter.this.hideHandler);
                messageBox.show();
            }

            @Override
            public void onSuccess(Void result) {
                display.clearInputs();
            }
        });
    }

    public interface Display {
        Widget asWidget();

        HasSelectHandlers getTemplateAddButton();

        HasSelectHandlers getcancelTemplateButton();

        void updateSelectedTemplate(TemplateDTO var1);

        TemplateDTO getTemplateDTO();

        void clearInputs();
    }
}
