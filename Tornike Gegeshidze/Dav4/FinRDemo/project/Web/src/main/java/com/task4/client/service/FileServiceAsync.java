//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.task4.shared.FileDTO;
import java.util.List;

public interface FileServiceAsync {
    void addFile(FileDTO var1, AsyncCallback<Void> var2);

    void loadFiles(AsyncCallback<List<FileDTO>> var1);

    void deleteFile(int var1, AsyncCallback<Void> var2);

    void checkFileByName(String var1, AsyncCallback<Boolean> var2);
}
