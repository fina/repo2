package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.AddReturnEventHandler;

public class AddReturnEvent extends GwtEvent<AddReturnEventHandler> {
    public static final Type<AddReturnEventHandler> TYPE = new Type();

    public AddReturnEvent() {
    }

    public Type<AddReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(AddReturnEventHandler addReturnEventHandler) {
        addReturnEventHandler.onReturnAdd(this);
    }
}
