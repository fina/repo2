//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.task4.entity.FileStore;
import com.task4.localservice.FileLocalService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet(
        name = "FileDownloadServlet"
)
public class FileDownloadServlet extends HttpServlet {
    @EJB
    FileLocalService fileLocalService;

    public FileDownloadServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStr = request.getParameter("id");
        int id = Integer.parseInt(idStr);
        FileStore fileStore = this.fileLocalService.getFile(id);
        response.setContentType("text/xml");
        response.setHeader("Content-disposition", "attachment; filename=" + fileStore.getFileName());
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(fileStore.getFileContent());
        outputStream.flush();
        outputStream.close();
    }
}
