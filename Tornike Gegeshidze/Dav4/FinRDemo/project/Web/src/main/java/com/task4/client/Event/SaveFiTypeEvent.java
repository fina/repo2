//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.SaveFiTypeEventHandler;

public class SaveFiTypeEvent extends GwtEvent<SaveFiTypeEventHandler> {
    public static final Type<SaveFiTypeEventHandler> TYPE = new Type();

    public SaveFiTypeEvent() {
    }

    public Type<SaveFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(SaveFiTypeEventHandler saveFiTypeEventHandler) {
        saveFiTypeEventHandler.onFiTypeSave(this);
    }
}
