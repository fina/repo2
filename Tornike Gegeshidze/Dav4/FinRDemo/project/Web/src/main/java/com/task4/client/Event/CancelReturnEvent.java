//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.CancelReturnEventHandler;

public class CancelReturnEvent extends GwtEvent<CancelReturnEventHandler> {
    public static final Type<CancelReturnEventHandler> TYPE = new Type();

    public CancelReturnEvent() {
    }

    public Type<CancelReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CancelReturnEventHandler cancelReturnEventHandler) {
        cancelReturnEventHandler.onReturnCancel(this);
    }
}
