//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import com.task4.entity.*;
import com.task4.shared.FileDTO;
import com.task4.shared.FinInstituteDTO;
import com.task4.shared.FinTypeDTO;
import com.task4.shared.ScheduleDTO;
import com.task4.shared.TemplateDTO;

public class ConverterClass {
    private static ConverterClass ourInstance = new ConverterClass();

    public static ConverterClass getInstance() {
        return ourInstance;
    }

    private ConverterClass() {
    }

    public FinInstitute convertFinDtoToEntity(FinInstituteDTO fiDTO) {
        FinInstitute fi = new FinInstitute();
        fi.setId(fiDTO.getId());
        fi.setName(fiDTO.getName());
        fi.setCode(fiDTO.getCode());
        fi.setAddress(fiDTO.getAddress());
        fi.setPhone(fiDTO.getPhone());
        fi.setRegDate(fiDTO.getRegDate());
        fi.setFinType(this.convertFinTypeDtoToEntity(fiDTO.getTypeDTO()));
        return fi;
    }

    public FileDTO convertFileStoreToDTO(FileStore fileStore) {
        FileDTO fileDTO = new FileDTO();
        fileDTO.setId(fileStore.getId());
        fileDTO.setTemplateDTO(getInstance().convertTemplateEntityToDto(fileStore.getTemplate()));
        fileDTO.setFinInstituteDTO(getInstance().convertFinEntityToDto(fileStore.getFinInstitute()));
        fileDTO.setFileName(fileStore.getFileName());
        fileDTO.setScheduleTime(fileStore.getScheduleTime());
        fileDTO.setUploadDate(fileStore.getUploadDate());
        fileDTO.setFileContent(fileStore.getFileContent());
        return fileDTO;
    }

    public FileStore convertFileDTOToStore(FileDTO fileDTO) {
        FileStore fileStore = new FileStore();
        fileStore.setId(fileDTO.getId());
        fileStore.setTemplate(getInstance().convertTemplateDTOToEntity(fileDTO.getTemplateDTO()));
        fileStore.setFinInstitute(getInstance().convertFinDtoToEntity(fileDTO.getFinInstituteDTO()));
        fileStore.setFileName(fileDTO.getFileName());
        fileStore.setScheduleTime(fileDTO.getScheduleTime());
        fileStore.setUploadDate(fileDTO.getUploadDate());
        fileStore.setFileContent(fileDTO.getFileContent());
        return fileStore;
    }

    public FinType convertFinTypeDtoToEntity(FinTypeDTO typeDTO) {
        FinType type = new FinType();
        type.setId(typeDTO.getId());
        type.setName(typeDTO.getName());
        type.setCode(typeDTO.getCode());
        return type;
    }

    public FinTypeDTO convertFinTypeToDTO(FinType type) {
        FinTypeDTO tmpType = new FinTypeDTO();
        tmpType.setId(type.getId());
        tmpType.setName(type.getName());
        tmpType.setCode(type.getCode());
        return tmpType;
    }

    public FinInstituteDTO convertFinEntityToDto(FinInstitute fi) {
        FinInstituteDTO tmpFi = new FinInstituteDTO();
        tmpFi.setId(fi.getId());
        tmpFi.setName(fi.getName());
        tmpFi.setCode(fi.getCode());
        tmpFi.setAddress(fi.getAddress());
        tmpFi.setPhone(fi.getPhone());
        tmpFi.setRegDate(fi.getRegDate());
        tmpFi.setTypeDTO(this.convertFinTypeToDTO(fi.getFinType()));
        return tmpFi;
    }

    public TemplateDTO convertTemplateEntityToDto(Template template) {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setId(template.getId());
        templateDTO.setName(template.getName());
        templateDTO.setCode(template.getCode());
        templateDTO.setSchedule(ScheduleDTO.valueOf(template.getSchedule()));
        return templateDTO;
    }

    public Template convertTemplateDTOToEntity(TemplateDTO templateDTO) {
        Template template = new Template();
        template.setId(templateDTO.getId());
        template.setCode(templateDTO.getCode());
        template.setName(templateDTO.getName());
        template.setSchedule(templateDTO.getSchedule().toString());
        return template;
    }
    public ReturnStore convertReturnDTOToEntity(ReturnDTO returnDTO) {
        ReturnStore aReturnStore = new ReturnStore();
        aReturnStore.setId(returnDTO.getId());
        aReturnStore.setInstitution(ConverterClass.getInstance().convertFinDtoToEntity(returnDTO.getFinInstituteDTO()));
        aReturnStore.setTemplate(ConverterClass.getInstance().convertTemplateDTOToEntity(returnDTO.getTemplateDTO()));
        return aReturnStore;
    }

    public ReturnDTO convertReturnEntityToDto(ReturnStore aReturnStore) {
        ReturnDTO returnDTO = new ReturnDTO();
        returnDTO.setId(aReturnStore.getId());
        returnDTO.setFinInstituteDTO(ConverterClass.getInstance().convertFinEntityToDto(aReturnStore.getInstitution()));
        returnDTO.setTemplateDTO(ConverterClass.getInstance().convertTemplateEntityToDto(aReturnStore.getTemplate()));
        return returnDTO;
    }

}
