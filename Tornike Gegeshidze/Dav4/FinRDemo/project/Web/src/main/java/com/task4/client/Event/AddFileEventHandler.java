
package com.task4.client.Event;

import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.AddFileEvent;

public interface AddFileEventHandler extends EventHandler {
    void onFileAdd(AddFileEvent var1);
}
