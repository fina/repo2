//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.task4.shared.FinInstituteDTO;
import java.util.Date;

public interface FiProperties extends PropertyAccess<FinInstituteDTO> {
    @Path("code")
    LabelProvider<FinInstituteDTO> codeLabel();

    ValueProvider<FinInstituteDTO, Integer> id();

    ValueProvider<FinInstituteDTO, String> name();

    ValueProvider<FinInstituteDTO, String> code();

    ValueProvider<FinInstituteDTO, String> address();

    ValueProvider<FinInstituteDTO, String> phone();

    ValueProvider<FinInstituteDTO, Date> regDate();
}
