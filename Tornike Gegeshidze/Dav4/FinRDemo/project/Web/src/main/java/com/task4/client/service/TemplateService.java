//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task4.shared.TemplateDTO;
import java.util.List;

@RemoteServiceRelativePath("templateService")
public interface TemplateService extends RemoteService {
    void addTemplate(TemplateDTO var1);

    List<TemplateDTO> loadTemplate();

    void deleteTemplate(TemplateDTO templateDTO);
}
