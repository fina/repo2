//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.CancelFiTypeEventHandler;

public class CancelFiTypeEvent extends GwtEvent<CancelFiTypeEventHandler> {
    public static final Type<CancelFiTypeEventHandler> TYPE = new Type();

    public CancelFiTypeEvent() {
    }

    public Type<CancelFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CancelFiTypeEventHandler cancelFiTypeEventHandler) {
        cancelFiTypeEventHandler.onFiTypeCancel(this);
    }
}
