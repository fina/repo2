//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.SaveFiEventHandler;

public class SaveFiEvent extends GwtEvent<SaveFiEventHandler> {
    public static final Type<SaveFiEventHandler> TYPE = new Type();

    public SaveFiEvent() {
    }

    public Type<SaveFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(SaveFiEventHandler addFiEventHandler) {
        addFiEventHandler.onFiTypeSave(this);
    }
}
