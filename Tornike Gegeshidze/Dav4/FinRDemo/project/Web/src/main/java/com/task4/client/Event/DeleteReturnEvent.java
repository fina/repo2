//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.DeleteReturnEventHandler;

public class DeleteReturnEvent extends GwtEvent<DeleteReturnEventHandler> {
    public static final Type<DeleteReturnEventHandler> TYPE = new Type();

    public DeleteReturnEvent() {
    }

    public Type<DeleteReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteReturnEventHandler handler) {
        handler.onReturnDelete(this);
    }
}
