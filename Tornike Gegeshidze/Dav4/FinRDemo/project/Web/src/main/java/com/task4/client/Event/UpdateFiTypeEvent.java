//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.UpdateFiTypeEventHandler;
import com.task4.shared.FinTypeDTO;

public class UpdateFiTypeEvent extends GwtEvent<UpdateFiTypeEventHandler> {
    public static final Type<UpdateFiTypeEventHandler> TYPE = new Type();
    private final FinTypeDTO finTypeDTO;

    public UpdateFiTypeEvent(FinTypeDTO typeDTO) {
        this.finTypeDTO = typeDTO;
    }

    public Type<UpdateFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateFiTypeEventHandler updateFiTypeEventHandler) {
        updateFiTypeEventHandler.onFiTypeUpdate(this);
    }

    public FinTypeDTO getFinTypeDTO() {
        return this.finTypeDTO;
    }
}
