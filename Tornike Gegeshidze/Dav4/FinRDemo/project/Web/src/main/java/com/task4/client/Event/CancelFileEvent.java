//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.CancelFileEventHandler;

public class CancelFileEvent extends GwtEvent<CancelFileEventHandler> {
    public static final Type<CancelFileEventHandler> TYPE = new Type();

    public CancelFileEvent() {
    }

    public Type<CancelFileEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CancelFileEventHandler cancelFileEventHandler) {
        cancelFileEventHandler.onFileCancel(this);
    }
}
