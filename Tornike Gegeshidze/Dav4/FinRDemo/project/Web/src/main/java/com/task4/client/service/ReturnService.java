//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task4.shared.ReturnDTO;
import java.util.List;

@RemoteServiceRelativePath("retServie")
public interface ReturnService extends RemoteService {
    void saveReturn(ReturnDTO var1);

    List<ReturnDTO> loadReturns();

    void deleteReturn(ReturnDTO returnDTO) throws Exception;
}
