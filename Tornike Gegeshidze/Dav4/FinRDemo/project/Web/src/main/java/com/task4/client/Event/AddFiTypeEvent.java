package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.AddFiTypeEventHandler;

public class AddFiTypeEvent extends GwtEvent<AddFiTypeEventHandler> {
    public static final Type<AddFiTypeEventHandler> TYPE = new Type();

    public AddFiTypeEvent() {
    }

    public Type<AddFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(AddFiTypeEventHandler addFiEventHandler) {
        addFiEventHandler.onFiTypeAdd(this);
    }
}
