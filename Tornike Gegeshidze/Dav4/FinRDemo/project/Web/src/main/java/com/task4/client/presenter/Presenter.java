package com.task4.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

public interface Presenter {
    void go(HasWidgets var1);
}
