//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.task4.entity.FileStore;
import com.task4.localservice.FiLocalService;
import com.task4.localservice.FileLocalService;
import com.task4.localservice.TemplateLocalService;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

@WebServlet(
        name = "FilesUploadServlet"
)
public class FilesUploadServlet extends HttpServlet {
    @EJB
    private FiLocalService fiLocalService;
    @EJB
    private TemplateLocalService templateLocalService;
    @EJB
    private FileLocalService fileLocalService;

    public FilesUploadServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = "";
        Object fileContent = null;
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload servletFileUpload = new ServletFileUpload(factory);

        try {
            List item = servletFileUpload.parseRequest(request);
            Iterator iterator = item.iterator();

            while(iterator.hasNext()) {
                FileItem fileItem = (FileItem)iterator.next();
                if(!fileItem.isFormField()) {
                    fileName = fileItem.getName();
                    if(fileName != null) {
                        fileName = FilenameUtils.getName(fileName);
                    }

                    String contentType = fileItem.getContentType();
                    InputStream inputStream = fileItem.getInputStream();
                    byte[] fileContent1 = new byte[inputStream.available()];
                    inputStream.read(fileContent1);
                    fileName = fileItem.getName();
                    FileStore fileStore = this.createFileStore(fileName, fileContent1);
                    this.fileLocalService.addFile(fileStore);
                    response.setStatus(201);
                    response.getWriter().print("The file was created successfully.");
                }
            }
        } catch (Exception var13) {
            ;
        }

    }

    private FileStore createFileStore(String fileName, byte[] fileContent) {
        FileStore fileStore = null;
        String[] fileNameParts = fileName.split("\\.");
        fileStore = new FileStore();
        fileStore.setFileName(fileName);
        fileStore.setFileContent(fileContent);
        fileStore.setFinInstitute(this.fiLocalService.getInstitutionByCode(fileNameParts[0]));
        fileStore.setTemplate(this.templateLocalService.getTemplateByCode(fileNameParts[1]));
        fileStore.setScheduleTime(fileNameParts[2]);
        fileStore.setUploadDate(new Date());
        return fileStore;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
