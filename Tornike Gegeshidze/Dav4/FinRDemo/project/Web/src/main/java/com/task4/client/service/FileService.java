//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task4.shared.FiNotFoundException;
import com.task4.shared.FileDTO;
import com.task4.shared.ReturnNotFoundException;
import com.task4.shared.TemplateNotFoundException;
import java.util.List;

@RemoteServiceRelativePath("fileService")
public interface FileService extends RemoteService {
    void addFile(FileDTO var1);

    List<FileDTO> loadFiles();

    void deleteFile(int var1);

    boolean checkFileByName(String var1) throws FiNotFoundException, TemplateNotFoundException, ReturnNotFoundException;
}
