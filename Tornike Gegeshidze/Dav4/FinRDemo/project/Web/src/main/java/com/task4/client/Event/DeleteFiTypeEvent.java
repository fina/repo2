//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.DeleteFiTypeEventHandler;

public class DeleteFiTypeEvent extends GwtEvent<DeleteFiTypeEventHandler> {
    public static final Type<DeleteFiTypeEventHandler> TYPE = new Type();

    public DeleteFiTypeEvent() {
    }

    public Type<DeleteFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteFiTypeEventHandler handler) {
        handler.onFiTypeDelete(this);
    }
}
