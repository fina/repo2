//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.CancelTemplateEvent;

public interface CancelTemplateEventHandler extends EventHandler {
    void onTemplateCancel(CancelTemplateEvent var1);
}
