package com.task4.client.Event;

/**
 * Created by tornike on 02-Sep-15.
 */
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.AddTemplateEventHandler;

public class AddTemplateEvent extends GwtEvent<AddTemplateEventHandler> {
    public static final Type<AddTemplateEventHandler> TYPE = new Type();

    public AddTemplateEvent() {
    }

    public Type<AddTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(AddTemplateEventHandler addTemplateEventHandler) {
        addTemplateEventHandler.onTemplateAdd(this);
    }
}