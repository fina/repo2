//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.SaveFileEventHandler;

public class SaveFileEvent extends GwtEvent<SaveFileEventHandler> {
    public static final Type<SaveFileEventHandler> TYPE = new Type();

    public SaveFileEvent() {
    }

    public Type<SaveFileEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(SaveFileEventHandler addFiEventHandler) {
        addFiEventHandler.onFileSave(this);
    }
}