//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.task4.shared.FinTypeDTO;
import java.util.List;

public interface FinTypeServiceAsync {
    void addFiType(FinTypeDTO var1, AsyncCallback<Void> var2);

    void loadFiType(AsyncCallback<List<FinTypeDTO>> var1);

    void deleteFiType(FinTypeDTO finTypeDTO, AsyncCallback<Void> var2);
}
