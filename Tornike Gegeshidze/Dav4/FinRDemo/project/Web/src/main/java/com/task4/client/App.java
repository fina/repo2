package com.task4.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;
import com.task4.client.AppController;
import com.task4.client.service.FileService;
import com.task4.client.service.FileServiceAsync;
import com.task4.client.service.FinService;
import com.task4.client.service.FinServiceAsync;
import com.task4.client.service.FinTypeService;
import com.task4.client.service.FinTypeServiceAsync;
import com.task4.client.service.ReturnService;
import com.task4.client.service.ReturnServiceAsync;
import com.task4.client.service.TemplateService;
import com.task4.client.service.TemplateServiceAsync;

public class App implements EntryPoint {
  public App() {
  }

  public void onModuleLoad() {
    FinTypeServiceAsync finTypeServiceAsync = (FinTypeServiceAsync)GWT.create(FinTypeService.class);
    FinServiceAsync finServiceAsync = (FinServiceAsync)GWT.create(FinService.class);
    TemplateServiceAsync templateServiceAsync = (TemplateServiceAsync)GWT.create(TemplateService.class);
    ReturnServiceAsync returnServiceAsync = (ReturnServiceAsync)GWT.create(ReturnService.class);
    FileServiceAsync fileServiceAsync = (FileServiceAsync)GWT.create(FileService.class);
    HandlerManager eventBus = new HandlerManager((Object)null);
    AppController appViewer = new AppController(eventBus, finTypeServiceAsync, finServiceAsync, templateServiceAsync, returnServiceAsync, fileServiceAsync);
    appViewer.go(RootPanel.get());
  }
}
