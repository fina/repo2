//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task4.client.service.FileService;
import com.task4.entity.FileStore;
import com.task4.localservice.FileLocalService;
import com.task4.shared.ConverterClass;
import com.task4.shared.FiNotFoundException;
import com.task4.shared.FileDTO;
import com.task4.shared.ReturnNotFoundException;
import com.task4.shared.TemplateNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;

public class FileServiceImpl extends RemoteServiceServlet implements FileService {
    @EJB
    FileLocalService fileLocalService;

    public FileServiceImpl() {
    }

    public void addFile(FileDTO fileDTO) {
        this.fileLocalService.addFile(ConverterClass.getInstance().convertFileDTOToStore(fileDTO));
    }

    public List<FileDTO> loadFiles() {
        ArrayList fileDTOs = new ArrayList();
        List fileStores = null;
        fileStores = this.fileLocalService.getFiles();
        Iterator var3 = fileStores.iterator();

        while(var3.hasNext()) {
            FileStore fileStore = (FileStore)var3.next();
            fileDTOs.add(ConverterClass.getInstance().convertFileStoreToDTO(fileStore));
        }

        return fileDTOs;
    }

    public void deleteFile(int id) {
        this.fileLocalService.deleteFile(id);
    }

    public boolean checkFileByName(String fileName) throws FiNotFoundException, TemplateNotFoundException, ReturnNotFoundException {
        String[] fileNameParts = fileName.split("\\.");
        if(!this.fileLocalService.checkFiCode(fileNameParts[0])) {
            throw new FiNotFoundException();
        } else if(!this.fileLocalService.checkTemplateCode(fileNameParts[1])) {
            throw new TemplateNotFoundException();
        } else if(!this.fileLocalService.checkForReturn(fileNameParts[0], fileNameParts[1])) {
            throw new ReturnNotFoundException();
        } else {
            return true;
        }
    }
}
