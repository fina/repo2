//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.task4.client.service.FinTypeService;
import com.task4.entity.FinType;
import com.task4.localservice.FiTypeLocalService;
import com.task4.shared.ConverterClass;
import com.task4.shared.FinTypeDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;

public class FinTypeServiceImpl extends RemoteServiceServlet implements FinTypeService {
    @EJB
    private FiTypeLocalService typeLocalService;

    public FinTypeServiceImpl() {
    }

    private FinType convertModel(FinTypeDTO typeDTO) {
        FinType type = new FinType();
        type.setId(typeDTO.getId());
        type.setName(typeDTO.getName());
        type.setCode(typeDTO.getCode());
        return type;
    }

    private FinTypeDTO convertModel2(FinType typee) {
        FinTypeDTO type = new FinTypeDTO();
        type.setId(typee.getId());
        type.setName(typee.getName());
        type.setCode(typee.getCode());
        return type;
    }

    public void addFiType(FinTypeDTO finTypeDTO) {
        FinType tmpFi = ConverterClass.getInstance().convertFinTypeDtoToEntity(finTypeDTO);

        try {
            this.typeLocalService.addFiType(tmpFi);
        } catch (SQLException var4) {
            var4.getSQLState();
            throw new RuntimeException();
        }
    }

    public List<FinTypeDTO> loadFiType() {
        List finTypes = this.typeLocalService.loadFiTpe();
        ArrayList finTypeDTOs = new ArrayList();
        Iterator var3 = finTypes.iterator();
        while(var3.hasNext()) {
            FinType a = (FinType)var3.next();
            finTypeDTOs.add(ConverterClass.getInstance().convertFinTypeToDTO(a));
        }
        return finTypeDTOs;
    }

    public void deleteFiType(FinTypeDTO finTypeDTO) throws Exception {
        if(!typeLocalService.typeIsUsed(ConverterClass.getInstance().convertFinTypeDtoToEntity(finTypeDTO))){
            typeLocalService.deleteFiType(finTypeDTO.getId());}
        else{
            throw new RuntimeException();
        }
    }
}
