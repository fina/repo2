//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.SaveReturnEventHandler;

public class SaveReturnEvent extends GwtEvent<SaveReturnEventHandler> {
    public static final Type<SaveReturnEventHandler> TYPE = new Type();

    public SaveReturnEvent() {
    }

    public Type<SaveReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(SaveReturnEventHandler addFiEventHandler) {
        addFiEventHandler.onReturnSave(this);
    }
}
