//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import com.task4.shared.FinInstituteDTO;
import com.task4.shared.TemplateDTO;
import java.io.Serializable;
import java.util.Date;

public class FileDTO implements Serializable {
    private int id;
    private String fileName;
    private TemplateDTO templateDTO;
    private FinInstituteDTO finInstituteDTO;
    private String scheduleTime;
    private Date uploadDate;
    private byte[] fileContent;

    public FileDTO(int id, String fileName, TemplateDTO templateDTO, FinInstituteDTO finInstituteDTO, String scheduleTime, Date uploadDate, byte[] fileContent) {
        this.fileName = fileName;
        this.templateDTO = templateDTO;
        this.id = id;
        this.scheduleTime = scheduleTime;
        this.finInstituteDTO = finInstituteDTO;
        this.uploadDate = uploadDate;
        this.fileContent = fileContent;
    }

    public FileDTO() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public TemplateDTO getTemplateDTO() {
        return this.templateDTO;
    }

    public void setTemplateDTO(TemplateDTO templateDTO) {
        this.templateDTO = templateDTO;
    }

    public FinInstituteDTO getFinInstituteDTO() {
        return this.finInstituteDTO;
    }

    public void setFinInstituteDTO(FinInstituteDTO finInstituteDTO) {
        this.finInstituteDTO = finInstituteDTO;
    }

    public Date getUploadDate() {
        return this.uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public byte[] getFileContent() {
        return this.fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getScheduleTime() {
        return this.scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }
}
