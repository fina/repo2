//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.task4.shared.FinInstituteDTO;
import java.util.List;

@RemoteServiceRelativePath("finServie")
public interface FinService extends RemoteService {
    boolean addFi(FinInstituteDTO var1);

    List<FinInstituteDTO> loadFies();

    void deleteSeletedFi(FinInstituteDTO finInstituteDTO) throws Exception;

}
