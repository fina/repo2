//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import com.task4.shared.ScheduleDTO;
import java.io.Serializable;

public class TemplateDTO implements Serializable {
    private int id;
    private String code;
    private String name;
    private ScheduleDTO schedule;

    public TemplateDTO() {
    }

    public TemplateDTO(int id, String name, String code, ScheduleDTO schedule) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.schedule = schedule;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ScheduleDTO getSchedule() {
        return this.schedule;
    }

    public void setSchedule(ScheduleDTO schedule) {
        this.schedule = schedule;
    }
}
