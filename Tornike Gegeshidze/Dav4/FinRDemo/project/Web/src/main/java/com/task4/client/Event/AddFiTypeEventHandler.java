package com.task4.client.Event;

/**
 * Created by tornike on 02-Sep-15.
 */
import com.google.gwt.event.shared.EventHandler;
import com.task4.client.Event.AddFiTypeEvent;

public interface AddFiTypeEventHandler extends EventHandler {
    void onFiTypeAdd(AddFiTypeEvent var1);
}
