//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.shared;

import java.io.Serializable;

public enum ScheduleDTO implements Serializable {
    DAILY,
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    YEARLY;

    private ScheduleDTO() {
    }
}
