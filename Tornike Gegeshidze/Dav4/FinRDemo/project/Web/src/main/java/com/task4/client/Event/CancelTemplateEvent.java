//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.CancelTemplateEventHandler;

public class CancelTemplateEvent extends GwtEvent<CancelTemplateEventHandler> {
    public static final Type<CancelTemplateEventHandler> TYPE = new Type();

    public CancelTemplateEvent() {
    }

    public Type<CancelTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CancelTemplateEventHandler cancelTemplateEventHandler) {
        cancelTemplateEventHandler.onTemplateCancel(this);
    }
}
