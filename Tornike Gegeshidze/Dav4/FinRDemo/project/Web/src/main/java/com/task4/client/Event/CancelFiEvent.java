package com.task4.client.Event;

/**
 * Created by tornike on 02-Sep-15.
*/
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.CancelFiEventHandler;

public class CancelFiEvent extends GwtEvent<CancelFiEventHandler> {
    public static final Type<CancelFiEventHandler> TYPE = new Type();

    public CancelFiEvent() {
    }

    public Type<CancelFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CancelFiEventHandler cancelFiEventHandler) {
        cancelFiEventHandler.onFiCancel(this);
    }
}
