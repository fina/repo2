//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.SaveTemplateEventHandler;

public class SaveTemplateEvent extends GwtEvent<SaveTemplateEventHandler> {
    public static final Type<SaveTemplateEventHandler> TYPE = new Type();

    public SaveTemplateEvent() {
    }

    public Type<SaveTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(SaveTemplateEventHandler saveTemplateEventHandler) {
        saveTemplateEventHandler.onTemplateSave(this);
    }
}
