//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.task4.client.Event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.task4.client.Event.DeleteFiEventHandler;

public class DeleteFiEvent extends GwtEvent<DeleteFiEventHandler> {
    public static final Type<DeleteFiEventHandler> TYPE = new Type();

    public DeleteFiEvent() {
    }

    public Type<DeleteFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteFiEventHandler handler) {
        handler.onFiDelete(this);
    }
}
