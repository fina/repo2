package com.task4.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class FileService_Proxy extends RemoteServiceProxy implements com.task4.client.service.FileServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.task4.client.service.FileService";
  private static final String SERIALIZATION_POLICY ="8DBE3587EECAC74A8952E2526E1E4492";
  private static final com.task4.client.service.FileService_TypeSerializer SERIALIZER = new com.task4.client.service.FileService_TypeSerializer();
  
  public FileService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "fileService", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void addFile(com.task4.shared.FileDTO var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FileService_Proxy", "addFile");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.FileDTO/1141009723");
      streamWriter.writeObject(var1);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void checkFileByName(java.lang.String var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FileService_Proxy", "checkFileByName");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString(var1);
      helper.finish(var2, ResponseReader.BOOLEAN);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void deleteFile(int var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FileService_Proxy", "deleteFile");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("I");
      streamWriter.writeInt(var1);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void loadFiles(com.google.gwt.user.client.rpc.AsyncCallback var1) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FileService_Proxy", "loadFiles");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 0);
      helper.finish(var1, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      var1.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
