package com.task4.client;

public class FiPropertiesImpl implements com.task4.client.FiProperties {
  public com.sencha.gxt.core.client.ValueProvider address() {
    return com.task4.shared.FinInstituteDTO_address_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider code() {
    return com.task4.shared.FinInstituteDTO_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.LabelProvider codeLabel() {
    return com.task4.shared.FinInstituteDTO_code_ModelLabelProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider id() {
    return com.task4.shared.FinInstituteDTO_id_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return com.task4.shared.FinInstituteDTO_name_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider phone() {
    return com.task4.shared.FinInstituteDTO_phone_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider regDate() {
    return com.task4.shared.FinInstituteDTO_regDate_ValueProviderImpl.INSTANCE;
  }
}
