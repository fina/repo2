package com.sencha.gxt.theme.blue.client.resizable;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources {
  private static BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator _instance0 = new BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator();
  private void handleEastInitializer() {
    handleEast = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleEast",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 10, 2000, false, false
    );
  }
  private static class handleEastInitializer {
    static {
      _instance0.handleEastInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleEast;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleEast() {
    return handleEastInitializer.get();
  }
  private void handleNortheastInitializer() {
    handleNortheast = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleNortheast",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 10, 10, false, false
    );
  }
  private static class handleNortheastInitializer {
    static {
      _instance0.handleNortheastInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleNortheast;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleNortheast() {
    return handleNortheastInitializer.get();
  }
  private void handleNorthwestInitializer() {
    handleNorthwest = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleNorthwest",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 10, 10, false, false
    );
  }
  private static class handleNorthwestInitializer {
    static {
      _instance0.handleNorthwestInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleNorthwest;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleNorthwest() {
    return handleNorthwestInitializer.get();
  }
  private void handleSouthInitializer() {
    handleSouth = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleSouth",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 2000, 10, false, false
    );
  }
  private static class handleSouthInitializer {
    static {
      _instance0.handleSouthInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleSouth;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleSouth() {
    return handleSouthInitializer.get();
  }
  private void handleSoutheastInitializer() {
    handleSoutheast = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleSoutheast",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 10, 10, false, false
    );
  }
  private static class handleSoutheastInitializer {
    static {
      _instance0.handleSoutheastInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleSoutheast;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleSoutheast() {
    return handleSoutheastInitializer.get();
  }
  private void handleSouthwestInitializer() {
    handleSouthwest = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "handleSouthwest",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 10, 10, false, false
    );
  }
  private static class handleSouthwestInitializer {
    static {
      _instance0.handleSouthwestInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return handleSouthwest;
    }
  }
  public com.google.gwt.resources.client.ImageResource handleSouthwest() {
    return handleSouthwestInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDEWB{position:" + ("absolute")  + ";z-index:" + ("100")  + ";font-size:" + ("1px")  + ";line-height:" + ("6px")  + ";overflow:" + ("hidden")  + ";filter:" + ("alpha(opacity=0)")  + ";zoom:" + ("1")  + ";}.GCGCW0WDFWB{width:" + ("6px")  + ";cursor:" + ("w-resize")  + ";left:" + ("0")  + ";top:") + (("0")  + ";height:" + ("100%")  + ";margin-left:" + ("-1px")  + ";}.GCGCW0WDJWB{width:" + ("100%")  + ";cursor:" + ("s-resize")  + ";right:" + ("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";margin-bottom:" + ("-1px")  + ";}.GCGCW0WDMWB{width:" + ("6px")  + ";cursor:" + ("e-resize") ) + (";right:" + ("0")  + ";top:" + ("0")  + ";height:" + ("100%")  + ";}.GCGCW0WDGWB{width:" + ("100%")  + ";cursor:" + ("n-resize")  + ";right:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";}.GCGCW0WDKWB{width:" + ("6px")  + ";cursor:" + ("sw-resize")  + ";left:") + (("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDIWB{width:" + ("6px")  + ";cursor:" + ("ne-resize")  + ";right:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDHWB{width:" + ("6px") ) + (";cursor:" + ("nw-resize")  + ";left:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDLWB{width:" + ("6px")  + ";cursor:" + ("se-resize")  + ";right:" + ("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";z-index:") + (("101")  + ";}.GCGCW0WDNWB .GCGCW0WDEWB,.GCGCW0WDPWB .GCGCW0WDEWB{filter:" + ("alpha(opacity=100)")  + ";zoom:" + ("1")  + ";}.GCGCW0WDNWB .GCGCW0WDFWB,.GCGCW0WDPWB .GCGCW0WDFWB,.GCGCW0WDNWB .GCGCW0WDMWB,.GCGCW0WDPWB .GCGCW0WDMWB{background-position:" + ("right")  + ";}.GCGCW0WDNWB .GCGCW0WDJWB,.GCGCW0WDPWB .GCGCW0WDJWB,.GCGCW0WDNWB .GCGCW0WDGWB,.GCGCW0WDPWB .GCGCW0WDGWB{background-position:" + ("top")  + ";}.GCGCW0WDNWB .GCGCW0WDKWB,.GCGCW0WDPWB .GCGCW0WDKWB{background-position:" + ("top"+ " " +"right")  + ";}.GCGCW0WDNWB .GCGCW0WDIWB,.GCGCW0WDPWB .GCGCW0WDIWB{background-position:" + ("bottom"+ " " +"left")  + ";}.GCGCW0WDNWB .GCGCW0WDHWB,.GCGCW0WDPWB .GCGCW0WDHWB{background-position:" + ("bottom"+ " " +"right")  + ";}.GCGCW0WDNWB .GCGCW0WDLWB,.GCGCW0WDPWB .GCGCW0WDLWB{background-position:" + ("top"+ " " +"left")  + ";}.GCGCW0WDAXB{border:" + ("1px"+ " " +"dashed")  + ";position:" + ("absolute") ) + (";overflow:" + ("hidden")  + ";display:" + ("none")  + ";right:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("50000")  + ";border-color:" + ("#3b5a82")  + ";}.GCGCW0WDOWB{width:" + ("100%")  + ";height:" + ("100%")  + ";display:" + ("none")  + ";position:" + ("absolute")  + ";right:") + (("0")  + ";top:" + ("0")  + ";z-index:" + ("200000")  + ";}.GCGCW0WDEWB{filter:" + ("alpha(opacity=0)")  + ";zoom:" + ("1")  + ";background-color:" + ("#fff")  + ";}.GCGCW0WDNWB .GCGCW0WDFWB,.GCGCW0WDPWB .GCGCW0WDFWB,.GCGCW0WDNWB .GCGCW0WDMWB,.GCGCW0WDPWB .GCGCW0WDMWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDJWB,.GCGCW0WDPWB .GCGCW0WDJWB,.GCGCW0WDNWB .GCGCW0WDGWB,.GCGCW0WDPWB .GCGCW0WDGWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getHeight() + "px") ) + (";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDKWB,.GCGCW0WDPWB .GCGCW0WDKWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDIWB,.GCGCW0WDPWB .GCGCW0WDIWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDHWB,.GCGCW0WDPWB .GCGCW0WDHWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDLWB,.GCGCW0WDPWB .GCGCW0WDLWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDOWB{background-color:" + ("#fff")  + ";}")) : ((".GCGCW0WDEWB{position:" + ("absolute")  + ";z-index:" + ("100")  + ";font-size:" + ("1px")  + ";line-height:" + ("6px")  + ";overflow:" + ("hidden")  + ";filter:" + ("alpha(opacity=0)")  + ";zoom:" + ("1")  + ";}.GCGCW0WDFWB{width:" + ("6px")  + ";cursor:" + ("e-resize")  + ";right:" + ("0")  + ";top:") + (("0")  + ";height:" + ("100%")  + ";margin-right:" + ("-1px")  + ";}.GCGCW0WDJWB{width:" + ("100%")  + ";cursor:" + ("s-resize")  + ";left:" + ("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";margin-bottom:" + ("-1px")  + ";}.GCGCW0WDMWB{width:" + ("6px")  + ";cursor:" + ("w-resize") ) + (";left:" + ("0")  + ";top:" + ("0")  + ";height:" + ("100%")  + ";}.GCGCW0WDGWB{width:" + ("100%")  + ";cursor:" + ("n-resize")  + ";left:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";}.GCGCW0WDKWB{width:" + ("6px")  + ";cursor:" + ("se-resize")  + ";right:") + (("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDIWB{width:" + ("6px")  + ";cursor:" + ("nw-resize")  + ";left:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDHWB{width:" + ("6px") ) + (";cursor:" + ("ne-resize")  + ";right:" + ("0")  + ";top:" + ("0")  + ";height:" + ("6px")  + ";z-index:" + ("101")  + ";}.GCGCW0WDLWB{width:" + ("6px")  + ";cursor:" + ("sw-resize")  + ";left:" + ("0")  + ";bottom:" + ("0")  + ";height:" + ("6px")  + ";z-index:") + (("101")  + ";}.GCGCW0WDNWB .GCGCW0WDEWB,.GCGCW0WDPWB .GCGCW0WDEWB{filter:" + ("alpha(opacity=100)")  + ";zoom:" + ("1")  + ";}.GCGCW0WDNWB .GCGCW0WDFWB,.GCGCW0WDPWB .GCGCW0WDFWB,.GCGCW0WDNWB .GCGCW0WDMWB,.GCGCW0WDPWB .GCGCW0WDMWB{background-position:" + ("left")  + ";}.GCGCW0WDNWB .GCGCW0WDJWB,.GCGCW0WDPWB .GCGCW0WDJWB,.GCGCW0WDNWB .GCGCW0WDGWB,.GCGCW0WDPWB .GCGCW0WDGWB{background-position:" + ("top")  + ";}.GCGCW0WDNWB .GCGCW0WDKWB,.GCGCW0WDPWB .GCGCW0WDKWB{background-position:" + ("top"+ " " +"left")  + ";}.GCGCW0WDNWB .GCGCW0WDIWB,.GCGCW0WDPWB .GCGCW0WDIWB{background-position:" + ("bottom"+ " " +"right")  + ";}.GCGCW0WDNWB .GCGCW0WDHWB,.GCGCW0WDPWB .GCGCW0WDHWB{background-position:" + ("bottom"+ " " +"left")  + ";}.GCGCW0WDNWB .GCGCW0WDLWB,.GCGCW0WDPWB .GCGCW0WDLWB{background-position:" + ("top"+ " " +"right")  + ";}.GCGCW0WDAXB{border:" + ("1px"+ " " +"dashed")  + ";position:" + ("absolute") ) + (";overflow:" + ("hidden")  + ";display:" + ("none")  + ";left:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("50000")  + ";border-color:" + ("#3b5a82")  + ";}.GCGCW0WDOWB{width:" + ("100%")  + ";height:" + ("100%")  + ";display:" + ("none")  + ";position:" + ("absolute")  + ";left:") + (("0")  + ";top:" + ("0")  + ";z-index:" + ("200000")  + ";}.GCGCW0WDEWB{filter:" + ("alpha(opacity=0)")  + ";zoom:" + ("1")  + ";background-color:" + ("#fff")  + ";}.GCGCW0WDNWB .GCGCW0WDFWB,.GCGCW0WDPWB .GCGCW0WDFWB,.GCGCW0WDNWB .GCGCW0WDMWB,.GCGCW0WDPWB .GCGCW0WDMWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleEast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDJWB,.GCGCW0WDPWB .GCGCW0WDJWB,.GCGCW0WDNWB .GCGCW0WDGWB,.GCGCW0WDPWB .GCGCW0WDGWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getHeight() + "px") ) + (";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouth()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDKWB,.GCGCW0WDPWB .GCGCW0WDKWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSoutheast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDIWB,.GCGCW0WDPWB .GCGCW0WDIWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNorthwest()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDHWB,.GCGCW0WDPWB .GCGCW0WDHWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleNortheast()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDNWB .GCGCW0WDLWB,.GCGCW0WDPWB .GCGCW0WDLWB{height:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getHeight() + "px")  + ";width:" + ((BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getSafeUri().asString() + "\") -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getLeft() + "px -" + (BlueResizableAppearance_BlueResizableResources_ie8_default_InlineClientBundleGenerator.this.handleSouthwest()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDOWB{background-color:" + ("#fff")  + ";}"));
      }
      public java.lang.String handle() {
        return "GCGCW0WDEWB";
      }
      public java.lang.String handleEast() {
        return "GCGCW0WDFWB";
      }
      public java.lang.String handleNorth() {
        return "GCGCW0WDGWB";
      }
      public java.lang.String handleNortheast() {
        return "GCGCW0WDHWB";
      }
      public java.lang.String handleNorthwest() {
        return "GCGCW0WDIWB";
      }
      public java.lang.String handleSouth() {
        return "GCGCW0WDJWB";
      }
      public java.lang.String handleSoutheast() {
        return "GCGCW0WDKWB";
      }
      public java.lang.String handleSouthwest() {
        return "GCGCW0WDLWB";
      }
      public java.lang.String handleWest() {
        return "GCGCW0WDMWB";
      }
      public java.lang.String over() {
        return "GCGCW0WDNWB";
      }
      public java.lang.String overlay() {
        return "GCGCW0WDOWB";
      }
      public java.lang.String pinned() {
        return "GCGCW0WDPWB";
      }
      public java.lang.String proxy() {
        return "GCGCW0WDAXB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAfQCAYAAAD7MM56AAABPklEQVR42u3RsQpAYBQGUE/tqbyFzWQyKBaLwWQj/KJsxCKp89U33VP31o2qbglFO4e8mUJWjyEth9NGIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiC4H9gnPR7H8Mtt6uP/P7Gq6EbX/o1CIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIg+BVcAVO0o5KOllsHAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAXklEQVR42mM4fOvnf2IwwwAqvPD4z38YOHn3F5yNobBszaf/IAwCyGycCkEmw9ggkzEU3nj57/+1F3//X3769z+yMzAU3nn97//NVxDFeK2+++bff5BikMnIzkBXCADEBGW4QU8/IwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAV0lEQVR42mM4fOvnf2IwwyBSCAMn7/6Csy88/oNdYdmaT2CMzMZQCDIJJgkyCadCZOsuP/37/9qLv/9vvPyH32qQopuv/v2/8xqLQmTrQCaBFN198+8/AN0kZbjL4SGaAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB9AAAAAKCAYAAADsOsW+AAAArElEQVR42u3ZIQ7CUBBF0Vk1q+ou6lCoCpLWYBAoHA3tNJDUIH8ymJ6XHPHWcGN8rAkAAAAAAAAARxfDfUkAAACg3ql7frV+AAAAoFZcbu8EAAAA6u1B/LOWDwAAANSK8zQnAAAA8B/7Wj8AAABQJ/rrKwEAAAAAAADg6AR0AAAAAAAAABDQAQAAAAAAAEBABwAAAAAAAAABHQAAAAAAAAAEdAAAAAAAAAD4sQHUjKQmuTKkLgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAXUlEQVR42mO4++bf/zuv//2/8fLf/7I1n8D4wuM//w/f+omCGUCKbr769//ai79whSCAoRBkEkjR5ad/wSbBAIZCZOtg7JN3f+FWCAJ4rUa2DmQSTqvRBXDhAVQIAMJtZbjoCYduAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAXklEQVR42mM4fOvnf2R84fGf/2VrPoHxjZf//t95/e//3Tf//jOgKwQBmMJrL/7+v/kKohirQhAAmXz56V+wYpDJGApP3v0FNxHZGXitRmbjtBpkMrIzMBTiwgOoEADbjWW4cUNROgAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource handleEast;
  private static com.google.gwt.resources.client.ImageResource handleNortheast;
  private static com.google.gwt.resources.client.ImageResource handleNorthwest;
  private static com.google.gwt.resources.client.ImageResource handleSouth;
  private static com.google.gwt.resources.client.ImageResource handleSoutheast;
  private static com.google.gwt.resources.client.ImageResource handleSouthwest;
  private static com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      handleEast(), 
      handleNortheast(), 
      handleNorthwest(), 
      handleSouth(), 
      handleSoutheast(), 
      handleSouthwest(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("handleEast", handleEast());
        resourceMap.put("handleNortheast", handleNortheast());
        resourceMap.put("handleNorthwest", handleNorthwest());
        resourceMap.put("handleSouth", handleSouth());
        resourceMap.put("handleSoutheast", handleSoutheast());
        resourceMap.put("handleSouthwest", handleSouthwest());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'handleEast': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleEast()();
      case 'handleNortheast': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleNortheast()();
      case 'handleNorthwest': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleNorthwest()();
      case 'handleSouth': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleSouth()();
      case 'handleSoutheast': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleSoutheast()();
      case 'handleSouthwest': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::handleSouthwest()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.resizable.BlueResizableAppearance.BlueResizableResources::style()();
    }
    return null;
  }-*/;
}
