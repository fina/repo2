package com.task4.shared;

public class FinTypeDTO_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinTypeDTO, java.lang.String> {
  public static final FinTypeDTO_name_ValueProviderImpl INSTANCE = new FinTypeDTO_name_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinTypeDTO object) {
    return object.getName();
  }
  public void setValue(com.task4.shared.FinTypeDTO object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
