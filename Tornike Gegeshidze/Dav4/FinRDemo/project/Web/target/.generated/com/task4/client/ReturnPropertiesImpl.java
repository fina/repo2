package com.task4.client;

public class ReturnPropertiesImpl implements com.task4.client.ReturnProperties {
  public com.sencha.gxt.core.client.ValueProvider finInstituteString() {
    return com.task4.shared.ReturnDTO_finInstituteString_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider id() {
    return com.task4.shared.ReturnDTO_id_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider templateString() {
    return com.task4.shared.ReturnDTO_templateString_ValueProviderImpl.INSTANCE;
  }
}
