package com.sencha.gxt.theme.base.client.container;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class MessageBoxDefaultAppearance_MessageBoxResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxResources {
  private static MessageBoxDefaultAppearance_MessageBoxResources_default_InlineClientBundleGenerator _instance0 = new MessageBoxDefaultAppearance_MessageBoxResources_default_InlineClientBundleGenerator();
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxBaseStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDAQ{float:" + ("right")  + ";width:" + ("47px")  + ";height:" + ("32px")  + ";display:" + ("none")  + ";}.GCGCW0WDBQ{padding:" + ("3px"+ " " +"0")  + ";}")) : ((".GCGCW0WDAQ{float:" + ("left")  + ";width:" + ("47px")  + ";height:" + ("32px")  + ";display:" + ("none")  + ";}.GCGCW0WDBQ{padding:" + ("3px"+ " " +"0")  + ";}"));
      }
      public java.lang.String content() {
        return "GCGCW0WDPP";
      }
      public java.lang.String icon() {
        return "GCGCW0WDAQ";
      }
      public java.lang.String message() {
        return "GCGCW0WDBQ";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxBaseStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxBaseStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxBaseStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance.MessageBoxResources::style()();
    }
    return null;
  }-*/;
}
