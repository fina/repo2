package com.task4.shared;

public class TemplateDTO_id_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.TemplateDTO, java.lang.Integer> {
  public static final TemplateDTO_id_ValueProviderImpl INSTANCE = new TemplateDTO_id_ValueProviderImpl();
  public java.lang.Integer getValue(com.task4.shared.TemplateDTO object) {
    return object.getId();
  }
  public void setValue(com.task4.shared.TemplateDTO object, java.lang.Integer value) {
    object.setId(value);
  }
  public String getPath() {
    return "id";
  }
}
