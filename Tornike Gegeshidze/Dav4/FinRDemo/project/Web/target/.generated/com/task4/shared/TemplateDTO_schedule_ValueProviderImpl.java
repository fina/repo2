package com.task4.shared;

public class TemplateDTO_schedule_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.TemplateDTO, com.task4.shared.ScheduleDTO> {
  public static final TemplateDTO_schedule_ValueProviderImpl INSTANCE = new TemplateDTO_schedule_ValueProviderImpl();
  public com.task4.shared.ScheduleDTO getValue(com.task4.shared.TemplateDTO object) {
    return object.getSchedule();
  }
  public void setValue(com.task4.shared.TemplateDTO object, com.task4.shared.ScheduleDTO value) {
    object.setSchedule(value);
  }
  public String getPath() {
    return "schedule";
  }
}
