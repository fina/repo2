package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ReturnNotFoundException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.ReturnNotFoundException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.task4.shared.ReturnNotFoundException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.ReturnNotFoundException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.ReturnNotFoundException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.ReturnNotFoundException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.ReturnNotFoundException_FieldSerializer.deserialize(reader, (com.task4.shared.ReturnNotFoundException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.ReturnNotFoundException_FieldSerializer.serialize(writer, (com.task4.shared.ReturnNotFoundException)object);
  }
  
}
