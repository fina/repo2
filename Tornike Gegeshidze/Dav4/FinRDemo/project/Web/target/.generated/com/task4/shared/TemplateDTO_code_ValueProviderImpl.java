package com.task4.shared;

public class TemplateDTO_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.TemplateDTO, java.lang.String> {
  public static final TemplateDTO_code_ValueProviderImpl INSTANCE = new TemplateDTO_code_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.TemplateDTO object) {
    return object.getCode();
  }
  public void setValue(com.task4.shared.TemplateDTO object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
