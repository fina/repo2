package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FinTypeDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCode(com.task4.shared.FinTypeDTO instance) /*-{
    return instance.@com.task4.shared.FinTypeDTO::code;
  }-*/;
  
  private static native void setCode(com.task4.shared.FinTypeDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinTypeDTO::code = value;
  }-*/;
  
  private static native int getId(com.task4.shared.FinTypeDTO instance) /*-{
    return instance.@com.task4.shared.FinTypeDTO::id;
  }-*/;
  
  private static native void setId(com.task4.shared.FinTypeDTO instance, int value) 
  /*-{
    instance.@com.task4.shared.FinTypeDTO::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.task4.shared.FinTypeDTO instance) /*-{
    return instance.@com.task4.shared.FinTypeDTO::name;
  }-*/;
  
  private static native void setName(com.task4.shared.FinTypeDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinTypeDTO::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.FinTypeDTO instance) throws SerializationException {
    setCode(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    
  }
  
  public static com.task4.shared.FinTypeDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.FinTypeDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.FinTypeDTO instance) throws SerializationException {
    streamWriter.writeString(getCode(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.FinTypeDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.FinTypeDTO_FieldSerializer.deserialize(reader, (com.task4.shared.FinTypeDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.FinTypeDTO_FieldSerializer.serialize(writer, (com.task4.shared.FinTypeDTO)object);
  }
  
}
