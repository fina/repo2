package com.task4.shared;

public class TemplateDTO_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.TemplateDTO, java.lang.String> {
  public static final TemplateDTO_name_ValueProviderImpl INSTANCE = new TemplateDTO_name_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.TemplateDTO object) {
    return object.getName();
  }
  public void setValue(com.task4.shared.TemplateDTO object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
