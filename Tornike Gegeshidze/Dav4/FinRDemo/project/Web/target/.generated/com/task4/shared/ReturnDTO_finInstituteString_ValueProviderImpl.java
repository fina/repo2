package com.task4.shared;

public class ReturnDTO_finInstituteString_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.ReturnDTO, java.lang.String> {
  public static final ReturnDTO_finInstituteString_ValueProviderImpl INSTANCE = new ReturnDTO_finInstituteString_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.ReturnDTO object) {
    return object.getFinInstituteString();
  }
  public void setValue(com.task4.shared.ReturnDTO object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "finInstituteString";
  }
}
