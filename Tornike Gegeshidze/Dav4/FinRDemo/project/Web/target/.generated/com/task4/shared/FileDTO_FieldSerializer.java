package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native byte[] getFileContent(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::fileContent;
  }-*/;
  
  private static native void setFileContent(com.task4.shared.FileDTO instance, byte[] value) 
  /*-{
    instance.@com.task4.shared.FileDTO::fileContent = value;
  }-*/;
  
  private static native java.lang.String getFileName(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::fileName;
  }-*/;
  
  private static native void setFileName(com.task4.shared.FileDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FileDTO::fileName = value;
  }-*/;
  
  private static native com.task4.shared.FinInstituteDTO getFinInstituteDTO(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::finInstituteDTO;
  }-*/;
  
  private static native void setFinInstituteDTO(com.task4.shared.FileDTO instance, com.task4.shared.FinInstituteDTO value) 
  /*-{
    instance.@com.task4.shared.FileDTO::finInstituteDTO = value;
  }-*/;
  
  private static native int getId(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::id;
  }-*/;
  
  private static native void setId(com.task4.shared.FileDTO instance, int value) 
  /*-{
    instance.@com.task4.shared.FileDTO::id = value;
  }-*/;
  
  private static native java.lang.String getScheduleTime(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::scheduleTime;
  }-*/;
  
  private static native void setScheduleTime(com.task4.shared.FileDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FileDTO::scheduleTime = value;
  }-*/;
  
  private static native com.task4.shared.TemplateDTO getTemplateDTO(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::templateDTO;
  }-*/;
  
  private static native void setTemplateDTO(com.task4.shared.FileDTO instance, com.task4.shared.TemplateDTO value) 
  /*-{
    instance.@com.task4.shared.FileDTO::templateDTO = value;
  }-*/;
  
  private static native java.util.Date getUploadDate(com.task4.shared.FileDTO instance) /*-{
    return instance.@com.task4.shared.FileDTO::uploadDate;
  }-*/;
  
  private static native void setUploadDate(com.task4.shared.FileDTO instance, java.util.Date value) 
  /*-{
    instance.@com.task4.shared.FileDTO::uploadDate = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.FileDTO instance) throws SerializationException {
    setFileContent(instance, (byte[]) streamReader.readObject());
    setFileName(instance, streamReader.readString());
    setFinInstituteDTO(instance, (com.task4.shared.FinInstituteDTO) streamReader.readObject());
    setId(instance, streamReader.readInt());
    setScheduleTime(instance, streamReader.readString());
    setTemplateDTO(instance, (com.task4.shared.TemplateDTO) streamReader.readObject());
    setUploadDate(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static com.task4.shared.FileDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.FileDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.FileDTO instance) throws SerializationException {
    streamWriter.writeObject(getFileContent(instance));
    streamWriter.writeString(getFileName(instance));
    streamWriter.writeObject(getFinInstituteDTO(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getScheduleTime(instance));
    streamWriter.writeObject(getTemplateDTO(instance));
    streamWriter.writeObject(getUploadDate(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.FileDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.FileDTO_FieldSerializer.deserialize(reader, (com.task4.shared.FileDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.FileDTO_FieldSerializer.serialize(writer, (com.task4.shared.FileDTO)object);
  }
  
}
