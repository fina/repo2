package com.task4.shared;

public class ReturnDTO_id_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.ReturnDTO, java.lang.Integer> {
  public static final ReturnDTO_id_ValueProviderImpl INSTANCE = new ReturnDTO_id_ValueProviderImpl();
  public java.lang.Integer getValue(com.task4.shared.ReturnDTO object) {
    return object.getId();
  }
  public void setValue(com.task4.shared.ReturnDTO object, java.lang.Integer value) {
    object.setId(value);
  }
  public String getPath() {
    return "id";
  }
}
