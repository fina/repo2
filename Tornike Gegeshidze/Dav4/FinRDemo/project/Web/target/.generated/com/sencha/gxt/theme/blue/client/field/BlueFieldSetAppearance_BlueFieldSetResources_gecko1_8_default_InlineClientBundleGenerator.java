package com.sencha.gxt.theme.blue.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueFieldSetAppearance_BlueFieldSetResources_gecko1_8_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetResources {
  private static BlueFieldSetAppearance_BlueFieldSetResources_gecko1_8_default_InlineClientBundleGenerator _instance0 = new BlueFieldSetAppearance_BlueFieldSetResources_gecko1_8_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDHQB{overflow:" + ("hidden")  + ";}.GCGCW0WDFQB{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";display:" + ("block")  + ";padding:" + ("5px"+ " " +"9px"+ " " +"10px"+ " " +"9px")  + ";}.GCGCW0WDDQB{overflow:" + ("hidden")  + ";}.GCGCW0WDFQB legend .GCGCW0WDJQB{margin-left:" + ("3px")  + ";margin-right:" + ("0")  + ";float:" + ("right")  + " !important;}.GCGCW0WDFQB legend input{margin-left:" + ("3px")  + ";float:") + (("right")  + " !important;height:" + ("13px")  + ";width:" + ("13px")  + ";}fieldset.GCGCW0WDEQB{padding-bottom:" + ("0")  + " !important;border-width:" + ("1px"+ " " +"1px"+ " " +"0"+ " " +"1px")  + " !important;border-right-color:" + ("transparent")  + ";border-left-color:" + ("transparent")  + ";}fieldset.GCGCW0WDEQB .GCGCW0WDDQB{visibility:" + ("hidden")  + ";position:" + ("absolute")  + ";right:" + ("-1000px")  + ";top:" + ("-1000px") ) + (";}.GCGCW0WDIQB{border:" + ("0"+ " " +"none"+ " " +"transparent")  + ";}.GCGCW0WDIQB legend{margin-right:" + ("-3px")  + ";}.GCGCW0WDGQB,.GCGCW0WDFQB legend>div{padding-top:" + ("1px")  + ";}.GCGCW0WDFQB{border-color:" + ("#b5b8c8")  + ";}.GCGCW0WDFQB legend{font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#15428b")  + ";}")) : ((".GCGCW0WDHQB{overflow:" + ("hidden")  + ";}.GCGCW0WDFQB{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";display:" + ("block")  + ";padding:" + ("5px"+ " " +"9px"+ " " +"10px"+ " " +"9px")  + ";}.GCGCW0WDDQB{overflow:" + ("hidden")  + ";}.GCGCW0WDFQB legend .GCGCW0WDJQB{margin-right:" + ("3px")  + ";margin-left:" + ("0")  + ";float:" + ("left")  + " !important;}.GCGCW0WDFQB legend input{margin-right:" + ("3px")  + ";float:") + (("left")  + " !important;height:" + ("13px")  + ";width:" + ("13px")  + ";}fieldset.GCGCW0WDEQB{padding-bottom:" + ("0")  + " !important;border-width:" + ("1px"+ " " +"1px"+ " " +"0"+ " " +"1px")  + " !important;border-left-color:" + ("transparent")  + ";border-right-color:" + ("transparent")  + ";}fieldset.GCGCW0WDEQB .GCGCW0WDDQB{visibility:" + ("hidden")  + ";position:" + ("absolute")  + ";left:" + ("-1000px")  + ";top:" + ("-1000px") ) + (";}.GCGCW0WDIQB{border:" + ("0"+ " " +"none"+ " " +"transparent")  + ";}.GCGCW0WDIQB legend{margin-left:" + ("-3px")  + ";}.GCGCW0WDGQB,.GCGCW0WDFQB legend>div{padding-top:" + ("1px")  + ";}.GCGCW0WDFQB{border-color:" + ("#b5b8c8")  + ";}.GCGCW0WDFQB legend{font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#15428b")  + ";}"));
      }
      public java.lang.String body() {
        return "GCGCW0WDDQB";
      }
      public java.lang.String collapsed() {
        return "GCGCW0WDEQB";
      }
      public java.lang.String fieldSet() {
        return "GCGCW0WDFQB";
      }
      public java.lang.String header() {
        return "GCGCW0WDGQB";
      }
      public java.lang.String legend() {
        return "GCGCW0WDHQB";
      }
      public java.lang.String noborder() {
        return "GCGCW0WDIQB";
      }
      public java.lang.String toolWrap() {
        return "GCGCW0WDJQB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetStyle css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetStyle css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.blue.client.field.BlueFieldSetAppearance.BlueFieldSetResources::css()();
    }
    return null;
  }-*/;
}
