package com.task4.shared;

public class ReturnDTO_templateString_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.ReturnDTO, java.lang.String> {
  public static final ReturnDTO_templateString_ValueProviderImpl INSTANCE = new ReturnDTO_templateString_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.ReturnDTO object) {
    return object.getTemplateString();
  }
  public void setValue(com.task4.shared.ReturnDTO object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "templateString";
  }
}
