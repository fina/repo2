package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ReturnDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.task4.shared.FinInstituteDTO getFinInstituteDTO(com.task4.shared.ReturnDTO instance) /*-{
    return instance.@com.task4.shared.ReturnDTO::finInstituteDTO;
  }-*/;
  
  private static native void setFinInstituteDTO(com.task4.shared.ReturnDTO instance, com.task4.shared.FinInstituteDTO value) 
  /*-{
    instance.@com.task4.shared.ReturnDTO::finInstituteDTO = value;
  }-*/;
  
  private static native java.lang.String getFinInstituteString(com.task4.shared.ReturnDTO instance) /*-{
    return instance.@com.task4.shared.ReturnDTO::finInstituteString;
  }-*/;
  
  private static native void setFinInstituteString(com.task4.shared.ReturnDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.ReturnDTO::finInstituteString = value;
  }-*/;
  
  private static native int getId(com.task4.shared.ReturnDTO instance) /*-{
    return instance.@com.task4.shared.ReturnDTO::id;
  }-*/;
  
  private static native void setId(com.task4.shared.ReturnDTO instance, int value) 
  /*-{
    instance.@com.task4.shared.ReturnDTO::id = value;
  }-*/;
  
  private static native com.task4.shared.TemplateDTO getTemplateDTO(com.task4.shared.ReturnDTO instance) /*-{
    return instance.@com.task4.shared.ReturnDTO::templateDTO;
  }-*/;
  
  private static native void setTemplateDTO(com.task4.shared.ReturnDTO instance, com.task4.shared.TemplateDTO value) 
  /*-{
    instance.@com.task4.shared.ReturnDTO::templateDTO = value;
  }-*/;
  
  private static native java.lang.String getTemplateString(com.task4.shared.ReturnDTO instance) /*-{
    return instance.@com.task4.shared.ReturnDTO::templateString;
  }-*/;
  
  private static native void setTemplateString(com.task4.shared.ReturnDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.ReturnDTO::templateString = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.ReturnDTO instance) throws SerializationException {
    setFinInstituteDTO(instance, (com.task4.shared.FinInstituteDTO) streamReader.readObject());
    setFinInstituteString(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setTemplateDTO(instance, (com.task4.shared.TemplateDTO) streamReader.readObject());
    setTemplateString(instance, streamReader.readString());
    
  }
  
  public static com.task4.shared.ReturnDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.ReturnDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.ReturnDTO instance) throws SerializationException {
    streamWriter.writeObject(getFinInstituteDTO(instance));
    streamWriter.writeString(getFinInstituteString(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeObject(getTemplateDTO(instance));
    streamWriter.writeString(getTemplateString(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.ReturnDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.ReturnDTO_FieldSerializer.deserialize(reader, (com.task4.shared.ReturnDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.ReturnDTO_FieldSerializer.serialize(writer, (com.task4.shared.ReturnDTO)object);
  }
  
}
