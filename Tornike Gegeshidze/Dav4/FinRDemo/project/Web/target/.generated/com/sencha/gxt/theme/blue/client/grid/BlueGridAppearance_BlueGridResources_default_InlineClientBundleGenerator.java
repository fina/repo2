package com.sencha.gxt.theme.blue.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridResources {
  private static BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator _instance0 = new BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDCRB{position:" + ("relative")  + ";overflow:" + ("hidden")  + ";outline:" + ("0"+ " " +"none")  + ";}.GCGCW0WDMRB{overflow:" + ("auto")  + ";zoom:" + ("1")  + ";position:" + ("relative")  + ";background-color:" + ("white")  + ";}.GCGCW0WDPQB{table-layout:" + ("fixed")  + ";border-collapse:" + ("separate")  + ";border-spacing:" + ("0")  + ";}.GCGCW0WDFRB{cursor:") + (("default")  + ";padding:" + ("0"+ " " +"1px")  + ";vertical-align:" + ("top")  + ";}.GCGCW0WDJRB{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GCGCW0WDGRB .GCGCW0WDLQB{background-color:" + ("#fafafa")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB{background-color:" + ("#efefef")  + ";border-width:" + ("1px"+ " " +"0")  + ";border-style:" + ("solid") ) + (";}.GCGCW0WDLQB{background-color:" + ("white")  + ";border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px"+ " " +"0")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDNQB{overflow:" + ("hidden")  + ";padding:" + ("4px"+ " " +"5px"+ " " +"3px"+ " " +"3px")  + ";line-height:" + ("13px")  + ";white-space:") + (("nowrap")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";}.GCGCW0WDERB{padding:" + ("0")  + ";height:" + ("100%")  + ";}.GCGCW0WDMQB{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GCGCW0WDFRB .GCGCW0WDLFC{background-color:" + ("#b8cfee")  + " !important;color:" + ("#000") ) + (";}.GCGCW0WDKRB .GCGCW0WDLQB,.GCGCW0WDKRB .GCGCW0WDLRB{border-color:" + ("#ddd")  + ";}.GCGCW0WDOQB .GCGCW0WDLQB{padding-left:" + ("0")  + ";border-left:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDARB{padding:" + ("10px")  + ";color:" + ("gray")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDHRB{font:" + ("11px"+ "/"+ " " +"13px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";padding:" + ("4px")  + ";}.GCGCW0WDLRB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:") + (("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB,.GCGCW0WDPFC .GCGCW0WDLRB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GCGCW0WDLRB .GCGCW0WDLQB,.GCGCW0WDLRB .GCGCW0WDNQB{border:" + ("none")  + ";}.GCGCW0WDBRB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block") ) + (";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GCGCW0WDBRB .GCGCW0WDLQB{background:" + ("none")  + ";}.x-treegrid .x-treegrid-column .GCGCW0WDNQB{padding:" + ("0")  + " !important;}.GCGCW0WDJRB{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GCGCW0WDGRB .GCGCW0WDLQB{background-color:" + ("#fafafa")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB{background-color:" + ("#efefef")  + ";}.GCGCW0WDLQB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";}.GCGCW0WDFRB .GCGCW0WDLFC{background-color:" + ("#b8cfee")  + " !important;color:") + (("#000")  + ";}.GCGCW0WDOQB .GCGCW0WDLQB{border-left-color:" + ("#ededed")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB,.GCGCW0WDKRB .GCGCW0WDLRB{border-color:" + ("#ddd")  + ";}.GCGCW0WDLRB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB,.GCGCW0WDPFC .GCGCW0WDLRB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";}.GCGCW0WDBRB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0") ) + (";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}")) : ((".GCGCW0WDCRB{position:" + ("relative")  + ";overflow:" + ("hidden")  + ";outline:" + ("0"+ " " +"none")  + ";}.GCGCW0WDMRB{overflow:" + ("auto")  + ";zoom:" + ("1")  + ";position:" + ("relative")  + ";background-color:" + ("white")  + ";}.GCGCW0WDPQB{table-layout:" + ("fixed")  + ";border-collapse:" + ("separate")  + ";border-spacing:" + ("0")  + ";}.GCGCW0WDFRB{cursor:") + (("default")  + ";padding:" + ("0"+ " " +"1px")  + ";vertical-align:" + ("top")  + ";}.GCGCW0WDJRB{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GCGCW0WDGRB .GCGCW0WDLQB{background-color:" + ("#fafafa")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB{background-color:" + ("#efefef")  + ";border-width:" + ("1px"+ " " +"0")  + ";border-style:" + ("solid") ) + (";}.GCGCW0WDLQB{background-color:" + ("white")  + ";border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px"+ " " +"0")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDNQB{overflow:" + ("hidden")  + ";padding:" + ("4px"+ " " +"3px"+ " " +"3px"+ " " +"5px")  + ";line-height:" + ("13px")  + ";white-space:") + (("nowrap")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";}.GCGCW0WDERB{padding:" + ("0")  + ";height:" + ("100%")  + ";}.GCGCW0WDMQB{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GCGCW0WDFRB .GCGCW0WDLFC{background-color:" + ("#b8cfee")  + " !important;color:" + ("#000") ) + (";}.GCGCW0WDKRB .GCGCW0WDLQB,.GCGCW0WDKRB .GCGCW0WDLRB{border-color:" + ("#ddd")  + ";}.GCGCW0WDOQB .GCGCW0WDLQB{padding-right:" + ("0")  + ";border-right:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDARB{padding:" + ("10px")  + ";color:" + ("gray")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDHRB{font:" + ("11px"+ "/"+ " " +"13px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";padding:" + ("4px")  + ";}.GCGCW0WDLRB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:") + (("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB,.GCGCW0WDPFC .GCGCW0WDLRB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GCGCW0WDLRB .GCGCW0WDLQB,.GCGCW0WDLRB .GCGCW0WDNQB{border:" + ("none")  + ";}.GCGCW0WDBRB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block") ) + (";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GCGCW0WDBRB .GCGCW0WDLQB{background:" + ("none")  + ";}.x-treegrid .x-treegrid-column .GCGCW0WDNQB{padding:" + ("0")  + " !important;}.GCGCW0WDJRB{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GCGCW0WDGRB .GCGCW0WDLQB{background-color:" + ("#fafafa")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB{background-color:" + ("#efefef")  + ";}.GCGCW0WDLQB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";}.GCGCW0WDFRB .GCGCW0WDLFC{background-color:" + ("#b8cfee")  + " !important;color:") + (("#000")  + ";}.GCGCW0WDOQB .GCGCW0WDLQB{border-right-color:" + ("#ededed")  + ";}.GCGCW0WDKRB .GCGCW0WDLQB,.GCGCW0WDKRB .GCGCW0WDLRB{border-color:" + ("#ddd")  + ";}.GCGCW0WDLRB{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDPFC .GCGCW0WDLQB,.GCGCW0WDPFC .GCGCW0WDLRB{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";}.GCGCW0WDBRB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0") ) + (";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}"));
      }
      public java.lang.String body() {
        return "GCGCW0WDKQB";
      }
      public java.lang.String cell() {
        return "GCGCW0WDLQB";
      }
      public java.lang.String cellDirty() {
        return "GCGCW0WDMQB";
      }
      public java.lang.String cellInner() {
        return "GCGCW0WDNQB";
      }
      public java.lang.String columnLines() {
        return "GCGCW0WDOQB";
      }
      public java.lang.String dataTable() {
        return "GCGCW0WDPQB";
      }
      public java.lang.String empty() {
        return "GCGCW0WDARB";
      }
      public java.lang.String footer() {
        return "GCGCW0WDBRB";
      }
      public java.lang.String grid() {
        return "GCGCW0WDCRB";
      }
      public java.lang.String headerRow() {
        return "GCGCW0WDDRB";
      }
      public java.lang.String noPadding() {
        return "GCGCW0WDERB";
      }
      public java.lang.String row() {
        return "GCGCW0WDFRB";
      }
      public java.lang.String rowAlt() {
        return "GCGCW0WDGRB";
      }
      public java.lang.String rowBody() {
        return "GCGCW0WDHRB";
      }
      public java.lang.String rowDirty() {
        return "GCGCW0WDIRB";
      }
      public java.lang.String rowHighlight() {
        return "GCGCW0WDJRB";
      }
      public java.lang.String rowOver() {
        return "GCGCW0WDKRB";
      }
      public java.lang.String rowWrap() {
        return "GCGCW0WDLRB";
      }
      public java.lang.String scroller() {
        return "GCGCW0WDMRB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle css() {
    return cssInitializer.get();
  }
  private void specialColumnInitializer() {
    specialColumn = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumn",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnInitializer {
    static {
      _instance0.specialColumnInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumn;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumn() {
    return specialColumnInitializer.get();
  }
  private void specialColumnSelectedInitializer() {
    specialColumnSelected = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumnSelected",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnSelectedInitializer {
    static {
      _instance0.specialColumnSelectedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumnSelected;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumnSelected() {
    return specialColumnSelectedInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle css;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAANklEQVR42rXNqREAIAwEQPqvLn2QH0xUmDmBxiBW76iqfhG5wWNd5glq0aIOLNaTFYiox+/gACe+uBEe1wRCAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAAXUlEQVR42rXNvQ6CMBSA0b7/o/AsDgSiRmIwKNTW8iOaMPTe5qOzu8OZjwnfxC//Sbg1Yd/KsCh9dp+VblJuo9IG5foSGi9cnHB+CqfsaIV6iFR9pHxEisOG+XewA9F9tJvfRlttAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource specialColumn;
  private static com.google.gwt.resources.client.ImageResource specialColumnSelected;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      specialColumn(), 
      specialColumnSelected(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("specialColumn", specialColumn());
        resourceMap.put("specialColumnSelected", specialColumnSelected());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridResources::css()();
      case 'specialColumn': return this.@com.sencha.gxt.theme.base.client.grid.GridBaseAppearance.GridResources::specialColumn()();
      case 'specialColumnSelected': return this.@com.sencha.gxt.theme.base.client.grid.GridBaseAppearance.GridResources::specialColumnSelected()();
    }
    return null;
  }-*/;
}
