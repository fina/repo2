package com.task4.shared;

public class FinInstituteDTO_address_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.lang.String> {
  public static final FinInstituteDTO_address_ValueProviderImpl INSTANCE = new FinInstituteDTO_address_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getAddress();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.lang.String value) {
    object.setAddress(value);
  }
  public String getPath() {
    return "address";
  }
}
