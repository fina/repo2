package com.task4.shared;

public class FinTypeDTO_id_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinTypeDTO, java.lang.Integer> {
  public static final FinTypeDTO_id_ValueProviderImpl INSTANCE = new FinTypeDTO_id_ValueProviderImpl();
  public java.lang.Integer getValue(com.task4.shared.FinTypeDTO object) {
    return object.getId();
  }
  public void setValue(com.task4.shared.FinTypeDTO object, java.lang.Integer value) {
    object.setId(value);
  }
  public String getPath() {
    return "id";
  }
}
