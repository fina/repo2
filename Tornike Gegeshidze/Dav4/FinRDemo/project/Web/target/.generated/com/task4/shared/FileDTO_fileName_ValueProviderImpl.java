package com.task4.shared;

public class FileDTO_fileName_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FileDTO, java.lang.String> {
  public static final FileDTO_fileName_ValueProviderImpl INSTANCE = new FileDTO_fileName_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FileDTO object) {
    return object.getFileName();
  }
  public void setValue(com.task4.shared.FileDTO object, java.lang.String value) {
    object.setFileName(value);
  }
  public String getPath() {
    return "fileName";
  }
}
