package com.task4.shared;

public class FinInstituteDTO_phone_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.lang.String> {
  public static final FinInstituteDTO_phone_ValueProviderImpl INSTANCE = new FinInstituteDTO_phone_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getPhone();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.lang.String value) {
    object.setPhone(value);
  }
  public String getPath() {
    return "phone";
  }
}
