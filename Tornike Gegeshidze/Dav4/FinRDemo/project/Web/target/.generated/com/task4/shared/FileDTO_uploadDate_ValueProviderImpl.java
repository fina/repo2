package com.task4.shared;

public class FileDTO_uploadDate_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FileDTO, java.util.Date> {
  public static final FileDTO_uploadDate_ValueProviderImpl INSTANCE = new FileDTO_uploadDate_ValueProviderImpl();
  public java.util.Date getValue(com.task4.shared.FileDTO object) {
    return object.getUploadDate();
  }
  public void setValue(com.task4.shared.FileDTO object, java.util.Date value) {
    object.setUploadDate(value);
  }
  public String getPath() {
    return "uploadDate";
  }
}
