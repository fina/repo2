package com.task4.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class TemplateService_Proxy extends RemoteServiceProxy implements com.task4.client.service.TemplateServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.task4.client.service.TemplateService";
  private static final String SERIALIZATION_POLICY ="9A65063C30EB11B96C994D51FB6DEBAC";
  private static final com.task4.client.service.TemplateService_TypeSerializer SERIALIZER = new com.task4.client.service.TemplateService_TypeSerializer();
  
  public TemplateService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "templateService", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void addTemplate(com.task4.shared.TemplateDTO var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("TemplateService_Proxy", "addTemplate");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.TemplateDTO/2084595818");
      streamWriter.writeObject(var1);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void deleteTemplate(com.task4.shared.TemplateDTO templateDTO, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("TemplateService_Proxy", "deleteTemplate");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.TemplateDTO/2084595818");
      streamWriter.writeObject(templateDTO);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void loadTemplate(com.google.gwt.user.client.rpc.AsyncCallback var1) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("TemplateService_Proxy", "loadTemplate");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 0);
      helper.finish(var1, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      var1.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
