package com.task4.shared;

public class FinInstituteDTO_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.lang.String> {
  public static final FinInstituteDTO_name_ValueProviderImpl INSTANCE = new FinInstituteDTO_name_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getName();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
