package com.sencha.gxt.theme.blue.client.window;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueWindowAppearance_BlueWindowResources_ie10_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowResources {
  private static BlueWindowAppearance_BlueWindowResources_ie10_default_InlineClientBundleGenerator _instance0 = new BlueWindowAppearance_BlueWindowResources_ie10_default_InlineClientBundleGenerator();
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDIBC{border-style:" + ("solid")  + ";border-width:" + ("0")  + ";outline:" + ("0"+ " " +"none")  + ";zoom:" + ("1")  + ";-moz-outline:" + ("none")  + ";outline:" + ("0"+ " " +"none")  + ";}.GCGCW0WDHBC{border-top-width:" + ("1px")  + ";}.GCGCW0WDGBC{position:" + ("relative")  + ";}.GCGCW0WDCBC{border-bottom:" + ("1px"+ " " +"solid")  + ";border-right:" + ("1px"+ " " +"solid")  + ";border-left:") + (("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDEBC{position:" + ("relative")  + ";}.GCGCW0WDFBC{z-index:" + ("12000")  + ";overflow:" + ("hidden")  + ";position:" + ("absolute")  + ";right:" + ("0")  + ";top:" + ("0") ) + (";opacity:" + ("0.5")  + ";}.GCGCW0WDFBC ul{margin:" + ("0")  + ";padding:" + ("0")  + ";overflow:" + ("hidden")  + ";font-size:" + ("0")  + ";line-height:" + ("0")  + ";border:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";display:" + ("block")  + ";}.GCGCW0WDFBC *{cursor:" + ("move")  + " !important;}.GCGCW0WDCBC{border:") + (("0")  + ";background:" + ("none")  + ";}.GCGCW0WDFBC ul{background-color:" + ("#cdd9e8")  + ";border-color:" + ("#90a4c4")  + ";}")) : ((".GCGCW0WDIBC{border-style:" + ("solid")  + ";border-width:" + ("0")  + ";outline:" + ("0"+ " " +"none")  + ";zoom:" + ("1")  + ";-moz-outline:" + ("none")  + ";outline:" + ("0"+ " " +"none")  + ";}.GCGCW0WDHBC{border-top-width:" + ("1px")  + ";}.GCGCW0WDGBC{position:" + ("relative")  + ";}.GCGCW0WDCBC{border-bottom:" + ("1px"+ " " +"solid")  + ";border-left:" + ("1px"+ " " +"solid")  + ";border-right:") + (("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDEBC{position:" + ("relative")  + ";}.GCGCW0WDFBC{z-index:" + ("12000")  + ";overflow:" + ("hidden")  + ";position:" + ("absolute")  + ";left:" + ("0")  + ";top:" + ("0") ) + (";opacity:" + ("0.5")  + ";}.GCGCW0WDFBC ul{margin:" + ("0")  + ";padding:" + ("0")  + ";overflow:" + ("hidden")  + ";font-size:" + ("0")  + ";line-height:" + ("0")  + ";border:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";display:" + ("block")  + ";}.GCGCW0WDFBC *{cursor:" + ("move")  + " !important;}.GCGCW0WDCBC{border:") + (("0")  + ";background:" + ("none")  + ";}.GCGCW0WDFBC ul{background-color:" + ("#cdd9e8")  + ";border-color:" + ("#90a4c4")  + ";}"));
      }
      public java.lang.String body() {
        return "GCGCW0WDCBC";
      }
      public java.lang.String bodyWrap() {
        return "GCGCW0WDDBC";
      }
      public java.lang.String footer() {
        return "GCGCW0WDEBC";
      }
      public java.lang.String ghost() {
        return "GCGCW0WDFBC";
      }
      public java.lang.String header() {
        return "GCGCW0WDGBC";
      }
      public java.lang.String noHeader() {
        return "GCGCW0WDHBC";
      }
      public java.lang.String panel() {
        return "GCGCW0WDIBC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowResources::style()();
    }
    return null;
  }-*/;
}
