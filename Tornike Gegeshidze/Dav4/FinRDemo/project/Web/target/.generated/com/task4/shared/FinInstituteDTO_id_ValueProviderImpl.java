package com.task4.shared;

public class FinInstituteDTO_id_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.lang.Integer> {
  public static final FinInstituteDTO_id_ValueProviderImpl INSTANCE = new FinInstituteDTO_id_ValueProviderImpl();
  public java.lang.Integer getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getId();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.lang.Integer value) {
    object.setId(value);
  }
  public String getPath() {
    return "id";
  }
}
