package com.sencha.gxt.theme.base.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources {
  private static SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator _instance0 = new SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDIOB{position:" + ("absolute")  + ";z-index:" + ("3")  + ";}.GCGCW0WDJOB{cursor:" + ("s-resize")  + ";cursor:" + ("row-resize")  + ";font-size:" + ("1px")  + ";line-height:" + ("1px")  + ";}.GCGCW0WDBPB{cursor:" + ("w-resize")  + ";cursor:" + ("col-resize")  + ";}.GCGCW0WDAPB{position:" + ("absolute")  + ";background-color:" + ("#929090")  + ";font-size:") + (("1px")  + ";line-height:" + ("1px")  + ";z-index:" + ("200")  + ";}.GCGCW0WDKOB{position:" + ("absolute")  + ";top:" + ("0")  + ";right:" + ("0")  + ";display:" + ("block")  + ";width:" + ("5px")  + ";height:" + ("35px")  + ";cursor:" + ("pointer")  + ";opacity:" + ("0.5") ) + (";}.GCGCW0WDNOB{opacity:" + ("1")  + ";filter:" + ("none")  + ";opacity:" + ("1")  + ";}.GCGCW0WDMOB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";}.GCGCW0WDOOB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getHeight() + "px")  + ";width:") + (((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GCGCW0WDMOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GCGCW0WDPOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDLOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDPOB,.GCGCW0WDLOB{height:" + ("5px")  + ";right:" + ("50%")  + ";margin-right:" + ("-17px")  + ";width:" + ("35px")  + ";}")) : ((".GCGCW0WDIOB{position:" + ("absolute")  + ";z-index:" + ("3")  + ";}.GCGCW0WDJOB{cursor:" + ("s-resize")  + ";cursor:" + ("row-resize")  + ";font-size:" + ("1px")  + ";line-height:" + ("1px")  + ";}.GCGCW0WDBPB{cursor:" + ("e-resize")  + ";cursor:" + ("col-resize")  + ";}.GCGCW0WDAPB{position:" + ("absolute")  + ";background-color:" + ("#929090")  + ";font-size:") + (("1px")  + ";line-height:" + ("1px")  + ";z-index:" + ("200")  + ";}.GCGCW0WDKOB{position:" + ("absolute")  + ";top:" + ("0")  + ";left:" + ("0")  + ";display:" + ("block")  + ";width:" + ("5px")  + ";height:" + ("35px")  + ";cursor:" + ("pointer")  + ";opacity:" + ("0.5") ) + (";}.GCGCW0WDNOB{opacity:" + ("1")  + ";filter:" + ("none")  + ";opacity:" + ("1")  + ";}.GCGCW0WDMOB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";}.GCGCW0WDOOB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getHeight() + "px")  + ";width:") + (((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniRight()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GCGCW0WDMOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniLeft()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GCGCW0WDPOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniTop()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDLOB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie9_default_InlineClientBundleGenerator.this.miniBottom()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDPOB,.GCGCW0WDLOB{height:" + ("5px")  + ";left:" + ("50%")  + ";margin-left:" + ("-17px")  + ";width:" + ("35px")  + ";}"));
      }
      public java.lang.String bar() {
        return "GCGCW0WDIOB";
      }
      public java.lang.String horizontalBar() {
        return "GCGCW0WDJOB";
      }
      public java.lang.String mini() {
        return "GCGCW0WDKOB";
      }
      public java.lang.String miniBottom() {
        return "GCGCW0WDLOB";
      }
      public java.lang.String miniLeft() {
        return "GCGCW0WDMOB";
      }
      public java.lang.String miniOver() {
        return "GCGCW0WDNOB";
      }
      public java.lang.String miniRight() {
        return "GCGCW0WDOOB";
      }
      public java.lang.String miniTop() {
        return "GCGCW0WDPOB";
      }
      public java.lang.String proxy() {
        return "GCGCW0WDAPB";
      }
      public java.lang.String verticalBar() {
        return "GCGCW0WDBPB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle css() {
    return cssInitializer.get();
  }
  private void miniBottomInitializer() {
    miniBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 35, 5, false, false
    );
  }
  private static class miniBottomInitializer {
    static {
      _instance0.miniBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniBottom() {
    return miniBottomInitializer.get();
  }
  private void miniLeftInitializer() {
    miniLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 5, 35, false, false
    );
  }
  private static class miniLeftInitializer {
    static {
      _instance0.miniLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniLeft() {
    return miniLeftInitializer.get();
  }
  private void miniRightInitializer() {
    miniRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 5, 35, false, false
    );
  }
  private static class miniRightInitializer {
    static {
      _instance0.miniRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniRight() {
    return miniRightInitializer.get();
  }
  private void miniTopInitializer() {
    miniTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 35, 5, false, false
    );
  }
  private static class miniTopInitializer {
    static {
      _instance0.miniTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniTop() {
    return miniTopInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle css;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAFCAYAAADPLFVyAAAANklEQVR42mO4ePHi/8GCGUAAxHjz5g3J2NnZFQOTYw7cITBADQdRxSHUcBBVHYLsILqmESQAACG6H+sTbbGtAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAjCAYAAABcmsDOAAAAQElEQVR42mNgQAMXL178jyGAIgjivHnzBiEIE4ALIguMEEFnZ1dUQZAAiiBMgLBKnGYO8/DEmpZwpjqc6RNZAgC3yB/roojJ2wAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAjCAYAAABcmsDOAAAAQklEQVR42mO4ePHifwZ0ABLEkAAJvHnzBlUCJogigSwIlxiegs7OrtgFYRIYgiBMWOUICDq8ghhpCSPVYU2f6AkZAKomH+vsosKnAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAFCAYAAADPLFVyAAAANUlEQVR42mNgQAMXL178Ty/MgA+AFLx584Zk7OzsSpY+nA6ixCEwTBUHUcMhVHEQPdMIIQwAgl8f6zRdECkAAAAASUVORK5CYII=";
  private static com.google.gwt.resources.client.ImageResource miniBottom;
  private static com.google.gwt.resources.client.ImageResource miniLeft;
  private static com.google.gwt.resources.client.ImageResource miniRight;
  private static com.google.gwt.resources.client.ImageResource miniTop;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      miniBottom(), 
      miniLeft(), 
      miniRight(), 
      miniTop(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("miniBottom", miniBottom());
        resourceMap.put("miniLeft", miniLeft());
        resourceMap.put("miniRight", miniRight());
        resourceMap.put("miniTop", miniTop());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::css()();
      case 'miniBottom': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniBottom()();
      case 'miniLeft': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniLeft()();
      case 'miniRight': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniRight()();
      case 'miniTop': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniTop()();
    }
    return null;
  }-*/;
}
