package com.task4.shared;

public class FinInstituteDTO_regDate_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.util.Date> {
  public static final FinInstituteDTO_regDate_ValueProviderImpl INSTANCE = new FinInstituteDTO_regDate_ValueProviderImpl();
  public java.util.Date getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getRegDate();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.util.Date value) {
    object.setRegDate(value);
  }
  public String getPath() {
    return "regDate";
  }
}
