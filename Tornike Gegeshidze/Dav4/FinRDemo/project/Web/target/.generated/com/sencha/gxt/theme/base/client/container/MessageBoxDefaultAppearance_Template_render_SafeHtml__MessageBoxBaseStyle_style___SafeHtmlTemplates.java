package com.sencha.gxt.theme.base.client.container;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface MessageBoxDefaultAppearance_Template_render_SafeHtml__MessageBoxBaseStyle_style___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("<div class=\"{0}\"></div><div class=\"{1}\"></div><div class=\"{2}\"></div>\n")
  SafeHtml render0(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2);
}
