package com.task4.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class FinTypeService_Proxy extends RemoteServiceProxy implements com.task4.client.service.FinTypeServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.task4.client.service.FinTypeService";
  private static final String SERIALIZATION_POLICY ="53E2B58688E22FD9D7FBA61E9B2EA64A";
  private static final com.task4.client.service.FinTypeService_TypeSerializer SERIALIZER = new com.task4.client.service.FinTypeService_TypeSerializer();
  
  public FinTypeService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "finTypeService", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void addFiType(com.task4.shared.FinTypeDTO var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinTypeService_Proxy", "addFiType");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.FinTypeDTO/4006819129");
      streamWriter.writeObject(var1);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void deleteFiType(com.task4.shared.FinTypeDTO finTypeDTO, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinTypeService_Proxy", "deleteFiType");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.FinTypeDTO/4006819129");
      streamWriter.writeObject(finTypeDTO);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void loadFiType(com.google.gwt.user.client.rpc.AsyncCallback var1) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinTypeService_Proxy", "loadFiType");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 0);
      helper.finish(var1, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      var1.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
