package com.task4.client;

public class TemplatePropertiesImpl implements com.task4.client.TemplateProperties {
  public com.sencha.gxt.core.client.ValueProvider code() {
    return com.task4.shared.TemplateDTO_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.LabelProvider codeLabel() {
    return com.task4.shared.TemplateDTO_code_ModelLabelProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider id() {
    return com.task4.shared.TemplateDTO_id_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return com.task4.shared.TemplateDTO_name_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider schedule() {
    return com.task4.shared.TemplateDTO_schedule_ValueProviderImpl.INSTANCE;
  }
}
