package com.task4.shared;

public class FinTypeDTO_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinTypeDTO, java.lang.String> {
  public static final FinTypeDTO_code_ValueProviderImpl INSTANCE = new FinTypeDTO_code_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinTypeDTO object) {
    return object.getCode();
  }
  public void setValue(com.task4.shared.FinTypeDTO object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
