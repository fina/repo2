package com.task4.client;

public class FiTypePropertiesImpl implements com.task4.client.FiTypeProperties {
  public com.sencha.gxt.core.client.ValueProvider code() {
    return com.task4.shared.FinTypeDTO_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.LabelProvider codeLabel() {
    return com.task4.shared.FinTypeDTO_code_ModelLabelProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider id() {
    return com.task4.shared.FinTypeDTO_id_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return com.task4.shared.FinTypeDTO_name_ValueProviderImpl.INSTANCE;
  }
}
