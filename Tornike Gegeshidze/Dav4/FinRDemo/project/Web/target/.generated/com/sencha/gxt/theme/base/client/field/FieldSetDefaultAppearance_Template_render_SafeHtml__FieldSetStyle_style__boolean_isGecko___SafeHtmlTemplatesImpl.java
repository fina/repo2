package com.sencha.gxt.theme.base.client.field;

public class FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplatesImpl implements com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates {
  
  public com.google.gwt.safehtml.shared.SafeHtml render0(java.lang.String arg0) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<span class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("\"></span>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}

public com.google.gwt.safehtml.shared.SafeHtml render1(java.lang.String arg0) {
StringBuilder sb = new java.lang.StringBuilder();
sb.append("<div class=\"");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
sb.append("\"></div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}

public com.google.gwt.safehtml.shared.SafeHtml render2(java.lang.String arg0,java.lang.String arg1,com.google.gwt.safehtml.shared.SafeHtml arg2,com.google.gwt.safehtml.shared.SafeHtml arg3,java.lang.String arg4,java.lang.String arg5) {
StringBuilder sb = new java.lang.StringBuilder();
sb.append("<fieldset class=\"");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
sb.append("\"><legend class=\"");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
sb.append("\">\n  ");
sb.append(arg2.asString());
sb.append("\n  ");
sb.append(arg3.asString());
sb.append("\n  <span class=\"");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg4));
sb.append("\"></span></legend><div class=\"");
sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg5));
sb.append("\"></div></fieldset>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
