package com.sencha.gxt.theme.blue.client.window;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources {
  private static BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator _instance0 = new BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 1, 1, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 6, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 6, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 6, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 1, 150, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 6, 150, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 6, 150, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDKAC{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GCGCW0WDJAC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getHeight() + "px")  + ";width:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getTop() + "px  no-repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDABC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:") + (((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-right:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDPAC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto") ) + (";}.GCGCW0WDBBC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-left:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GCGCW0WDHAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:") + (("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDGAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto") ) + (";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDIAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"bottom")  + ";padding-left:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDLAC{width:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto") ) + (";width:" + ("auto")  + ";}.GCGCW0WDOAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("left"+ " " +"0")  + ";padding-left:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDABC{height:" + ("auto")  + ";border:") + (("none")  + ";}.GCGCW0WDPAC,.GCGCW0WDBBC{height:" + ("auto")  + ";}.noheader .GCGCW0WDABC,.noheader .GCGCW0WDPAC{height:" + ("3px")  + ";border:" + ("none")  + ";}.GCGCW0WDLAC{background-color:" + ("#dfe8f6")  + ";padding-right:" + ("6px")  + ";}.GCGCW0WDOAC{background-color:" + ("#dfe8f6")  + ";padding-left:" + ("6px")  + ";}.GCGCW0WDJAC{padding-top:" + ("0")  + ";background:" + ("#cdd9e8")  + " !important;}.GCGCW0WDGAC,.GCGCW0WDHAC,.GCGCW0WDIAC{height:" + ("6px") ) + (";}")) : ((".GCGCW0WDKAC{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GCGCW0WDJAC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getHeight() + "px")  + ";width:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.background()).getTop() + "px  no-repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDABC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:") + (((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-left:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDPAC{height:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto") ) + (";}.GCGCW0WDBBC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-right:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GCGCW0WDHAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:") + (("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDGAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto") ) + (";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDIAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"bottom")  + ";padding-right:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GCGCW0WDLAC{width:" + ((BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto") ) + (";width:" + ("auto")  + ";}.GCGCW0WDOAC{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueWindowAppearance_BlueWindowDivFrameResources_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("right"+ " " +"0")  + ";padding-right:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDABC{height:" + ("auto")  + ";border:") + (("none")  + ";}.GCGCW0WDPAC,.GCGCW0WDBBC{height:" + ("auto")  + ";}.noheader .GCGCW0WDABC,.noheader .GCGCW0WDPAC{height:" + ("3px")  + ";border:" + ("none")  + ";}.GCGCW0WDLAC{background-color:" + ("#dfe8f6")  + ";padding-left:" + ("6px")  + ";}.GCGCW0WDOAC{background-color:" + ("#dfe8f6")  + ";padding-right:" + ("6px")  + ";}.GCGCW0WDJAC{padding-top:" + ("0")  + ";background:" + ("#cdd9e8")  + " !important;}.GCGCW0WDGAC,.GCGCW0WDHAC,.GCGCW0WDIAC{height:" + ("6px") ) + (";}"));
      }
      public java.lang.String bodyWrap() {
        return "GCGCW0WDFAC";
      }
      public java.lang.String bottom() {
        return "GCGCW0WDGAC";
      }
      public java.lang.String bottomLeft() {
        return "GCGCW0WDHAC";
      }
      public java.lang.String bottomRight() {
        return "GCGCW0WDIAC";
      }
      public java.lang.String content() {
        return "GCGCW0WDJAC";
      }
      public java.lang.String contentArea() {
        return "GCGCW0WDKAC";
      }
      public java.lang.String left() {
        return "GCGCW0WDLAC";
      }
      public java.lang.String over() {
        return "GCGCW0WDMAC";
      }
      public java.lang.String pressed() {
        return "GCGCW0WDNAC";
      }
      public java.lang.String right() {
        return "GCGCW0WDOAC";
      }
      public java.lang.String top() {
        return "GCGCW0WDPAC";
      }
      public java.lang.String topLeft() {
        return "GCGCW0WDABC";
      }
      public java.lang.String topRight() {
        return "GCGCW0WDBBC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAGCAYAAAACEPQxAAAAGklEQVR42mM4c/PFfwY04tXbt/8ZOhbs+w8APS8U2XCkrSsAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAJ0lEQVR42mPoWLDv/6u3b/+fufkCBTNQWWLxllNgSWTMAAIgSXQMABJYduvtOBteAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAKUlEQVR42mM4c/PFf2T86u3b/x0L9v1noKIEiIGMF285BZEAEeiYAQgAjxh26/XQwd0AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAABCAYAAAD9yd/wAAAAF0lEQVR42mPoW3Lw/+OXb/+fvfkCBQMA/bYU39EIZ3wAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAABCAYAAAD9yd/wAAAAGElEQVR42mM4e/PFf2T8+OXb/31LDv4HAAfdFN+f/ockAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAACWCAYAAAAfduJyAAAAHUlEQVR42mPoWLDvP8Ozl2//M5y5+WKUGCWGMwEA92UTrwNgE/IAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAACWCAYAAAD9qvkLAAAAQ0lEQVR42mNgAIKOBfv+o2Ow4OItp/4/e/kWBYMlQIwzN1+g4FGJUYlRiVGJUYlRiVGJUYlRiVGJUYlRiVGJUQkKJQBGFSxfHKawhwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAACWCAYAAAD9qvkLAAAAQ0lEQVR42mPoWLDvPzpmAIFnL9/+R8aLt5yCSJ65+eI/MgZJjkqMSoxKjEqMSoxKjEqMSoxKjEqMSoxKjEqMSlBBAgDdLyxf5RSunAAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.window.BlueWindowAppearance.BlueWindowDivFrameResources::style()();
    }
    return null;
  }-*/;
}
