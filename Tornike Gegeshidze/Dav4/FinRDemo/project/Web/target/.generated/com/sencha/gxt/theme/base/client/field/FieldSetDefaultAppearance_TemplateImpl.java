package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class FieldSetDefaultAppearance_TemplateImpl implements com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance.Template {
  public com.google.gwt.safehtml.shared.SafeHtml render(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance.FieldSetStyle style, boolean isGecko){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    SafeHtml subTemplate;
    SafeHtmlBuilder subTemplate_builder = new SafeHtmlBuilder();
    if ((isGecko)) {
      SafeHtml innerTemplate;
      
      /**
       * <tpl> tag: {if=isGecko}
       */
      
      /**
       * safehtml content:
         * <span class="{0}"></span>
       * params:
         * com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_toolWrap_ValueProviderImpl.INSTANCE.getValue(style)
       */
      innerTemplate = GWT.<com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates.class).render0(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_toolWrap_ValueProviderImpl.INSTANCE.getValue(style));
      subTemplate_builder.append(innerTemplate);
    }
    subTemplate = subTemplate_builder.toSafeHtml();
    SafeHtml subTemplate1;
    SafeHtmlBuilder subTemplate1_builder = new SafeHtmlBuilder();
    if (!(isGecko)) {
      SafeHtml innerTemplate1;
      
      /**
       * <tpl> tag: {if=!isGecko}
       */
      
      /**
       * safehtml content:
         * <div class="{0}"></div>
       * params:
         * com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_toolWrap_ValueProviderImpl.INSTANCE.getValue(style)
       */
      innerTemplate1 = GWT.<com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates.class).render1(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_toolWrap_ValueProviderImpl.INSTANCE.getValue(style));
      subTemplate1_builder.append(innerTemplate1);
    }
    subTemplate1 = subTemplate1_builder.toSafeHtml();
    
    /**
     * safehtml content:
       * <fieldset class="{0}"><legend class="{1}">
       *   {2}
       *   {3}
       *   <span class="{4}"></span></legend><div class="{5}"></div></fieldset>
     * params:
       * com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_fieldSet_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_legend_ValueProviderImpl.INSTANCE.getValue(style), subTemplate, subTemplate1, com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_header_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_body_ValueProviderImpl.INSTANCE.getValue(style)
     */
    outer = GWT.<com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates.class).render2(com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_fieldSet_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_legend_ValueProviderImpl.INSTANCE.getValue(style), subTemplate, subTemplate1, com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_header_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.field.FieldSetDefaultAppearance_FieldSetStyle_body_ValueProviderImpl.INSTANCE.getValue(style));
    return outer;
  }
}
