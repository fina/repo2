package com.task4.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class ReturnService_Proxy extends RemoteServiceProxy implements com.task4.client.service.ReturnServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.task4.client.service.ReturnService";
  private static final String SERIALIZATION_POLICY ="BF4571F99D501A1D8D04A3665B97BD2B";
  private static final com.task4.client.service.ReturnService_TypeSerializer SERIALIZER = new com.task4.client.service.ReturnService_TypeSerializer();
  
  public ReturnService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "retServie", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void deleteReturn(com.task4.shared.ReturnDTO returnDTO, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("ReturnService_Proxy", "deleteReturn");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.ReturnDTO/419958979");
      streamWriter.writeObject(returnDTO);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void loadReturns(com.google.gwt.user.client.rpc.AsyncCallback var1) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("ReturnService_Proxy", "loadReturns");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 0);
      helper.finish(var1, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      var1.onFailure(ex);
    }
  }
  
  public void saveReturn(com.task4.shared.ReturnDTO var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("ReturnService_Proxy", "saveReturn");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.ReturnDTO/419958979");
      streamWriter.writeObject(var1);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
