package com.task4.client;

public class FilePropertiesImpl implements com.task4.client.FileProperties {
  public com.sencha.gxt.core.client.ValueProvider fileName() {
    return com.task4.shared.FileDTO_fileName_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider id() {
    return com.task4.shared.FileDTO_id_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider uploadDate() {
    return com.task4.shared.FileDTO_uploadDate_ValueProviderImpl.INSTANCE;
  }
}
