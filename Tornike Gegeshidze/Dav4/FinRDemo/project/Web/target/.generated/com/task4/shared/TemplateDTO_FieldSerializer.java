package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TemplateDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCode(com.task4.shared.TemplateDTO instance) /*-{
    return instance.@com.task4.shared.TemplateDTO::code;
  }-*/;
  
  private static native void setCode(com.task4.shared.TemplateDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.TemplateDTO::code = value;
  }-*/;
  
  private static native int getId(com.task4.shared.TemplateDTO instance) /*-{
    return instance.@com.task4.shared.TemplateDTO::id;
  }-*/;
  
  private static native void setId(com.task4.shared.TemplateDTO instance, int value) 
  /*-{
    instance.@com.task4.shared.TemplateDTO::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.task4.shared.TemplateDTO instance) /*-{
    return instance.@com.task4.shared.TemplateDTO::name;
  }-*/;
  
  private static native void setName(com.task4.shared.TemplateDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.TemplateDTO::name = value;
  }-*/;
  
  private static native com.task4.shared.ScheduleDTO getSchedule(com.task4.shared.TemplateDTO instance) /*-{
    return instance.@com.task4.shared.TemplateDTO::schedule;
  }-*/;
  
  private static native void setSchedule(com.task4.shared.TemplateDTO instance, com.task4.shared.ScheduleDTO value) 
  /*-{
    instance.@com.task4.shared.TemplateDTO::schedule = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.TemplateDTO instance) throws SerializationException {
    setCode(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setSchedule(instance, (com.task4.shared.ScheduleDTO) streamReader.readObject());
    
  }
  
  public static com.task4.shared.TemplateDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.TemplateDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.TemplateDTO instance) throws SerializationException {
    streamWriter.writeString(getCode(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getSchedule(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.TemplateDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.TemplateDTO_FieldSerializer.deserialize(reader, (com.task4.shared.TemplateDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.TemplateDTO_FieldSerializer.serialize(writer, (com.task4.shared.TemplateDTO)object);
  }
  
}
