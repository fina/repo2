package com.task4.shared;

public class FinInstituteDTO_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FinInstituteDTO, java.lang.String> {
  public static final FinInstituteDTO_code_ValueProviderImpl INSTANCE = new FinInstituteDTO_code_ValueProviderImpl();
  public java.lang.String getValue(com.task4.shared.FinInstituteDTO object) {
    return object.getCode();
  }
  public void setValue(com.task4.shared.FinInstituteDTO object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
