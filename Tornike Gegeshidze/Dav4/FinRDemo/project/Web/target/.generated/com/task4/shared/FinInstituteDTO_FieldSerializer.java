package com.task4.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FinInstituteDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAddress(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::address;
  }-*/;
  
  private static native void setAddress(com.task4.shared.FinInstituteDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::address = value;
  }-*/;
  
  private static native java.lang.String getCode(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::code;
  }-*/;
  
  private static native void setCode(com.task4.shared.FinInstituteDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::code = value;
  }-*/;
  
  private static native int getId(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::id;
  }-*/;
  
  private static native void setId(com.task4.shared.FinInstituteDTO instance, int value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::name;
  }-*/;
  
  private static native void setName(com.task4.shared.FinInstituteDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::name = value;
  }-*/;
  
  private static native java.lang.String getPhone(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::phone;
  }-*/;
  
  private static native void setPhone(com.task4.shared.FinInstituteDTO instance, java.lang.String value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::phone = value;
  }-*/;
  
  private static native java.util.Date getRegDate(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::regDate;
  }-*/;
  
  private static native void setRegDate(com.task4.shared.FinInstituteDTO instance, java.util.Date value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::regDate = value;
  }-*/;
  
  private static native com.task4.shared.FinTypeDTO getTypeDTO(com.task4.shared.FinInstituteDTO instance) /*-{
    return instance.@com.task4.shared.FinInstituteDTO::typeDTO;
  }-*/;
  
  private static native void setTypeDTO(com.task4.shared.FinInstituteDTO instance, com.task4.shared.FinTypeDTO value) 
  /*-{
    instance.@com.task4.shared.FinInstituteDTO::typeDTO = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.task4.shared.FinInstituteDTO instance) throws SerializationException {
    setAddress(instance, streamReader.readString());
    setCode(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setPhone(instance, streamReader.readString());
    setRegDate(instance, (java.util.Date) streamReader.readObject());
    setTypeDTO(instance, (com.task4.shared.FinTypeDTO) streamReader.readObject());
    
  }
  
  public static com.task4.shared.FinInstituteDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.task4.shared.FinInstituteDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.task4.shared.FinInstituteDTO instance) throws SerializationException {
    streamWriter.writeString(getAddress(instance));
    streamWriter.writeString(getCode(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getPhone(instance));
    streamWriter.writeObject(getRegDate(instance));
    streamWriter.writeObject(getTypeDTO(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.task4.shared.FinInstituteDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.task4.shared.FinInstituteDTO_FieldSerializer.deserialize(reader, (com.task4.shared.FinInstituteDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.task4.shared.FinInstituteDTO_FieldSerializer.serialize(writer, (com.task4.shared.FinInstituteDTO)object);
  }
  
}
