package com.sencha.gxt.theme.blue.client.tabs;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources {
  private static BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator _instance0 = new BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator();
  private void bottomInactiveLeftBackgroundInitializer() {
    bottomInactiveLeftBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomInactiveLeftBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 3, 49, false, false
    );
  }
  private static class bottomInactiveLeftBackgroundInitializer {
    static {
      _instance0.bottomInactiveLeftBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomInactiveLeftBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomInactiveLeftBackground() {
    return bottomInactiveLeftBackgroundInitializer.get();
  }
  private void bottomInactiveRightBackgroundInitializer() {
    bottomInactiveRightBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomInactiveRightBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 300, 50, false, false
    );
  }
  private static class bottomInactiveRightBackgroundInitializer {
    static {
      _instance0.bottomInactiveRightBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomInactiveRightBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomInactiveRightBackground() {
    return bottomInactiveRightBackgroundInitializer.get();
  }
  private void bottomLeftBackgroundInitializer() {
    bottomLeftBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 3, 50, false, false
    );
  }
  private static class bottomLeftBackgroundInitializer {
    static {
      _instance0.bottomLeftBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBackground() {
    return bottomLeftBackgroundInitializer.get();
  }
  private void bottomRightBackgroundInitializer() {
    bottomRightBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 300, 50, false, false
    );
  }
  private static class bottomRightBackgroundInitializer {
    static {
      _instance0.bottomRightBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBackground() {
    return bottomRightBackgroundInitializer.get();
  }
  private void scrollerLeftInitializer() {
    scrollerLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "scrollerLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 18, 43, false, false
    );
  }
  private static class scrollerLeftInitializer {
    static {
      _instance0.scrollerLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return scrollerLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource scrollerLeft() {
    return scrollerLeftInitializer.get();
  }
  private void scrollerLeftOverInitializer() {
    scrollerLeftOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "scrollerLeftOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 18, 43, false, false
    );
  }
  private static class scrollerLeftOverInitializer {
    static {
      _instance0.scrollerLeftOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return scrollerLeftOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource scrollerLeftOver() {
    return scrollerLeftOverInitializer.get();
  }
  private void scrollerRightInitializer() {
    scrollerRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "scrollerRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 18, 43, false, false
    );
  }
  private static class scrollerRightInitializer {
    static {
      _instance0.scrollerRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return scrollerRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource scrollerRight() {
    return scrollerRightInitializer.get();
  }
  private void scrollerRightOverInitializer() {
    scrollerRightOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "scrollerRightOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 18, 43, false, false
    );
  }
  private static class scrollerRightOverInitializer {
    static {
      _instance0.scrollerRightOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return scrollerRightOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource scrollerRightOver() {
    return scrollerRightOverInitializer.get();
  }
  private void tabCenterInitializer() {
    tabCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabCenter",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 1, 50, false, false
    );
  }
  private static class tabCenterInitializer {
    static {
      _instance0.tabCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabCenter() {
    return tabCenterInitializer.get();
  }
  private void tabCenterActiveInitializer() {
    tabCenterActive = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabCenterActive",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage8),
      0, 0, 1, 50, false, false
    );
  }
  private static class tabCenterActiveInitializer {
    static {
      _instance0.tabCenterActiveInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabCenterActive;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabCenterActive() {
    return tabCenterActiveInitializer.get();
  }
  private void tabCenterOverInitializer() {
    tabCenterOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabCenterOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage9),
      0, 0, 1, 50, false, false
    );
  }
  private static class tabCenterOverInitializer {
    static {
      _instance0.tabCenterOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabCenterOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabCenterOver() {
    return tabCenterOverInitializer.get();
  }
  private void tabCloseInitializer() {
    tabClose = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabClose",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage10),
      0, 0, 11, 11, false, false
    );
  }
  private static class tabCloseInitializer {
    static {
      _instance0.tabCloseInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabClose;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabClose() {
    return tabCloseInitializer.get();
  }
  private void tabLeftInitializer() {
    tabLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage11),
      0, 0, 24, 50, false, false
    );
  }
  private static class tabLeftInitializer {
    static {
      _instance0.tabLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabLeft() {
    return tabLeftInitializer.get();
  }
  private void tabLeftActiveInitializer() {
    tabLeftActive = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabLeftActive",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage12),
      0, 0, 24, 51, false, false
    );
  }
  private static class tabLeftActiveInitializer {
    static {
      _instance0.tabLeftActiveInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabLeftActive;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabLeftActive() {
    return tabLeftActiveInitializer.get();
  }
  private void tabLeftOverInitializer() {
    tabLeftOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabLeftOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage13),
      0, 0, 24, 50, false, false
    );
  }
  private static class tabLeftOverInitializer {
    static {
      _instance0.tabLeftOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabLeftOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabLeftOver() {
    return tabLeftOverInitializer.get();
  }
  private void tabRightInitializer() {
    tabRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage14),
      0, 0, 24, 50, false, false
    );
  }
  private static class tabRightInitializer {
    static {
      _instance0.tabRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabRight() {
    return tabRightInitializer.get();
  }
  private void tabRightActiveInitializer() {
    tabRightActive = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabRightActive",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage15),
      0, 0, 24, 50, false, false
    );
  }
  private static class tabRightActiveInitializer {
    static {
      _instance0.tabRightActiveInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabRightActive;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabRightActive() {
    return tabRightActiveInitializer.get();
  }
  private void tabRightOverInitializer() {
    tabRightOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabRightOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage16),
      0, 0, 24, 49, false, false
    );
  }
  private static class tabRightOverInitializer {
    static {
      _instance0.tabRightOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabRightOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabRightOver() {
    return tabRightOverInitializer.get();
  }
  private void tabStripBackgroundInitializer() {
    tabStripBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabStripBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage17),
      0, 0, 1, 26, false, false
    );
  }
  private static class tabStripBackgroundInitializer {
    static {
      _instance0.tabStripBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabStripBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabStripBackground() {
    return tabStripBackgroundInitializer.get();
  }
  private void tabStripBottomBackgroundInitializer() {
    tabStripBottomBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "tabStripBottomBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage18),
      0, 0, 1, 26, false, false
    );
  }
  private static class tabStripBottomBackgroundInitializer {
    static {
      _instance0.tabStripBottomBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return tabStripBottomBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource tabStripBottomBackground() {
    return tabStripBottomBackgroundInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDE1B{overflow:" + ("hidden")  + ";outline:" + ("none")  + ";}.GCGCW0WDP1B,.GCGCW0WDN1B{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDP1B{border:" + ("1px"+ " " +"solid")  + ";padding-bottom:" + ("2px")  + ";}.GCGCW0WDN1B{border:" + ("1px"+ " " +"solid")  + ";padding-top:" + ("2px")  + ";}.GCGCW0WDH3B{width:" + ("100%")  + ";overflow:" + ("hidden")  + ";position:") + (("relative")  + ";}.GCGCW0WDN2B{display:" + ("block")  + ";width:" + ("1000000px")  + ";}.GCGCW0WDG3B{margin-top:" + ("1px")  + ";background:" + ("repeat-x"+ " " +"bottom")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDP2B{padding-bottom:" + ("1px")  + ";background:" + ("repeat-x"+ " " +"top")  + ";border-top:" + ("1px"+ " " +"solid")  + ";border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDN2B li{float:" + ("right") ) + (";position:" + ("relative")  + ";margin-right:" + ("2px")  + ";}.GCGCW0WDN2B li.GCGCW0WDM1B{float:" + ("right")  + ";margin:" + ("0")  + " !important;padding:" + ("0")  + " !important;border:" + ("0"+ " " +"none")  + " !important;font-size:" + ("1px")  + " !important;line-height:" + ("1px")  + " !important;overflow:" + ("hidden")  + ";background:" + ("transparent")  + " !important;width:") + (("1px")  + ";}.GCGCW0WDN2B a{display:" + ("block")  + ";text-decoration:" + ("none")  + " !important;cursor:" + ("pointer")  + ";outline:" + ("none")  + ";}.GCGCW0WDN2B span,.GCGCW0WDN2B em{display:" + ("block")  + ";}.GCGCW0WDD3B{overflow:" + ("hidden")  + ";text-overflow:" + ("ellipsis")  + ";}.GCGCW0WDN2B span.GCGCW0WDF3B{white-space:" + ("nowrap")  + ";cursor:" + ("pointer")  + ";padding:" + ("4px"+ " " +"0") ) + (";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica")  + ";color:" + ("#416aa3")  + ";}.GCGCW0WDG3B .GCGCW0WDK3B .GCGCW0WDE2B{padding-right:" + ("6px")  + ";}.GCGCW0WDN2B .GCGCW0WDK3B span.GCGCW0WDF3B{padding-right:" + ("20px")  + ";background-position:" + ("0"+ " " +"3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDO2B,.GCGCW0WDO2B a.GCGCW0WDE2B,.GCGCW0WDO2B span.GCGCW0WDF3B,.GCGCW0WDC3B .GCGCW0WDL3B{cursor:" + ("default")  + ";}.GCGCW0WDH1B{overflow:" + ("hidden")  + ";border:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDL1B{overflow:" + ("hidden")  + ";}.GCGCW0WDN2B .GCGCW0WDE2B,.GCGCW0WDA3B{position:") + (("relative")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B{margin-bottom:" + ("-1px")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B span.GCGCW0WDF3B{padding-bottom:" + ("5px")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B{margin-top:" + ("-1px")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B span.GCGCW0WDF3B{padding-top:" + ("5px")  + ";}.GCGCW0WDP2B .GCGCW0WDC2B{padding:" + ("0"+ " " +"10px")  + ";}.GCGCW0WDP2B .GCGCW0WDE2B{padding:" + ("0")  + ";}.GCGCW0WDN2B .GCGCW0WDB3B{display:" + ("none")  + ";}.GCGCW0WDA3B .GCGCW0WDC2B{padding-left:" + ("19px")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B{background-repeat:" + ("no-repeat")  + ";display:" + ("block") ) + (";width:" + ("11px")  + ";height:" + ("11px")  + ";position:" + ("absolute")  + ";top:" + ("3px")  + ";left:" + ("3px")  + ";cursor:" + ("pointer")  + ";z-index:" + ("2")  + ";opacity:" + ("0.6")  + ";}.GCGCW0WDN2B .GCGCW0WDO2B a.GCGCW0WDB3B{opacity:" + ("0.8")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B:hover{opacity:" + ("1")  + ";}.GCGCW0WDK1B{border-top:") + (("0"+ " " +"none")  + ";}.GCGCW0WDI1B{border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDF2B{background:" + ("transparent"+ " " +"no-repeat"+ " " +"-18px"+ " " +"0")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";width:" + ("18px")  + ";position:" + ("absolute")  + ";right:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("10")  + ";cursor:" + ("pointer")  + ";}.GCGCW0WDG2B,.GCGCW0WDJ2B{cursor:" + ("default") ) + (";opacity:" + ("0.5")  + ";}.GCGCW0WDI2B{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";width:" + ("18px")  + ";position:" + ("absolute")  + ";left:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("10")  + ";cursor:" + ("pointer")  + ";}.GCGCW0WDM2B .GCGCW0WDF2B,.GCGCW0WDM2B .GCGCW0WDI2B{margin-top:" + ("1px")  + ";}.GCGCW0WDL2B .GCGCW0WDH3B{margin-right:") + (("18px")  + ";margin-left:" + ("18px")  + ";}.GCGCW0WDL2B{position:" + ("relative")  + ";}.GCGCW0WDF1B .GCGCW0WDM3B,.GCGCW0WDI3B .GCGCW0WDM3B{border:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";padding:" + ("2px")  + ";}.GCGCW0WDN2B li .GCGCW0WDB2B{position:" + ("absolute")  + ";top:" + ("3px")  + ";right:" + ("5px")  + ";}.GCGCW0WDG3B .GCGCW0WDE2B{top:" + ("0") ) + (";height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getTop() + "px  no-repeat")  + ";padding-right:" + ("10px")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getTop() + "px  repeat")  + ";padding-left:" + ("10px")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"left")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDA3B .GCGCW0WDC2B{padding-left:" + ("19px")  + ";}.GCGCW0WDG3B .GCGCW0WDD3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDE2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getTop() + "px  repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"left")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDD3B{height:") + (((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDC2B{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getTop() + "px  repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"left")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDD3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDP1B,.GCGCW0WDN1B{background-color:" + ("#deecfd")  + ";border-color:") + (("#8db2e3")  + ";}.GCGCW0WDG3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getTop() + "px  repeat-x")  + ";background-color:" + ("#cedff5")  + ";border-bottom-color:" + ("#8db2e3")  + ";height:" + ("auto")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDP2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getTop() + "px  repeat-x") ) + (";background-color:" + ("#cedff5")  + ";border-top-color:" + ("#8db2e3")  + ";height:" + ("auto")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDE3B span.GCGCW0WDF3B{color:" + ("#15428b")  + ";}.GCGCW0WDO2B span.GCGCW0WDF3B{color:" + ("#15428b")  + ";font-weight:" + ("bold")  + ";}.GCGCW0WDC3B .GCGCW0WDL3B{color:" + ("#aaa")  + ";}.GCGCW0WDP2B .GCGCW0WDE2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getTop() + "px  repeat")  + ";background-position:") + (("bottom"+ " " +"left")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"right")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"right")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"left")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:") + (("no-repeat")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B,.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B:hover{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDH1B{border-color:" + ("#8db2e3")  + ";background-color:" + ("#fff")  + ";}.GCGCW0WDK1B{border-top:" + ("0"+ " " +"none")  + ";}.GCGCW0WDI1B{border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDF2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getTop() + "px  no-repeat")  + ";border-bottom-color:" + ("#8db2e3")  + ";}.GCGCW0WDH2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDI2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getTop() + "px  no-repeat")  + ";border-bottom-color:" + ("#8db2e3")  + ";}.GCGCW0WDK2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDF1B .GCGCW0WDM3B,.GCGCW0WDI3B .GCGCW0WDM3B{border-color:" + ("#99bbe8")  + ";}.GCGCW0WDD2B .GCGCW0WDJ1B{border-width:" + ("0")  + ";}.GCGCW0WDD2B .GCGCW0WDA2B{border-width:" + ("0"+ " " +"0"+ " " +"1px"+ " " +"0")  + ";}.GCGCW0WDD2B .GCGCW0WDO1B{border-width:" + ("1px"+ " " +"0"+ " " +"0"+ " " +"0")  + ";}.GCGCW0WDG1B .GCGCW0WDM3B{border-width:" + ("1px"+ " " +"0"+ " " +"0"+ " " +"0") ) + (";border-style:" + ("solid")  + ";border-top-color:" + ("#99bbe8")  + ";}.GCGCW0WDJ3B .GCGCW0WDM3B{border-width:" + ("0"+ " " +"0"+ " " +"1px")  + ";border-style:" + ("solid")  + ";border-bottom-color:" + ("#99bbe8")  + ";}")) : ((".GCGCW0WDE1B{overflow:" + ("hidden")  + ";outline:" + ("none")  + ";}.GCGCW0WDP1B,.GCGCW0WDN1B{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";}.GCGCW0WDP1B{border:" + ("1px"+ " " +"solid")  + ";padding-bottom:" + ("2px")  + ";}.GCGCW0WDN1B{border:" + ("1px"+ " " +"solid")  + ";padding-top:" + ("2px")  + ";}.GCGCW0WDH3B{width:" + ("100%")  + ";overflow:" + ("hidden")  + ";position:") + (("relative")  + ";}.GCGCW0WDN2B{display:" + ("block")  + ";width:" + ("1000000px")  + ";}.GCGCW0WDG3B{margin-top:" + ("1px")  + ";background:" + ("repeat-x"+ " " +"bottom")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDP2B{padding-bottom:" + ("1px")  + ";background:" + ("repeat-x"+ " " +"top")  + ";border-top:" + ("1px"+ " " +"solid")  + ";border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDN2B li{float:" + ("left") ) + (";position:" + ("relative")  + ";margin-left:" + ("2px")  + ";}.GCGCW0WDN2B li.GCGCW0WDM1B{float:" + ("left")  + ";margin:" + ("0")  + " !important;padding:" + ("0")  + " !important;border:" + ("0"+ " " +"none")  + " !important;font-size:" + ("1px")  + " !important;line-height:" + ("1px")  + " !important;overflow:" + ("hidden")  + ";background:" + ("transparent")  + " !important;width:") + (("1px")  + ";}.GCGCW0WDN2B a{display:" + ("block")  + ";text-decoration:" + ("none")  + " !important;cursor:" + ("pointer")  + ";outline:" + ("none")  + ";}.GCGCW0WDN2B span,.GCGCW0WDN2B em{display:" + ("block")  + ";}.GCGCW0WDD3B{overflow:" + ("hidden")  + ";text-overflow:" + ("ellipsis")  + ";}.GCGCW0WDN2B span.GCGCW0WDF3B{white-space:" + ("nowrap")  + ";cursor:" + ("pointer")  + ";padding:" + ("4px"+ " " +"0") ) + (";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica")  + ";color:" + ("#416aa3")  + ";}.GCGCW0WDG3B .GCGCW0WDK3B .GCGCW0WDE2B{padding-left:" + ("6px")  + ";}.GCGCW0WDN2B .GCGCW0WDK3B span.GCGCW0WDF3B{padding-left:" + ("20px")  + ";background-position:" + ("0"+ " " +"3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDO2B,.GCGCW0WDO2B a.GCGCW0WDE2B,.GCGCW0WDO2B span.GCGCW0WDF3B,.GCGCW0WDC3B .GCGCW0WDL3B{cursor:" + ("default")  + ";}.GCGCW0WDH1B{overflow:" + ("hidden")  + ";border:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDL1B{overflow:" + ("hidden")  + ";}.GCGCW0WDN2B .GCGCW0WDE2B,.GCGCW0WDA3B{position:") + (("relative")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B{margin-bottom:" + ("-1px")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B span.GCGCW0WDF3B{padding-bottom:" + ("5px")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B{margin-top:" + ("-1px")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B span.GCGCW0WDF3B{padding-top:" + ("5px")  + ";}.GCGCW0WDP2B .GCGCW0WDC2B{padding:" + ("0"+ " " +"10px")  + ";}.GCGCW0WDP2B .GCGCW0WDE2B{padding:" + ("0")  + ";}.GCGCW0WDN2B .GCGCW0WDB3B{display:" + ("none")  + ";}.GCGCW0WDA3B .GCGCW0WDC2B{padding-right:" + ("19px")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B{background-repeat:" + ("no-repeat")  + ";display:" + ("block") ) + (";width:" + ("11px")  + ";height:" + ("11px")  + ";position:" + ("absolute")  + ";top:" + ("3px")  + ";right:" + ("3px")  + ";cursor:" + ("pointer")  + ";z-index:" + ("2")  + ";opacity:" + ("0.6")  + ";}.GCGCW0WDN2B .GCGCW0WDO2B a.GCGCW0WDB3B{opacity:" + ("0.8")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B:hover{opacity:" + ("1")  + ";}.GCGCW0WDK1B{border-top:") + (("0"+ " " +"none")  + ";}.GCGCW0WDI1B{border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDF2B{background:" + ("transparent"+ " " +"no-repeat"+ " " +"-18px"+ " " +"0")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";width:" + ("18px")  + ";position:" + ("absolute")  + ";left:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("10")  + ";cursor:" + ("pointer")  + ";}.GCGCW0WDG2B,.GCGCW0WDJ2B{cursor:" + ("default") ) + (";opacity:" + ("0.5")  + ";}.GCGCW0WDI2B{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";width:" + ("18px")  + ";position:" + ("absolute")  + ";right:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("10")  + ";cursor:" + ("pointer")  + ";}.GCGCW0WDM2B .GCGCW0WDF2B,.GCGCW0WDM2B .GCGCW0WDI2B{margin-top:" + ("1px")  + ";}.GCGCW0WDL2B .GCGCW0WDH3B{margin-left:") + (("18px")  + ";margin-right:" + ("18px")  + ";}.GCGCW0WDL2B{position:" + ("relative")  + ";}.GCGCW0WDF1B .GCGCW0WDM3B,.GCGCW0WDI3B .GCGCW0WDM3B{border:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";padding:" + ("2px")  + ";}.GCGCW0WDN2B li .GCGCW0WDB2B{position:" + ("absolute")  + ";top:" + ("3px")  + ";left:" + ("5px")  + ";}.GCGCW0WDG3B .GCGCW0WDE2B{top:" + ("0") ) + (";height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeft()).getTop() + "px  no-repeat")  + ";padding-left:" + ("10px")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRight()).getTop() + "px  repeat")  + ";padding-right:" + ("10px")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"right")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDA3B .GCGCW0WDC2B{padding-right:" + ("19px")  + ";}.GCGCW0WDG3B .GCGCW0WDD3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenter()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDE2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftOver()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightOver()).getTop() + "px  repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"right")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDG3B .GCGCW0WDE3B .GCGCW0WDD3B{height:") + (((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterOver()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDE2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabLeftActive()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDC2B{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabRightActive()).getTop() + "px  repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("top"+ " " +"right")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDG3B .GCGCW0WDO2B .GCGCW0WDD3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabCenterActive()).getTop() + "px  repeat-x")  + ";height:" + ("auto")  + ";}.GCGCW0WDP1B,.GCGCW0WDN1B{background-color:" + ("#deecfd")  + ";border-color:") + (("#8db2e3")  + ";}.GCGCW0WDG3B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBackground()).getTop() + "px  repeat-x")  + ";background-color:" + ("#cedff5")  + ";border-bottom-color:" + ("#8db2e3")  + ";height:" + ("auto")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDP2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabStripBottomBackground()).getTop() + "px  repeat-x") ) + (";background-color:" + ("#cedff5")  + ";border-top-color:" + ("#8db2e3")  + ";height:" + ("auto")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDE3B span.GCGCW0WDF3B{color:" + ("#15428b")  + ";}.GCGCW0WDO2B span.GCGCW0WDF3B{color:" + ("#15428b")  + ";font-weight:" + ("bold")  + ";}.GCGCW0WDC3B .GCGCW0WDL3B{color:" + ("#aaa")  + ";}.GCGCW0WDP2B .GCGCW0WDE2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveRightBackground()).getTop() + "px  repeat")  + ";background-position:") + (("bottom"+ " " +"right")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomInactiveLeftBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"left")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDE2B{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomRightBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"left")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:" + ("no-repeat")  + ";}.GCGCW0WDP2B .GCGCW0WDO2B .GCGCW0WDC2B{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.bottomLeftBackground()).getTop() + "px  repeat")  + ";background-position:" + ("bottom"+ " " +"right")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-repeat:") + (("no-repeat")  + ";}.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B,.GCGCW0WDN2B .GCGCW0WDA3B a.GCGCW0WDB3B:hover{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.tabClose()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDH1B{border-color:" + ("#8db2e3")  + ";background-color:" + ("#fff")  + ";}.GCGCW0WDK1B{border-top:" + ("0"+ " " +"none")  + ";}.GCGCW0WDI1B{border-bottom:" + ("0"+ " " +"none")  + ";}.GCGCW0WDF2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeft()).getTop() + "px  no-repeat")  + ";border-bottom-color:" + ("#8db2e3")  + ";}.GCGCW0WDH2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerLeftOver()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDI2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRight()).getTop() + "px  no-repeat")  + ";border-bottom-color:" + ("#8db2e3")  + ";}.GCGCW0WDK2B{height:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getHeight() + "px")  + ";width:" + ((BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getSafeUri().asString() + "\") -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getLeft() + "px -" + (BlueTabPanelAppearance_BlueTabPanelResources_ie9_default_InlineClientBundleGenerator.this.scrollerRightOver()).getTop() + "px  no-repeat")  + ";}.GCGCW0WDF1B .GCGCW0WDM3B,.GCGCW0WDI3B .GCGCW0WDM3B{border-color:" + ("#99bbe8")  + ";}.GCGCW0WDD2B .GCGCW0WDJ1B{border-width:" + ("0")  + ";}.GCGCW0WDD2B .GCGCW0WDA2B{border-width:" + ("0"+ " " +"0"+ " " +"1px"+ " " +"0")  + ";}.GCGCW0WDD2B .GCGCW0WDO1B{border-width:" + ("1px"+ " " +"0"+ " " +"0"+ " " +"0")  + ";}.GCGCW0WDG1B .GCGCW0WDM3B{border-width:" + ("1px"+ " " +"0"+ " " +"0"+ " " +"0") ) + (";border-style:" + ("solid")  + ";border-top-color:" + ("#99bbe8")  + ";}.GCGCW0WDJ3B .GCGCW0WDM3B{border-width:" + ("0"+ " " +"0"+ " " +"1px")  + ";border-style:" + ("solid")  + ";border-bottom-color:" + ("#99bbe8")  + ";}"));
      }
      public java.lang.String tab() {
        return "GCGCW0WDE1B";
      }
      public java.lang.String tabBbar() {
        return "GCGCW0WDF1B";
      }
      public java.lang.String tabBbarNoborder() {
        return "GCGCW0WDG1B";
      }
      public java.lang.String tabBody() {
        return "GCGCW0WDH1B";
      }
      public java.lang.String tabBodyBottom() {
        return "GCGCW0WDI1B";
      }
      public java.lang.String tabBodyNoborder() {
        return "GCGCW0WDJ1B";
      }
      public java.lang.String tabBodyTop() {
        return "GCGCW0WDK1B";
      }
      public java.lang.String tabBwrap() {
        return "GCGCW0WDL1B";
      }
      public java.lang.String tabEdge() {
        return "GCGCW0WDM1B";
      }
      public java.lang.String tabFooter() {
        return "GCGCW0WDN1B";
      }
      public java.lang.String tabFooterNoborder() {
        return "GCGCW0WDO1B";
      }
      public java.lang.String tabHeader() {
        return "GCGCW0WDP1B";
      }
      public java.lang.String tabHeaderNoborder() {
        return "GCGCW0WDA2B";
      }
      public java.lang.String tabImage() {
        return "GCGCW0WDB2B";
      }
      public java.lang.String tabLeft() {
        return "GCGCW0WDC2B";
      }
      public java.lang.String tabNoborder() {
        return "GCGCW0WDD2B";
      }
      public java.lang.String tabRight() {
        return "GCGCW0WDE2B";
      }
      public java.lang.String tabScrollerLeft() {
        return "GCGCW0WDF2B";
      }
      public java.lang.String tabScrollerLeftDisabled() {
        return "GCGCW0WDG2B";
      }
      public java.lang.String tabScrollerLeftOver() {
        return "GCGCW0WDH2B";
      }
      public java.lang.String tabScrollerRight() {
        return "GCGCW0WDI2B";
      }
      public java.lang.String tabScrollerRightDisabled() {
        return "GCGCW0WDJ2B";
      }
      public java.lang.String tabScrollerRightOver() {
        return "GCGCW0WDK2B";
      }
      public java.lang.String tabScrolling() {
        return "GCGCW0WDL2B";
      }
      public java.lang.String tabScrollingBottom() {
        return "GCGCW0WDM2B";
      }
      public java.lang.String tabStrip() {
        return "GCGCW0WDN2B";
      }
      public java.lang.String tabStripActive() {
        return "GCGCW0WDO2B";
      }
      public java.lang.String tabStripBottom() {
        return "GCGCW0WDP2B";
      }
      public java.lang.String tabStripClosable() {
        return "GCGCW0WDA3B";
      }
      public java.lang.String tabStripClose() {
        return "GCGCW0WDB3B";
      }
      public java.lang.String tabStripDisabled() {
        return "GCGCW0WDC3B";
      }
      public java.lang.String tabStripInner() {
        return "GCGCW0WDD3B";
      }
      public java.lang.String tabStripOver() {
        return "GCGCW0WDE3B";
      }
      public java.lang.String tabStripText() {
        return "GCGCW0WDF3B";
      }
      public java.lang.String tabStripTop() {
        return "GCGCW0WDG3B";
      }
      public java.lang.String tabStripWrap() {
        return "GCGCW0WDH3B";
      }
      public java.lang.String tabTbar() {
        return "GCGCW0WDI3B";
      }
      public java.lang.String tabTbarNoborder() {
        return "GCGCW0WDJ3B";
      }
      public java.lang.String tabWithIcon() {
        return "GCGCW0WDK3B";
      }
      public java.lang.String tabsText() {
        return "GCGCW0WDL3B";
      }
      public java.lang.String xToolbar() {
        return "GCGCW0WDM3B";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAxCAYAAAAfmhMZAAAAnUlEQVR42u3PwQ7BQBAG4Hn/xAPg4ubQmysuLg6aRhARpFnaLDZdRaVorV+7kew69Opkbt9kMjM/1bt7uNsX/EiB/vgtptzCSlhgUhnwkwWRWIi+cLUgKxGnwFo8DZaH3IwtdrlZPefZ5+hFYRY+QI2eKD4AJsEd1OwLhLHCeFOgNZD6hsduoPbwjLJcPwWVKZ1RohvEjpmOXesEeAOZUw4BxKLGngAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAAyCAYAAADm1uYqAAABOElEQVR42u3cwS5DQRiG4XP/SS8AGzsLu26xsbHQiCAiSHM4TXHSKipVrfo5SK+gi5l4vuS5gVm8ySSTKbqDRQCkrFmrXUXhMIDUdW4/Y23nXrAAwQIQLECwAAQLQLAAwQIQLADBAgQLQLAABAsQLADBAhAsQLAABAtAsADBAhAsAMECBAtAsAAECxAsAMECECxAsAAEC/i/TvuCBWTiqhYsIBPlcCFYQB76T4IFZKIeCxaQiYFgAdkE61WwgEwMBQsQLIAVuq4/YjSJ32BdPswDIGXLYF3czQMgZcsr4Xl/FgApWz5rOOu9B0DK6pe/YJ1U0wBIWT2OWN+tozi+mQZAynqjRWzsfQfrqHwLgJQ1zxs294dRdLqTAEhZs62D5yjCzCzxbR+Of34dLVrtKgBS1sSqfJzFF7R9q/JNnSUmAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAyCAYAAACZDmG3AAAAVUlEQVR42mO49+bvfxDo3fT4P8MoZ5hzXn76g+C8/ozEefcVSdmHb0iczz+QOF9//kNwfvxG4vz6g8T5C2FDOCDG2XufERwQ4/Kzf/8ZYIwH7379BwDijCEfD/qNiQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAAyCAYAAADm1uYqAAAA6ElEQVR42u3UwQnCQBRF0fQPNmMfrtwHQRBECWJQYszMiIK/AhdfOA9OA29xu9V6397rzwUgtU6wAMECECxAsJwBCBaAYAGCBSBYAIIFCBaAYAEIFiBYAIIFIFiAYAEIFoBgAYIFIFgAggUIFoBgAQgWIFgAggUgWIBgAQgWIFiCBQgWgGABguUMQLAABAsQLADBAvhJsI7XBSC1CNZpXABSi2ANtwKQWgTrci8AqUWwxqkApBbBuj0qQGoRrOlZAVKLYM1LBUgtglUqQG6fYG36sZmZZV+3PdQmWmb2F8HaDXP7Rgsgsxfk/SZ3GfCBAQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAArCAYAAAB1u9gyAAAArUlEQVR42u3SMQrCQBCF4T2YBxE8iIXXsPcEtjZ2VhZaKKgQUEQEQRDWIYWNPtmAYQhJdmEGUZiBv3t81TindYPRARq5/PGCtALKTh6S9KHt0UNSCa33HpLOt+eXoW5/nAYtMo+mOr1hUdsmCn0QEcSRumqh+Y7AiyEhvi+h2YZQLQbxbStUxZo2SRDHxFAo/FESNF0RJP0wNFkSJKlBl7sSdCWDDDLIIIP+DXoD2ckavG79kKUAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAArCAYAAAB1u9gyAAAAtUlEQVR42u3SMQrCMBTG8VzOiwiews2L9ByOjg5uLh0sBaXFQgfFwSV5ksCLjzavDSaL5RX+25cfoa1SuZ5tUUGO1OttIDUH3XoNKS0YsjnoctfA1T2/cZtZCIH1bj+JeahsNAzDg6tN4aI3w3DLQkMkGjpfNWAhJJTd4JkRFIvMQsk3OtUGaLHvCPcsFMJ+hihG/6PQzkPHygAXvQG38dChNJDSgqH2kQmyH0IggQQSSKD/gj4lrXlMaxHraQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAArCAYAAAB1u9gyAAAAqUlEQVR42mNgoBaonn/vPzUw2KAvP/5RjOEG3Xr0gSJMfYOuPfhAEQYb9OTt3/+X7n6gCNPXoIjytcQbdObWB5zYJnoKGONTQ5JB+AzDahCyRmwYr0Enrn+CY0IGgTCyehCGG3To6ic4JmQIsloYxmoQOiZkCMkG4VNDlEGgdIRPHsWg3Rc/UYQHsUHbzn6iCIMNev6RSga9+jRq0KhBowaNGjRq0EgyCAC/hxq8dGSaowAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAArCAYAAAB1u9gyAAAAtElEQVR42mNgoBaonn/vPzUw2KAvP/5RjOEGPXn7lyI8SA2i1BAUg+6+xI5ffUJgXGpAGK9BMAOSmnYSNAxu0M1nEIzsAhh2TZ0PxjA+TC0yJskgfIbBDbryGILRNWLDMMNgekCYLIOwGUZ9F5198A+MiQ0jmHoYJskgXIZgNQgZY0tH2NShGHTy3j+sGNlluNSAMNyggzf/UYQHqUHPP1LJIFBAjho0atCoQaMGjRo0cgwCAEqeeUw7OcV5AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAyCAYAAACd+7GKAAAAMklEQVR42mOonn/vP8Pff//+Mxy6+gVKHLvx/T/DidtA4vTdH1Di3MM//xmuPf07bAgAbe24aHR3Ga8AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAyCAYAAACd+7GKAAAAK0lEQVR42mOonn/vP8Pff/8wiK+/gMTHb3+hxKtPMOLx+39Q4trTv0OXAACxursCASoSSAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAyCAYAAACd+7GKAAAAK0lEQVR42mPoXf/yP8Pff//+M5x7+Oc/w7Wnf5GJG8+BErdfoxCP3w8LAgDpKL1JXa6wQwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage10 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAoklEQVR42mNggIKZu1/8x4UZkAFIAB+Aa4ApfPj5///jtz////DzPxyD+CBxuAYQ8fsvRCJ/4gEwBimEsUHiIHm44u9//v9/8e3//4zuPRgYJA6Shyv+8gviDBBOatsBxzAxkDxcMUjg/JPf/6MbNmNgkDhIHkVx76bH/0Oq1oPxped/4GyQOIbiay//gSVANAzD+HDFsOCDuQ8bxhoxhGIQAMDHclofqMA+AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage11 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAyCAYAAABGQBuoAAAAiklEQVR42u3UsQqDQBCE4XtvH8ZniYUIAbsE5YggQQKmCIigRW5X7pCtBZmkmYO/na9Zzrn9ZXmniGw8iECy8eI2Q0rAfQhYwL9Fy2aBlIDnR7TyCyQDro8V0m+AcQpa919IBsRLQkTgGBCviMB/geYFBlDjBsQ/AwpcWiVAgAABAgQIECBAgMDZNo0hG1YA0sdoAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage12 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAzCAYAAACNHMgNAAAAY0lEQVR42u3NMQqAMBBE0dzbw3gX76CljdjYBCVJIWwUJOQEzlZ/4LfzQmgbxvVR1M+tVknf+TQnLaA69wXyrakDZzFJfsBxmSQ/YIua/IBlN0kAAAAAAAAAAAAAAAAAAP/1ApOZuxiTG+cWAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage13 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAyCAYAAABGQBuoAAAAiElEQVR42u3UwQpAQBSF4Xlvz+AZPIK1pbWyUSJRkoSFsrIwM5qbrJWOxXSm/u396tYdpe4XxrNFJMODqLPaGEgyPClOm/eYBChHDUuAZjVYYNh+AKoJ0wO4NSESYNm1B4BbEyJPANRwAu8Bd9JQwH1KUCBrDyyQ1pYAAQIECBAgQIAAAQJfuwCuDYY71C+1GQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage14 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAyCAYAAABGQBuoAAAAh0lEQVR42u3UwQpAQBSFYe/tYTwLCyllRyNRkhQLJTUWuJqMeYPDwpn6t/M13el6ftAIIu85x3kKIodE+SqIHIICiv64gVhpQVRN9gVJpQVRN1sgrTdB9B6QtbsgGhc7ZDNtRAS+B9wvIvAZoAYw4NY16nKzj6BAWAoBAgQIECBAgAABAv8CLmXAG1YDNwzAAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage15 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAyCAYAAABGQBuoAAAAZ0lEQVR42u3NMQ5AUBRE0b9vi7EXe6DUiEYjflBI3htRIPo/rzE3ufVJVd2Dcbozd5S+adcXYQAfhAVcxwDb4WD8AMtuYBwHTNnAOA4YZgfjOKAbDYwFCBAgQIAAAQIECBAgQMAPgRPXNmIBRetLYQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage16 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAxCAYAAADA1GkGAAAAfElEQVR42u3VMQqAMBBE0dwfPIs3sVTBSkTEWAiCjZgdCeheQAZEJvDbPALJJhTlDEbhWckMjByp+hOMHKnHBEbDSgbilrjAftwnaCcDIwe6xcDoR0C+TowECHifzyIW4NOUtXl+bFSgieAC+VcTIECAAAECBAgQIOBLwAWVrjhU+WP7SwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage17 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAaCAYAAAB2BDbRAAAAVklEQVR42jXENRKAQBAEwP3/13CXE1wynHSZo6CDpmq+mfSE1HShEUmTGFA5nExF/5d3B1PWotSUNG87U1yjyBRWKNDbl6+Qp1YmVyLnTSBbLExWufADSPFfBJewk+oAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage18 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAaCAYAAAB2BDbRAAAASklEQVR42mPYeeHtfwYwceLWx/8M5+5//c9w4QGQuPTo23+Gy4+BxNWn3/8zXH/24z/DzRdA4varn/8Z7rwGEvfe/vrP8OAdZQQAMrhgjRAUw80AAAAASUVORK5CYII=";
  private static com.google.gwt.resources.client.ImageResource bottomInactiveLeftBackground;
  private static com.google.gwt.resources.client.ImageResource bottomInactiveRightBackground;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBackground;
  private static com.google.gwt.resources.client.ImageResource bottomRightBackground;
  private static com.google.gwt.resources.client.ImageResource scrollerLeft;
  private static com.google.gwt.resources.client.ImageResource scrollerLeftOver;
  private static com.google.gwt.resources.client.ImageResource scrollerRight;
  private static com.google.gwt.resources.client.ImageResource scrollerRightOver;
  private static com.google.gwt.resources.client.ImageResource tabCenter;
  private static com.google.gwt.resources.client.ImageResource tabCenterActive;
  private static com.google.gwt.resources.client.ImageResource tabCenterOver;
  private static com.google.gwt.resources.client.ImageResource tabClose;
  private static com.google.gwt.resources.client.ImageResource tabLeft;
  private static com.google.gwt.resources.client.ImageResource tabLeftActive;
  private static com.google.gwt.resources.client.ImageResource tabLeftOver;
  private static com.google.gwt.resources.client.ImageResource tabRight;
  private static com.google.gwt.resources.client.ImageResource tabRightActive;
  private static com.google.gwt.resources.client.ImageResource tabRightOver;
  private static com.google.gwt.resources.client.ImageResource tabStripBackground;
  private static com.google.gwt.resources.client.ImageResource tabStripBottomBackground;
  private static com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      bottomInactiveLeftBackground(), 
      bottomInactiveRightBackground(), 
      bottomLeftBackground(), 
      bottomRightBackground(), 
      scrollerLeft(), 
      scrollerLeftOver(), 
      scrollerRight(), 
      scrollerRightOver(), 
      tabCenter(), 
      tabCenterActive(), 
      tabCenterOver(), 
      tabClose(), 
      tabLeft(), 
      tabLeftActive(), 
      tabLeftOver(), 
      tabRight(), 
      tabRightActive(), 
      tabRightOver(), 
      tabStripBackground(), 
      tabStripBottomBackground(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("bottomInactiveLeftBackground", bottomInactiveLeftBackground());
        resourceMap.put("bottomInactiveRightBackground", bottomInactiveRightBackground());
        resourceMap.put("bottomLeftBackground", bottomLeftBackground());
        resourceMap.put("bottomRightBackground", bottomRightBackground());
        resourceMap.put("scrollerLeft", scrollerLeft());
        resourceMap.put("scrollerLeftOver", scrollerLeftOver());
        resourceMap.put("scrollerRight", scrollerRight());
        resourceMap.put("scrollerRightOver", scrollerRightOver());
        resourceMap.put("tabCenter", tabCenter());
        resourceMap.put("tabCenterActive", tabCenterActive());
        resourceMap.put("tabCenterOver", tabCenterOver());
        resourceMap.put("tabClose", tabClose());
        resourceMap.put("tabLeft", tabLeft());
        resourceMap.put("tabLeftActive", tabLeftActive());
        resourceMap.put("tabLeftOver", tabLeftOver());
        resourceMap.put("tabRight", tabRight());
        resourceMap.put("tabRightActive", tabRightActive());
        resourceMap.put("tabRightOver", tabRightOver());
        resourceMap.put("tabStripBackground", tabStripBackground());
        resourceMap.put("tabStripBottomBackground", tabStripBottomBackground());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'bottomInactiveLeftBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::bottomInactiveLeftBackground()();
      case 'bottomInactiveRightBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::bottomInactiveRightBackground()();
      case 'bottomLeftBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::bottomLeftBackground()();
      case 'bottomRightBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::bottomRightBackground()();
      case 'scrollerLeft': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::scrollerLeft()();
      case 'scrollerLeftOver': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::scrollerLeftOver()();
      case 'scrollerRight': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::scrollerRight()();
      case 'scrollerRightOver': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::scrollerRightOver()();
      case 'tabCenter': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabCenter()();
      case 'tabCenterActive': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabCenterActive()();
      case 'tabCenterOver': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabCenterOver()();
      case 'tabClose': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabClose()();
      case 'tabLeft': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabLeft()();
      case 'tabLeftActive': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabLeftActive()();
      case 'tabLeftOver': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabLeftOver()();
      case 'tabRight': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabRight()();
      case 'tabRightActive': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabRightActive()();
      case 'tabRightOver': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabRightOver()();
      case 'tabStripBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabStripBackground()();
      case 'tabStripBottomBackground': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::tabStripBottomBackground()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.tabs.BlueTabPanelAppearance.BlueTabPanelResources::style()();
    }
    return null;
  }-*/;
}
