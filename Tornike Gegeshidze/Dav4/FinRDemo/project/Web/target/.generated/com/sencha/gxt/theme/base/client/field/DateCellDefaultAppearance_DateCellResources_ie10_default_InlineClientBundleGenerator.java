package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources {
  private static DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator _instance0 = new DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (("input.GCGCW0WDCR,textarea.GCGCW0WDCR{border:" + ("1px"+ " " +"solid"+ " " +"#7eadd9")  + ";}input.GCGCW0WDDR,textarea.GCGCW0WDDR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getTop() + "px  repeat-x")  + ";background-color:" + ("#fff")  + ";background-position:" + ("bottom")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#c30")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";}.GCGCW0WDJR{position:" + ("relative")  + ";right:") + (("0")  + ";top:" + ("0")  + ";zoom:" + ("1")  + ";white-space:" + ("nowrap")  + ";text-align:" + ("right")  + ";}.GCGCW0WDAR{font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDPQ{color:" + ("gray")  + ";}.GCGCW0WDHR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getTop() + "px  repeat-x")  + ";height:" + ("auto") ) + (";background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GCGCW0WDNQ{background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:") + (("none")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";border-bottom-color:" + ("#b5b8c8")  + ";}.GCGCW0WDCR .GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDFR .GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDOQ .GCGCW0WDIR{height:") + (((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDER{cursor:" + ("pointer")  + ";}")) : (("input.GCGCW0WDCR,textarea.GCGCW0WDCR{border:" + ("1px"+ " " +"solid"+ " " +"#7eadd9")  + ";}input.GCGCW0WDDR,textarea.GCGCW0WDDR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.invalidLine()).getTop() + "px  repeat-x")  + ";background-color:" + ("#fff")  + ";background-position:" + ("bottom")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#c30")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";}.GCGCW0WDJR{position:" + ("relative")  + ";left:") + (("0")  + ";top:" + ("0")  + ";zoom:" + ("1")  + ";white-space:" + ("nowrap")  + ";text-align:" + ("left")  + ";}.GCGCW0WDAR{font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDPQ{color:" + ("gray")  + ";}.GCGCW0WDHR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.textBackground()).getTop() + "px  repeat-x")  + ";height:" + ("auto") ) + (";background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GCGCW0WDNQ{background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:") + (("none")  + ";overflow:" + ("visible")  + ";}.GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrow()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom:" + ("1px"+ " " +"solid")  + ";border-bottom-color:" + ("#b5b8c8")  + ";}.GCGCW0WDCR .GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowFocus()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDFR .GCGCW0WDIR{height:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowOver()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDOQ .GCGCW0WDIR{height:") + (((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getHeight() + "px")  + ";width:" + ((DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getSafeUri().asString() + "\") -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getLeft() + "px -" + (DateCellDefaultAppearance_DateCellResources_ie10_default_InlineClientBundleGenerator.this.triggerArrowClick()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GCGCW0WDER{cursor:" + ("pointer")  + ";}"));
      }
      public java.lang.String area() {
        return "GCGCW0WDNQ";
      }
      public java.lang.String click() {
        return "GCGCW0WDOQ";
      }
      public java.lang.String empty() {
        return "GCGCW0WDPQ";
      }
      public java.lang.String field() {
        return "GCGCW0WDAR";
      }
      public java.lang.String file() {
        return "GCGCW0WDBR";
      }
      public java.lang.String focus() {
        return "GCGCW0WDCR";
      }
      public java.lang.String invalid() {
        return "GCGCW0WDDR";
      }
      public java.lang.String noedit() {
        return "GCGCW0WDER";
      }
      public java.lang.String over() {
        return "GCGCW0WDFR";
      }
      public java.lang.String readonly() {
        return "GCGCW0WDGR";
      }
      public java.lang.String text() {
        return "GCGCW0WDHR";
      }
      public java.lang.String trigger() {
        return "GCGCW0WDIR";
      }
      public java.lang.String wrap() {
        return "GCGCW0WDJR";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellStyle css() {
    return cssInitializer.get();
  }
  private void invalidLineInitializer() {
    invalidLine = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "invalidLine",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 4, 3, false, false
    );
  }
  private static class invalidLineInitializer {
    static {
      _instance0.invalidLineInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return invalidLine;
    }
  }
  public com.google.gwt.resources.client.ImageResource invalidLine() {
    return invalidLineInitializer.get();
  }
  private void textBackgroundInitializer() {
    textBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 18, false, false
    );
  }
  private static class textBackgroundInitializer {
    static {
      _instance0.textBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource textBackground() {
    return textBackgroundInitializer.get();
  }
  private void triggerArrowInitializer() {
    triggerArrow = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrow",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowInitializer {
    static {
      _instance0.triggerArrowInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrow;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrow() {
    return triggerArrowInitializer.get();
  }
  private void triggerArrowClickInitializer() {
    triggerArrowClick = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowClick",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowClickInitializer {
    static {
      _instance0.triggerArrowClickInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowClick;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowClick() {
    return triggerArrowClickInitializer.get();
  }
  private void triggerArrowFocusInitializer() {
    triggerArrowFocus = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowFocus",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowFocusInitializer {
    static {
      _instance0.triggerArrowFocusInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowFocus;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowFocus() {
    return triggerArrowFocusInitializer.get();
  }
  private void triggerArrowOverInitializer() {
    triggerArrowOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowOverInitializer {
    static {
      _instance0.triggerArrowOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowOver() {
    return triggerArrowOverInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellStyle css;
  private static final java.lang.String externalImage = GWT.getModuleBaseForStaticFiles() + "2659A66C9CEC1586DA091ACEC4A3AE6B.cache.png";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAASCAYAAACaV7S8AAAANElEQVR42pXEuQnAQAwAwe2/SCcGxwYhhLhn7RZuguG6H3kjJDIlq6S6pceQOZesvcXfUR9VjEbUejI8wwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAYCAYAAAAcYhYyAAAA6UlEQVR42u2SS2rDMBRFvetsoCvoQjIr7QI66NQfbGywuwF/ofHAbnvDiZAo1LSWQmZ5cJBA7x59UPT08qZriKjPr2+FUpTvRjRNk0Kwoqsk8HE6Gck4joLja6/D4/Mu6CWzrquRDMMg2CuwkHGSvu8FLDRN8wvKjnZOL5ndEstDFF3YlHRdJ2Bha1eKsC3m9JJxkrZtBb4SMsuyGEld1wLf65CZ59lIqqoS/HWSrYcl4yRlWQp8JWScpCgKwX/X+Qm9ZJwkz3OB70nIOEmWZQLfH0vGSdI0VShOkiSJQnGSOI4Vyl1yY8kZKtJ7tQewBjMAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAYCAYAAAAcYhYyAAAAnUlEQVR42mOoW3vzPyWYAQRO3X72n1y88/xziEGUGALCg9SQdUfe/S+ftJ0oDFKL1RBiDYBhnIbcu3cPA4MAjIaxSTYEhlsYGMCYoCHYbAUBkGYYALExDFl38fF/ECbVELC+C4+hhlyAcEj1DkwfhiG4XIItYGlrCD7vIGPauGT9hSf/QZjUFAvSAzdkA5BDLoYbshHIIRePGkJjQwDzzk5ykMw99QAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAYCAYAAAAcYhYyAAAA10lEQVR42u2QvQrCMBRG89ZuTj6B76GTKOLqIlKqiIODVesfLoK/INZeOUougq2aOuoHhwZyv5OkJl8ayDcYco5iyUrdX95F/elGshBdYiFfSUAlvXArUG6uJVesfQSzdFTSnewEPhVY6KjEH+8F2AiC4Aliv3bNLB2VeKO9QJrEUjDmhpXQUUk7OAiwkXQqoWzDmlk6KmkNjwKuEjoqqbRCAdfn0JlvTndJtT0TeHWTpB9LRyV1byHgKqGjkkZnJfDuOY8wSydR4nKTVIkLiZIs/CU/I7kCGYZpx9NLtNIAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAYCAYAAAAcYhYyAAAAzklEQVR42u2QvQrCMBhF+9ZuTj6B76GruCsOIs4ObSn+4iCIf9SqV05LgmCrTTvqhUMD+e5JUq/ZmakOHkluD1WlP11nou1VqoJJLYkRpZJNLEF3sFOj3SsFs3SsZHWRoKzAQMdK5mcJ2AiC4A1ivmbNLB0riU4SFEkMLc9LMRI6VhIeJWAj71RC2YQ1s3SsxD9I4CqhYyVDPxa4PofOch9nklGYCD7dJO/H0rGScXQXuEroWMlkIcG357zCLJ1cictNCiUu5Eqq8Jf8jOQJmcOUfWE2cSsAAAAASUVORK5CYII=";
  private static com.google.gwt.resources.client.ImageResource invalidLine;
  private static com.google.gwt.resources.client.ImageResource textBackground;
  private static com.google.gwt.resources.client.ImageResource triggerArrow;
  private static com.google.gwt.resources.client.ImageResource triggerArrowClick;
  private static com.google.gwt.resources.client.ImageResource triggerArrowFocus;
  private static com.google.gwt.resources.client.ImageResource triggerArrowOver;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      invalidLine(), 
      textBackground(), 
      triggerArrow(), 
      triggerArrowClick(), 
      triggerArrowFocus(), 
      triggerArrowOver(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("invalidLine", invalidLine());
        resourceMap.put("textBackground", textBackground());
        resourceMap.put("triggerArrow", triggerArrow());
        resourceMap.put("triggerArrowClick", triggerArrowClick());
        resourceMap.put("triggerArrowFocus", triggerArrowFocus());
        resourceMap.put("triggerArrowOver", triggerArrowOver());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::css()();
      case 'invalidLine': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::invalidLine()();
      case 'textBackground': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::textBackground()();
      case 'triggerArrow': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::triggerArrow()();
      case 'triggerArrowClick': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::triggerArrowClick()();
      case 'triggerArrowFocus': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::triggerArrowFocus()();
      case 'triggerArrowOver': return this.@com.sencha.gxt.theme.base.client.field.DateCellDefaultAppearance.DateCellResources::triggerArrowOver()();
    }
    return null;
  }-*/;
}
