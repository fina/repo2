package com.task4.shared;

public class FileDTO_id_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.task4.shared.FileDTO, java.lang.Integer> {
  public static final FileDTO_id_ValueProviderImpl INSTANCE = new FileDTO_id_ValueProviderImpl();
  public java.lang.Integer getValue(com.task4.shared.FileDTO object) {
    return object.getId();
  }
  public void setValue(com.task4.shared.FileDTO object, java.lang.Integer value) {
    object.setId(value);
  }
  public String getPath() {
    return "id";
  }
}
