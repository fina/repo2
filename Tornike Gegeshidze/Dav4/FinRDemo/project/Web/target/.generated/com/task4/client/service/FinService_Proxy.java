package com.task4.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class FinService_Proxy extends RemoteServiceProxy implements com.task4.client.service.FinServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.task4.client.service.FinService";
  private static final String SERIALIZATION_POLICY ="CF37E941F41440D018D205FF11A13258";
  private static final com.task4.client.service.FinService_TypeSerializer SERIALIZER = new com.task4.client.service.FinService_TypeSerializer();
  
  public FinService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "finServie", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void addFi(com.task4.shared.FinInstituteDTO var1, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinService_Proxy", "addFi");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.FinInstituteDTO/1137180806");
      streamWriter.writeObject(var1);
      helper.finish(var2, ResponseReader.BOOLEAN);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void deleteSeletedFi(com.task4.shared.FinInstituteDTO finInstituteDTO, com.google.gwt.user.client.rpc.AsyncCallback var2) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinService_Proxy", "deleteSeletedFi");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.task4.shared.FinInstituteDTO/1137180806");
      streamWriter.writeObject(finInstituteDTO);
      helper.finish(var2, ResponseReader.VOID);
    } catch (SerializationException ex) {
      var2.onFailure(ex);
    }
  }
  
  public void loadFies(com.google.gwt.user.client.rpc.AsyncCallback var1) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("FinService_Proxy", "loadFies");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 0);
      helper.finish(var1, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      var1.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
