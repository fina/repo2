/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.presenter;

import com.example.client.TemplateServiceAsync;
import com.example.client.event.TemplateUpdatedCancelledEvent;
import com.example.client.event.TemplateUpdatedEvent;
import com.example.shared.Template;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author tornike
 */
public class EditTemplatePresenter implements Presenter {

    public interface Display {
        HasClickHandlers getSaveButton();
        HasClickHandlers getCancelButton();
        HasValue<String> getID();
        HasValue<String> getCode();
        HasValue<String> getName();
        Widget asWidget();
        HasValue<String> getList();
        void setEditable(boolean edit);
        void hideSaveButton(boolean save);
        int getListIndex();
        void setListIndex(String index);
        void hideIdLabel(boolean id);

    }

    private Template contact;
    private final TemplateServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;

    public EditTemplatePresenter(TemplateServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.contact = new Template();
        this.display = display;
        this.display.setEditable(false);
        this.display.hideIdLabel(true);

        bind();
    }

    public EditTemplatePresenter(TemplateServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();

        rpcService.getContact(id, new AsyncCallback<Template>() {
            @Override
            public void onSuccess(Template result) {
                contact = result;
                EditTemplatePresenter.this.display.hideIdLabel(true);
                EditTemplatePresenter.this.display.hideSaveButton(true);
                EditTemplatePresenter.this.display.getID().setValue(contact.getID());
                EditTemplatePresenter.this.display.getCode().setValue(contact.getCode());
                EditTemplatePresenter.this.display.getName().setValue(contact.getName());
                EditTemplatePresenter.this.display.setListIndex(contact.getIndex());


            }

            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Error retrieving contact");
            }
        });

    }

    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (EditTemplatePresenter.this.display.getID().getValue().equals("")) {
                    doSave();
                }
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new TemplateUpdatedCancelledEvent());
            }
        });
    }

    @Override
    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    private void doSave() {

        contact.setCode(display.getCode().getValue());
        contact.setName(display.getName().getValue());
        contact.setIndex(EditTemplatePresenter.this.display.getListIndex() + "");

        rpcService.updateContact(contact, new AsyncCallback<Template>() {
            @Override
            public void onSuccess(Template result) {


                eventBus.fireEvent(new TemplateUpdatedEvent(result));
            }

            @Override
            public void onFailure(Throwable caught) {
                Window.alert("this code is used");
            }
        });
    }

}
