/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client;

import com.example.client.event.*;
import com.example.client.presenter.EditTemplatePresenter;
import com.example.client.presenter.Presenter;
import com.example.client.presenter.TemplatePresenter;
import com.example.client.view.EditTemplateView;
import com.example.client.view.TemplateView;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * @author tornike
 */
public class AppController implements Presenter, ValueChangeHandler<String> {


    private final HandlerManager eventBus;
    private final TemplateServiceAsync rpcService;
    private HasWidgets container;

    public AppController(TemplateServiceAsync rpcService, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.rpcService = rpcService;
        bind();
    }

    public void bind() {
        History.addValueChangeHandler(this);

        eventBus.addHandler(AddTemplateEvent.TYPE, new AddTemplateEventHandler() {

            @Override
            public void onAddContact(AddTemplateEvent event) {
                doAddNewContact();
            }
        });

        eventBus.addHandler(EditTemplateEvent.TYPE, new EditTemplateEventHandler() {


            @Override
            public void onEditContact(EditTemplateEvent event) {
                doEditContact(event.getId());
            }
        });

        eventBus.addHandler(TemplateUpdatedCancelledEvent.TYPE,
                new TemplateUpdatedCancelledEventHandler() {

                    @Override
                    public void onEditContactCancelled(TemplateUpdatedCancelledEvent event) {
                        doEditContactCancelled();
                    }
                });

        eventBus.addHandler(TemplateUpdatedEvent.TYPE,
                new TemplateUpdatedEventHandler() {
                    @Override
                    public void onContactUpdated(TemplateUpdatedEvent event) {
                        doContactUpdated();
                    }
                });

    }

    private void doAddNewContact() {
        History.newItem("add");
    }

    private void doEditContact(String id) {
        Presenter presenter = new EditTemplatePresenter(rpcService, eventBus, new EditTemplateView(), id);
        presenter.go(container);
        History.newItem("edit", false);
    }

    private void doContactUpdated() {
        History.newItem("list");
    }

    private void doEditContactCancelled() {
        History.newItem("list");
    }

    @Override
    public void go(HasWidgets container) {
        this.container = container;

        if ("".equals(History.getToken())) {
            History.newItem("list");
        } else {
            History.fireCurrentHistoryState();
        }
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        if (token != null) {
            Presenter presenter = null;

            if (token.equals("list")) {
                presenter = new TemplatePresenter(rpcService, eventBus, new TemplateView());
            } else if (token.equals("add")) {
                presenter = new EditTemplatePresenter(rpcService, eventBus, new EditTemplateView());
            } else if (token.equals("edit")) {
                presenter = new EditTemplatePresenter(rpcService, eventBus, new EditTemplateView());
            }

            if (presenter != null) {
                presenter.go(container);
            }
        }
    }

}
