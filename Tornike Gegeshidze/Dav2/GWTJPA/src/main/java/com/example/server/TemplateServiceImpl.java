/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.server;

import com.example.client.I18n;
import com.example.client.Key;
import com.example.client.TemplateService;
import com.example.client.TemplateTable;
import com.example.shared.Template;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;

/**
 * @author tornike
 */
@SuppressWarnings("serial")
public class TemplateServiceImpl extends RemoteServiceServlet implements TemplateService {
    private static final String[] contactsFirstNameData = new String[]{
            "Hollie", "Emerson", "Healy", "Brigitte", "Elba", "Claudio",
            "Dena", "Christina", "Gail", "Orville", "Rae", "Mildred",
            "Candice", "Louise", "Emilio", "Geneva", "Heriberto", "Bulrush",
            "Abigail", "Chad", "Terry", "Bell"};

    private final String[] contactsLastNameData = new String[]{
            "Voss", "Milton", "Colette", "Cobb", "Lockhart", "Engle",
            "Pacheco", "Blake", "Horton", "Daniel", "Childers", "Starnes",
            "Carson", "Kelchner", "Hutchinson", "Underwood", "Rush", "Bouchard",
            "Louis", "Andrews", "English", "Snedden"};

    private final String[] contactsEmailData = new String[]{
            "mark@example.com", "hollie@example.com", "boticario@example.com",
            "emerson@example.com", "healy@example.com", "brigitte@example.com",
            "elba@example.com", "claudio@example.com", "dena@example.com",
            "brasilsp@example.com", "parker@example.com", "derbvktqsr@example.com",
            "qetlyxxogg@example.com", "antenas_sul@example.com",
            "cblake@example.com", "gailh@example.com", "orville@example.com",
            "post_master@example.com", "rchilders@example.com", "buster@example.com",
            "user31065@example.com", "ftsgeolbx@example.com"};
    private static final String PERSISTENCE_UNIT_NAME = "persistenceUnit";
    private static EntityManagerFactory factory;
    private final HashMap<String, Template> contacts = new HashMap<String, Template>();

    public TemplateServiceImpl() {
        //  initContacts();
    }

	 /* private void initContacts() {
        // TODO: Create a real UID for each contact
	    //
	    for (int i = 0; i < contactsFirstNameData.length && i < contactsLastNameData.length && i < contactsEmailData.length; ++i) {
	    	Template contact = new Template(String.valueOf(i), contactsFirstNameData[i], contactsLastNameData[i]);
	      contacts.put(contact.getID(), contact); 
	    }
	  }*/


    @Override
    public Template addContact(Template contact) {
        contact.setID(String.valueOf(contacts.size()));
        contacts.put(contact.getID(), contact);
        return contact;
    }

    @Override
    public Boolean deleteContact(String code) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("DELETE FROM TemplateTable c WHERE c.code = :code");
        q.setParameter("code", code).executeUpdate();
        em.getTransaction().commit();
        em.close();

        return true;
    }

    @Override
    public List<TemplateTable> deleteContacts(List<String> ids) {


        for (int i = 0; i < ids.size(); ++i) {


            deleteContact(ids.get(i));
        }

        List<TemplateTable> res = loadData();
        return res;
    }

    /*@Override
    public ArrayList<TemplateTable> getContactDetails() {
    	    ArrayList<TemplateTable> loadeddata = new ArrayList<TemplateTable>();
	    
	    Iterator<String> it = contacts.keySet().iterator();
	    while(it.hasNext()) { 
	    	Template contact = contacts.get(it.next());          
	      contactDetails.add(contact.getLightWeightContact());
	    }
	    
	    return contactDetails;   
    }
*/
    @Override
    public Template getContact(String id) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("SELECT x FROM TemplateTable x WHERE x.code=?1");
        q.setParameter(1, id);
        TemplateTable tm = (TemplateTable) q.getSingleResult();
        em.getTransaction().commit();
        em.close();
        Template template = new Template();
        String tmp = String.valueOf(tm.getId());
        template.setID(tmp);
        template.setCode(tm.getCode());
        template.setName(tm.getName());
        template.setIndex(tm.getRowIndex());

        return template;

    }

    @Override
    public Template updateContact(Template contact) {

        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        TemplateTable tm = new TemplateTable();
        tm.setCode(contact.getCode());
        tm.setName(contact.getName());
        tm.setRowIndex(contact.getIndex());
        em.persist(tm);
        em.getTransaction().commit();
        em.close();
        //   contacts.remove(contact.getID());
        // contacts.put(contact.getID(), contact);
        return contact;
    }


    @Override
    public void jpaconnect() {


        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        TemplateTable todo = new TemplateTable();
        todo.setCode("This");
        todo.setName("test");
        em.persist(todo);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<TemplateTable> loadData() {

        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("SELECT x FROM TemplateTable x");
        List<TemplateTable> results = q.getResultList();
        em.getTransaction().commit();

        //results =new ArrayList<TemplateTable>(em.createQuery("SELECT x FROM TemplateTable x",TemplateTable.class).getResultList()) ;


        return results;
        //      System.out.print(results.get(2).getName());


    }

    @Override
    public void getSingleTemplate() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("SELECT x FROM TemplateTable x WHERE x.id=7");
        TemplateTable tt = (TemplateTable) q.getSingleResult();
        em.getTransaction().commit();
        em.close();
        System.out.print(tt.getName());

    }

    @Override
    public I18n task() {

        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("select x from I18n x where x.mykey.id=1 and  x.mykey.version=1");
        Key primaryKey = new Key();
        primaryKey.setId(1);
        primaryKey.setVersion(1);

        I18n i18n = em.find(I18n.class, primaryKey);
        q.getSingleResult();
        em.getTransaction().commit();
        em.close();
        return i18n;
    }


}
