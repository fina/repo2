/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author tornike
 */
public interface AddTemplateEventHandler extends EventHandler {
    void onAddContact(AddTemplateEvent event);
}