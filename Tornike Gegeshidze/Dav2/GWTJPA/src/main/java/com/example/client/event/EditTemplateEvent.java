/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * @author tornike
 */
public class EditTemplateEvent extends GwtEvent<EditTemplateEventHandler> {
    public static Type<EditTemplateEventHandler> TYPE = new Type<EditTemplateEventHandler>();
    private String id;

    public EditTemplateEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public Type<EditTemplateEventHandler> getAssociatedType() {

        return TYPE;
    }

    @Override
    protected void dispatch(EditTemplateEventHandler handler) {
        handler.onEditContact(this);

    }


}