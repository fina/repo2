/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.presenter;

import com.example.client.I18n;
import com.example.client.TemplateServiceAsync;
import com.example.client.TemplateTable;
import com.example.client.event.AddTemplateEvent;
import com.example.client.event.EditTemplateEvent;
import com.example.shared.TemplateDetails;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tornike
 */
public class TemplatePresenter implements Presenter {


    public interface Display {
        HasClickHandlers getAddButton();

        HasClickHandlers getDeleteButton();

        HasClickHandlers getList();

        int getClickedRow(ClickEvent event);

        List<Integer> getSelectedRow();

        void setData(List<String> data);

        void setLoadedData(List<String> data);

        Widget asWidget();
        HasClickHandlers getTaskButton();



    }


    private List<TemplateDetails> TemplateDetails;
    private List<TemplateTable> loadedData;
    private final TemplateServiceAsync rpcService;
    private final HandlerManager eventbus;
    private final Display view;

    public TemplatePresenter(TemplateServiceAsync rpcService, HandlerManager eventbus, Display view) {
        this.rpcService = rpcService;
        this.eventbus = eventbus;
        this.view = view;
    }


    @Override
    public void go(HasWidgets container) {
        bind();
        container.clear();
        container.add(view.asWidget());
        getLoadedData();

    }

    public void bind() {
        view.getTaskButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                getTask();

            }
        });
        view.getAddButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                eventbus.fireEvent(new AddTemplateEvent());
            }
        });
        view.getDeleteButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                deleteSelectedContacts();
            }
        });
        view.getList().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                int selectedRow = view.getClickedRow(event);
                if (selectedRow >= 0) {
                    String id = loadedData.get(selectedRow).getCode();
                    eventbus.fireEvent(new EditTemplateEvent(id));
                }

            }
        });



    }

    public TemplateDetails getContactDetail(int index) {
        return TemplateDetails.get(index);
    }


    private void deleteSelectedContacts() {
        List<Integer> selectedRows = view.getSelectedRow();
        ArrayList<String> ids = new ArrayList<String>();


        for (int i = 0; i < selectedRows.size(); ++i) {
            ids.add(loadedData.get(selectedRows.get(i)).getCode());
        }

        rpcService.deleteContacts(ids, new AsyncCallback<List<TemplateTable>>() {
            @Override
            public void onSuccess(List<TemplateTable> result) {
                loadedData = result;

                List<String> data = new ArrayList<String>();

                for (int i = 0; i < result.size(); ++i) {
                    data.add(loadedData.get(i).getName());
                }

                view.setLoadedData(data);

            }

            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Error deleting selected contacts");
            }
        });
    }

    public void getLoadedData() {
        rpcService.loadData(new AsyncCallback<List<TemplateTable>>() {

            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(List<TemplateTable> result) {
                loadedData = result;
                List<String> data = new ArrayList<>();
                for (int i = 0; i < loadedData.size(); i++) {
                    data.add(loadedData.get(i).getName());
                }
                view.setLoadedData(data);


            }
        });


    }
    public void getTask(){
        rpcService.task(new AsyncCallback<I18n>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(I18n result) {

                Window.alert(result.getValue());

            }
        });

    }


}
