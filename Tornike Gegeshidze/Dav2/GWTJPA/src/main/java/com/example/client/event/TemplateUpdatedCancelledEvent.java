/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * @author tornike
 */
public class TemplateUpdatedCancelledEvent extends GwtEvent<TemplateUpdatedCancelledEventHandler> {

    public static Type<TemplateUpdatedCancelledEventHandler> TYPE = new Type<TemplateUpdatedCancelledEventHandler>();

    @Override
    public Type<TemplateUpdatedCancelledEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(TemplateUpdatedCancelledEventHandler handler) {
        handler.onEditContactCancelled(this);
    }


}
