/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shared;

/**
 *
 * @author tornike
 */
import java.io.Serializable;

@SuppressWarnings("serial")
public class TemplateDetails implements Serializable {
	private String code;
	private String displayname;
	
	public TemplateDetails() {
		
	}
	public TemplateDetails(String id,String displayname){
		this.code=id;
		this.displayname=displayname;
	}
		
	
	public String getCode() {
		return code;
	}
	public void setCode(String id) {
		this.code = id;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	

}
