/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.view;

import com.example.client.presenter.TemplatePresenter;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tornike
 */
public class TemplateView extends Composite implements TemplatePresenter.Display {

    private final Button addButton;
    private final Button deleteButton;
    private final FlexTable contentTable;
    private FlexTable contactsTable;
    private final Button taskButton;


    public TemplateView() {
        DecoratorPanel decoratorPanel = new DecoratorPanel();
        initWidget(decoratorPanel);
        decoratorPanel.setWidth("100%");
        decoratorPanel.setWidth("16em");

        contentTable = new FlexTable();
        contentTable.setWidth("100%");
        contentTable.getCellFormatter().addStyleName(0, 0, "contacts-ListContainer");
        contentTable.getCellFormatter().setWidth(0, 0, "100%");
        contentTable.getFlexCellFormatter().setVerticalAlignment(0, 0, DockPanel.ALIGN_TOP);

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setBorderWidth(0);
        hPanel.setSpacing(0);
        hPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
        addButton = new Button("Add");
        hPanel.add(addButton);
        deleteButton = new Button("Delete");
        taskButton=new Button("get record");
        hPanel.add(deleteButton);
        hPanel.add(taskButton);
        contentTable.getCellFormatter().addStyleName(0, 0, "contacts-ListMenu");
        contentTable.setWidget(0, 0, hPanel);

        contactsTable = new FlexTable();
        contactsTable.setCellSpacing(0);
        contactsTable.setCellPadding(0);
        contactsTable.setWidth("100%");
        contactsTable.addStyleName("contacts-ListContents");
        contactsTable.getColumnFormatter().setWidth(0, "15px");
        contentTable.setWidget(1, 0, contactsTable);

        decoratorPanel.add(contentTable);


    }

    @Override
    public HasClickHandlers getAddButton() {
        return addButton;
    }

    @Override
    public HasClickHandlers getDeleteButton() {
        return deleteButton;
    }

    @Override
    public HasClickHandlers getList() {
        return contactsTable;
    }

    @Override
    public int getClickedRow(ClickEvent event) {
        int selectedRow = -1;
        HTMLTable.Cell cell = contactsTable.getCellForEvent(event);
        if (cell != null) {
            if (cell.getCellIndex() > 0) {
                selectedRow = cell.getRowIndex();
            }
        }
        return selectedRow;
    }

    @Override
    public List<Integer> getSelectedRow() {
        List<Integer> selectedRow = new ArrayList<Integer>();
        for (int i = 0; i < contactsTable.getRowCount(); ++i) {
            CheckBox checkBox = (CheckBox) contactsTable.getWidget(i, 0);
            if (checkBox.getValue()) {
                selectedRow.add(i);
            }
        }
        return selectedRow;
    }

    @Override
    public void setData(List<String> data) {
        contactsTable.removeAllRows();
        for (int i = 0; i < data.size(); ++i) {
            contactsTable.setWidget(i, 0, new CheckBox());
            contactsTable.setText(i, 1, data.get(i));
        }
    }



    @Override
    public void setLoadedData(List<String> data) {
        contactsTable.removeAllRows();
        for (int i = 0; i < data.size(); i++) {
            contactsTable.setWidget(i, 0, new CheckBox());
            contactsTable.setText(i, 1, data.get(i));
        }
    }

    @Override
    public HasClickHandlers getTaskButton() {
        return taskButton;
    }

}
