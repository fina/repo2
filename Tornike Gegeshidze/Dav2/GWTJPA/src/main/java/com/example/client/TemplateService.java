/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client;

/**
 *
 * @author tornike
 */

import com.example.shared.Template;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("templateService")
public interface TemplateService extends RemoteService {
    Template addContact(Template contact);

    Boolean deleteContact(String id);

    List<TemplateTable> deleteContacts(List<String> ids);

    //  ArrayList<TemplateDetails> getContactDetails();
    Template getContact(String id);

    Template updateContact(Template contact);

    void jpaconnect();

    List<TemplateTable> loadData();

    void getSingleTemplate();

    I18n task();
}
