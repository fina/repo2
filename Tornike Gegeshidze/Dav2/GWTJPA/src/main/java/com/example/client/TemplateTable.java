/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;


/**
 * @author tornike
 */
@Entity
public class TemplateTable implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String code;

    private String name;
    private String rowIndex;

    public TemplateTable() {

    }

    public TemplateTable(String code, String name, String index) {
        this.code = code;
        this.name = name;
        this.setRowIndex(index);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(String rowIndex) {
        this.rowIndex = rowIndex;
    }
}
