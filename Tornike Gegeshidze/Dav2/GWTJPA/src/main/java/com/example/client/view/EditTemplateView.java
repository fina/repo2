/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.view;

import com.example.client.presenter.EditTemplatePresenter;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

/**
 * @author tornike
 */
public class EditTemplateView extends Composite implements EditTemplatePresenter.Display {


    private final TextBox ID;
    private final TextBox Code;
    private final TextBox Name;
    private final FlexTable detailsTable;
    private final Button saveButton;
    private final Button cancelButton;
    private final ListBox scheduleBox;

    public EditTemplateView() {
        DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("18em");
        initWidget(contentDetailsDecorator);

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");

        // Create the contacts list
        //
        detailsTable = new FlexTable();
        detailsTable.setCellSpacing(0);
        detailsTable.setWidth("100%");
        detailsTable.addStyleName("contacts-ListContainer");
        detailsTable.getColumnFormatter().addStyleName(1, "add-contact-input");
        ID = new TextBox();
        Code = new TextBox();
        Name = new TextBox();
        scheduleBox = new ListBox();
        scheduleBox.addItem("Daily");
        scheduleBox.addItem("Weekly");
        scheduleBox.addItem("Monthly");
        scheduleBox.addItem("Yearly");
        initDetailsTable();
        contentDetailsPanel.add(detailsTable);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");
        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);
    }

    private void initDetailsTable() {
        detailsTable.setWidget(0, 0, new Label("ID"));
        detailsTable.setWidget(0, 1, ID);
        detailsTable.setWidget(1, 0, new Label("Code"));
        detailsTable.setWidget(1, 1, Code);
        detailsTable.setWidget(2, 0, new Label("Name"));
        detailsTable.setWidget(2, 1, Name);
        detailsTable.setWidget(3, 0, new Label("Schedule"));
        detailsTable.setWidget(3, 1, scheduleBox);
        ID.setFocus(true);
    }

    @Override
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    @Override
    public HasValue<String> getID() {
        return ID;
    }

    @Override
    public HasValue<String> getCode() {
        return Code;
    }

    @Override
    public HasValue<String> getName() {
        return Name;
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public HasValue<String> getList() {
        return (HasValue<String>) scheduleBox;
    }

    @Override
    public void setEditable(boolean edit) {
        ID.setReadOnly(!edit);

        // Code.setReadOnly(!edit);
    }

    @Override
    public void hideSaveButton(boolean save) {
        saveButton.setVisible(!save);
    }

    @Override
    public int getListIndex() {
        return scheduleBox.getSelectedIndex();

    }

    @Override
    public void setListIndex(String index) {
        scheduleBox.setSelectedIndex(Integer.parseInt(index));
    }

    @Override
    public void hideIdLabel(boolean id) {

        ID.setVisible(!id);
        detailsTable.removeRow(0);
    }

}
