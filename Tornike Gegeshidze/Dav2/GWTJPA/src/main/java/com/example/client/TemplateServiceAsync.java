/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client;

import com.example.shared.Template;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

/**
 * @author tornike
 */
public interface TemplateServiceAsync {
    void addContact(Template contact, AsyncCallback<Template> async);
    void deleteContact(String id, AsyncCallback<Boolean> async);
    void deleteContacts(List<String> ids, AsyncCallback<List<TemplateTable>> async);
    //  ArrayList<TemplateDetails> getContactDetails();
    void getContact(String id, AsyncCallback<Template> async);
    void updateContact(Template contact, AsyncCallback<Template> async);
    void jpaconnect(AsyncCallback<Void> async);
    void loadData(AsyncCallback<List<TemplateTable>> async);
    void getSingleTemplate(AsyncCallback<Void> async);
    void task(AsyncCallback<I18n> async);
}

