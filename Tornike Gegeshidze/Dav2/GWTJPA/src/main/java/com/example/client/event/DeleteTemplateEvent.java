/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * @author tornike
 */
public class DeleteTemplateEvent extends GwtEvent<DeleteTemplateEventHandler> {
    public static Type<DeleteTemplateEventHandler> TYPE = new Type<DeleteTemplateEventHandler>();

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<DeleteTemplateEventHandler> getAssociatedType() {

        return TYPE;
    }

    @Override
    protected void dispatch(DeleteTemplateEventHandler handler) {
        handler.onDeleteContact(this);


    }
}
