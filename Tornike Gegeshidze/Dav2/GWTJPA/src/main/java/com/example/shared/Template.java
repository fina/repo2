/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Template implements Serializable {

    private String ID;
    private String Code;
    private String Name;
    private String index;

    public Template() {
    }

    public Template(String id, String code, String name,String index) {
        this.ID = id;
        this.Code = code;
        this.Name = name;
        this.index = index;
    }

    public TemplateDetails getLightWeightContact() {
        return new TemplateDetails(ID, getFullName());
    }

    public String getFullName() {
        return Code + " " + Name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

}
