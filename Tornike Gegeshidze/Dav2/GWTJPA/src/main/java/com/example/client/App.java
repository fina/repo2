package com.example.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class App implements EntryPoint {
   /*  private EntityManager manager;
     private static final String PERSISTENCE_UNIT_NAME = "persistenceUnit";
     private static EntityManagerFactory factory;
     

*/

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        TemplateServiceAsync rpcService = GWT.create(TemplateService.class);
        HandlerManager evenbus = new HandlerManager(null);
        AppController appviewer = new AppController(rpcService, evenbus);
        appviewer.go(RootPanel.get());
                
             /*   factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                EntityManager em = factory.createEntityManager();
                em.getTransaction().begin();
                TemplateTable todo = new TemplateTable();
                todo.setCode("This");
                todo.setName("test");
                em.persist(todo);
                em.getTransaction().commit();

                em.close();
                
                */

    }
}