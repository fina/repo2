/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.example.shared.Template;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author tornike
 */

public class TemplateUpdatedEvent extends GwtEvent<TemplateUpdatedEventHandler> {
    public static Type<TemplateUpdatedEventHandler> TYPE = new Type<TemplateUpdatedEventHandler>();
    private final Template updatedContact;

    public TemplateUpdatedEvent(Template updatedContact) {
        this.updatedContact = updatedContact;
    }

    public Template getUpdatedContact() {
        return updatedContact;
    }


    @Override
    public Type<TemplateUpdatedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(TemplateUpdatedEventHandler handler) {
        handler.onContactUpdated(this);
    }

}
