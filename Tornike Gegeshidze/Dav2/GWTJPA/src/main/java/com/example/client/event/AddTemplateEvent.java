/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * @author tornike
 */
public class AddTemplateEvent extends GwtEvent<AddTemplateEventHandler> {
    public static Type<AddTemplateEventHandler> TYPE = new Type<AddTemplateEventHandler>();

    @Override
    public Type<AddTemplateEventHandler> getAssociatedType() {

        return TYPE;
    }

    @Override
    protected void dispatch(AddTemplateEventHandler handler) {
        handler.onAddContact(this);

    }

}

