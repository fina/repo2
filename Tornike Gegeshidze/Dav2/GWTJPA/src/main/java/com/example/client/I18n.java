package com.example.client;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by tornike on 18-Dec-14.
 */
@Entity
public class I18n implements Serializable {

        @EmbeddedId
        private Key mykey;
    @Column
    private
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}


