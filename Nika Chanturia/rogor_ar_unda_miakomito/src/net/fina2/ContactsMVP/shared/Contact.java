package net.fina2.ContactsMVP.shared;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement(name="Contact")
@XmlAccessorType (XmlAccessType.FIELD)
public class Contact implements Serializable {
	@XmlElement(name="id")
	public int id;
	@XmlElement(name="name")
	public String name;
	@XmlElement(name="lastName")
	public String lastName;
	@XmlElement(name="phoneNumber")
	public String phoneNumber;
	public Contact (){}
	
	public void setId(int id){
		this.id=id;
	}	
	
	public void setName(String name){
		this.name=name;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
	
	public int getId(){
		return id;
	}	
	public String getName(){
		return name;
	}
	public String getLastName(){
		return lastName;
	}
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public String getFullName(){
		
		return getName()+" "+getLastName();
	}
	
}
