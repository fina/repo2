package net.fina2.ContactsMVP.server;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.fina2.ContactsMVP.shared.Contact;
@XmlRootElement(name="ContactList")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contacts {
	
	@XmlElement(name = "contact", type = Contact.class)
	public List<Contact> contacts = new ArrayList<Contact>();

	public Contacts(){};
	
	public Contacts(List<Contact> contacts){
		this.contacts = contacts;
	}
	
	public List<Contact> getContacts(){
		return contacts;
	}
	public void setContacts(List<Contact> contacts){
		this.contacts=contacts;
	}
}
