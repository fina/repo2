package net.fina2.ContactsMVP.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.fina2.ContactsMVP.shared.Contact;

public class DbQuery {
	public static Connection conn;
	public PreparedStatement pstmt;

	public void insert(String name, String lastName, String phoneNumber)
			throws SQLException, ClassNotFoundException {
		try {
			Connect connect = new Connect();
			conn = connect.connect();

			String sql = "INSERT INTO contacts "
					+ "(name,lastname,phonenumber)" + "	VALUES (?,?,?)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setString(2, lastName);
			pstmt.setString(3, phoneNumber);
			pstmt.execute();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}
	public Contact select(int id) throws ClassNotFoundException, SQLException{
		Connect connect = new Connect();
		conn = connect.connect();

		String sql = "SELECT * FROM contacts WHERE id = ?";
		pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		Contact contact = new Contact();
		while(rs.next()){
		
		contact.setId(rs.getInt("id"));
		contact.setName(rs.getString("name"));
		contact.setLastName(rs.getString("lastname"));
		contact.setPhoneNumber(rs.getString("phonenumber"));
		}
		if (pstmt != null) {
			pstmt.close();
		}
		if (conn != null) {
			conn.close();
		}
		return contact;
	}
	public Contact select(String phoneNumber) throws ClassNotFoundException, SQLException{
		Connect connect = new Connect();
		conn = connect.connect();

		String sql = "SELECT * FROM contacts WHERE phonenumber = ?";
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, phoneNumber);
		ResultSet rs = pstmt.executeQuery();
		
		Contact contact = new Contact();
		
		while(rs.next()){
		
		contact.setId(rs.getInt("id"));
		contact.setName(rs.getString("name"));
		contact.setLastName(rs.getString("lastname"));
		contact.setPhoneNumber(rs.getString("phonenumber"));
		}
		if (pstmt != null) {
			pstmt.close();
		}
		if (conn != null) {
			conn.close();
		}
		return contact;
	}

	public static List<Contact> selectAll() throws ClassNotFoundException,
			SQLException {
		Connect connect = new Connect();
		List<Contact> list = new ArrayList<Contact>();

		String sql = "SELECT * FROM  contacts";

		conn = connect.connect();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {

			Contact contact = new Contact();
			contact.setId(rs.getInt("id"));
			contact.setName(rs.getString("name"));
			contact.setLastName(rs.getString("lastname"));
			contact.setPhoneNumber(rs.getString("phonenumber"));
			list.add(contact);

		}
		stmt.close();
		conn.close();
		return list;

	}

	public void delete(String phoneNumber) throws ClassNotFoundException,
			SQLException {
		Connect connect = new Connect();
		conn = connect.connect();
		String sql = "DELETE FROM contacts WHERE phonenumber = ?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, phoneNumber);
		pstmt.execute();

		pstmt.close();
		conn.close();

	}

	public void update(int id, String name, String lastName,
			String phoneNumber) throws ClassNotFoundException, SQLException {
		Connect connect = new Connect();
		conn = connect.connect();
		String sql = "UPDATE contacts " + "SET name = ?," + "lastname = ?,"
				+ "phonenumber = ?" + "WHERE id = ?";

		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, name);
		pstmt.setString(2, lastName);
		pstmt.setString(3, phoneNumber);
		pstmt.setInt(4, id);
		pstmt.execute();
		pstmt.close();
		conn.close();

	}

	public void updateXml() throws ClassNotFoundException, SQLException,
			JAXBException {
		ExtractXml makeXml = new ExtractXml();
		makeXml.getXml();
	}

}
