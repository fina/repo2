package net.fina2.ContactsMVP.server;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.fina2.ContactsMVP.shared.Contact;

public class ExtractXml {
	public static Connection conn;

	public void getXml() throws ClassNotFoundException, SQLException,
			JAXBException {
		Connect connect = new Connect();
		conn = connect.connect();
		Contacts contactList = new Contacts();
		List<Contact> list = new ArrayList<Contact>();

		list = DbQuery.selectAll();

		contactList.setContacts(list);

		JAXBContext jaxbContext = JAXBContext.newInstance(Contacts.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller
				.marshal(
						contactList,
						new File(
								"/home/eve/eclipseProjects/ContactsMVP/war/contactsmvp/contacts.xml"));

	}

	public void getObject(String srcFile) throws JAXBException,
			ClassNotFoundException, SQLException {

		Contacts contacts = new Contacts();
		JAXBContext context = JAXBContext.newInstance(Contacts.class);
		javax.xml.bind.Unmarshaller um = context.createUnmarshaller();
		File file = new File(srcFile);
		contacts = (Contacts) um.unmarshal(file);

		DbQuery db = new DbQuery();
		for (Contact contact : contacts.getContacts()) {

			String name = contact.getName();
			String lastName = contact.getLastName();
			String phoneNumber = contact.getPhoneNumber();

			db.insert(name, lastName, phoneNumber);

		}

	}

	public void inputXml(File file) throws JAXBException,
			ClassNotFoundException, SQLException {

		Contacts contacts = new Contacts();
		JAXBContext context = JAXBContext.newInstance(Contacts.class);
		javax.xml.bind.Unmarshaller um = context.createUnmarshaller();

		contacts = (Contacts) um.unmarshal(file);

		DbQuery db = new DbQuery();
		for (Contact contact : contacts.getContacts()) {

			String name = contact.getName();
			String lastName = contact.getLastName();
			String phoneNumber = contact.getPhoneNumber();

			db.insert(name, lastName, phoneNumber);

		}

	}
}