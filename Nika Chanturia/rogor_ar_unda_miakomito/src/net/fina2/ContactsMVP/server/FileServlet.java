package net.fina2.ContactsMVP.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class FileServlet extends HttpServlet {


	   private static final int BUFFER_SIZE = 4096;
	    private final String uploadedDirectory = "/home/eve/eclipseProjects/ContactsMVP/war/contactsmvp";
	    
	    @Override
	    public void init(){
	    	
	    }
	    
	    @Override
	    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    	
	    	resp.setContentType(req.getParameter("fileType"));
	        File file = new File(uploadedDirectory + req.getParameter("fileName") + "." + req.getParameter("fileType"));
	        resp.setHeader("Content-disposition", "attachment; filename=" + req.getParameter("fileName") + "." + req.getParameter("fileType"));
	        FileInputStream inputStream = new FileInputStream(file);
	        OutputStream outputStream = resp.getOutputStream();
	        byte[] bytes = new byte[BUFFER_SIZE];
	        int count;
	        while ((count = inputStream.read(bytes, 0, bytes.length)) > 0) {
	            outputStream.write(bytes, 0, count);
	        }
	        inputStream.close();
	        outputStream.flush();
	        outputStream.close();

	    }
	    
}
