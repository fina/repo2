package net.fina2.ContactsMVP.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.fina2.ContactsMVP.client.services.ContactsService;
import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.dev.util.collect.HashMap;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class ContactsServiceImpl extends RemoteServiceServlet implements
		ContactsService {

	private final HashMap<String, Contact> contacts = new HashMap<String, Contact>();

	final DbQuery dbQuery = new DbQuery();

	public ContactsServiceImpl() {
		try {
			initContacts();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initContacts() throws ClassNotFoundException, SQLException {
		List<Contact> list = new ArrayList<Contact>();
		list = DbQuery.selectAll();
		for (int i = 0; i < list.size(); i++) {
			Contact contact = list.get(i);
			contacts.put(String.valueOf(contact.getId()), contact);

		}
		try {
			makeXml();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public Contact addContact(Contact contact) {
		String name = contact.getName();
		String lastName = contact.getLastName();
		String phoneNumber = contact.getPhoneNumber();

		contact.setId(contacts.size());

		contacts.put(String.valueOf(contact.getId()), contact);

		DbQuery insert = new DbQuery();
		try {
			insert.insert(name, lastName, phoneNumber);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return contact;
	}

	public Contact updateContact(Contact contact) {

		int id = contact.getId();
		Contact contact1 = getContact(id);

		if (!(contact1.getLastName() == null)) {
			String name = contact.getName();
			String lastName = contact.getLastName();
			String phoneNumber = contact.getPhoneNumber();
			try {
				dbQuery.update(id, name, lastName, phoneNumber);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			addContact(contact);
		}
		try {
			makeXml();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return contact;
	}

	public ArrayList<Contact> deleteContact(String phoneNumber) {
		List<Contact> list = new ArrayList<Contact>();
		list = getContacts();
		for (Contact contact : list) {
			if (contact.getPhoneNumber() == phoneNumber) {
				contacts.remove(contact);
			}
		}

		try {
			dbQuery.delete(phoneNumber);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			makeXml();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return getContacts();
	}

	public Contact getContact(int id) {
		Contact contact = null;
		try {
			contact = dbQuery.select(id);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return contact;
	}

	public Contact getContact(String phoneNumber) {
		Contact contact = null;
		try {
			contact = dbQuery.select(phoneNumber);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return contact;
	}

	public ArrayList<Contact> getContacts() {
		List<Contact> list = new ArrayList<Contact>();
		try {
			list = DbQuery.selectAll();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (ArrayList<Contact>) list;
	}

	public String getXml() {
		ExtractXml makeXml = new ExtractXml();
		try {
			makeXml.getXml();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void makeXml() throws ClassNotFoundException, SQLException,
			JAXBException {
		ExtractXml makeXml = new ExtractXml();
		makeXml.getXml();

	}
}
