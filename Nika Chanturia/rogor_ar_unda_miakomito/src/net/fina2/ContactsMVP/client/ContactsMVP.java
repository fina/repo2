package net.fina2.ContactsMVP.client;

import net.fina2.ContactsMVP.client.services.ContactsService;
import net.fina2.ContactsMVP.client.services.ContactsServiceAsync;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;

public class ContactsMVP implements EntryPoint {

	public void onModuleLoad() {
		ContactsServiceAsync rpcService = GWT.create(ContactsService.class);
		HandlerManager eventBus = new HandlerManager(null);
		AppController appViewer = new AppController(rpcService, eventBus);
		appViewer.go(RootPanel.get());
	}
}
