package net.fina2.ContactsMVP.client.presenter;

import java.util.ArrayList;
import java.util.List;

import net.fina2.ContactsMVP.client.event.AddContactEvent;
import net.fina2.ContactsMVP.client.event.EditContactEvent;
import net.fina2.ContactsMVP.client.services.ContactsServiceAsync;
import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ContactsPresenter implements Presenter {

	private List<Contact> contactList;

	public interface Display {
		HasClickHandlers getAddButton();

		void setData(List<Contact> contactList);

		String getClickedContactPhoneNumber(ClickEvent event);

		List<Integer> getSelectedRows();

		int getClickedRow(ClickEvent event);

		Widget asWidget();
	}

	private static ContactsServiceAsync rpcService;
	private static HandlerManager eventBus;
	public static Display display;

	public ContactsPresenter(ContactsServiceAsync rpcService,
			HandlerManager eventBus, Display view) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = view;
	}

	public void bind() {
		display.getAddButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new AddContactEvent());
			}
		});

	}

	public static void doEdit(final String phoneNumber) {

		rpcService.getContact(phoneNumber, new AsyncCallback<Contact>() {

			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}

			public void onSuccess(Contact result) {

				int id = result.getId();

				eventBus.fireEvent(new EditContactEvent(id));

			}
		});

	}

	public static void doDelete(String phoneNumber) {
		rpcService.deleteContact(phoneNumber,
				new AsyncCallback<ArrayList<Contact>>() {
					public void onSuccess(ArrayList<Contact> result) {

						List<Contact> data = new ArrayList<Contact>();

						for (int i = 0; i < result.size(); ++i) {
							data.add(result.get(i));
						}

						display.setData(data);

					}

					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
					}
				});
	}

	public void go(final HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
		fetchContactDetails();
	}

	public Contact getcontactList(int index) {
		return contactList.get(index);
	}

	private void fetchContactDetails() {
		rpcService.getContacts(new AsyncCallback<ArrayList<Contact>>() {
			public void onSuccess(ArrayList<Contact> result) {

				List<Contact> contactList = new ArrayList<Contact>();

				for (int i = 0; i < result.size(); ++i) {
					contactList.add(result.get(i));
				}

				display.setData(contactList);
			}

			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}

		});
	}

	public static Widget getUploadWidget() {

		final FormPanel form = new FormPanel();
		form.setAction(GWT.getModuleBaseURL() + "uploaded");

		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);

		VerticalPanel panel = new VerticalPanel();
		form.setWidget(panel);

		FileUpload upload = new FileUpload();
		upload.setName("FileUpload");
		panel.add(upload);
		panel.add(new Button("Submit", new ClickHandler() {
			public void onClick(ClickEvent event) {
				form.submit();
			}

		}));

		form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
			public void onSubmitComplete(SubmitCompleteEvent event) {

				Window.alert(event.getResults());

			}
		});
		return form;
	}

}
