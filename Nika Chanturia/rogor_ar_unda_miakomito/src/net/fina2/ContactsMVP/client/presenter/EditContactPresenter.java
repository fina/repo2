package net.fina2.ContactsMVP.client.presenter;

import net.fina2.ContactsMVP.client.event.ContactUpdatedEvent;
import net.fina2.ContactsMVP.client.event.EditContactCancelledEvent;
import net.fina2.ContactsMVP.client.services.ContactsServiceAsync;
import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class EditContactPresenter implements Presenter {
	public interface Display {
		HasClickHandlers getSaveButton();

		HasClickHandlers getCancelButton();

		HasValue<String> getName();

		HasValue<String> getLastName();

		HasValue<String> getPhoneNumber();

		Widget asWidget();
	}

	private Contact contact;
	private final ContactsServiceAsync rpcService;
	private final HandlerManager eventBus;
	private final Display display;

	public EditContactPresenter(ContactsServiceAsync rpcService,
			HandlerManager eventBus, Display display) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.contact = new Contact();
		this.display = display;
		bind();
	}

	public EditContactPresenter(ContactsServiceAsync rpcService,
			HandlerManager eventBus, Display display, int id) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = display;
		bind();

		rpcService.getContact(id, new AsyncCallback<Contact>() {
			public void onSuccess(Contact result) {
				contact = result;
				EditContactPresenter.this.display.getName().setValue(
						contact.getName());
				EditContactPresenter.this.display.getLastName().setValue(
						contact.getLastName());
				EditContactPresenter.this.display.getPhoneNumber().setValue(
						contact.getPhoneNumber());
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error retrieving contact");
			}
		});

	}

	public void bind() {
		this.display.getSaveButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doSave();
			}
		});

		this.display.getCancelButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new EditContactCancelledEvent());
			}
		});
	}

	public void go(final HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	private void doSave() {
		String name = display.getName().getValue().trim();
		if (!name.matches("[a-zA-Z]+\\.?")) {
			Window.alert("'" + name + "' is not a valid name.");
			return;

		} else {
			contact.setName(name);
		}
		String lastName = display.getLastName().getValue().trim();
		if (!lastName.matches("[a-zA-Z]+\\.?")) {
			Window.alert("'" + lastName + "' is not a valid last name");
			return;
		} else {
			contact.setLastName(lastName);

		}
		String phoneNumber = display.getPhoneNumber().getValue().trim();
		if (!phoneNumber.matches("[0-9]+\\.?")) {
			Window.alert("'" + phoneNumber + "' is not a valid phone number");
			return;
		} else {
			contact.setPhoneNumber(phoneNumber);

		}

		rpcService.updateContact(contact, new AsyncCallback<Contact>() {
			public void onSuccess(Contact result) {
				eventBus.fireEvent(new ContactUpdatedEvent(result));
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error updating contact");
			}
		});
	}

}
