package net.fina2.ContactsMVP.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DownloadContactsEventHandler extends EventHandler {
	  void onDownloadContacts(DownloadContactsEvent event);
	}