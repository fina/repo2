package net.fina2.ContactsMVP.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class DownloadContactsEvent extends GwtEvent<DownloadContactsEventHandler> {
	  public static Type<DownloadContactsEventHandler> TYPE = new Type<DownloadContactsEventHandler>();
	  
	  @Override
	  public Type<DownloadContactsEventHandler> getAssociatedType() {
	    return TYPE;
	  }

	  @Override
	  protected void dispatch(DownloadContactsEventHandler handler) {
	    handler.onDownloadContacts(this);
	  }
	}
