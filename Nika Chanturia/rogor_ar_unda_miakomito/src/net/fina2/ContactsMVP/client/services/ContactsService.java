package net.fina2.ContactsMVP.client.services;

import java.util.ArrayList;

import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("contactsService")
public interface ContactsService extends RemoteService {
	
	Contact addContact(Contact contact);

	ArrayList<Contact> deleteContact(String phoneNumber);

	Contact getContact(int id);
	
	Contact getContact(String phoneNumber);

	Contact updateContact(Contact contact);

	ArrayList<Contact> getContacts();
	
	String getXml();
}
