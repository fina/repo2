package net.fina2.ContactsMVP.client.services;

import java.util.ArrayList;

import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ContactsServiceAsync {

	public void addContact(Contact contact, AsyncCallback<Contact> callback);

	void deleteContact(String phoneNumber, AsyncCallback<ArrayList<Contact>> asyncCallback);

	public void getContacts(
			AsyncCallback<ArrayList<Contact>> callback);
	
	void getContact(int id, AsyncCallback<Contact> callback);

	void getContact(String phoneNumber, AsyncCallback<Contact> callback);

	void updateContact(Contact contact, AsyncCallback<Contact> callback);

	public void getXml(AsyncCallback<String> callback);


}
