package net.fina2.ContactsMVP.client.view;

import java.util.ArrayList;
import java.util.List;

import net.fina2.ContactsMVP.client.presenter.ContactsPresenter;
import net.fina2.ContactsMVP.shared.Contact;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ContactsView extends Composite implements
		ContactsPresenter.Display {
	private final VerticalPanel mainPanel = new VerticalPanel();
	private final FlexTable contactsFlexTable = new FlexTable();
	private final HorizontalPanel hpanel = new HorizontalPanel();
	private final HorizontalPanel hpanel1 = new HorizontalPanel();
	private final FlowPanel flowPanel = new FlowPanel();

	private final TextBox nameTextBox = new TextBox();
	private final TextBox lastNameTextBox = new TextBox();
	private final TextBox phoneNumberTextBox = new TextBox();
	private final Button addContactButton = new Button("Add");

	final Label lastUpdateLabel = new Label();

	final ArrayList<String> names = new ArrayList<String>();
	final ArrayList<String> lastNames = new ArrayList<String>();
	final ArrayList<String> phoneNumbers = new ArrayList<String>();

	final VerticalPanel v1 = new VerticalPanel();

	final CellTable<Contact> table = new CellTable<Contact>();

	final FormPanel form = new FormPanel();

	@SuppressWarnings("deprecation")
	public ContactsView() {

		contactsFlexTable.setTitle("Contacts");

		addContactButton.getElement().setAttribute("class", "addButton");

		hpanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		hpanel.setWidth("400px");

		DOM.setElementAttribute(contactsFlexTable.getElement(), "class",
				"flexTable");

		VerticalPanel table = new VerticalPanel();
		table.add(contactsFlexTable);
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(addContactButton);
		hp.add(downloadButton());
		
		mainPanel.add(table);
		mainPanel.add(hp);
		mainPanel.add(lastUpdateLabel);

		
		DOM.setElementAttribute(hpanel.getElement(), "class", "hpanel");
		DOM.setElementAttribute(hpanel1.getElement(), "class", "hpanel1");

		flowPanel.add(mainPanel);
		flowPanel.add(hpanel);
		flowPanel.add(hpanel1);
		flowPanel.add(form);

		nameTextBox.setFocus(Boolean.TRUE);
		lastNameTextBox.setFocus(Boolean.TRUE);
		phoneNumberTextBox.setFocus(Boolean.TRUE);

		VerticalPanel v2 = new VerticalPanel();
		String link = GWT.getModuleBaseURL() + "contacts.xml";
		v2.add(new HTML("<a class=\"push_button blue\" href=\"" + link
				+ "\">Download XML</a>"));

		HorizontalPanel hp1 = new HorizontalPanel();
		hp1.add(v1);
		mainPanel.add(hp1);
		mainPanel.add(ContactsPresenter.getUploadWidget());
		flowPanel.getElement().getStyle().setProperty("margin", "0 auto");
		flowPanel.getElement().getStyle().setProperty("TextAlign", "-moz-center");
		flowPanel.addStyleName("center");
		
		RootPanel.get().add(flowPanel);
		initWidget(mainPanel);

	}

	public Button downloadButton() {
		Button downloadButton = new Button("Export");
		downloadButton.getElement().setAttribute("class", "addButton");

		downloadButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				String link = GWT.getModuleBaseURL() + "contacts.xml";

				Window.open(link, "_self", "enabled");

			}
		});

		return downloadButton;
	}

	public Button deleteButton() {
		Button deleteContactButton = new Button("delete");

		deleteContactButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String phoneNumber = getClickedContactPhoneNumber(event);
				ContactsPresenter.doDelete(phoneNumber);

			}

		});

		return deleteContactButton;

	}

	public Button updateButton() {
		Button updateContactButton = new Button("update");

		updateContactButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				String phoneNumber = getClickedContactPhoneNumber(event);

				ContactsPresenter.doEdit(phoneNumber);

			}
		});

		return updateContactButton;
	}

	public HasClickHandlers getAddButton() {
		return addContactButton;
	}

	public HasClickHandlers getDeleteButton() {

		return deleteButton();
	}

	public HasClickHandlers getUpdateButton() {

		return updateButton();
	}

	public void setData(List<Contact> contactList) {

		contactsFlexTable.removeAllRows();

		contactsFlexTable.setText(0, 1, "Name");
		contactsFlexTable.setText(0, 2, "Last Name");
		contactsFlexTable.setText(0, 3, "Phone Number");
		contactsFlexTable.setText(0, 4, "Update");
		contactsFlexTable.setText(0, 5, "Remove");

		for (int i = 1; i - 1 < contactList.size(); ++i) {
			int j = i - 1;

			contactsFlexTable.setText(i, 1, contactList.get(j).getName());
			contactsFlexTable.setText(i, 2, contactList.get(j).getLastName());
			contactsFlexTable
					.setText(i, 3, contactList.get(j).getPhoneNumber());
			contactsFlexTable.setWidget(i, 4, deleteButton());
			contactsFlexTable.setWidget(i, 5, updateButton());

		}

	}

	public String getClickedContactPhoneNumber(ClickEvent event) {
		String phoneNumber = "";
		int rowIndex = contactsFlexTable.getCellForEvent(event).getRowIndex();

		phoneNumber = contactsFlexTable.getText(rowIndex, 3);

		return phoneNumber;
	}

	public int getClickedRow(ClickEvent event) {
		int selectedRow = -1;
		HTMLTable.Cell cell = contactsFlexTable.getCellForEvent(event);

		if (cell != null) {

			if (cell.getCellIndex() > 0) {
				selectedRow = cell.getRowIndex();
			}
		}

		return selectedRow;
	}

	public List<Integer> getSelectedRows() {
		return null;
	}

	public Widget asWidget() {
		return this;
	}

}
