package com.learn2develop.shared;

/**
 * Created by oto on 9/5/2014.
 */
public enum Schedule {
    DAILY, WEEKLY, MONTHLY
}
