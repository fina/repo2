package com.learn2develop.shared;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TemplateTabe")
public class Template implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String code;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SCHEDULE")
    private String schedule;
    @ManyToMany(cascade =CascadeType.ALL,fetch =FetchType.EAGER, mappedBy = "templist")
    private List<FinInstitute> finlist;

    public Template() {
        finlist = new ArrayList<FinInstitute>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public List<FinInstitute> getFinlist() {
        return finlist;
    }

    public void setFinlist(List<FinInstitute> finlist) {
        this.finlist = finlist;
    }
}
