package com.learn2develop.shared;
public class FieldVerifier {
  public static boolean isValidName(String name) {
    if (name == null) {
      return false;
    }
    return name.trim().length() > 3;
  }
}
