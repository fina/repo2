package com.learn2develop.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 9/19/2014.
 */
public class FinInstituteModel implements Serializable {
    private int id;
    private String fiType;
    private String fiCode;
    private String fiDescritpion;
    private String fiName;
    private String insCode;
    private String address;
    private String phone;
    private String mail;
    private String fax;
    private String date;
    private List<TemplateModel> templateModelList=new ArrayList<TemplateModel>();

    public FinInstituteModel() {
    }

    public FinInstituteModel(FinInstitute fi) {
        this.id = fi.getId();
        this.fiType = fi.getFiType();
        this.fiCode = fi.getCode();
        this.fiDescritpion = fi.getFiDescritpion();
        this.fiName = fi.getFiName();
        this.insCode = fi.getInsCode();
        this.address = fi.getAddress();
        this.phone = fi.getPhone();
        this.mail = fi.getMail();
        this.fax = fi.getFax();
        this.date = fi.getDate();
        TemplateModel tempM = null;
        for (int i = 0; i < fi.getTemplist().size(); i++) {
            tempM = new TemplateModel(fi.getTemplist().get(i).getId(), fi.getTemplist().get(i).getCode(), fi.getTemplist().get(i).getName(), fi.getTemplist().get(i).getSchedule());
            this.templateModelList.add(tempM);
        }
    }

    public List<TemplateModel> getTemplateModelList() {
        return templateModelList;
    }

    public List<Template> getTemplateList(){
        List<Template> list=new ArrayList<Template>();
        Template template=new Template();
        for(int i=0;i<templateModelList.size();i++){
            template.setId(templateModelList.get(i).getId());
            template.setCode(templateModelList.get(i).getCode());
            template.setName(templateModelList.get(i).getName());
            template.setSchedule(templateModelList.get(i).getSchedule());
            list.add(template);
        }
        return list;
    }

    public void setTemplateModelList(List<TemplateModel> templateModelList) {
        this.templateModelList = templateModelList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFiType() {
        return fiType;
    }

    public void setFiType(String fiType) {
        this.fiType = fiType;
    }

    public String getFiCode() {
        return fiCode;
    }

    public void setFiCode(String fiCode) {
        this.fiCode = fiCode;
    }

    public String getFiDescritpion() {
        return fiDescritpion;
    }

    public void setFiDescritpion(String fiDescritpion) {
        this.fiDescritpion = fiDescritpion;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }

    public String getInsCode() {
        return insCode;
    }

    public void setInsCode(String insCode) {
        this.insCode = insCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



}

