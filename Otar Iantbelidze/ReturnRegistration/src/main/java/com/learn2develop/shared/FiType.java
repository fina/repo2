package com.learn2develop.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FiType {
	public static List<String> FIList = new ArrayList<String>();
	public static Map<String, String> fiNames = new HashMap<String, String>();
	public static Map<String,Integer> fiCodes=new HashMap<String,Integer>();

	static {
		fiNames.put("BANK", "Bank");
		fiNames.put("MFI", "Micro Financial Institution.");
		fiNames.put("FXB", "EXCHANGE");
	}
	
	static{
		fiCodes.put("BANK", 9870);
		fiCodes.put("MFI", 4321);
		fiCodes.put("FXB", 6754);
	}

	static {
		FIList.add("");
		FIList.add("BANK");
		FIList.add("MFI");
		FIList.add("FXB");
	}
	
	public static void addDescription(String key,String desc){
		fiNames.put(key, desc);
	}

	public static void addNewType(String type) {
		FIList.add(type);
	}
	
	public static void addCodes(String type,String code){
		fiCodes.put(type, Integer.parseInt(code));
	}

}
