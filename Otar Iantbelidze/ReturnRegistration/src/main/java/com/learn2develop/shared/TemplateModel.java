package com.learn2develop.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 9/19/2014.
 */
public class TemplateModel implements Serializable {
    private int id;
    private String code;
    private String name;
    private String schedule;
    private List<FinInstituteModel> finInstituteModelList =new ArrayList<FinInstituteModel>();


    public TemplateModel() {
    }

    public TemplateModel(int id, String code, String name, String schedule) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.schedule = schedule;
    }

    public List<FinInstituteModel> getFinInstituteModelList() {
        return finInstituteModelList;
    }

    public void setFinInstituteModelList(List<FinInstituteModel> finInstituteModelList) {
        this.finInstituteModelList = finInstituteModelList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }
}
