package com.learn2develop.shared;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;

/**
 * Created by oto on 10/2/2014.
 */
public interface Resources extends ClientBundle{
    @ClientBundle.Source("Images//loader.gif")
    ImageResource getImage();

}
