package com.learn2develop.shared;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
public class FinInstitute implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String fiType;
    private String code;
    private String fiDescritpion;
    private String fiName;
    @Column(unique = true)
    private String insCode;
    private String address;
    private String phone;
    private String mail;
    private String fax;
    private String date;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "fi_temp",
            joinColumns = @JoinColumn(name = "FI_ID"),
            inverseJoinColumns = @JoinColumn(name = "TEMP_ID"))
    private List<Template> templist;


    public FinInstitute() {
        templist = new ArrayList<Template>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFiType() {
        return fiType;
    }

    public void setFiType(String fiType) {
        this.fiType = fiType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String fiCode) {
        this.code = fiCode;
    }

    public String getFiDescritpion() {
        return fiDescritpion;
    }

    public void setFiDescritpion(String fiDescritpion) {
        this.fiDescritpion = fiDescritpion;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }

    public String getInsCode() {
        return insCode;
    }

    public void setInsCode(String insCode) {
        this.insCode = insCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date.toString();
    }

    public List<Template> getTemplist() {
        return templist;
    }

    public void setTemplist(List<Template> templist) {
        this.templist = templist;
    }


}
