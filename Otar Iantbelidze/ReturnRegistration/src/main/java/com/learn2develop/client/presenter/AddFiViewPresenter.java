package com.learn2develop.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.learn2develop.client.RPCServiceAsync;
import com.learn2develop.client.event.CancelEvent;
import com.learn2develop.client.view.ListBoxView;
import com.learn2develop.client.view.TypeDialogView;
import com.learn2develop.shared.*;

import java.util.Date;

/**
 * Created by oto on 8/26/2014.
 */
public class AddFiViewPresenter implements Presenter {


    public Display display;
    public Template template;
    FinInstitute in = new FinInstitute();
    HandlerManager eventBus;
    RPCServiceAsync service;

    public AddFiViewPresenter(HandlerManager eventBus, Display display, RPCServiceAsync service) {
        this.eventBus = eventBus;
        this.display = display;
        this.service = service;
        addListeners();
    }

    public AddFiViewPresenter(HandlerManager eventBus, final Display display, final RPCServiceAsync service, String code) {
        this.eventBus = eventBus;
        this.display = display;
        this.service = service;
        addListeners();

        service.getInstituteByCode(code, new AsyncCallback<FinInstituteModel>() {
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            public void onSuccess(FinInstituteModel result) {
                display.getDescription().setValue(result.getFiName());
                display.getName().setValue(result.getFiDescritpion());
                display.getTypeCode().setValue(result.getFiCode());
                display.getType().setValue(result.getFiType());
                display.getAddress().setValue(result.getAddress());
                display.getEmail().setValue(result.getMail());
                display.getFax().setValue(result.getFax());
                display.getPhone().setValue(result.getPhone());
                display.getInsCode().setValue(result.getInsCode());
                in.setId(result.getId());
                in.setTemplist(result.getTemplateList());
            }
        });
    }

    public void addListeners() {
        display.getInsCodeValue().addValueChangeHandler(new ValueChangeHandler<String>() {
            public void onValueChange(ValueChangeEvent<String> event) {
                in.setInsCode(display.getInsCode().getValue());
                service.validate(display.getInsCode().getValue(), new AsyncCallback<Boolean>() {
                    public void onFailure(Throwable caught) {
                        Window.alert(caught.getMessage());
                    }

                    public void onSuccess(Boolean result) {
                        if (result) {
                            Window.alert("User with this code already exists!!!");
                            display.saveButton().setEnabled(false);
                        } else {
                            display.getErrorLabel().setVisible(false);
                            display.saveButton().setEnabled(true);
                        }
                    }
                });

            }
        });
        display.getSaveButton().addClickHandler(new ClickHandler() {

            public void onClick(final ClickEvent event) {
                if (display.getInsCode().getValue().trim().length() < 1 || display.getInsCode().getValue() == null) {
                    display.getErrorLabel().setVisible(true);
                    display.getErrorLabel().setText("Please Enter Code");
                    return;
                }
                in.setFax(display.getFax().getValue());
                in.setFiName(display.getName().getValue());
                in.setCode(display.getTypeCode().getValue());
                in.setInsCode(display.getInsCode().getValue());
                in.setAddress(display.getAddress().getValue());
                in.setPhone(display.getPhone().getValue());
                in.setDate(new Date());
                in.setMail(display.getEmail().getValue());
                in.setFiDescritpion(display.getDescription().getValue());
                in.setFiType(display.getType().getValue());
                if (display.getInsCode().getValue().trim().length() == 0) {
                    Window.alert("please enter Code");
                    return;
                }
                service.insert(in, new AsyncCallback<Boolean>() {
                    public void onFailure(Throwable caught) {
                        Window.alert("error inserting ENTITY");
                    }

                    public void onSuccess(Boolean result) {
                        display.clearDisplay();
                        eventBus.fireEvent(new CancelEvent());
                    }
                });

            }
        });
        display.getCreateButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                createTemplate();
            }
        });
        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new CancelEvent());
            }
        });

        display.getCreateTypeButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new TypeDialogView();
            }
        });
        display.getListBox().addValueChangeHandler(new ValueChangeHandler<String>() {
            public void onValueChange(ValueChangeEvent<String> event) {
                display.getTypeCode().setValue(String.valueOf(FiType.fiCodes.get(event.getValue())));
                display.getDescription().setValue(FiType.fiNames.get(event.getValue()));
            }
        });

    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    public void createTemplate() {
        Button saveButton;
        Button cancelButton;
        final TextBox codeBox;
        final TextBox nameBox;
        final ListBoxView scheduleList;
        FlexTable table;

        final DialogBox dialog = new DialogBox();
        dialog.setText("Create Template");
        dialog.setAnimationEnabled(true);
        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");
        table = new FlexTable();
        table.setWidth("100%");
        codeBox = new TextBox();
        nameBox = new TextBox();
        scheduleList = new ListBoxView();
        Schedule[] mass = Schedule.values();
        scheduleList.addItem("");
        for (int i = 0; i < Schedule.values().length; i++) {
            scheduleList.addItem(mass[i].toString());
        }
        table.setWidget(0, 0, new Label("Code"));
        table.setWidget(0, 1, codeBox);
        table.setWidget(1, 0, new Label("choose "));
        table.setWidget(1, 1, scheduleList);
        table.setWidget(2, 0, new Label("description "));
        table.setWidget(2, 1, nameBox);
        contentDetailsPanel.add(table);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");

        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        dialog.add(contentDetailsPanel);

        dialog.center();
        dialog.show();
        saveButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (FieldVerifier.isValidName(codeBox.getText()) && FieldVerifier.isValidName(nameBox.getText())) {
                    display.saveButton().setEnabled(true);
                    template = new Template();
                    template.setName(nameBox.getText());
                    template.setCode(codeBox.getText());
                    template.setSchedule(scheduleList.getValue());
                    in.getTemplist().add(template);
                    dialog.hide();
                } else
                    Window.alert("Enter 4 Symbols at least");
            }
        });
        cancelButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                dialog.hide();
            }
        });

    }

    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCreateTypeButton();

        HasClickHandlers getCancelButton();

        HasClickHandlers getCreateButton();

        HasValue<String> getTypeCode();

        HasValueChangeHandlers<String> getInsCodeValue();

        HasValue<String> getEmail();

        HasValue<String> getDescription();

        HasValue<String> getInsCode();

        HasValue<String> getType();

        HasValue<String> getName();

        HasValue<String> getAddress();

        HasValue<String> getFax();

        HasValue<String> getPhone();

        HasValue<String> getListBox();

        HasEnabled saveButton();

        com.github.gwtbootstrap.client.ui.Label getErrorLabel();

        void clearDisplay();

        Widget asWidget();
    }
}
