package com.learn2develop.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.learn2develop.client.RPCServiceAsync;
import com.learn2develop.client.event.AddReturnEvent;
import com.learn2develop.client.event.EditReturnEvent;
import com.learn2develop.shared.FinInstituteModel;
import com.learn2develop.shared.Resources;

import java.util.List;

/**
 * Created by oto on 8/25/2014.
 */
public class ViewPresenter implements Presenter {
    HandlerManager eventBus;
    Display display;
    RPCServiceAsync service;

    PopupPanel p;
    Resources rs = GWT.create(Resources.class);

    public ViewPresenter(RPCServiceAsync service, Display display, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.display = display;
        this.service = service;
    }

    void addListeners() {
        display.onAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AddReturnEvent());
            }
        });
        display.onDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                service.deleteByCode(display.getSelectedRowsCode(), new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        Window.alert("error deleting entity");
                    }

                    public void onSuccess(Void result) {
                        addInitData();
                    }
                });

            }
        });

        display.getTable().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int selectedRow = display.getClickedFiCode(event);
                if (selectedRow > 0) {
                    String code = display.getCode(event);
                    eventBus.fireEvent(new EditReturnEvent(code));
                }
            }
        });
//        display.getShowButton().addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                Window.alert("" + display.getSelectedCell(event));
//            }
//        });
        display.getCheckBox().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                display.checkAll();
            }
        });
    }

    public void addInitData() {
        p = display.getPopup();
        p.setWidget(new Image(rs.getImage()));
        p.center();
        p.show();
        p.setGlassEnabled(true);

        service.getData(new AsyncCallback<List<FinInstituteModel>>() {
            public void onFailure(Throwable caught) {
                Window.alert("FAILURE");
            }

            public void onSuccess(List<FinInstituteModel> result) {

                display.setData(result);
                p.hide();
            }
        });

    }

    public void go(HasWidgets container) {

        addListeners();
        container.clear();
        container.add(display.asWidget());
        addInitData();
    }

    public interface Display {
        HasClickHandlers onAddButton();

        HasClickHandlers onDeleteButton();

        String getCode(ClickEvent event);

        List<String> getSelectedRowsCode();

        void setData(List<FinInstituteModel> dat);

        CheckBox getCheckBox();

        int getClickedFiCode(ClickEvent event);

        HasClickHandlers getTable();

        PopupPanel getPopup();

        void checkAll();

        Widget asWidget();

    }


}
