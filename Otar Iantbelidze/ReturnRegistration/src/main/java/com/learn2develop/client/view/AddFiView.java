package com.learn2develop.client.view;


import com.github.gwtbootstrap.client.ui.Bar;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Navbar;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.*;
import com.learn2develop.client.presenter.AddFiViewPresenter;
import com.learn2develop.shared.FiType;


public class AddFiView extends Composite implements AddFiViewPresenter.Display {

    public static ListBoxView listBox;
    private Button createType;
    private Button save;
    private Button createTemplate;
    private Button cancelButton;
    private TextBox codeBox;
    private TextBox nameBox;
    private TextBox description;
    private TextBox insCode;
    private TextBox address;
    private TextBox phone;
    private TextBox fax;
    private TextBox email;
    private FlexTable table;
    private com.github.gwtbootstrap.client.ui.Label errorLabel;

    public AddFiView() {
        Navbar nb = new Navbar();
        Bar bar = new Bar();
        bar.setText("REGISTRATION FORM");
        nb.setStyleName("navbar");
        nb.add(bar);
        DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setStyleName("center");
        contentDetailsDecorator.setWidth("40em");
        initWidget(contentDetailsDecorator);
        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.add(nb);
        contentDetailsPanel.setWidth("100%");
        table = new FlexTable();
        table.setWidth("100%");
        createType = new Button("Create Type");
        createType.setType(ButtonType.PRIMARY);
        codeBox = new TextBox();
        nameBox = new TextBox();
        description = new TextBox();
        insCode = new TextBox();
        fax = new TextBox();
        address = new TextBox();
        phone = new TextBox();
        email = new TextBox();
        listBox = new ListBoxView();
        errorLabel = new com.github.gwtbootstrap.client.ui.Label();
        errorLabel.setVisible(false);
        errorLabel.setType(LabelType.WARNING);
        init();
        contentDetailsPanel.add(table);

        HorizontalPanel menuPanel = new HorizontalPanel();
        save = new Button("Save");
        save.setType(ButtonType.PRIMARY);
        cancelButton = new Button("Cancel");
        cancelButton.setType(ButtonType.PRIMARY);
        createTemplate = new Button("Attach Template");
        createTemplate.setType(ButtonType.PRIMARY);
        menuPanel.add(save);
        menuPanel.add(cancelButton);
        menuPanel.add(createTemplate);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);
    }

    void addTypesToListBox() {
        java.util.List<String> typeList = FiType.FIList;
        for (int i = 0; i < FiType.FIList.size(); i++) {
            listBox.addItem(typeList.get(i));
        }
    }

    public void init() {
        addTypesToListBox();
        description.setEnabled(false);
        codeBox.setEnabled(false);
        table.setWidget(0, 0, createType);
        table.setWidget(1, 0, new Label("Select Fi Type "));
        table.setWidget(1, 1, listBox);
        table.setWidget(2, 0, new Label("CODE "));
        table.setWidget(2, 1, codeBox);
        table.setWidget(3, 0, new Label("FI Description"));
        table.setWidget(3, 1, description);
        table.setWidget(4, 0, new Label("FI Code"));
        table.setWidget(4, 1, insCode);
        table.setWidget(4, 2, errorLabel);
        table.setWidget(5, 0, new Label("Fi NAME"));
        table.setWidget(5, 1, nameBox);
        table.setWidget(6, 0, new Label("Enter Address"));
        table.setWidget(6, 1, address);
        table.setWidget(7, 0, new Label("Enter Phone"));
        table.setWidget(7, 1, phone);
        table.setWidget(8, 0, new Label("Enter Fax"));
        table.setWidget(8, 1, fax);
        table.setWidget(9, 0, new Label("Enter E-Mail"));
        table.setWidget(9, 1, email);

    }

    public void clearDisplay() {
        saveButton().setEnabled(false);
        nameBox.setText("");
        codeBox.setText("");
        description.setText("");
        insCode.setText("");
        phone.setText("");
        address.setText("");
        email.setText("");
        fax.setText("");
    }

    public HasClickHandlers getSaveButton() {
        return save;
    }

    public HasClickHandlers getCreateTypeButton() {
        return createType;
    }

    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    public HasClickHandlers getCreateButton() {
        return createTemplate;
    }

    public HasValue<String> getTypeCode() {
        return codeBox;
    }

    public HasValueChangeHandlers<String> getInsCodeValue() {
        return insCode;
    }

    public HasValue<String> getEmail() {
        return email;
    }

    public HasValue<String> getDescription() {
        return description;
    }

    public HasValue<String> getInsCode() {
        return insCode;
    }

    public HasValue<String> getType() {
        return description;
    }

    public HasValue<String> getName() {
        return nameBox;
    }

    public HasValue<String> getAddress() {
        return address;
    }

    public HasValue<String> getFax() {
        return fax;
    }

    public HasValue<String> getPhone() {
        return phone;
    }

    public HasValue<String> getListBox() {
        return listBox;
    }

    public HasEnabled saveButton() {
        return save;
    }

    public com.github.gwtbootstrap.client.ui.Label getErrorLabel() {
        return errorLabel;
    }

    @Override
    public Widget asWidget() {
        return this;
    }
}