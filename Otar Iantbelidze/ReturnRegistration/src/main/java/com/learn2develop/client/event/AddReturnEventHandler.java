package com.learn2develop.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 8/25/2014.
 */
public interface AddReturnEventHandler extends EventHandler {
    void onAddReturnEvent(AddReturnEvent event);
}
