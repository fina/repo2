package com.learn2develop.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.learn2develop.shared.FinInstitute;
import com.learn2develop.shared.FinInstituteModel;
import com.learn2develop.shared.Template;
import com.learn2develop.shared.TemplateModel;

import java.util.List;

/**
 * Created by oto on 8/26/2014.
 */
@RemoteServiceRelativePath("greet")
public interface RPCService extends RemoteService {

    public List<FinInstituteModel> getData();

    public boolean insert(FinInstitute r);

    void deleteByCode(List<String> code);

    FinInstituteModel getInstituteByCode(String code);

    boolean validate(String insCode);

    List<TemplateModel> getTemplateData(String code);

    boolean deleteTemplate(String code);
}

