package com.learn2develop.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 8/26/2014.
 */
public class CreateEvent extends GwtEvent<CreateEventHandler> {
    public static Type<CreateEventHandler> TYPE = new Type<CreateEventHandler>();

    @Override
    public Type<CreateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateEventHandler handler) {
        handler.onCreateEvent(this);
    }
}
