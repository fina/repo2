package com.learn2develop.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.learn2develop.client.event.*;
import com.learn2develop.client.presenter.AddFiViewPresenter;
import com.learn2develop.client.presenter.Presenter;
import com.learn2develop.client.presenter.TemplatePresenter;
import com.learn2develop.client.presenter.ViewPresenter;
import com.learn2develop.client.view.AddFiView;
import com.learn2develop.client.view.TemplateView;
import com.learn2develop.client.view.View;

/**
 * Created by oto on 8/25/2014.
 */
public class AppController implements Presenter, ValueChangeHandler<String> {

    RPCServiceAsync service;
    HandlerManager eventBus;
    HasWidgets container;
    Presenter presenter = null;

    public AppController(RPCServiceAsync service, HandlerManager enevtBus) {
        this.service = service;
        this.eventBus = enevtBus;
        bind();

    }

    public void bind() {
        History.addValueChangeHandler(this);
        eventBus.addHandler(AddReturnEvent.TYPE, new AddReturnEventHandler() {
            public void onAddReturnEvent(AddReturnEvent event) {
                History.newItem("add");
            }
        });
        eventBus.addHandler(CancelEvent.TYPE, new CancelEventHandler() {
            public void onCancelEvent(CancelEvent event) {
                History.newItem("list");
            }
        });
        eventBus.addHandler(CreateEvent.TYPE, new CreateEventHandler() {
            public void onCreateEvent(CreateEvent event) {
                History.newItem("create");
            }
        });
        eventBus.addHandler(EditReturnEvent.TYPE, new EditReturnEventHandler() {
            public void onEditReturn(EditReturnEvent event) {
                History.newItem("edit");
                presenter = new AddFiViewPresenter(eventBus, new AddFiView(), service, event.getCode());
                presenter.go(container);
            }
        });

    }

    public void go(HasWidgets container) {
        this.container = container;

        if ("".equals(History.getToken())) {
            History.newItem("list");
        } else {
            History.fireCurrentHistoryState();
        }
    }

    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        if (token != null) {

            if (token.equals("list")) {
                presenter = new ViewPresenter(service, new View(), eventBus);
            } else if (token.equals("add")) {
                presenter = new AddFiViewPresenter(eventBus, new AddFiView(), service);
            }
            if (presenter != null) {
                presenter.go(container);
            }
        }
    }
}
