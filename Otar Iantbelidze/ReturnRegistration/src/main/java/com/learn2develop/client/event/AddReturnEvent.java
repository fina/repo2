package com.learn2develop.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 8/25/2014.
 */
public class AddReturnEvent extends GwtEvent<AddReturnEventHandler> {
    public static Type<AddReturnEventHandler> TYPE=new Type<AddReturnEventHandler>();
    @Override
    public Type<AddReturnEventHandler> getAssociatedType() {
        return TYPE;
    }
    @Override
    protected void dispatch(AddReturnEventHandler handler) {
        handler.onAddReturnEvent(this);
    }
}
