package com.learn2develop.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 8/26/2014.
 */
public interface CancelEventHandler extends EventHandler {
    void onCancelEvent(CancelEvent event);
}

