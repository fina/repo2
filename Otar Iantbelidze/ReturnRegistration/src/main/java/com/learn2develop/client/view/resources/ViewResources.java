package com.learn2develop.client.view.resources;

import com.github.gwtbootstrap.client.ui.resources.Resources;
import com.google.gwt.resources.client.TextResource;

public interface ViewResources extends Resources {
    @Source("css/bootstrap.min.css")
    TextResource bootstrapCss();
}
