package com.learn2develop.client.view;


import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.learn2develop.client.RPCService;
import com.learn2develop.client.RPCServiceAsync;
import com.learn2develop.client.presenter.ViewPresenter;
import com.learn2develop.shared.FinInstituteModel;
import com.learn2develop.shared.TemplateModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 8/25/2014.
 */
public class View extends Composite implements ViewPresenter.Display {


    private static ViewUiBinder ourUiBinder = GWT.create(ViewUiBinder.class);
    @UiField
    Button addButton;
    @UiField
    Button deleteButton;
    @UiField
    FlexTable flexTable;
    @UiField
    PopupPanel popup;
    CheckBox chk = new CheckBox("Select All");
    Button show;
    RPCServiceAsync service = GWT.create(RPCService.class);
    TemplateView tempView;

    public View() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public String getCode(ClickEvent event) {
        int selectedRow = -1;
        HTMLTable.Cell cell = flexTable.getCellForEvent(event);
        if (cell != null) {
            if (cell.getCellIndex() > 0) {
                selectedRow = cell.getRowIndex();
            }
        }
        return flexTable.getText(selectedRow, 5);
    }

    public int getSelectedCell(ClickEvent event) {
        int cellIndex = flexTable.getCellForEvent(event).getCellIndex();
        return cellIndex;
    }

    public List<String> getSelectedRowsCode() {
        ArrayList<String> rowList = new ArrayList<String>();
        for (int i = 1; i < flexTable.getRowCount(); i++) {
            CheckBox checkBox = (CheckBox) flexTable.getWidget(i, 0);
            if (checkBox.getValue())
                rowList.add(flexTable.getText(i, 5));
        }
        return rowList;
    }

    public void setData(List<FinInstituteModel> data) {

        flexTable.setBorderWidth(4);
        flexTable.removeAllRows();
        flexTable.setWidget(0, 0, chk);
        flexTable.setText(0, 1, "FiType");
        flexTable.setText(0, 2, "Code");
        flexTable.setText(0, 3, "Description");
        flexTable.setText(0, 4, "Name");
        flexTable.setText(0, 5, "FiCode");
        flexTable.setText(0, 6, "Address");
        flexTable.setText(0, 7, "Phone");
        flexTable.setText(0, 8, "fax");
        flexTable.setText(0, 9, "E-Mail");
        flexTable.setText(0, 10, "Date Registered");
        flexTable.setText(0, 11, "Template ");
        flexTable.getRowFormatter().addStyleName(0, "tableHeader");

        for (int i = 0; i < data.size(); ++i) {
            show = new Button("Show Template");
            show.setType(ButtonType.WARNING);
            flexTable.setWidget(i + 1, 0, new CheckBox());
            flexTable.setText(i + 1, 1, data.get(i).getFiType());
            flexTable.setText(i + 1, 2, data.get(i).getFiCode());
            flexTable.setText(i + 1, 3, data.get(i).getFiDescritpion());
            flexTable.setText(i + 1, 4, data.get(i).getFiName());
            flexTable.setText(i + 1, 5, data.get(i).getInsCode());
            flexTable.setText(i + 1, 6, data.get(i).getAddress());
            flexTable.setText(i + 1, 7, data.get(i).getPhone());
            flexTable.setText(i + 1, 8, data.get(i).getFax());
            flexTable.setText(i + 1, 9, data.get(i).getMail());
            flexTable.setText(i + 1, 10, data.get(i).getDate());
            flexTable.setWidget(i + 1, 11, show);
            show.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    int rowIndex = flexTable.getCellForEvent(event).getRowIndex();
                    String code = flexTable.getText(rowIndex, 5);
                    tempView = new TemplateView();
                    service.getTemplateData(code, new AsyncCallback<List<TemplateModel>>() {
                        public void onFailure(Throwable caught) {
                            Window.alert("Failure");
                        }

                        public void onSuccess(List<TemplateModel> result) {
                            tempView.setData(result);
                        }
                    });
                }
            });
        }
        applyDataRowStyles();
    }

    public void checkAll() {
        for (int i = 1; i < flexTable.getRowCount(); i++) {
            if (((CheckBox) flexTable.getWidget(0, 0)).getValue()) {
                ((CheckBox) flexTable.getWidget(i, 0)).setValue(true);
            } else {
                ((CheckBox) flexTable.getWidget(i, 0)).setValue(false);
            }
        }
    }

    private void applyDataRowStyles() {
        HTMLTable.RowFormatter rf = flexTable.getRowFormatter();

        for (int row = 1; row < flexTable.getRowCount(); ++row) {
            if ((row % 2) != 0) {
                rf.addStyleName(row, "Table-OddRow");
            } else {
                rf.addStyleName(row, "Table-EvenRow");
            }
        }
    }

    public CheckBox getCheckBox() {
        return chk;
    }

    public int getClickedFiCode(ClickEvent event) {
        int selectedRow = -1;
        int selectedColumn = getSelectedCell(event);
        HTMLTable.Cell cell = flexTable.getCellForEvent(event);
        if (cell != null) {

            if (cell.getCellIndex() > 0 && selectedColumn < 11) {
                selectedRow = cell.getRowIndex();
            }
        }

        return selectedRow;
    }

    public HasClickHandlers getTable() {
        return flexTable;
    }

    public PopupPanel getPopup() {
        return popup;
    }

    public HasClickHandlers onAddButton() {
        return addButton;
    }

    public HasClickHandlers onDeleteButton() {
        return deleteButton;
    }

    public Widget asWidget() {
        return this;
    }

    interface ViewUiBinder extends UiBinder<Widget, View> {
    }
}