package com.learn2develop.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.*;
import com.learn2develop.shared.FiType;

public class TypeDialogView extends DialogBox {

    TextBox code = new TextBox();
    TextBox name = new TextBox();
    DigitKeyHandler handler = new DigitKeyHandler();
    Button add = new Button("Add");
    Grid grid = new Grid(4, 2);


    public TypeDialogView() {
        setText("crete new FI type");
        grid.setWidget(0, 0, new Label("enter Code"));
        grid.setWidget(0, 1, code);
        grid.setWidget(1, 0, new Label("enter Name"));
        grid.setWidget(1, 1, name);
        grid.setWidget(2, 0, add);

        code.addKeyPressHandler(handler);

        add.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                FiType.addDescription(name.getText(), name.getText());
                FiType.addCodes(name.getText(), code.getText());
                AddFiView.listBox.addItem(name.getText());
                hide();
            }
        });

        setAnimationEnabled(true);
        setAutoHideEnabled(true);
        this.setWidget(grid);
        this.center();
    }

    public class DigitKeyHandler implements KeyPressHandler {


        public void onKeyPress(KeyPressEvent event) {
            if (!Character.isDigit(event.getCharCode())) {
                ((TextBox) event.getSource()).cancelKey();
            }

        }

    }


}
