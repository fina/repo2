package com.learn2develop.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.learn2develop.client.RPCServiceAsync;
import com.learn2develop.shared.TemplateModel;

import java.util.List;

/**
 * Created by oto on 10/3/2014.
 */
public class TemplatePresenter implements Presenter {
    private RPCServiceAsync service;
    private Display display;
    private HandlerManager eventBus;

    public TemplatePresenter(RPCServiceAsync service, Display display, HandlerManager eventBus) {
        this.service = service;
        this.display = display;
        this.eventBus = eventBus;
    }

    public interface Display{
        void setData(List<TemplateModel> templateModelList);
        Widget asWidget();
    }
    void initTable(){

    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }
}
