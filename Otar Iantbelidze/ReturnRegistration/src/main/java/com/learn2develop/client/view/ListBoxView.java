package com.learn2develop.client.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;

public class ListBoxView extends ListBox implements HasValue<String> {

	private boolean valueChangeHandlerInitialized;

	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		if (!valueChangeHandlerInitialized) {
			valueChangeHandlerInitialized = true;
			addChangeHandler(new ChangeHandler() {
				public void onChange(ChangeEvent event) {
					ValueChangeEvent.fire(ListBoxView.this, getValue());
				}
			});
		}
		return addHandler(handler, ValueChangeEvent.getType());
	}

	public String getValue() {
		return getItemText(getSelectedIndex());
	}

	public void setValue(String value) {
		for (int i = 0; i < getItemCount(); i++) {
			if (getItemText(i).equals(value)) {
				setSelectedIndex(i);
				break;

			}
		}

	}

	public void setValue(String value, boolean fireEvents) {
		setValue(value);
		if (fireEvents)
			ValueChangeEvent.fire(this, value);

	}

}
