package com.learn2develop.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Created by oto on 8/25/2014.
 */
public interface Presenter {
    void go(HasWidgets container);
}
