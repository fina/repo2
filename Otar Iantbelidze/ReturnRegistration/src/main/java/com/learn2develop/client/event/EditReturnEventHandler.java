package com.learn2develop.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 9/21/2014.
 */
public interface EditReturnEventHandler extends EventHandler {
    public void onEditReturn(EditReturnEvent event);
}
