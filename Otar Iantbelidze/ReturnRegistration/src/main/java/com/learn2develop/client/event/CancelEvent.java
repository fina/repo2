package com.learn2develop.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 8/26/2014.
 */
public class CancelEvent extends GwtEvent<CancelEventHandler> {
    public static Type<CancelEventHandler> TYPE=new Type<CancelEventHandler>();
    @Override
    public Type<CancelEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CancelEventHandler handler) {
            handler.onCancelEvent(this);
    }
}
