package com.learn2develop.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;


public class ModuleLoader implements EntryPoint {

    public void onModuleLoad() {
        RPCServiceAsync service = GWT.create(RPCService.class);
        HandlerManager eventBus = new HandlerManager(null);
        AppController app=new AppController(service,eventBus);
        app.go(RootPanel.get());
    }
}