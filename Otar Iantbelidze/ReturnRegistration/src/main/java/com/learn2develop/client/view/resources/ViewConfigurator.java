package com.learn2develop.client.view.resources;

import com.github.gwtbootstrap.client.ui.config.Configurator;
import com.github.gwtbootstrap.client.ui.resources.Resources;
import com.google.gwt.core.client.GWT;

public class ViewConfigurator implements Configurator {
    public Resources getResources() {
        return GWT.create(ViewResources.class);
    }

    public boolean hasResponsiveDesign() {
        return false;
    }
}
