package com.learn2develop.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.HandlerManager;

/**
 * Created by oto on 8/26/2014.
 */
public interface CreateEventHandler extends EventHandler {
    void onCreateEvent(CreateEvent event);
}
