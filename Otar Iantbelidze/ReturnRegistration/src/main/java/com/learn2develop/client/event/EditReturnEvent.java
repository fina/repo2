package com.learn2develop.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 9/21/2014.
 */
public class EditReturnEvent extends GwtEvent<EditReturnEventHandler> {
    public static final Type<EditReturnEventHandler>TYPE=new Type<EditReturnEventHandler>();
    private String code;

    public EditReturnEvent(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public Type<EditReturnEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditReturnEventHandler handler) {
        handler.onEditReturn(this);
    }
}
