package com.learn2develop.client.view;

import com.github.gwtbootstrap.client.ui.Bar;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Hero;
import com.github.gwtbootstrap.client.ui.Navbar;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.learn2develop.client.RPCService;
import com.learn2develop.client.RPCServiceAsync;
import com.learn2develop.shared.TemplateModel;

import java.util.List;

/**
 * Created by oto on 10/3/2014.
 */
public class TemplateView {
    private DialogBox dialogBox;
    private FlexTable table;
    private Button closeButton;
    private VerticalPanel vPanel;

    private RPCServiceAsync service = GWT.create(RPCService.class);

    public TemplateView() {
        initView();
    }

    public void initView() {
        Navbar navbar = new Navbar();
        navbar.setStyleName("navbar");
        Bar bar = new Bar();
        bar.setText("Template Table");
        bar.setColor(Bar.Color.DEFAULT);
        bar.setColor(Bar.Color.DANGER);
        Hero hero = new Hero();
        dialogBox = new DialogBox();
        closeButton = new Button("close");
        closeButton.setType(ButtonType.WARNING);
        initTable();
        vPanel = new VerticalPanel();
        navbar.add(bar);
        vPanel.add(navbar);
        vPanel.add(hero);
        vPanel.add(closeButton);
        hero.add(table);
        dialogBox.add(vPanel);
        dialogBox.center();
        dialogBox.setAnimationEnabled(true);
        dialogBox.setGlassEnabled(true);
        dialogBox.show();
        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                dialogBox.hide();
            }
        });
    }

    public void initTable() {
        table = new FlexTable();
        table.setBorderWidth(5);
        table.setWidth("400px");
        table.getRowFormatter().addStyleName(0, "tableHeader");
        table.setText(0, 0, "Code");
        table.setText(0, 1, "Name");
        table.setText(0, 2, "Schedule");
        table.setText(0, 3, "Remove");
    }

    Button delete;

    public void setData(List<TemplateModel> templateModelList) {
        for (int i = 0; i < templateModelList.size(); i++) {
            delete = new Button("X");
            delete.setType(ButtonType.DANGER);
            table.setText(i + 1, 0, templateModelList.get(i).getCode());
            table.setText(i + 1, 1, templateModelList.get(i).getName());
            table.setText(i + 1, 2, templateModelList.get(i).getSchedule());
            table.setWidget(i + 1, 3, delete);
            delete.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    int rowIndex=table.getCellForEvent(event).getRowIndex();
                    String code=table.getText(rowIndex,0);
                    removeTemplate(code);
                    table.removeRow(rowIndex);
                }
            });
        }
        applyDataRowStyles();
    }

    public void removeTemplate(String code) {
        service.deleteTemplate(code, new AsyncCallback<Boolean>() {
            public void onFailure(Throwable caught) {
                Window.alert("Can't Delete Template From DB");
            }

            public void onSuccess(Boolean result) {

            }
        });
    }

    private void applyDataRowStyles() {
        HTMLTable.RowFormatter rf = table.getRowFormatter();

        for (int row = 1; row < table.getRowCount(); ++row) {
            if ((row % 2) != 0) {
                rf.addStyleName(row, "Table-OddRow");
            } else {
                rf.addStyleName(row, "Table-EvenRow");
            }
        }
    }
}
