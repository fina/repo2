package com.learn2develop.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.learn2develop.client.RPCService;
import com.learn2develop.shared.FinInstitute;
import com.learn2develop.shared.FinInstituteModel;
import com.learn2develop.shared.TemplateModel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * Created by oto on 8/26/2014.
 */
public class RPCServiceImpl extends RemoteServiceServlet implements RPCService {
    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("JPA");
    private EntityManager manager;
    private EntityTransaction transaction;
    private Logger logger = Logger.getLogger("log:");

    /**
     * this method returns all Entities from  DataBase
     */
    public List<FinInstituteModel> getData() {
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        Query queue = manager.createQuery(" from FinInstitute");
        List<FinInstitute> result = new ArrayList<FinInstitute>(queue.getResultList());
        List<FinInstituteModel> modelList = new ArrayList<FinInstituteModel>();
        for (FinInstitute fi : result) {
            FinInstituteModel fim = new FinInstituteModel(fi);
            modelList.add(fim);
        }
        return modelList;
    }

    FinInstitute getFiByCode(String code) {
        try {
            manager = factory.createEntityManager();
            Query query = manager.createQuery("select f from FinInstitute f where  insCode=:insCode").setParameter("insCode", code);
            FinInstitute fi = (FinInstitute) query.getSingleResult();
            return fi;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public boolean insert(FinInstitute f) {
        try {
            EntityManager manager = factory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.merge(f);
            manager.flush();
            manager.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            transaction.rollback();
            return false;
        }

    }


    public void deleteByCode(List<String> code) {
        manager = factory.createEntityManager();
        Query query = null;
        for (String s : code) {
            FinInstitute fi = getFiByCode(s);
            manager.getTransaction().begin();
            logger.info("INSTITUTE ID = " + fi.getId());
            manager.remove(fi);
            manager.getTransaction().commit();
        }
        manager.close();

    }

    public FinInstituteModel getInstituteByCode(String code) {
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createQuery("from FinInstitute where insCode=:insCode").setParameter("insCode", code);
        FinInstitute fi = (FinInstitute) query.getSingleResult();
        manager.getTransaction().commit();
        FinInstituteModel fim = new FinInstituteModel(fi);
        return fim;
    }

    public boolean validate(String insCode) {
        try {
            manager = factory.createEntityManager();
            manager.getTransaction().begin();
            Query query = manager.createQuery("select f from FinInstitute f where insCode=:insCode").setParameter("insCode", insCode.trim());
            FinInstitute fi = (FinInstitute) query.getSingleResult();
            manager.getTransaction().commit();
            logger.info("founded Entity insCOde : " + fi.getInsCode());
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } finally {
            if (manager != null)
                manager.close();
        }

    }

    public List<TemplateModel> getTemplateData(String code) {
        FinInstituteModel instituteModel = getInstituteByCode(code);
        List<TemplateModel> templateList = instituteModel.getTemplateModelList();
        return templateList;
    }

    public boolean deleteTemplate(String code) {
        manager=factory.createEntityManager();
        manager.getTransaction().begin();
        Query query=manager.createQuery("delete  from Template where code=:code").setParameter("code",code.trim());
        int update=query.executeUpdate();
        manager.getTransaction().commit();
        return update>0;
    }

}
