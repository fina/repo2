package com.learn2develop.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RPCServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void getData( AsyncCallback<java.util.List<com.learn2develop.shared.FinInstituteModel>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void insert( com.learn2develop.shared.FinInstitute r, AsyncCallback<java.lang.Boolean> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void deleteByCode( java.util.List<java.lang.String> code, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void getInstituteByCode( java.lang.String code, AsyncCallback<com.learn2develop.shared.FinInstituteModel> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void validate( java.lang.String insCode, AsyncCallback<java.lang.Boolean> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void getTemplateData( java.lang.String code, AsyncCallback<java.util.List<com.learn2develop.shared.TemplateModel>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.learn2develop.client.RPCService
     */
    void deleteTemplate( java.lang.String code, AsyncCallback<java.lang.Boolean> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static RPCServiceAsync instance;

        public static final RPCServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (RPCServiceAsync) GWT.create( RPCService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
