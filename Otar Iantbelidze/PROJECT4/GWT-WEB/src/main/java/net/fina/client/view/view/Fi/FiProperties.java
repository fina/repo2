package net.fina.client.view.view.Fi;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.FiTypeModel;

import java.util.Date;

/**
 * Created by oto on 10/30/2014.
 */
public interface FiProperties extends PropertyAccess<FiModel> {
    @Editor.Path("id")
    ModelKeyProvider<FiModel> key();

    ValueProvider<FiModel, String> name();

    ValueProvider<FiModel, String> code();

    ValueProvider<FiModel, String> phone();

    ValueProvider<FiModel, String> mail();

    ValueProvider<FiModel, String> fax();

    ValueProvider<FiModel, String> address();

    ValueProvider<FiModel, Date> date();

    ValueProvider<FiModel, String> typeString();

}
