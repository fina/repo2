package net.fina.shared.model;

import java.io.Serializable;

/**
 * Created by oto on 11/6/2014.
 */
public class FiTypeModel implements Serializable {
    private int id;
    private String code;
    private String name;

    public FiTypeModel() {
    }

    public FiTypeModel(int id,String code, String name) {
        this.id=id;
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
