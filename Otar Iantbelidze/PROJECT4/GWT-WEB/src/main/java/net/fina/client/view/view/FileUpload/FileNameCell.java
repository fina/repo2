package net.fina.client.view.view.FileUpload;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

/**
 * Created by oto on 12/17/2014.
 */
public class FileNameCell extends AbstractCell<String> {

    private static String servletName = GWT.getModuleBaseURL() + "FileDownloadServlet";

    public FileNameCell() {
        super();
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event, ValueUpdater<String> valueUpdater) {
        // Check that the value is not null.
        if (value == null) {
            return;
        }

        // Call the super handler, which handlers the enter key.
        super.onBrowserEvent(context, parent, value, event, valueUpdater);
    }

    @Override
    public void render(Context context, String value, SafeHtmlBuilder sb) {
        if (value == null) {
            return;
        }

        long fileId = FileUploadTabView.grid.getStore().get(context.getIndex()).getId();
        sb.appendHtmlConstant("<div>");
        sb.appendHtmlConstant("<a href=\"" + servletName + "?id=" + fileId + "\" >");
        sb.appendEscaped(value);
        sb.appendHtmlConstant("</a></div>");
    }
}
