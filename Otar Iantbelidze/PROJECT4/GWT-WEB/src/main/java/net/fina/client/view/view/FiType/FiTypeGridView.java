package net.fina.client.view.view.FiType;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import net.fina.shared.model.FiTypeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 10/29/2014.
 */
public class FiTypeGridView implements IsWidget {

    private static final FiTypeProperties props = GWT.create(FiTypeProperties.class);
    private ContentPanel panel;
    private List<FiTypeModel> lst;


    private static Grid<FiTypeModel> grid;

    public Widget asWidget() {
        if (panel == null) {

            lst = new ArrayList<FiTypeModel>();

            IdentityValueProvider<FiTypeModel> identity = new IdentityValueProvider<FiTypeModel>();
            CheckBoxSelectionModel<FiTypeModel> checkBoxSelectionModel = new CheckBoxSelectionModel<FiTypeModel>(identity);

            ColumnConfig<FiTypeModel, String> nameCol = new ColumnConfig<FiTypeModel, String>(props.name(), 200, "Name");
            ColumnConfig<FiTypeModel, String> codeCol = new ColumnConfig<FiTypeModel, String>(props.code(), 100, "Code");

            List<ColumnConfig<FiTypeModel, ?>> columnConfigs = new ArrayList<ColumnConfig<FiTypeModel, ?>>();
            columnConfigs.add(checkBoxSelectionModel.getColumn());
            columnConfigs.add(nameCol);
            columnConfigs.add(codeCol);

            ColumnModel<FiTypeModel> cModel = new ColumnModel<FiTypeModel>(columnConfigs);

            panel = new ContentPanel();
            panel.setHeadingText("CheckBox Grid");
            panel.setPixelSize(600, 320);

            //grid
            ListStore<FiTypeModel> store = new ListStore<FiTypeModel>(props.key());

            grid = new Grid<FiTypeModel>(store, cModel);
            grid.setSelectionModel(checkBoxSelectionModel);
            grid.getView().setAutoExpandColumn(nameCol);
            grid.setBorders(true);
            grid.getView().setStripeRows(true);
            grid.getView().setColumnLines(true);

            VerticalLayoutContainer con = new VerticalLayoutContainer();
            con.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));

            panel.add(con, new VerticalLayoutContainer.VerticalLayoutData(1, 1));

        }
        return panel;
    }

    public static List<FiTypeModel> gerSelectedRows(){
        return grid.getSelectionModel().getSelectedItems();
    }
    public static FiTypeModel getSelectedRow(){
        return grid.getSelectionModel().getSelectedItem();
    }

    public void setData(List<FiTypeModel> modelList) {
        if (grid != null) {
            grid.getStore().clear();
            grid.getStore().addAll(modelList);
        }
    }

    public static Grid<FiTypeModel> getGrid() {
        return grid;
    }
}
