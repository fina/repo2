package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.ValueBaseField;
import net.fina.client.service.FiTypeService;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.FiTypeModel;

/**
 * Created by oto on 10/28/2014.
 */
public class EditFiTypePresenter implements Presenter {
    private Display display;
    private HandlerManager eventBus;
    private FiTypeModel type = new FiTypeModel();

    public EditFiTypePresenter(Display display) {
        this.display = display;
        bind();
    }

    public EditFiTypePresenter(Display display, FiTypeModel type) {
        this.display = display;
        this.type = type;
        display.name().setValue(type.getName());
        display.code().setValue(type.getCode());
        bind();
    }

    public interface Display {
        SelectEvent.HasSelectHandlers getSaveButton();

        SelectEvent.HasSelectHandlers getCancelButton();

        ValueBaseField<String> code();

        ValueBaseField<String> name();

        HasWidgets getWindow();

        boolean isValid();

        Widget asWidget();
    }

    public void bind() {
        display.getSaveButton().addSelectHandler(
                new SelectEvent.SelectHandler() {
                    public void onSelect(SelectEvent selectEvent) {
                        if (!display.isValid()) {
                            new AlertMessageBox("INFO", "Please Fill All Fields").show();
                            return;
                        }
                        type.setCode(display.code().getValue().trim());
                        type.setName(display.name().getValue().trim());

                        FiTypeService.Util.getInstance().addFiType(type, new AsyncCallback<Void>() {
                            public void onFailure(Throwable caught) {
                                if (caught instanceof FieldUniqueException) {
                                    new AlertMessageBox("ERROR", caught.getMessage()).show();
                                    return;
                                }
                                else {
                                    new AlertMessageBox("ERROR", caught.getMessage()).show();
                                    return;
                                }
                            }

                            public void onSuccess(Void result) {
                                ((Window) display.getWindow()).hide();
                                refreshGrid();
                            }
                        });

                    }
                }

        );
        display.getCancelButton().addSelectHandler(
                new SelectEvent.SelectHandler() {

                    public void onSelect(SelectEvent event) {
                        ((Window) display.getWindow()).hide();
                    }
                }

        );

    }

    public void refreshGrid() {
        FiTypeTabPresenter.loadFiTypes();
    }

    public void go(HasWidgets container) {
        display.asWidget();
    }
}
