package net.fina.client.presenter;

import com.extjs.gxt.ui.client.widget.Info;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent;
import net.fina.client.service.FiModelService;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.UploadFileModel;

import java.util.Date;
import java.util.List;

/**
 * Created by oto on 12/8/2014.
 */
public class FileUploadWindowPresenter implements Presenter {
    private Display display;
    private HandlerManager eventBus;
    private static UploadFileModel model;
    private String fileName;

    public FileUploadWindowPresenter(Display display) {
        this.display = display;
        bind();
    }

    public interface Display {
        SelectEvent.HasSelectHandlers getStartUploadButton();

        SelectEvent.HasSelectHandlers getRemoveButton();

        HasChangeHandlers getUploadField();

        SelectEvent.HasSelectHandlers getNextButton();

        SelectEvent.HasSelectHandlers getBackButton();

        HasValue<FiModel> getFiModel();

        String getFileName();

        void addModelToGird(UploadFileModel model);

        void removeModel();

        void submitForm();

        void nextAction();

        void backAction();

        void enableUploadButton(boolean enable);

        void addSubmitHandler();

        void initFiCombo(List<FiModel> modelList);

        void hideWindow();

        SubmitCompleteEvent.HasSubmitCompleteHandlers onSubmitEvent();

        void addDataToUploadGrid(List<UploadFileModel> list);

        boolean isComboValid();

        Widget asWidget();
    }

    public void bind() {
        initComboAtStartup();
        display.getUploadField().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                model = new UploadFileModel();
                String filePath = display.getFileName();
                fileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
                model.setFileName(fileName);
                model.setUploadDate(new Date());
                model.setFiModel(display.getFiModel().getValue());
                display.addModelToGird(model);
                display.enableUploadButton(true);
            }
        });
        display.getRemoveButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                display.removeModel();
                display.enableUploadButton(false);
            }
        });
        display.getStartUploadButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!check()) {
                    new AlertMessageBox("INFO", "Invalid pattern or fi doesn't have templates").show();
                    return;
                }
                display.submitForm();

            }
        });
        display.getNextButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!display.isComboValid()) {
                    new AlertMessageBox("INFO", "Please Select Fi..").show();
                    return;
                }
                display.nextAction();
            }
        });
        display.getBackButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                display.backAction();
            }
        });
        display.onSubmitEvent().addSubmitCompleteHandler(new SubmitCompleteEvent.SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {

                try {
                    String result = event.getResults();
                    if (result.contains("done")) {
                        MessageBox box = new MessageBox("File Upload Info", "file Uploaded Successfully");
                        box.setIcon(MessageBox.ICONS.info());
                        display.hideWindow();
                        box.getButton(Dialog.PredefinedButton.OK).addSelectHandler(new SelectEvent.SelectHandler() {
                            @Override
                            public void onSelect(SelectEvent event) {
                                FileUploadTabPresenter.getDisplay().refreshToolbarData();
                            }
                        });
                        box.show();
                    } else if (result.contains("Code_Error")) {
                        new AlertMessageBox("INFO", "Fi code in File Name and Fi Code in file doesn't match!!!").show();
                    } else if (result.contains("Schedule_Error")) {
                        new AlertMessageBox("INFO", "Date in file Name and Date in file are not compatible with each Other!!").show();
                    }
                } catch (Exception ex) {
                    new AlertMessageBox("ERROR", ex.getMessage()).show();
                }

            }
        });
    }

    public void initComboAtStartup() {
        FiModelService.Util.getInstance().getModelList(new AsyncCallback<List<FiModel>>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", "Can't Retrieve Fi DAta \n" + caught.getMessage()).show();
            }

            @Override
            public void onSuccess(List<FiModel> result) {
                display.initFiCombo(result);
            }
        });
    }

    public static UploadFileModel getModel() {
        return model;
    }

    public boolean check() {
        StringBuilder sb = new StringBuilder();
        String tempName = fileName.substring(0, fileName.lastIndexOf("."));
        FiModel fiModel = display.getFiModel().getValue();
        if (fiModel.getTemplateModelList().size() == 0)
            return false;
        for (int i = 0; i < fiModel.getTemplateModelList().size(); i++) {
            sb.append(fiModel.getType().getName())
                    .append(".")
                    .append(fiModel.getCode())
                    .append(".")
                    .append(fiModel.getTemplateModelList().get(i).getCode())
                    .append(".")
                    .append(DateTimeFormat.getFormat("ddMMyyyy").format(new Date()));
          /*  switch (fiModel.getTemplateModelList().get(i).getScheduleModel()) {
                case "MONTHLY": {
                    Date current = new Date();
                    DateTimeFormat dtf = DateTimeFormat.getFormat("ddMMyyyy");
                    sb.append(dtf.format(current));
                }
                break;
                case "DAYLY": {

                }
            }*/
            if (tempName.trim().equalsIgnoreCase(sb.toString().trim())) {
                return true;
            }
            sb.setLength(0);
        }
        return false;
    }

    @Override
    public void go(HasWidgets container) {
        display.asWidget();
    }
}
