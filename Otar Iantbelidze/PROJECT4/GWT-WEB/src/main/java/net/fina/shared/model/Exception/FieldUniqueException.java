package net.fina.shared.model.Exception;

import java.io.Serializable;

/**
 * Created by oto on 12/17/2014.
 */
public class FieldUniqueException extends Exception implements Serializable {
    public FieldUniqueException() {
    }

    public FieldUniqueException(String message) {
        super(message);
    }
}

