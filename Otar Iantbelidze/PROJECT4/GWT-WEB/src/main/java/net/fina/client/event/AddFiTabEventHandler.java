package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 10/28/2014.
 */
public interface AddFiTabEventHandler extends EventHandler {
    public void onAddTab(AddFiTabEvent event);
}
