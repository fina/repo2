package net.fina.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import net.fina.shared.model.FiModel;

import java.util.List;

/**
 * Created by oto on 11/13/2014.
 */
@RemoteServiceRelativePath("fiModelService")
public interface FiModelService extends RemoteService {

    public static class Util {
        private static FiModelServiceAsync instance;

        public static FiModelServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(FiModelService.class);
            }
            return instance;
        }

    }


    public List<FiModel> getModelList();

    public void saveFi(FiModel fiModel)throws Exception;

    public boolean deleteFi(FiModel fiModel);

    public boolean deleteFiList(List<FiModel> modelList);

    public boolean setTemplatesToFi(FiModel fiModel);
}
