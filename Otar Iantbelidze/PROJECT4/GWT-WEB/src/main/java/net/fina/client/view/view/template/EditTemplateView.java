package net.fina.client.view.view.template;

import com.google.gwt.user.client.ui.*;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.*;
import net.fina.client.presenter.EdiTemplatePresenter;
import net.fina.shared.model.ScheduleModel;

import java.util.Arrays;

/**
 * Created by oto on 11/14/2014.
 */
public class EditTemplateView extends Composite implements EdiTemplatePresenter.Display, IsWidget {
    private Window window;
    private VerticalPanel vp;
    private FramedPanel framePanel;

    private TextField nameField;
    private TextField codeField;
    private SimpleComboBox<ScheduleModel> scheduleCombo;

    private TextButton saveButton;
    private TextButton cancelButton;
    private boolean valid;


    public EditTemplateView() {
        createView();
    }

    public void createView() {
        if (vp == null) {
            vp = new VerticalPanel();
            vp.setSpacing(10);


            framePanel = new FramedPanel();
            framePanel.setBodyStyle("background: none; padding: 10px");
            framePanel.setWidth(350);

            FieldSet fieldSet = new FieldSet();
            fieldSet.setHeadingText("Template Creation");
            fieldSet.setCollapsible(true);
            framePanel.add(fieldSet);

            VerticalLayoutContainer vlc = new VerticalLayoutContainer();
            fieldSet.add(vlc);

            nameField = new TextField();
            nameField.setAllowBlank(false);
            vlc.add(new FieldLabel(nameField, "enter name"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            codeField = new TextField();
            codeField.setAllowBlank(false);
            vlc.add(new FieldLabel(codeField, "enter code"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));


            scheduleCombo = new SimpleComboBox<ScheduleModel>(new LabelProvider<ScheduleModel>() {
                public String getLabel(ScheduleModel item) {
                    return item.name();
                }
            });
            scheduleCombo.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
            scheduleCombo.setAllowBlank(false);
            scheduleCombo.getStore().clear();
            scheduleCombo.getStore().addAll(Arrays.asList(ScheduleModel.values()));
            vlc.add(new FieldLabel(scheduleCombo, "select Schedule"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            saveButton = new TextButton("Save");
            cancelButton = new TextButton("Cancel");


            framePanel.addButton(saveButton);
            framePanel.addButton(cancelButton);

            vp.add(framePanel);
            window = new Window();
            window.add(framePanel);
            window.setModal(true);

        }
    }


    public SelectEvent.HasSelectHandlers getSaveButton() {
        return saveButton;
    }

    public SelectEvent.HasSelectHandlers getCancelbutton() {
        return cancelButton;
    }

    public ValueBaseField<String> name() {
        return nameField;
    }

    public ValueBaseField<String> code() {
        return codeField;
    }

    public HasValue<String> getValueFromCombo() {
        return null;
    }

    @Override
    public HasValue<String> getName() {
        return nameField;
    }

    @Override
    public HasValue<String> getCode() {
        return codeField;
    }

    @Override
    public HasValue<ScheduleModel> getSch() {
        return scheduleCombo;
    }

    public SimpleComboBox<ScheduleModel> getCombo() {
        return scheduleCombo;
    }

    public HasWidgets getWindow() {
        return window;
    }

    @Override
    public boolean isValid() {
        valid = true;
        valid &= nameField.isValid();
        valid &= codeField.isValid();
        valid &= scheduleCombo.isValid();
        return valid;
    }

    @Override
    public void hideWindow() {
        this.window.hide();
    }


    public Widget asWidget() {
        window.show();
        return this;
    }


}
