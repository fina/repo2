package net.fina.shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
public class FiModel implements Serializable {
    private int id;
    private String code;
    private String name;
    private String address;
    private String phone;
    private String fax;
    private String mail;
    private Date reg_date;
    private FiTypeModel type;
    private String typeString;
    private List<TemplateModel> templateModelList;

    public FiModel() {
        templateModelList=new ArrayList<>();
    }

    public FiModel(String code, String name, String address, String phone, String fax, String mail, java.util.Date regdate, FiTypeModel type) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.mail = mail;
        reg_date = regdate;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public FiTypeModel getType() {
        return type;
    }

    public void setType(FiTypeModel type) {
        this.type = type;
    }

    public List<TemplateModel> getTemplateModelList() {
        return templateModelList;
    }

    public void setTemplateModelList(List<TemplateModel> templateModelList) {
        this.templateModelList = templateModelList;
    }

    public void setTypeString(String typeString) {
        this.typeString = type.getName();
    }

    /* this method returns String representation of fi Type
     */
    public String getTypeString() {
        return type.getName();
    }
}
