package net.fina.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import net.fina.shared.model.FiModel;

import java.util.List;

public interface FiModelServiceAsync {
    void getModelList(AsyncCallback<List<FiModel>> async);

    void saveFi(FiModel fiModel, AsyncCallback<Void> async);

    void deleteFi(FiModel fiModel, AsyncCallback<Boolean> async);

    void deleteFiList(List<FiModel> modelList, AsyncCallback<Boolean> async);

    void setTemplatesToFi(FiModel fiModel, AsyncCallback<Boolean> async);
}
