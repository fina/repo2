package net.fina.client.view.view.FileUpload;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.TemplateModel;
import net.fina.shared.model.UploadFileModel;

import java.util.Date;

/**
 * Created by oto on 12/7/2014.
 */
public interface FileProperties extends PropertyAccess<UploadFileModel> {
    @Editor.Path("id")
    ModelKeyProvider<UploadFileModel> key();

    ValueProvider<UploadFileModel, Date> uploadDate();

    ValueProvider<UploadFileModel, Date> ScheduleDate();

    ValueProvider<UploadFileModel, String> fileName();
}
