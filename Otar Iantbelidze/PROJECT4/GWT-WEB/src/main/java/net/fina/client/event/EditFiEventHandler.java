package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 10/27/2014.
 */
public interface EditFiEventHandler extends EventHandler {
    public void onEditFiEvent(EditFiEvent event);
}
