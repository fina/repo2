package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 12/7/2014.
 */
public interface AddUploadTabEventHandler extends EventHandler {
    public void onAddTab(AddUploadTabEvent event);
}
