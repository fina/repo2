package net.fina.client.view.view.Fi;

import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.*;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.*;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;
import net.fina.client.presenter.FiTabPresenter;
import net.fina.client.service.TemplateService;
import net.fina.client.view.view.Images.Images;
import net.fina.client.view.view.template.TemplateProperties;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.TemplateModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 10/18/2014.
 */
public class FiTabView extends Composite implements FiTabPresenter.Display, IsWidget {

    private ContentPanel lccenter;
    private ContentPanel gridPanel;
    private ContentPanel con;
    private ButtonBar buttonBar;
    private TextButton createButton;
    private TextButton editButton;
    private TextButton deleteButton;
    private TextButton refreshButton;
    private TextButton mapTemplates;
    public static int level = 0;

    private ListStore<FiModel> store;
    private static final FiProperties props = GWT.create(FiProperties.class);
    private static final TemplateProperties tempProps = GWT.create(TemplateProperties.class);
    private ListStore<TemplateModel> TemplatInfoStore;

    private Grid<FiModel> fiGrid;
    private Grid<TemplateModel> templateGrid;
    private Grid<TemplateModel> templateInfoGrid;
    private Viewport framePanel;
    private VerticalLayoutContainer verticalLayoutContainer;
    private ContentPanel rightPanel;

    private Dialog dialog;
    private TextButton cancel = new TextButton("Cancel");
    private TextButton save = new TextButton("Save");
    ContentPanel panel;

    public FiTabView() {
        if (panel == null) {

            HorizontalLayoutContainer hlc = new HorizontalLayoutContainer();

            panel = new ContentPanel();
            panel.setHeadingText("Create Fi");
            panel.setPixelSize(600, 550);


            HBoxLayoutContainer lcwest = new HBoxLayoutContainer();
            lcwest.setPadding(new Padding(5));
            lcwest.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.STRETCH);

            buttonBar = new ButtonBar();
            buttonBar.setMinButtonWidth(75);
            buttonBar.getElement().setMargins(10);

            createButton = new TextButton("Create", Images.INSTANCE.createIcon());
            editButton = new TextButton("Edit", Images.INSTANCE.editIcon());
            deleteButton = new TextButton("Delete", Images.INSTANCE.deleteIcon());
            refreshButton = new TextButton("Refresh", Images.INSTANCE.refreshIcon());
            mapTemplates = new TextButton("Set Templates");

            buttonBar.add(createButton);
            buttonBar.add(editButton);
            buttonBar.add(deleteButton);
            buttonBar.add(refreshButton);
            buttonBar.add(mapTemplates);

            lcwest.add(buttonBar);

            BorderLayoutContainer border = new BorderLayoutContainer();
            panel.add(border);

            BorderLayoutContainer.BorderLayoutData north = new BorderLayoutContainer.BorderLayoutData(40);
            north.setMargins(new Margins(5));

            border.setNorthWidget(lcwest, north);

            lccenter = new ContentPanel();

            MarginData centerMargin = new MarginData(new Margins(5));
            border.setCenterWidget(lccenter, centerMargin);

            rightPanel = new ContentPanel();
            rightPanel.setHeadingText("Template Properties");
            rightPanel.setVisible(false);
            rightPanel.setLayoutData(new FitLayout());
            createGrid();
            lccenter.add(fiGrid);

            hlc.add(panel);
            hlc.add(rightPanel);
            //viewport.add(hlc);
            initWidget(hlc);
        }
    }

    public void createGrid() {
        if (gridPanel == null) {

            store = new ListStore<>(props.key());
            IdentityValueProvider<FiModel> identity = new IdentityValueProvider<>();
            CheckBoxSelectionModel<FiModel> selectionModel = new CheckBoxSelectionModel<>(identity);


            List<ColumnConfig<FiModel, ?>> cfgs = new ArrayList<ColumnConfig<FiModel, ?>>();
            cfgs.add(selectionModel.getColumn());

            ColumnConfig<FiModel, String> nameCol = new ColumnConfig<FiModel, String>(props.name(), 150, "Name");
            nameCol.setCell(new TextCell());
            cfgs.add(nameCol);

            ColumnConfig<FiModel, String> codeCol = new ColumnConfig<FiModel, String>(props.code(), 200, "Code");
            codeCol.setCell(new TextCell());
            cfgs.add(codeCol);

            ColumnConfig<FiModel, String> tpcol = new ColumnConfig<FiModel, String>(props.typeString(), 200);
            tpcol.setHeader("type");
            tpcol.setCell(new TextCell());
            cfgs.add(tpcol);


            ColumnModel<FiModel> cModel = new ColumnModel<>(cfgs);


            GroupingView<FiModel> view = new GroupingView<FiModel>();
            view.setShowGroupedColumn(false);
            view.setForceFit(true);

            fiGrid = new Grid<>(store, cModel);
            fiGrid.setSelectionModel(selectionModel);
            fiGrid.setLoadMask(true);
            fiGrid.setView(view);
            view.groupBy(tpcol);


            gridPanel = new ContentPanel();
            gridPanel.setHeadingText("CheckBox Grid");
            //gridPanel.setPixelSize(600, 320);

            VerticalLayoutContainer con = new VerticalLayoutContainer();
            con.add(fiGrid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
            gridPanel.add(con);
        }
    }


    public Widget createTemplateGrid(final int id) {
        if (framePanel == null) {
            framePanel = new Viewport();
            //framePanel.setPixelSize(620,480);
            //proxy for PagingToolbar
            RpcProxy<PagingLoadConfig, PagingLoadResult<TemplateModel>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<TemplateModel>>() {

                @Override
                public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<TemplateModel>> callback) {
                    TemplateService.Util.getInstance().getFiTemplates(id, loadConfig, callback);
                }
            };

            TemplatInfoStore = new ListStore<TemplateModel>(tempProps.key());

            //Create Paging Toolbar
            final PagingLoader<PagingLoadConfig, PagingLoadResult<TemplateModel>> loadeer = new PagingLoader<PagingLoadConfig, PagingLoadResult<TemplateModel>>(proxy);
            loadeer.setRemoteSort(true);
            loadeer.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, TemplateModel, PagingLoadResult<TemplateModel>>(TemplatInfoStore));
            final PagingToolBar toolBar = new PagingToolBar(50);
            toolBar.getElement().getStyle().setProperty("borderBottom", "none");
            toolBar.bind(loadeer);
            toolBar.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
            toolBar.setPageSize(20);


            //fiGrid Columns
            ColumnConfig<TemplateModel, String> nameCol = new ColumnConfig<TemplateModel, String>(tempProps.name(), 100, "Name");
            ColumnConfig<TemplateModel, String> codeCol = new ColumnConfig<TemplateModel, String>(tempProps.code(), 100, "Code");
            ColumnConfig<TemplateModel, String> scheduleCol = new ColumnConfig<TemplateModel, String>(tempProps.scheduleModel(), 100, "Schedule");
            List<ColumnConfig<TemplateModel, ?>> columnConfigs = new ArrayList<ColumnConfig<TemplateModel, ?>>();
            columnConfigs.add(nameCol);
            columnConfigs.add(codeCol);
            columnConfigs.add(scheduleCol);
            ColumnModel<TemplateModel> cModel = new ColumnModel<TemplateModel>(columnConfigs);

            //Create Grid
            templateInfoGrid = new Grid<TemplateModel>(TemplatInfoStore, cModel) {
                @Override
                protected void onAfterFirstAttach() {
                    super.onAfterFirstAttach();
                    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                        @Override
                        public void execute() {
                            loadeer.load();
                        }
                    });
                }
            };
            templateInfoGrid.getView().setForceFit(true);
            templateInfoGrid.getView().setAutoExpandColumn(nameCol);
            templateInfoGrid.setBorders(true);
            templateInfoGrid.getView().setStripeRows(true);
            templateInfoGrid.getView().setColumnLines(true);
            templateInfoGrid.setColumnReordering(true);
            templateInfoGrid.setLoadMask(true);

            verticalLayoutContainer = new VerticalLayoutContainer();
            //
            verticalLayoutContainer.add(templateInfoGrid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
            //verticalLayoutContainer.add(toolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            framePanel.add(verticalLayoutContainer);
        }
        return framePanel;
    }


    @Override
    public void initTemplateMapWidget() {
        dialog = new Dialog();
        dialog.setBodyBorder(false);
        dialog.setHeadingText("Template Info");
        dialog.setWidth(800);
        dialog.setHeight(650);
        dialog.getButtonBar().setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        dialog.getButtonBar().add(save);
        dialog.getButtonBar().add(cancel);
        dialog.getButton(Dialog.PredefinedButton.OK).hide();

        BorderLayoutContainer layout = new BorderLayoutContainer();
        dialog.add(layout);

        // Layout - west
        ContentPanel panel = new ContentPanel();
       /* panel.setHeadingText("West");
        BorderLayoutContainer.BorderLayoutData data = new BorderLayoutContainer.BorderLayoutData(150);
        data.setMargins(new Margins(0, 5, 0, 0));
        panel.setLayoutData(data);
        layout.setWestWidget(panel);
*/
        // Layout - center
        panel = new ContentPanel();
        panel.setHeadingText("Center");

        IdentityValueProvider<TemplateModel> identity = new IdentityValueProvider<>();
        CheckBoxSelectionModel<TemplateModel> selectionModel = new CheckBoxSelectionModel<TemplateModel>(identity);
        final ListStore<TemplateModel> store = new ListStore<TemplateModel>(tempProps.key());


        ColumnConfig<TemplateModel, String> nameCol = new ColumnConfig<TemplateModel, String>(tempProps.name(), 100, "Name");
        ColumnConfig<TemplateModel, String> codeCol = new ColumnConfig<TemplateModel, String>(tempProps.code(), 100, "Code");
        ColumnConfig<TemplateModel, String> scheduleCol = new ColumnConfig<TemplateModel, String>(tempProps.scheduleModel(), 100, "Schedule");

        List<ColumnConfig<TemplateModel, ?>> columnConfigs = new ArrayList<ColumnConfig<TemplateModel, ?>>();
        columnConfigs.add(selectionModel.getColumn());
        columnConfigs.add(nameCol);
        columnConfigs.add(codeCol);
        columnConfigs.add(scheduleCol);

        ColumnModel<TemplateModel> columnModel = new ColumnModel<>(columnConfigs);
        templateGrid = new Grid<>(store, columnModel);
        templateGrid.setSelectionModel(selectionModel);
        templateGrid.getView().setAutoExpandColumn(nameCol);
        templateGrid.setBorders(true);
        templateGrid.getView().setStripeRows(true);
        templateGrid.getView().setColumnLines(true);
        templateGrid.setColumnReordering(true);
        templateGrid.setLoadMask(true);

        TemplateService.Util.getInstance().getData(new AsyncCallback<List<TemplateModel>>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage());
            }

            @Override
            public void onSuccess(List<TemplateModel> result) {
                store.addAll(result);
                templateGrid.unmask();
            }
        });
        panel.add(templateGrid);
        layout.setCenterWidget(panel);
        dialog.show();
    }

    @Override
    public boolean checkSelectedIFis() {
        return fiGrid.getSelectionModel().getSelectedItems().size() > 0;
    }


    public SelectEvent.HasSelectHandlers getCreateButton() {
        return createButton;
    }


    public SelectEvent.HasSelectHandlers getDeleteButton() {
        return deleteButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getRefreshButton() {
        return refreshButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getEditButton() {
        return editButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getTemplateButton() {
        return mapTemplates;
    }

    @Override
    public SelectEvent.HasSelectHandlers getCancelButton() {
        return cancel;
    }

    @Override
    public SelectEvent.HasSelectHandlers getTemplateSaveButton() {
//        return dialog.getButton(Dialog.PredefinedButton.OK);
        return save;
    }

    @Override
    public void hideDialog() {
        dialog.hide();
    }

    @Override
    public void setDataToGrid(List<FiModel> modelList) {
        fiGrid.getStore().clear();
        fiGrid.getStore().addAll(modelList);
    }

    @Override
    public void addTemplateGrid(int id) {

    }


    @Override
    public void remove(FiModel model) {
        this.fiGrid.getStore().remove(model);
    }

    @Override
    public Grid getFiGrid() {
        return this.fiGrid;
    }

    @Override
    public HasVisibility getGridPanel() {
        return rightPanel;
    }

    @Override
    public HasWidgets getPropertiesPanel() {
        return rightPanel;
    }

    @Override
    public void addTemplateInfo(int id) {
        rightPanel.clear();
        framePanel = null;
        rightPanel.add(createTemplateGrid(id));
    }


    @Override
    public List<FiModel> getSelectedRows() {
        return fiGrid.getSelectionModel().getSelectedItems();
    }

    @Override
    public List<TemplateModel> templateList() {
        return templateGrid.getSelectionModel().getSelectedItems();
    }

    @Override
    public FiModel getSelectedRow() {
        return fiGrid.getSelectionModel().getSelectedItem();
    }

    public Widget asWidget() {
        return this;
    }


}
