package net.fina.client.view.view.FiType;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import net.fina.shared.model.FiTypeModel;

/**
 * Created by oto on 10/29/2014.
 */
public interface FiTypeProperties extends PropertyAccess<FiTypeModel> {
    @Editor.Path("id")
    ModelKeyProvider<FiTypeModel> key();

    ValueProvider<FiTypeModel, String> name();

    ValueProvider<FiTypeModel, String> code();

}
