package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.FiTypeModel;

/**
 * Created by oto on 10/27/2014.
 */
public class EditFiEvent extends GwtEvent<EditFiEventHandler> {
    public static Type<EditFiEventHandler>TYPE=new Type<EditFiEventHandler>();

    private FiModel fiModel;

    public EditFiEvent() {
    }

    public EditFiEvent(FiModel fiModel) {
        this.fiModel = fiModel;
    }

    public FiModel getFiModel() {

        return fiModel;
    }

    @Override
    public Type getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditFiEventHandler handler) {
        handler.onEditFiEvent(this);
    }

}
