package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;
import net.fina.shared.model.TemplateModel;

/**
 * Created by oto on 11/14/2014.
 */
public class CreateTemplateEvent extends GwtEvent<CreateTemplateEventHandler> {
    public static Type<CreateTemplateEventHandler> TYPE = new Type<CreateTemplateEventHandler>();
    /*private TemplateModel templateModel;

    public CreateTemplateEvent() {
    }

    public CreateTemplateEvent(TemplateModel templateModel) {
        this.templateModel = templateModel;
    }

    public TemplateModel getTemplateModel() {
        return templateModel;
    }*/

    @Override
    public Type<CreateTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateTemplateEventHandler handler) {
        handler.onCreate(this);
    }
}
