package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.event.BeforeCloseEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.event.CreateFiTypeEvent;
import net.fina.client.event.EditFiTypeEvent;
import net.fina.client.service.FiTypeService;
import net.fina.client.view.view.FiType.FiTypeGridView;
import net.fina.client.view.view.FiType.FiTypeTabView;
import net.fina.shared.model.FiTypeModel;

import java.util.List;

/**
 * Created by oto on 10/27/2014.
 */
public class FiTypeTabPresenter implements Presenter {
    private static Display display;
    private HandlerManager eventBus;
    private static final String TABNAME = "Fi Type";
    private boolean confirm = false;

    public FiTypeTabPresenter(Display display, HandlerManager eventBus) {
        this.display = display;
        this.eventBus = eventBus;
        addTab(TABNAME, display.asWidget());
        bind();
    }

    public interface Display {
        SelectEvent.HasSelectHandlers getCreateButton();

        SelectEvent.HasSelectHandlers getDeleteButton();

        SelectEvent.HasSelectHandlers getRefreshButton();

        SelectEvent.HasSelectHandlers getEditButton();

        List<FiTypeModel> getSelectedRows();

        FiTypeModel getSelectedRow();

        void setDataToGrid(List<FiTypeModel> modelList);

        Widget asWidget();
    }

    public void bind() {
        loadFiTypes();
        display.getCreateButton().addSelectHandler(new SelectEvent.SelectHandler() {

            public void onSelect(SelectEvent event) {
                eventBus.fireEvent(new CreateFiTypeEvent());
            }
        });
        display.getRefreshButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent selectEvent) {
                loadFiTypes();
            }
        });
        display.getDeleteButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent selectEvent) {
                List<FiTypeModel> modelList = display.getSelectedRows();
                if (modelList.size() > 0)
                    confirmDelete(modelList);
            }
        });

        display.getEditButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent selectEvent) {
                eventBus.fireEvent(new EditFiTypeEvent(display.getSelectedRow()));
            }
        });
    }

    public void deleteFiTypeModel(final FiTypeModel model) {
        FiTypeService.Util.getInstance().deleteFiTypeModel(model, new AsyncCallback<Void>() {
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            public void onSuccess(Void result) {
                FiTypeGridView.getGrid().getStore().remove(model);
            }
        });
    }

    public void deleteFiType(final List<FiTypeModel> modelList) {
        FiTypeService.Util.getInstance().deleteFiTypeList(modelList, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    for (FiTypeModel model : modelList) {
                        FiTypeGridView.getGrid().getStore().remove(model);
                    }
                } else {
                    new AlertMessageBox("INFO", "Has Dependencies").show();
                }
            }
        });
    }


    public static void loadFiTypes() {
        FiTypeService.Util.getInstance().getFiTypeList(new AsyncCallback<List<FiTypeModel>>() {
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            public void onSuccess(List<FiTypeModel> result) {
                display.setDataToGrid(result);
            }
        });
    }

    public void addTab(String name, Widget widget) {

        ((TabPanel) MainPresenter.display.getTabPanel()).add(widget, name);
        TabItemConfig config = ((TabPanel) MainPresenter.display.getTabPanel()).getConfig(widget);
        config.setClosable(true);
        ((TabPanel) MainPresenter.display.getTabPanel()).update(widget, config);
        ((TabPanel) MainPresenter.display.getTabPanel()).setActiveWidget(widget);
        ((TabPanel) MainPresenter.display.getTabPanel()).addBeforeCloseHandler(new BeforeCloseEvent.BeforeCloseHandler<Widget>() {
            @Override
            public void onBeforeClose(BeforeCloseEvent<Widget> event) {
                FiTypeTabView.level = 0;
            }
        });
    }

    public void confirmDelete(final List<FiTypeModel> modelList) {
        ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete?");
        box.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
            @Override
            public void onDialogHide(DialogHideEvent event) {
                confirm = event.getHideButton().name().equalsIgnoreCase("yes");
                if (!confirm) return;
                deleteFiType(modelList);
            }
        });
        box.show();
    }


    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }
}
