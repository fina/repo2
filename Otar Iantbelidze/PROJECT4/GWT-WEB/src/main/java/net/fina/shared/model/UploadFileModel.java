package net.fina.shared.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by oto on 12/7/2014.
 */
public class UploadFileModel implements Serializable{
    private int id;
    private TemplateModel templateModel;
    private FiModel fiModel;
    private byte[] uploadedFile;
    private Date uploadDate;
    private Date ScheduleDate;
    private String fileName;

    public UploadFileModel() {
    }

    public UploadFileModel(int id, TemplateModel templateModel, FiModel fiModel, byte[] uploadedFile, Date uploadDate, Date scheduleDate) {
        this.id = id;
        this.templateModel = templateModel;
        this.fiModel = fiModel;
        this.uploadedFile = uploadedFile;
        this.uploadDate = uploadDate;
        ScheduleDate = scheduleDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TemplateModel getTemplateModel() {
        return templateModel;
    }

    public void setTemplateModel(TemplateModel templateModel) {
        this.templateModel = templateModel;
    }

    public FiModel getFiModel() {
        return fiModel;
    }

    public void setFiModel(FiModel fiModel) {
        this.fiModel = fiModel;
    }

    public byte[] getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(byte[] uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getScheduleDate() {
        return ScheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        ScheduleDate = scheduleDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
