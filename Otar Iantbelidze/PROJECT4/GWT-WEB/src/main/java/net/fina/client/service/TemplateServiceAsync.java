package net.fina.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import net.fina.shared.model.TemplateModel;

import java.util.List;

public interface TemplateServiceAsync {
    void getData(AsyncCallback<List<TemplateModel>> async);

    void saveTemplate(TemplateModel model, AsyncCallback<Boolean> async);

    void delete(TemplateModel model, AsyncCallback<Void> async);

    void getPagingData(PagingLoadConfig config, AsyncCallback<PagingLoadResult<TemplateModel>> async);

    void getFiTemplates(int id,PagingLoadConfig config, AsyncCallback<PagingLoadResult<TemplateModel>> async);

    void deleteTempList(List<TemplateModel> templateModels, AsyncCallback<Boolean> async);
}
