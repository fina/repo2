package net.fina.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import net.fina.shared.model.FiTypeModel;

import java.util.List;

public interface FiTypeServiceAsync {
    void getFiTypeList(AsyncCallback<List<FiTypeModel>> async);

    void addFiType(FiTypeModel type, AsyncCallback<Void> async);

    void deleteFiTypeModel(FiTypeModel model, AsyncCallback<Void> async);

    void deleteFiTypeList(List<FiTypeModel> list, AsyncCallback<Boolean> async);

}
