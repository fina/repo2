package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 10/28/2014.
 */
public interface CreateFiTypeEventHandler extends EventHandler {
    public void onCreate(CreateFiTypeEvent event);
}
