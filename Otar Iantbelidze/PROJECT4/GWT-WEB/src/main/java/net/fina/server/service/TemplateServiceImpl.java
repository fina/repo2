package net.fina.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import net.fina.client.service.TemplateService;
import net.fina.session.api.TemplateServiceLocal;
import net.fina.session.entity.Template;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.TemplateModel;

import javax.ejb.EJB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public class TemplateServiceImpl extends RemoteServiceServlet implements TemplateService {
    @EJB
    private TemplateServiceLocal service;

    private static ModelConverter<TemplateModel, Template> converter = new ModelConverter<TemplateModel, Template>();

    public List<TemplateModel> getData() {
        try {
            List<Template> templates = service.getTemplateData();
            List<TemplateModel> templateModels = new ArrayList<TemplateModel>();
            TemplateModel model = null;

            for (int i = 0; i < templates.size(); i++) {
                model = new TemplateModel();
                converter.toModel(model, templates.get(i));
                model.setScheduleModel(templates.get(i).getSchedule());
                templateModels.add(model);
            }
            return templateModels;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean saveTemplate(TemplateModel model) throws Exception {
        try {
            Template template = new Template();
            converter.toEntity(model, template);
            template.setSchedule(model.getScheduleModel());
            service.saveTemplate(template);
            return true;

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new FieldUniqueException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public void delete(TemplateModel model) {
        Template template = new Template();
        template.setId(model.getId());
        try {
            service.deleteTemplate(model.getId());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean deleteTempList(List<TemplateModel> templateModels) {
        boolean deleted = false;
        try {
            List<Integer> idList = new ArrayList<>();
            for (TemplateModel temp : templateModels) {
                idList.add(temp.getId());
            }
            deleted = service.deleteTempList(idList);
            return deleted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public PagingLoadResult<TemplateModel> getPagingData(PagingLoadConfig config) {
        List<TemplateModel> models = null;
        ArrayList<TemplateModel> sublist = null;
        try {

            List<Template> templates = new ArrayList<>(service.getTemplateData());
            models = new ArrayList<>();
            TemplateModel templateModel = null;
            for (int i = 0; i < templates.size(); i++) {
                templateModel = new TemplateModel();
                converter.toModel(templateModel, templates.get(i));
                templateModel.setScheduleModel(templates.get(i).getSchedule());
                models.add(templateModel);
            }

            if (config.getSortInfo().size() > 0) {
                SortInfo sort = config.getSortInfo().get(0);
                final String sortField = sort.getSortField();
                if (sortField != null) {
                    Collections.sort(models, sort.getSortDir().comparator(new Comparator<TemplateModel>() {
                        public int compare(TemplateModel p1, TemplateModel p2) {
                            if (sortField.equalsIgnoreCase("name")) {
                                return p1.getCode().compareTo(p2.getName());
                            } else if (sortField.equalsIgnoreCase("code")) {
                                return p1.getCode().compareTo(p2.getCode());
                            } else if (sortField.equalsIgnoreCase("schedule")) {
                                return p1.getScheduleModel().compareTo(p2.getScheduleModel());
                            }
                            return 0;
                        }
                    }));
                }
            }

            sublist = new ArrayList<>();
            int start = config.getOffset();
            int limit = models.size();
            if (config.getLimit() > 0) {
                limit = Math.min(start + config.getLimit(), limit);
            }
            for (int i = config.getOffset(); i < limit; i++) {
                sublist.add(models.get(i));
            }

            return new PagingLoadResultBean<TemplateModel>(sublist, models.size(), config.getOffset());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new PagingLoadResultBean<TemplateModel>(sublist, models.size(), config.getOffset());
    }

    @Override
    public PagingLoadResult<TemplateModel> getFiTemplates(int id, PagingLoadConfig config) {

        try {
            List<TemplateModel> models = new ArrayList<>();
            List<Template> templates = service.getFiTemplates(id);
            List<TemplateModel> sublist;

            TemplateModel templateModel;
            for (int i = 0; i < templates.size(); i++) {
                templateModel = new TemplateModel();
                converter.toModel(templateModel, templates.get(i));
                templateModel.setScheduleModel(templates.get(i).getSchedule());
                models.add(templateModel);
            }

            if (config.getSortInfo().size() > 0) {
                SortInfo sort = config.getSortInfo().get(0);
                final String sortField = sort.getSortField();
                if (sortField != null) {
                    Collections.sort(models, sort.getSortDir().comparator(new Comparator<TemplateModel>() {
                        public int compare(TemplateModel p1, TemplateModel p2) {
                            if (sortField.equalsIgnoreCase("name")) {
                                return p1.getCode().compareTo(p2.getName());
                            } else if (sortField.equalsIgnoreCase("code")) {
                                return p1.getCode().compareTo(p2.getCode());
                            } else if (sortField.equalsIgnoreCase("schedule")) {
                                return p1.getScheduleModel().compareTo(p2.getScheduleModel());
                            }
                            return 0;
                        }
                    }));
                }
            }

            sublist = new ArrayList<>();
            int start = config.getOffset();
            int limit = models.size();
            if (config.getLimit() > 0) {
                limit = Math.min(start + config.getLimit(), limit);
            }
            for (int i = config.getOffset(); i < limit; i++) {
                sublist.add(models.get(i));
            }

            return new PagingLoadResultBean<TemplateModel>(sublist, models.size(), config.getOffset());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
