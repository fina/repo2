package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.event.AddFiTabEvent;
import net.fina.client.event.AddFiTypeEvent;
import net.fina.client.event.AddTemplateTabEvent;
import net.fina.client.event.AddUploadTabEvent;
import net.fina.client.view.view.Fi.FiTabView;
import net.fina.client.view.view.FiType.FiTypeTabView;
import net.fina.client.view.view.FileUpload.FileUploadTabView;
import net.fina.client.view.view.template.TemplateTabView;

/**
 * Created by oto on 10/16/2014.
 */
public class MainPresenter implements Presenter {
    public Display getDisplay() {
        return display;
    }

    public static Display display;
    private HandlerManager eventBus;


    public MainPresenter(Display display, HandlerManager eventBus) {
        this.display = display;
        this.eventBus = eventBus;
    }

    public interface Display {
        HasWidgets getTabPanel();

        SelectEvent.HasSelectHandlers getFiTypeButton();

        SelectEvent.HasSelectHandlers getFiButton();

        SelectEvent.HasSelectHandlers getTemplateButton();

        SelectEvent.HasSelectHandlers getUploadButton();

        Widget asWidget();
    }


    public void bind() {
        display.getFiButton().addSelectHandler(new SelectEvent.SelectHandler() {

            public void onSelect(SelectEvent event) {
                if (FiTabView.level > 0) {
                    ((TabPanel) display.getTabPanel()).setAutoSelect(true);
                    return;
                }
                eventBus.fireEvent(new AddFiTabEvent());
                FiTabView.level++;
            }
        });
        display.getFiTypeButton().addSelectHandler(new SelectEvent.SelectHandler() {

            public void onSelect(SelectEvent event) {
                if (FiTypeTabView.level > 0) {
                    ((TabPanel) display.getTabPanel()).setAutoSelect(true);
                    return;
                }
                eventBus.fireEvent(new AddFiTypeEvent());
                FiTypeTabView.level++;
            }
        });
        display.getTemplateButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                if (TemplateTabView.level > 0) {
                    ((TabPanel) display.getTabPanel()).setAutoSelect(true);
                    return;
                }
                eventBus.fireEvent(new AddTemplateTabEvent());
                TemplateTabView.level++;
            }
        });
        display.getUploadButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (FileUploadTabView.level > 0) {
                    ((TabPanel) display.getTabPanel()).setAutoSelect(true);
                    return;
                }
                eventBus.fireEvent(new AddUploadTabEvent());
                FileUploadTabView.level++;
            }
        });
    }


    public void go(HasWidgets container) {
        bind();
        container.clear();
        container.add(display.asWidget());
    }


}
