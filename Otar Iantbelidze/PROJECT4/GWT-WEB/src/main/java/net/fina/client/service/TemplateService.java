package net.fina.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import net.fina.shared.model.TemplateModel;

import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
@RemoteServiceRelativePath("templateService")
public interface TemplateService extends RemoteService {


    public static class Util {
        private static TemplateServiceAsync instance;

        public static TemplateServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(TemplateService.class);
            }
            return instance;
        }

    }

    public List<TemplateModel> getData();

    public boolean saveTemplate(TemplateModel model)throws Exception;

    public void delete(TemplateModel model);

    public boolean deleteTempList(List<TemplateModel> templateModels);

    public PagingLoadResult<TemplateModel> getPagingData(PagingLoadConfig config);

    public PagingLoadResult<TemplateModel> getFiTemplates(int id, PagingLoadConfig config);
}
