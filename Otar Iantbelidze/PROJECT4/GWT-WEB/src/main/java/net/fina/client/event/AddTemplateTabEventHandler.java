package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 11/14/2014.
 */
public interface AddTemplateTabEventHandler extends EventHandler {
    public void onAddTab(AddTemplateTabEvent event);
}
