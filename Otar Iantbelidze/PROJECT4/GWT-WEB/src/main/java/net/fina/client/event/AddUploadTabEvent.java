package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 12/7/2014.
 */
public class AddUploadTabEvent extends GwtEvent<AddUploadTabEventHandler> {
    public static final Type<AddUploadTabEventHandler> TYPE = new Type<>();

    @Override
    public Type<AddUploadTabEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddUploadTabEventHandler handler) {
        handler.onAddTab(this);
    }
}
