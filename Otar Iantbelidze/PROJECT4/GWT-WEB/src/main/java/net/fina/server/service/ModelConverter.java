package net.fina.server.service;

import org.jboss.logging.Logger;

import java.lang.reflect.Field;

/**
 * Created by oto on 11/19/2014.
 */
public class ModelConverter<M, E> {
    private static Logger log = Logger.getLogger(ModelConverter.class);

    public E toEntity(M m, E e) {
        convert(m, e);
        return e;
    }

    public M toModel(M m,E e){
        convert(e,m);
        return m;
    }


    public static void convert(Object source, Object target) {
        try {
            Class<?> sourceClass = source.getClass();
            Class<?> taegetClass = target.getClass();


            for (Field sf : sourceClass.getDeclaredFields()) {
                sf.setAccessible(true);
                Field tf = null;

                try {
                    tf = taegetClass.getDeclaredField(sf.getName());
                } catch (Exception ex) {

                }

                if (tf != null) {
                    tf.setAccessible(true);
                    Object value = sf.get(source);
                    tf.set(target, value);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
