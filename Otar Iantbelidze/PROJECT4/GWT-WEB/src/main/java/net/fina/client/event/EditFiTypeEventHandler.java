package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 10/27/2014.
 */
public interface EditFiTypeEventHandler extends EventHandler {
    public void onEditEvent(EditFiTypeEvent event);
}
