package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 12/5/2014.
 */
public interface EditTemplateEventHandler extends EventHandler {
    public void onEditEvent(EditTemplateEvent event);
}
