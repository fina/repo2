package net.fina.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Created by oto on 10/16/2014.
 */
public interface Presenter {
    public void go(HasWidgets container);
}
