package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 10/28/2014.
 */
public class CreateFiEvent extends GwtEvent<CreateFiEventHandler> {
    public static final Type<CreateFiEventHandler> TYPE = new Type<CreateFiEventHandler>();

    @Override
    public Type<CreateFiEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateFiEventHandler handler) {
        handler.onCreateEvent(this);
    }
}
