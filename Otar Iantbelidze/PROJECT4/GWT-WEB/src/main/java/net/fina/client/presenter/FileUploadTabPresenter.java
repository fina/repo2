package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.event.BeforeCloseEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.service.UploadService;
import net.fina.client.view.view.FileUpload.FileUploadTabView;
import net.fina.client.view.view.FileUpload.FileUploadWindow;
import net.fina.shared.model.UploadFileModel;

import java.util.List;

/**
 * Created by oto on 12/7/2014.
 */
public class FileUploadTabPresenter implements Presenter {
    private static Display display;
    private HandlerManager eventBus;
    private boolean confirm;
    private AutoProgressMessageBox progress;

    public interface Display {
        SelectEvent.HasSelectHandlers getUploadButton();

        SelectEvent.HasSelectHandlers getDeleteButton();

        SelectEvent.HasSelectHandlers getRefreshButton();

        List<UploadFileModel> getSelectedRows();

        void remove(UploadFileModel fileModel);

        void refreshToolbarData();

        public Widget asWidget();
    }

    public FileUploadTabPresenter(Display display, HandlerManager eventBus) {
        this.display = display;
        this.eventBus = eventBus;
        addTab("File Upload", display.asWidget());
        bind();
    }

    public void bind() {
        display.getUploadButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                FileUploadWindowPresenter fuv = new FileUploadWindowPresenter(new FileUploadWindow());
                //fuv.show();
            }
        });
        display.getDeleteButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                List<UploadFileModel> modelList = display.getSelectedRows();
                if (modelList.size() > 0)
                    confirmDelete(modelList);
            }
        });
        display.getRefreshButton().addSelectHandler(new SelectEvent.SelectHandler() {

            @Override
            public void onSelect(SelectEvent event) {
                display.refreshToolbarData();
            }
        });
    }

    @Override
    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    public void confirmDelete(final List<UploadFileModel> modelList) {
        ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete?");
        box.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
            @Override
            public void onDialogHide(DialogHideEvent event) {
                confirm = event.getHideButton().name().equalsIgnoreCase("yes");
                if (!confirm)
                    return;
                deleteUploadedFiles(modelList);
            }
        });
        box.show();
    }

    public void deleteUploadedFiles(final List<UploadFileModel> models) {

        progress = new AutoProgressMessageBox("Progress", "Deleting your data, please wait...");
        progress.setProgressText("Deleting...");
        progress.auto();
        progress.show();

        UploadService.Util.getInstance().deleteModels(models, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("INFO", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    for (UploadFileModel model : models) {
                        display.remove(model);
                    }
                    progress.hide();
                } else {
                    new AlertMessageBox("INFO", "Server Returned " + result);
                }
            }
        });
    }

    public static Display getDisplay() {
        return display;
    }

    public void addTab(String name, Widget widget) {

        ((TabPanel) MainPresenter.display.getTabPanel()).add(widget, name);
        TabItemConfig config = ((TabPanel) MainPresenter.display.getTabPanel()).getConfig(widget);
        config.setClosable(true);
        ((TabPanel) MainPresenter.display.getTabPanel()).update(widget, config);
        ((TabPanel) MainPresenter.display.getTabPanel()).setActiveWidget(widget);
        ((TabPanel) MainPresenter.display.getTabPanel()).addBeforeCloseHandler(new BeforeCloseEvent.BeforeCloseHandler<Widget>() {
            @Override
            public void onBeforeClose(BeforeCloseEvent<Widget> event) {
                FileUploadTabView.level=0;
            }
        });
    }
}
