package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.event.BeforeCloseEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent;
import net.fina.client.event.CreateFiEvent;
import net.fina.client.event.EditFiEvent;
import net.fina.client.service.FiModelService;
import net.fina.client.view.view.Fi.FiTabView;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.TemplateModel;

import java.util.List;

/**
 * Created by oto on 10/24/2014.
 */
public class FiTabPresenter implements Presenter {
    private static Display display;
    private HandlerManager eventBus;
    private final String TABNAME = "Fi ";
    private boolean confirm;
    AutoProgressMessageBox progress;

    public interface Display {
        SelectEvent.HasSelectHandlers getCreateButton();

        SelectEvent.HasSelectHandlers getDeleteButton();

        SelectEvent.HasSelectHandlers getRefreshButton();

        SelectEvent.HasSelectHandlers getEditButton();

        SelectEvent.HasSelectHandlers getTemplateButton();

        SelectEvent.HasSelectHandlers getCancelButton();

        SelectEvent.HasSelectHandlers getTemplateSaveButton();

        void hideDialog();

        void setDataToGrid(List<FiModel> modelList);

        List<FiModel> getSelectedRows();

        List<TemplateModel> templateList();

        FiModel getSelectedRow();

        void remove(FiModel model);

        Grid getFiGrid();

        HasVisibility getGridPanel();

        HasWidgets getPropertiesPanel();

        void addTemplateInfo(int id);

        void addTemplateGrid(int id);

        void initTemplateMapWidget();

        boolean checkSelectedIFis();

        Widget asWidget();
    }

    public FiTabPresenter(Display display, HandlerManager eventBus) {
        this.display = display;
        this.eventBus = eventBus;
        addTab(TABNAME, display.asWidget());
        bind();
    }

    public void bind() {
        loadFis();
        display.getCreateButton().addSelectHandler(new SelectEvent.SelectHandler() {

            public void onSelect(SelectEvent event) {
                eventBus.fireEvent(new CreateFiEvent());
            }
        });
        display.getEditButton().addSelectHandler(
                new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        FiModel model = display.getSelectedRow();
                        eventBus.fireEvent(new EditFiEvent(model));
                    }
                }
        );
        display.getDeleteButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                List<FiModel> modelList = display.getSelectedRows();
                if (modelList.size() > 0)
                    confirmDelete(modelList);
                //refreshGrid();
            }
        });
        display.getRefreshButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                refreshGrid();
            }
        });
        display.getFiGrid().getSelectionModel().addSelectionChangedHandler(new SelectionChangedEvent.SelectionChangedHandler() {
            @Override
            public void onSelectionChanged(SelectionChangedEvent event) {
                FiModel model = (FiModel) event.getSelection().get(0);
                Info.display(model.getName(), "" + model.getId());
                // TemplateService.Util.getInstance().getFiTemplates(model.getId(),new );
                display.addTemplateInfo(model.getId());
                display.getGridPanel().setVisible(true);
                //display.addTemplateGrid(model.getId());
            }
        });
        display.getTemplateButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!display.checkSelectedIFis())
                    return;
                display.initTemplateMapWidget();
            }
        });

        //set templates to fi
        display.getCancelButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                display.hideDialog();
            }
        });

        display.getTemplateSaveButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                FiModel fiModel = display.getSelectedRow();
                if (display.templateList().size() == 0) {
                    Info.display("Warning!!!", "please select one or more template");
                    return;
                }
                fiModel.setTemplateModelList(display.templateList());
                FiModelService.Util.getInstance().setTemplatesToFi(fiModel, new AsyncCallback<Boolean>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        new AlertMessageBox("Error", caught.getMessage()).show();
                    }

                    @Override
                    public void onSuccess(Boolean result) {
                        Info.display("info", "server returned " + result);
                        display.hideDialog();
                    }
                });
            }
        });
    }

    public void confirmDelete(final List<FiModel> modelList) {
        ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete?");
        box.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
            @Override
            public void onDialogHide(DialogHideEvent event) {
                confirm = event.getHideButton().name().equalsIgnoreCase("yes");
                if (!confirm)
                    return;
                deleteFiList(modelList);
            }
        });
        box.show();
    }

    public void deleteFiModel(final FiModel fiModel) {
        FiModelService.Util.getInstance().deleteFi(fiModel, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(Boolean result) {
                display.remove(fiModel);
                refreshGrid();
            }
        });
    }

    public void deleteFiList(final List<FiModel> fiModelList) {
        progress = new AutoProgressMessageBox("Progress", "Deleting your data, please wait...");
        progress.setProgressText("Deleting...");
        progress.auto();
        progress.show();
        FiModelService.Util.getInstance().deleteFiList(fiModelList, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("INFO", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    for (FiModel fi : fiModelList) {
                        try {
                            display.remove(fi);
                        } catch (Exception ex) {
                            // ex.printStackTrace();
                        }
                    }
                    display.getGridPanel().setVisible(false);
                    progress.hide();
                } else {
                    new AlertMessageBox("INFO", "server returned " + result).show();
                }
            }
        });
    }

    public static void refreshGrid() {
        loadFis();
    }

    public static void loadFis() {
        FiModelService.Util.getInstance().getModelList(new AsyncCallback<List<FiModel>>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(List<FiModel> result) {
                display.setDataToGrid(result);
            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    public void addTab(String name, Widget widget) {

        ((TabPanel) MainPresenter.display.getTabPanel()).add(widget, name);
        TabItemConfig config = ((TabPanel) MainPresenter.display.getTabPanel()).getConfig(widget);
        config.setClosable(true);
        ((TabPanel) MainPresenter.display.getTabPanel()).update(widget, config);
        ((TabPanel) MainPresenter.display.getTabPanel()).setActiveWidget(widget);
        ((TabPanel) MainPresenter.display.getTabPanel()).addBeforeCloseHandler(new BeforeCloseEvent.BeforeCloseHandler<Widget>() {
            @Override
            public void onBeforeClose(BeforeCloseEvent<Widget> event) {
                FiTabView.level = 0;
            }
        });
    }
}
