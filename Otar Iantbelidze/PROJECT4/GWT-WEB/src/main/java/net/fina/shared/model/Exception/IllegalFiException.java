package net.fina.shared.model.Exception;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by oto on 12/15/2014.
 */
public class IllegalFiException extends IOException implements Serializable {
    public IllegalFiException() {
    }

    public IllegalFiException(String message) {
        super(message);
    }
}
