package net.fina.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import net.fina.shared.model.FiTypeModel;

import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
@RemoteServiceRelativePath("fiTypeService")
public interface FiTypeService extends RemoteService {


    public static class Util {
        private static FiTypeServiceAsync instance;

        public static FiTypeServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(FiTypeService.class);
            }
            return instance;
        }

    }

    List<FiTypeModel> getFiTypeList();

    public void addFiType(FiTypeModel type)throws Exception;

    public void deleteFiTypeModel(FiTypeModel model);

    public boolean deleteFiTypeList(List<FiTypeModel> list);

}
