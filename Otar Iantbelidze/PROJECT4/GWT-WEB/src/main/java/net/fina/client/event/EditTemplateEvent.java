package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;
import net.fina.shared.model.TemplateModel;

/**
 * Created by oto on 12/5/2014.
 */
public class EditTemplateEvent extends GwtEvent<EditTemplateEventHandler> {
    public static Type<EditTemplateEventHandler> TYPE = new Type<EditTemplateEventHandler>();
    private TemplateModel templateModel;

    public EditTemplateEvent() {
    }

    public EditTemplateEvent(TemplateModel templateModel) {
        this.templateModel = templateModel;
    }

    public TemplateModel getTemplateModel() {
        return templateModel;
    }

    @Override
    public Type<EditTemplateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditTemplateEventHandler handler) {
        handler.onEditEvent(this);
    }
}
