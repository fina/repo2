package net.fina.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import net.fina.client.service.FiTypeService;
import net.fina.session.api.FiTypeLocal;
import net.fina.session.entity.FiType;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.FiTypeModel;

import javax.ejb.EJB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
public class FiTypeServiceImpl extends RemoteServiceServlet implements FiTypeService {
    @EJB
    private FiTypeLocal fiTypeLocal;

    private static ModelConverter<FiTypeModel, FiType> converter = new ModelConverter<FiTypeModel, FiType>();


    public List<FiTypeModel> getFiTypeList() {

        try {
            List<FiTypeModel> typeModelList = new ArrayList<FiTypeModel>();
            List<FiType> returnedList = new ArrayList<FiType>(fiTypeLocal.getFiTypeList());
            FiTypeModel model = null;
            for (int i = 0; i < returnedList.size(); i++) {
                model = new FiTypeModel();
                model.setId(returnedList.get(i).getId());
                model.setCode(returnedList.get(i).getCode());
                model.setName(returnedList.get(i).getName());
                typeModelList.add(model);
            }
            return typeModelList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addFiType(FiTypeModel type) throws Exception {
        try {
            FiType fiType = new FiType();
            converter.toEntity(type, fiType);
            fiTypeLocal.saveFiType(fiType);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FieldUniqueException(e.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public void deleteFiTypeModel(FiTypeModel model) {
        try {
            fiTypeLocal.deleteFiTypeByID(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean deleteFiTypeList(List<FiTypeModel> list) {
        boolean deleted = false;
        try {
            List<Integer> idList = new ArrayList<>();
            for (FiTypeModel model : list) {
                idList.add(model.getId());
            }
            deleted = fiTypeLocal.deleteFiTypeList(idList);
            return deleted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


}

