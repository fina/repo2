package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.service.FiModelService;
import net.fina.client.service.FiTypeService;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.FiTypeModel;

import java.util.Date;
import java.util.List;

/**
 * Created by oto on 10/27/2014.
 */
public class EditFiPresenter implements Presenter {
    private Display display;
    private HandlerManager eventBus;
    private FiModel model = new FiModel();

    public EditFiPresenter(Display display, HandlerManager eventBus) {
        this.display = display;
        this.eventBus = eventBus;
        bind();
    }

    public EditFiPresenter(Display display, FiModel fiModel) {
        this.display = display;
        this.model = fiModel;
        display.getFax().setValue("" + model.getFax());
        display.getCode().setValue("" + model.getCode());
        display.getAddress().setValue(model.getAddress());
        display.getDate().setValue(model.getReg_date());
        display.getMail().setValue(model.getMail());
        display.getName().setValue(model.getName());
        display.getPhone().setValue(model.getPhone());
        display.getType().setValue(model.getType());
        bind();
    }


    public interface Display {
        SelectEvent.HasSelectHandlers getCancelButton();

        SelectEvent.HasSelectHandlers getSaveButton();

        HasValue<String> getCode();

        HasValue<String> getName();

        HasValue<String> getAddress();

        HasValue<String> getFax();

        HasValue<String> getPhone();

        HasValue<Date> getDate();

        HasValue<FiTypeModel> getType();

        HasValue<String> getMail();

        void hide();

        void setDataToTypeCombo(List<FiTypeModel> modelList);

        boolean isValid();

        Widget asWidget();
    }


    public void bind() {
        display.getCancelButton().addSelectHandler(new SelectEvent.SelectHandler() {

            public void onSelect(SelectEvent event) {
                display.hide();

            }
        });
        display.getSaveButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                if (!display.isValid()) {
                    new AlertMessageBox("INFO", "Please Fill All Required Fields").show();
                    return;
                }
                model.setCode(display.getCode().getValue().trim());
                model.setName(display.getName().getValue());
                model.setAddress(display.getAddress().getValue());
                model.setPhone(display.getPhone().getValue());
                model.setMail(display.getMail().getValue());
                model.setFax(display.getFax().getValue());
                model.setType(display.getType().getValue());
                //validate Date
                model.setReg_date(display.getDate().getValue());

                final AutoProgressMessageBox box = new AutoProgressMessageBox("Progress", "Saving your data, please wait...");
                box.setProgressText("Saving...");
                box.auto();
                box.show();

                FiModelService.Util.getInstance().saveFi(model, new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        if (caught instanceof FieldUniqueException) {
                            new AlertMessageBox("ERROR", caught.getMessage()).show();
                            box.hide();
                            return;
                        } else {
                            new AlertMessageBox("ERROR", caught.getMessage()).show();
                            box.hide();
                            return;
                        }
                    }

                    public void onSuccess(Void result) {
                        display.hide();
                        FiTabPresenter.loadFis();
                        box.hide();
                    }
                });
            }
        });
        loadCombos();
    }

    public void loadCombos() {
        FiTypeService.Util.getInstance().getFiTypeList(new AsyncCallback<List<FiTypeModel>>() {
            public void onFailure(Throwable caught) {
                new AlertMessageBox("Error", caught.getMessage()).show();
            }

            public void onSuccess(List<FiTypeModel> result) {
                display.setDataToTypeCombo(result);
            }
        });
        /*TemplateService.Util.getInstance().getData(new AsyncCallback<List<TemplateModel>>() {
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            public void onSuccess(List<TemplateModel> result) {
                display.setDataToTemplateCombo(result);
            }
        });*/
    }

    public void go(HasWidgets container) {
        display.asWidget();
    }
}
