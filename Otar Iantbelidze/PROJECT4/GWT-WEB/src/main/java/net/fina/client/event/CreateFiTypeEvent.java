package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 10/28/2014.
 */
public class CreateFiTypeEvent extends GwtEvent<CreateFiTypeEventHandler> {
    public static final Type<CreateFiTypeEventHandler> TYPE = new Type<CreateFiTypeEventHandler>();

    @Override
    public Type<CreateFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateFiTypeEventHandler handler) {
        handler.onCreate(this);
    }
}
