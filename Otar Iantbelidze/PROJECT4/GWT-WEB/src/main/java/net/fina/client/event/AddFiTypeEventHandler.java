package net.fina.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by oto on 10/24/2014.
 */
public interface AddFiTypeEventHandler extends EventHandler{
    public void onFiTypeClickEvent(AddFiTypeEvent event);
}
