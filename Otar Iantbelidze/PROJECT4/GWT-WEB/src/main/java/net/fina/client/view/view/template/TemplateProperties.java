package net.fina.client.view.view.template;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import net.fina.shared.model.ScheduleModel;
import net.fina.shared.model.TemplateModel;

/**
 * Created by oto on 11/14/2014.
 */
public interface TemplateProperties extends PropertyAccess<TemplateModel> {
    @Editor.Path("id")
    ModelKeyProvider<TemplateModel> key();

    ValueProvider<TemplateModel,String> name();
    ValueProvider<TemplateModel,String>code();
    ValueProvider<TemplateModel,String>scheduleModel();
}
