package net.fina.client.view.view.FileUpload;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.*;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;
import net.fina.client.presenter.FileUploadTabPresenter;
import net.fina.client.service.UploadService;
import net.fina.client.view.view.Images.Images;
import net.fina.shared.model.UploadFileModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by oto on 12/7/2014.
 */
public class FileUploadTabView extends Composite implements FileUploadTabPresenter.Display {
    private ContentPanel panel;
    private ButtonBar buttonBar;
    private TextButton uploadButton;
    private TextButton deleteButton;
    private TextButton refreshButton;
    public static int level = 0;
    private static PagingToolBar pagingToolBar;

    private static FileProperties props = GWT.create(FileProperties.class);

    private ListStore<UploadFileModel> store;

    public static Grid<UploadFileModel> grid;

    public FileUploadTabView() {
        createTab();
    }

    public void createTab() {
        if (panel == null) {
            panel = new ContentPanel();
            panel.setHeadingText("File Upload Tab");
            panel.setPixelSize(650, 550);

            BorderLayoutContainer borderLayout = new BorderLayoutContainer();
            panel.setWidget(borderLayout);

            HBoxLayoutContainer barPanel = new HBoxLayoutContainer();
            barPanel.setPadding(new Padding(5));
            barPanel.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.STRETCH);

            buttonBar = new ButtonBar();
            buttonBar.setMinButtonWidth(75);
            buttonBar.getElement().setMargins(10);

            uploadButton = new TextButton("Upload", Images.INSTANCE.addIcon());
            refreshButton = new TextButton("Refresh", Images.INSTANCE.refreshIcon());
            deleteButton = new TextButton("Delete", Images.INSTANCE.removeIcon());

            buttonBar.add(uploadButton);
            buttonBar.add(deleteButton);
            buttonBar.add(refreshButton);

            barPanel.add(buttonBar);
            BorderLayoutContainer.BorderLayoutData north = new BorderLayoutContainer.BorderLayoutData(40);
            north.setMargins(new Margins(5));


            borderLayout.setNorthWidget(barPanel, north);

            ContentPanel centerPanel = new ContentPanel();
            centerPanel.setHeaderVisible(false);

            MarginData center = new MarginData(new Margins(5));
            borderLayout.setCenterWidget(centerPanel, center);
            BoxLayoutContainer.BoxLayoutData hBoxData = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 5));
            hBoxData.setFlex(1);

            centerPanel.add(gridWidget());

            VerticalLayoutContainer vlc = new VerticalLayoutContainer();
            // vlc.add(barPanel,new VerticalLayoutContainer.VerticalLayoutData(1,1));
            // vlc.add(gridWidget(),new VerticalLayoutContainer.VerticalLayoutData(1,-1));

            //panel.add(vlc);
            initWidget(panel);
        }
    }

    public Widget gridWidget() {
        ContentPanel panel = new ContentPanel();
        RpcProxy<PagingLoadConfig, PagingLoadResult<UploadFileModel>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<UploadFileModel>>() {

            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<UploadFileModel>> callback) {
                UploadService.Util.getInstance().getPagingData(loadConfig, callback);
            }
        };

        store = new ListStore<>(props.key());

        final PagingLoader<PagingLoadConfig, PagingLoadResult<UploadFileModel>> loadeer = new PagingLoader<PagingLoadConfig, PagingLoadResult<UploadFileModel>>(proxy);
        loadeer.setRemoteSort(true);
        loadeer.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, UploadFileModel, PagingLoadResult<UploadFileModel>>(store));

        pagingToolBar = new PagingToolBar(50);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(loadeer);
        pagingToolBar.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
        pagingToolBar.setPageSize(20);

        IdentityValueProvider<UploadFileModel> identity = new IdentityValueProvider<>();
        CheckBoxSelectionModel<UploadFileModel> selectionModel = new CheckBoxSelectionModel<>(identity);

        FileNameCell nameCell = new FileNameCell();
        ColumnConfig<UploadFileModel, String> fileName = new ColumnConfig<UploadFileModel, String>(props.fileName(), 200, "File Name");
        fileName.setCell(nameCell);
        ColumnConfig<UploadFileModel, Date> uploadDate = new ColumnConfig<UploadFileModel, Date>(props.uploadDate(), 200, "Upload Date");
        uploadDate.setCell(new DateCell(com.google.gwt.i18n.shared.DateTimeFormat.getFormat("EEE, MMM d, yyyy hh:mm")));

        List<ColumnConfig<UploadFileModel, ?>> columnConfigs = new ArrayList<>();
        columnConfigs.add(selectionModel.getColumn());
        columnConfigs.add(fileName);
        columnConfigs.add(uploadDate);

        ColumnModel<UploadFileModel> columnModel = new ColumnModel<>(columnConfigs);
        grid = new Grid<UploadFileModel>(store, columnModel) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        loadeer.load();
                    }
                });
            }
        };

        grid.getView().setForceFit(true);
        grid.setSelectionModel(selectionModel);
        grid.getView().setAutoExpandColumn(fileName);
        grid.setBorders(true);
        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.setColumnReordering(true);
        grid.setLoadMask(true);

        VerticalLayoutContainer vlc = new VerticalLayoutContainer();
        vlc.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        vlc.add(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        panel.add(vlc);
        return panel;
    }

    @Override
    public void refreshToolbarData() {
        pagingToolBar.refresh();
    }

    @Override
    public SelectEvent.HasSelectHandlers getUploadButton() {
        return uploadButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getDeleteButton() {
        return deleteButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getRefreshButton() {
        return refreshButton;
    }

    @Override
    public List<UploadFileModel> getSelectedRows() {
        return grid.getSelectionModel().getSelectedItems();
    }

    @Override
    public void remove(UploadFileModel fileModel) {
        grid.getStore().remove(fileModel);
    }

    public Widget asWidget() {
        return this;
    }
}
