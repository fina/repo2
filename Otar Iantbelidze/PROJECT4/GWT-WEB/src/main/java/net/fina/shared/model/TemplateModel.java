package net.fina.shared.model;

import java.io.Serializable;

/**
 * Created by oto on 11/14/2014.
 */
public class TemplateModel implements Serializable {
    private int id;
    private String name;
    private String code;
    private ScheduleModel scheduleModel;

    public TemplateModel() {
    }

    public TemplateModel(int id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getScheduleModel() {
        return scheduleModel.toString();
    }

    public ScheduleModel getSchedule(){
        return scheduleModel;
    }

    public void setScheduleModel(String scheduleModel) {
        this.scheduleModel = ScheduleModel.valueOf(scheduleModel);
    }
}
