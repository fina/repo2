package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 10/28/2014.
 */
public class AddFiTabEvent extends GwtEvent<AddFiTabEventHandler> {
    public static Type<AddFiTabEventHandler> TYPE = new Type<AddFiTabEventHandler>();

    @Override
    public Type<AddFiTabEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddFiTabEventHandler handler) {
        handler.onAddTab(this);
    }
}
