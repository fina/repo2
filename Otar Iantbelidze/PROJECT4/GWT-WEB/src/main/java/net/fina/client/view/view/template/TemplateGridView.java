package net.fina.client.view.view.template;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;
import net.fina.client.service.TemplateService;
import net.fina.shared.model.TemplateModel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public class TemplateGridView extends Grid implements IsWidget {
    private ContentPanel panel;


    private static TemplateProperties props = GWT.create(TemplateProperties.class);

    private static Grid<TemplateModel> grid;

    public Widget asWidget() {
        if (panel == null) {
            RpcProxy<PagingLoadConfig, PagingLoadResult<TemplateModel>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<TemplateModel>>() {

                @Override
                public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<TemplateModel>> callback) {
                    TemplateService.Util.getInstance().getPagingData(loadConfig, callback);
                }
            };
            ListStore<TemplateModel> store = new ListStore<TemplateModel>(props.key());


            final PagingLoader<PagingLoadConfig, PagingLoadResult<TemplateModel>> loadeer = new PagingLoader<PagingLoadConfig, PagingLoadResult<TemplateModel>>(proxy);
            loadeer.setRemoteSort(true);
            loadeer.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, TemplateModel, PagingLoadResult<TemplateModel>>(store));
            final PagingToolBar toolBar = new PagingToolBar(50);
            toolBar.getElement().getStyle().setProperty("borderBottom", "none");
            toolBar.bind(loadeer);
            toolBar.setPack(BoxLayoutContainer.BoxLayoutPack.CENTER);
            toolBar.setPageSize(20);

            IdentityValueProvider<TemplateModel> identity = new IdentityValueProvider<TemplateModel>();
            CheckBoxSelectionModel<TemplateModel> sm = new CheckBoxSelectionModel<TemplateModel>(identity);

            ColumnConfig<TemplateModel, String> nameCol = new ColumnConfig<TemplateModel, String>(props.name(), 100, "Name");
            ColumnConfig<TemplateModel, String> codeCol = new ColumnConfig<TemplateModel, String>(props.code(), 100, "Code");
            ColumnConfig<TemplateModel, String> scheduleCol = new ColumnConfig<TemplateModel, String>(props.scheduleModel(), 100, "Schedule");

            List<ColumnConfig<TemplateModel, ?>> columnConfigs = new ArrayList<ColumnConfig<TemplateModel, ?>>();
            columnConfigs.add(sm.getColumn());
            columnConfigs.add(nameCol);
            columnConfigs.add(codeCol);
            columnConfigs.add(scheduleCol);

            ColumnModel<TemplateModel> cModel = new ColumnModel<TemplateModel>(columnConfigs);

            panel = new ContentPanel();
            panel.setHeadingText("CheckBox Grid");
            panel.setPixelSize(600, 320);

            //grid
            grid = new Grid<TemplateModel>(store, cModel) {
                @Override
                protected void onAfterFirstAttach() {
                    super.onAfterFirstAttach();
                    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                        @Override
                        public void execute() {
                            loadeer.load();
                        }
                    });
                }
            };
            grid.getView().setForceFit(true);
            grid.setSelectionModel(sm);
            grid.getView().setAutoExpandColumn(nameCol);
            grid.setBorders(true);
            grid.getView().setStripeRows(true);
            grid.getView().setColumnLines(true);
            grid.setColumnReordering(true);
            grid.setLoadMask(true);

            VerticalLayoutContainer con = new VerticalLayoutContainer();

            con.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
            con.add(toolBar,new VerticalLayoutContainer.VerticalLayoutData(1,-1));
            panel.add(con, new VerticalLayoutContainer.VerticalLayoutData(1, 1));

        }
        return panel;
    }

    public List<TemplateModel> getSelectedRows() {
        return grid.getSelectionModel().getSelectedItems();
    }

    public TemplateModel getSelectedRow(){
        return grid.getSelectionModel().getSelectedItem();
    }

    public static Grid<TemplateModel> getGrid() {
        return grid;
    }

    public void setDataToGrid(List<TemplateModel> modelList) {
        if (grid != null) {
            grid.getStore().clear();
            grid.getStore().addAll(modelList);
        }


    }
}
