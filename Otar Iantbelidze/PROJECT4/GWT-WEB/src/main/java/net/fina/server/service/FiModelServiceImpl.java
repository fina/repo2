package net.fina.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import net.fina.client.service.FiModelService;
import net.fina.session.api.FiLocalService;
import net.fina.session.entity.Fi;
import net.fina.session.entity.FiType;
import net.fina.session.entity.Template;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.FiTypeModel;
import net.fina.shared.model.TemplateModel;

import javax.ejb.EJB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public class FiModelServiceImpl extends RemoteServiceServlet implements FiModelService {

    ModelConverter<FiModel, Fi> fiHelper = new ModelConverter<FiModel, Fi>();
    ModelConverter<FiTypeModel, FiType> typeHelper = new ModelConverter<FiTypeModel, FiType>();
    ModelConverter<TemplateModel, Template> templateHelper = new ModelConverter<TemplateModel, Template>();

    @EJB
    private FiLocalService service;

    public List<FiModel> getModelList() {
        List<Fi> fiList = service.getFiList();
        FiModel fiModel = null;
        FiTypeModel fiTypeModel = null;
        TemplateModel templateModel = null;
        List<TemplateModel> templateModelList = new ArrayList<>();
        List<FiModel> modelList = new ArrayList<>();
        for (int i = 0; i < fiList.size(); i++) {
            fiModel = new FiModel();
            fiHelper.toModel(fiModel, fiList.get(i));
            fiTypeModel = new FiTypeModel();
            typeHelper.toModel(fiTypeModel, fiList.get(i).getFiType());
            fiModel.setType(fiTypeModel);
            if (fiList.get(i).getTemplateList() != null) {
                for (Template t : fiList.get(i).getTemplateList()) {
                    templateModel = new TemplateModel();
                    templateHelper.toModel(templateModel, t);
                    templateModel.setScheduleModel(t.getSchedule());
                    templateModelList.add(templateModel);
                }
                fiModel.setTemplateModelList(templateModelList);
                templateModelList = new ArrayList<>();
            }
            modelList.add(fiModel);
        }
        return modelList;
    }

    public void saveFi(FiModel fiModel) throws Exception {
        try {
            Fi fi = new Fi();
            FiType fiType = new FiType();
            FiTypeModel typeModel = fiModel.getType();

            typeHelper.toEntity(typeModel, fiType);
            fiHelper.toEntity(fiModel, fi);
            fi.setDate(fiModel.getReg_date());
            fi.setFiType(fiType);
            service.saveFi(fi);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            throw new FieldUniqueException(sqlEx.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    public boolean deleteFi(FiModel fiModel) {
        try {
            service.deleteFi(fiModel.getId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteFiList(List<FiModel> modelList) {
        boolean deleted = false;
        try {
            List<Integer> idList = new ArrayList<>();
            for (FiModel fi : modelList) {
                idList.add(fi.getId());
            }
            deleted = service.deleteFiList(idList);
            return deleted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean setTemplatesToFi(FiModel fiModel) {
        try {
            Fi fi = new Fi();

            FiType fiType = new FiType();
            FiTypeModel typeModel = fiModel.getType();

            typeHelper.toEntity(typeModel, fiType);
            fiHelper.toEntity(fiModel, fi);

            fi.setFiType(fiType);

            List<TemplateModel> modelList = fiModel.getTemplateModelList();
            List<Template> templates = new ArrayList<>();
            Template template = null;
            for (int i = 0; i < modelList.size(); i++) {
                template = new Template();
                templateHelper.toEntity(modelList.get(i), template);
                template.setSchedule(modelList.get(i).getScheduleModel());
                templates.add(template);
            }
            fi.setTemplateList(templates);
            service.setTemplateListToFi(fi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
