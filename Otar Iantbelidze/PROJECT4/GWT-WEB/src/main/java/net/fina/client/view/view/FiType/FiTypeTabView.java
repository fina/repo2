package net.fina.client.view.view.FiType;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.presenter.FiTypeTabPresenter;
import net.fina.client.view.view.Images.Images;
import net.fina.shared.model.FiTypeModel;

import java.util.List;

/**
 * Created by oto on 10/24/2014.
 */
public class FiTypeTabView extends Composite implements FiTypeTabPresenter.Display, IsWidget {
    private ContentPanel centerPanel;
    private ContentPanel rightPanel;
    private ContentPanel panel;
    private ButtonBar buttonBar;
    private TextButton createButton;
    private TextButton editButton;
    private TextButton deleteButton;
    private TextButton refreshButton;
    public static int level = 0;

    private FiTypeGridView grid;

    public FiTypeTabView() {
        if (panel == null) {
            panel = new ContentPanel();
            panel.setLayoutData(new MarginData(10));

            panel.setHeadingText("Create Fi Type");
            panel.setPixelSize(650, 550);

            BorderLayoutContainer borderLayout = new BorderLayoutContainer();
            panel.setWidget(borderLayout);

            HBoxLayoutContainer barPanel = new HBoxLayoutContainer();
            barPanel.setPadding(new Padding(5));
            barPanel.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.STRETCH);

            /* buttonBar for CRUD operations */
            buttonBar = new ButtonBar();
            buttonBar.setMinButtonWidth(75);
            buttonBar.getElement().setMargins(10);

            createButton = new TextButton("Create", Images.INSTANCE.createIcon());
            editButton = new TextButton("Edit", Images.INSTANCE.editIcon());
            deleteButton = new TextButton("Delete", Images.INSTANCE.deleteIcon());
            refreshButton = new TextButton("Refresh", Images.INSTANCE.refreshIcon());

            buttonBar.add(createButton);
            buttonBar.add(editButton);
            buttonBar.add(deleteButton);
            buttonBar.add(refreshButton);

            barPanel.add(buttonBar);

            BorderLayoutContainer.BorderLayoutData north = new BorderLayoutContainer.BorderLayoutData(40);
            north.setMargins(new Margins(5));
            // west.setSplit(true);

            borderLayout.setNorthWidget(barPanel, north);

            centerPanel = new ContentPanel();
            centerPanel.setHeaderVisible(false);
            MarginData center = new MarginData(new Margins(5));
            borderLayout.setCenterWidget(centerPanel, center);
            BoxLayoutContainer.BoxLayoutData hBoxData = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 5));
            hBoxData.setFlex(1);
            grid = new FiTypeGridView();
            centerPanel.add(grid.asWidget());

            initWidget(panel);
        }
    }


    public SelectEvent.HasSelectHandlers getCreateButton() {
        return createButton;
    }


    public SelectEvent.HasSelectHandlers getDeleteButton() {
        return deleteButton;
    }

    public SelectEvent.HasSelectHandlers getRefreshButton() {
        return refreshButton;
    }

    public SelectEvent.HasSelectHandlers getEditButton() {
        return editButton;
    }

    public List<FiTypeModel> getSelectedRows() {
        List<FiTypeModel> modelList;
        modelList = FiTypeGridView.gerSelectedRows();
        return modelList;
    }

    public FiTypeModel getSelectedRow() {
        return FiTypeGridView.getSelectedRow();
    }

    public void setDataToGrid(List<FiTypeModel> modelList) {
        grid.setData(modelList);
    }

    public Widget asWidget() {
        return this;
    }


}
