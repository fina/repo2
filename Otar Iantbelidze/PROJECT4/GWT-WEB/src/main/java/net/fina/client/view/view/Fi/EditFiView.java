package net.fina.client.view.view.Fi;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.*;
import net.fina.client.presenter.EditFiPresenter;
import net.fina.client.view.view.FiType.FiTypeProperties;
import net.fina.shared.model.FiTypeModel;

import java.util.Date;
import java.util.List;


/**
 * Created by oto on 10/27/2014.
 */
public class EditFiView extends Composite implements EditFiPresenter.Display, IsWidget {
    private VerticalPanel vp;
    private Window window;
    private TextButton saveButton;
    private TextButton cancelButton;

    private TextField codeField;
    private TextField nameField;
    private TextField addressField;
    private TextField phoneField;
    private TextField faxField;
    private TextField mailField;

    private ComboBox<FiTypeModel> typeComboBox;
    private DateField dateField;

    private ListStore<FiTypeModel> typeStore;

    public static FiTypeProperties properties = GWT.create(FiTypeProperties.class);
    private boolean valid;

    public EditFiView() {
        createWindow();
    }


    public void createWindow() {
        if (vp == null) {
            vp = new VerticalPanel();
            vp.setSpacing(10);
            FramedPanel form = new FramedPanel();
            form.setHeadingText("Create Financial Institute");
            form.setBodyStyle("background: none; padding: 10px");
            form.setWidth(350);

            FieldSet fieldSet = new FieldSet();
            fieldSet.setHeadingText("FI Information");
            fieldSet.setCollapsible(true);
            form.add(fieldSet);

            VerticalLayoutContainer p = new VerticalLayoutContainer();
            fieldSet.add(p);

            typeStore = new ListStore<FiTypeModel>(properties.key());


            typeComboBox = new ComboBox<FiTypeModel>(typeStore, new LabelProvider<FiTypeModel>() {

                public String getLabel(FiTypeModel item) {
                    return item.getName();
                }
            });
            typeComboBox.setEmptyText("Choose Fi Type");
            typeComboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
            typeComboBox.setAutoValidate(true);
            typeComboBox.setAllowBlank(false);
            p.add(new FieldLabel(typeComboBox, "Choose Type"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));


           /* templateStore = new ListStore<TemplateModel>(templateProp.key());
            templateCombo = new ComboBox<TemplateModel>(templateStore, new LabelProvider<TemplateModel>() {
                public String getLabel(TemplateModel item) {
                    return item.getName() + item.getCode();
                }
            });
            templateCombo.setEmptyText("select Template");
            templateCombo.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
            templateCombo.setAllowBlank(false);
            templateCombo.setAutoValidate(true);
            p.add(new FieldLabel(templateCombo,"Select Template"),new VerticalLayoutContainer.VerticalLayoutData(1,-1));*/

            codeField = new TextField();
            codeField.setAllowBlank(false);
            p.add(new FieldLabel(codeField, "Code"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            nameField = new TextField();
            nameField.setAllowBlank(false);
            p.add(new FieldLabel(nameField, "FI Name"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            addressField = new TextField();
            addressField.setAllowBlank(false);
            p.add(new FieldLabel(addressField, "Address"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            phoneField = new TextField();
            phoneField.setAllowBlank(false);
            p.add(new FieldLabel(phoneField, "Phone"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            faxField = new TextField();
            faxField.setAllowBlank(true);
            p.add(new FieldLabel(faxField, "FAX"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));


            mailField = new TextField();
            mailField.setAllowBlank(true);
            p.add(new FieldLabel(mailField, "E-Mail"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            dateField = new DateField();
            dateField.setMaxValue(new Date());
            dateField.setAllowBlank(false);
            dateField.setAutoValidate(true);
            p.add(new FieldLabel(dateField, "Date"));

            saveButton = new TextButton("Save");
            cancelButton = new TextButton("Cancel");

            form.addButton(saveButton);
            form.addButton(cancelButton);

            this.window = new Window();
            vp.add(form);
            window.add(form);
            window.setModal(true);

        }
    }


    public SelectEvent.HasSelectHandlers getCancelButton() {
        return cancelButton;
    }

    public SelectEvent.HasSelectHandlers getSaveButton() {
        return saveButton;
    }

    public HasValue<String> getCode() {
        return codeField;
    }

    public HasValue<String> getName() {
        return nameField;
    }

    public HasValue<String> getAddress() {
        return addressField;
    }

    public HasValue<String> getFax() {
        return faxField;
    }

    public HasValue<String> getPhone() {
        return phoneField;
    }

    public HasValue<Date> getDate() {
        return dateField;
    }

    public HasValue<FiTypeModel> getType() {
        return typeComboBox;
    }

    public HasValue<String> getMail() {
        return mailField;
    }

    @Override
    public void hide() {
        window.hide();
    }


    public HasWidgets getWindow() {
        return window;
    }

    public void setDataToTypeCombo(List<FiTypeModel> modelList) {
        typeStore.addAll(modelList);
    }

    @Override
    public boolean isValid() {
        valid = true;
        valid &= typeComboBox.isValid();
        valid &= nameField.isValid();
        valid &= codeField.isValid();
        valid &= dateField.isValid();
        return valid;
    }

   /* public void setDataToTemplateCombo(List<TemplateModel> modelList) {
        templateStore.addAll(modelList);
    }*/


    @Override
    public Widget asWidget() {
        this.window.show();
        return this;
    }

}
