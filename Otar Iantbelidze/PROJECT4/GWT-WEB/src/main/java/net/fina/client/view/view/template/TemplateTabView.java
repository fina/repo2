package net.fina.client.view.view.template;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.IsWidget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.*;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.Grid;
import net.fina.client.presenter.TemplateTabPresenter;
import net.fina.client.view.view.FiType.FiTypeGridView;
import net.fina.client.view.view.Images.Images;
import net.fina.shared.model.TemplateModel;

import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public class TemplateTabView  extends Composite implements TemplateTabPresenter.Display, IsWidget {
    private ContentPanel lccenter;
    private ContentPanel panel;
    private ButtonBar buttonBar;
    private TextButton createButton;
    private TextButton editButton;
    private TextButton deleteButton;
    private TextButton refreshButton;
    public static int level = 0;

    private static TemplateGridView grid;

    public TemplateTabView() {
        if (panel == null) {
            panel = new ContentPanel();
            panel.setLayoutData(new MarginData(10));

            ContentPanel panel = new ContentPanel();
            panel.setHeadingText("Create Template    ");
            panel.setPixelSize(600, 550);

            BorderLayoutContainer border = new BorderLayoutContainer();
            panel.setWidget(border);

            HBoxLayoutContainer lcwest = new HBoxLayoutContainer();
            lcwest.setPadding(new Padding(5));
            lcwest.setHBoxLayoutAlign(HBoxLayoutContainer.HBoxLayoutAlign.STRETCH);

            buttonBar = new ButtonBar();
            buttonBar.setMinButtonWidth(75);
            buttonBar.getElement().setMargins(10);

            createButton = new TextButton("Create",Images.INSTANCE.createIcon());
            editButton = new TextButton("Edit",Images.INSTANCE.editIcon());
            deleteButton = new TextButton("Delete",Images.INSTANCE.deleteIcon());
            refreshButton = new TextButton("Refresh", Images.INSTANCE.refreshIcon());

            buttonBar.add(createButton);
            buttonBar.add(editButton);
            buttonBar.add(deleteButton);
            buttonBar.add(refreshButton);

            lcwest.add(buttonBar);

            BorderLayoutContainer.BorderLayoutData west = new BorderLayoutContainer.BorderLayoutData(40);
            west.setMargins(new Margins(5));
            // west.setSplit(true);

            border.setNorthWidget(lcwest, west);

            lccenter = new ContentPanel();
            lccenter.setHeaderVisible(false);
//            lccenter.add(new HTML(
//                    "<p style=\"padding:10px;color:#556677;font-size:11px;\">Select a configuration on the left</p>"));

            MarginData center = new MarginData(new Margins(5));

            border.setCenterWidget(lccenter, center);

            BoxLayoutContainer.BoxLayoutData hBoxData = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 5));
            hBoxData.setFlex(1);

            grid = new TemplateGridView();

            VerticalLayoutContainer container = new VerticalLayoutContainer();
            this.panel.setWidget(container);


            container.add(panel, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            lccenter.add(grid.asWidget());


            initWidget(panel);
        }

    }

    public SelectEvent.HasSelectHandlers getCreateButton() {
        return createButton;
    }

    public SelectEvent.HasSelectHandlers getRefreshButton() {
        return refreshButton;
    }

    public SelectEvent.HasSelectHandlers getEditButton() {
        return editButton;
    }

    public SelectEvent.HasSelectHandlers getDeleteButton() {
        return deleteButton;
    }

    @Override
    public TemplateModel getSelectedRow() {
        return grid.getSelectedRow();
    }


    public List<TemplateModel> getSelectedRows() {
        return grid.getSelectedRows();
    }


    public void setDataToGrid(List<TemplateModel> data) {
        grid.setDataToGrid(data);
    }
}
