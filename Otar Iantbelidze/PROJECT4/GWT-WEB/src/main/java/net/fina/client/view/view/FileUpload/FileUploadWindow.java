package net.fina.client.view.view.FileUpload;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.CenterLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.grid.CellSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import net.fina.client.presenter.FileUploadWindowPresenter;
import net.fina.client.service.UploadService;
import net.fina.client.view.view.Fi.FiProperties;
import net.fina.client.view.view.Images.Images;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.UploadFileModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by oto on 12/7/2014.
 */
public class FileUploadWindow extends Window implements FileUploadWindowPresenter.Display {
    private FramedPanel panel;
    private FormPanel formPanel;
    private TextButton removeButton;
    private TextButton uploadButton;
    private ContentPanel contentPanel;
    private TextButton nextButton;
    private TextButton backbutton;
    private FileUploadField uploadField;

    private ComboBox<FiModel> fiCombo;
    private ListStore<FiModel> fiStore;

    private ListStore<UploadFileModel> store;
    private static FileProperties props = GWT.create(FileProperties.class);
    private static FiProperties fiProps = GWT.create(FiProperties.class);
    private Grid<UploadFileModel> grid;

    public FileUploadWindow() {
        if (panel == null) {
            panel = new FramedPanel();
            panel.setHeaderVisible(false);

            this.removeButton = new TextButton("Remove", Images.INSTANCE.removeIcon());
            this.uploadButton = new TextButton("Start Upload", Images.INSTANCE.uploadIcon());
            this.nextButton = new TextButton("next", Images.INSTANCE.nextIcon());
            this.backbutton = new TextButton("Back", Images.INSTANCE.backIcon());

            this.uploadButton.setEnabled(false);

            super.setWidth(400);
            super.setHeight(300);
            super.getButtonBar().add(backbutton);
            super.getButtonBar().add(removeButton);
            super.getButtonBar().add(uploadButton);
            super.getButtonBar().add(nextButton);

            initForm();
            panel.add(createSelectionWizard());
            add(panel);
            show();
        }
    }

    @Override
    public void backAction() {
        clear();
        panel.add(contentPanel);
        backbutton.setEnabled(false);
        nextButton.setEnabled(true);
        uploadButton.setEnabled(false);
        add((panel));
    }

    @Override
    public void enableUploadButton(boolean enable) {
        uploadButton.setEnabled(enable);
    }

    @Override
    public void nextAction() {
        clear();
        panel.add(formPanel);
        backbutton.setEnabled(true);
        nextButton.setEnabled(false);
        uploadButton.setEnabled(true);
        add((panel));
    }

    public Widget initForm() {
        formPanel = new FormPanel();
        //TODO
        formPanel.setAction(GWT.getModuleBaseURL() + "FileUploadServlet");
        formPanel.setEncoding(FormPanel.Encoding.MULTIPART);
        formPanel.setMethod(FormPanel.Method.POST);

        uploadField = new FileUploadField();
        uploadField.getElement().setPropertyString("accept","xml");


        store = new ListStore<>(props.key());

        CellSelectionModel<UploadFileModel> selectionModel = new CellSelectionModel<>();

        ColumnConfig<UploadFileModel, String> nameCol = new ColumnConfig<UploadFileModel, String>(props.fileName(), 100, "Name");
        ColumnConfig<UploadFileModel, Date> date = new ColumnConfig<UploadFileModel, Date>(props.uploadDate(), 100, "Upload Time");
        date.setCell(new DateCell(DateTimeFormat.getFormat("EEE, MMM d, yyyy hh:mm")));

        List<ColumnConfig<UploadFileModel, ?>> columnConfigs = new ArrayList<>();
        columnConfigs.add(nameCol);
        columnConfigs.add(date);

        ColumnModel<UploadFileModel> columnModel = new ColumnModel<>(columnConfigs);

        grid = new Grid<>(store, columnModel);
        grid.setSelectionModel(selectionModel);
        grid.getView().setAutoExpandColumn(nameCol);

        VerticalLayoutContainer vlc1 = new VerticalLayoutContainer();

        formPanel.add(vlc1);
        vlc1.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        vlc1.add(uploadField, new VerticalLayoutContainer.VerticalLayoutData(1, -1));


        return formPanel;

    }

    public boolean isFormValid() {
        return formPanel.isValid() && uploadField.isValid();
    }

    public boolean isComboValid() {
        boolean valid = true;
        valid &= fiCombo.isValid();
        return valid;
    }

    public Widget createSelectionWizard() {
        if (contentPanel == null) {
            contentPanel = new ContentPanel();
            //contentPanel.setHeaderVisible(false);
            contentPanel.setHeadingText("Select Fi ");
            CenterLayoutContainer centerLayoutContainer = new CenterLayoutContainer();
            VerticalLayoutContainer vlc = new VerticalLayoutContainer();


            backbutton.setEnabled(false);

            fiStore = new ListStore<>(fiProps.key());

            fiCombo = new ComboBox<FiModel>(fiStore, new LabelProvider<FiModel>() {
                @Override
                public String getLabel(FiModel item) {
                    return item.getTypeString() + " " + item.getCode();
                }
            });
            fiCombo.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
            fiCombo.setAllowBlank(false);
            fiCombo.setAutoValidate(true);
            fiCombo.setEmptyText("Select BANK");


            vlc.add(new FieldLabel(fiCombo, "Choose Fi"), new VerticalLayoutContainer.VerticalLayoutData(1, 1));

            centerLayoutContainer.add(vlc);
            contentPanel.setWidget(centerLayoutContainer);
        }
        return contentPanel;
    }


    @Override
    public void hideWindow() {
        hide();
    }

    @Override
    public SelectEvent.HasSelectHandlers getStartUploadButton() {
        return uploadButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getRemoveButton() {
        return removeButton;
    }

    @Override
    public HasChangeHandlers getUploadField() {
        return uploadField;
    }

    @Override
    public SelectEvent.HasSelectHandlers getNextButton() {
        return nextButton;
    }

    @Override
    public SelectEvent.HasSelectHandlers getBackButton() {
        return backbutton;
    }

    @Override
    public HasValue<FiModel> getFiModel() {
        return fiCombo;
    }


    @Override
    public String getFileName() {
        return uploadField.getValue();
    }

    @Override
    public void addModelToGird(UploadFileModel model) {
        grid.getStore().clear();
        grid.getStore().add(model);
    }

    @Override
    public void removeModel() {
        grid.getStore().clear();
        uploadField.clear();
    }

    @Override
    public void submitForm() {
        formPanel.submit();
    }

    @Override
    public void addSubmitHandler() {
        formPanel.addSubmitCompleteHandler(new SubmitCompleteEvent.SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                String result = event.getResults();
                MessageBox box = new MessageBox("File Upload Example", result);
                box.setIcon(MessageBox.ICONS.info());
                box.show();
                hideWindow();
            }
        });
    }


    public void addDataToUploadGrid() {
        UploadService.Util.getInstance().getData(new AsyncCallback<List<UploadFileModel>>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(List<UploadFileModel> result) {
                grid.getStore().clear();
                grid.getStore().addAll(result);
            }
        });
    }

    @Override
    public void initFiCombo(List<FiModel> modelList) {
        fiStore.addAll(modelList);
    }

    @Override
    public SubmitCompleteEvent.HasSubmitCompleteHandlers onSubmitEvent() {
        return formPanel;
    }

    @Override
    public void addDataToUploadGrid(List<UploadFileModel> list) {
        grid.getStore().clear();
        grid.getStore().addAll(list);
    }
}
