package net.fina.server.service;

import net.fina.session.api.FiLocalService;
import net.fina.session.api.TemplateServiceLocal;
import net.fina.session.api.UploadFileServiceLocal;
import net.fina.session.entity.Fi;
import net.fina.session.entity.Schedule;
import net.fina.session.entity.Template;
import net.fina.session.entity.UploadedFile;
import net.fina.shared.model.Exception.IllegalFiException;
import net.fina.shared.model.Exception.ScheduleException;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.TemplateModel;
import net.fina.shared.model.UploadFileModel;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by oto on 12/8/2014.
 */
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/FileUploadServlet"})
public class FileUploadServlett extends HttpServlet {

    private static Logger log = Logger.getLogger(FileUploadServlett.class.getName());
    private static ModelConverter<UploadFileModel, UploadedFile> converter = new ModelConverter<>();
    private static ModelConverter<FiModel, Fi> fiConverter = new ModelConverter<>();
    private static ModelConverter<TemplateModel, Template> tempConverter = new ModelConverter<>();

    @EJB
    private UploadFileServiceLocal fileServiceLocal;

    @EJB
    private FiLocalService fiService;

    @EJB
    private TemplateServiceLocal templateService;

    public FileUploadServlett() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        log.info("============== (DoPost) starting file Upload=====================");

        ServletFileUpload upload = new ServletFileUpload();

        try {
            FileItemIterator iter = upload.getItemIterator(request);

            while (iter.hasNext()) {
                FileItemStream item = iter.next();
                String fileName = item.getName();
                log.info("====== fileName isn : " + fileName);
                log.info("====== contentTYpe is : " + item.getContentType());
                InputStream stream = item.openStream();

                UploadFileModel uploadFileModel = new UploadFileModel();
                UploadedFile uf = new UploadedFile();

                byte[] content = IOUtils.toByteArray(stream);
                uploadFileModel.setFileName(fileName);
                uploadFileModel.setUploadDate(new Date());
                uploadFileModel.setUploadedFile(content);


                converter.toEntity(uploadFileModel, uf);

                log.info("============ content =" + content.length);

                DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(new ByteArrayInputStream(content));

                doc.getDocumentElement().normalize();

                Element root = doc.getDocumentElement();
                NodeList nodeList = root.getChildNodes();

                log.info("=============== Root =" + root.getNodeName());

                String fiCode = fileName.split("\\.")[1];
                String templateCode = fileName.split("\\.")[2];
                String fileDate = fileName.split("\\.")[3];
                String contentCode = "";
                String scheduleDate = "";


                for (int i = 0; i < nodeList.getLength(); i++) {
                    //Header Node
                    Node node = nodeList.item(i);
                    if (node instanceof Element) {
                        Element e = (Element) node;
                        log.info("======= node Name = " + e.getNodeName());
                        NodeList list = e.getChildNodes();
                        for (int j = 0; j < list.getLength(); j++) {
                            if (list.item(j) instanceof Element) {
                                Element element = ((Element) (list.item(j)));
                                log.info("====== child Node Name = " + element.getNodeName());
                                log.info("===================================  child Node Content = " + list.item(j).getTextContent());
                                if ((element.getTagName().contains("BANKCODE"))) {
                                    contentCode = element.getTextContent();
                                } else if (element.getTagName().contains("SCHEDULEDATE")) {
                                    scheduleDate = element.getTextContent();
                                }
                            }
                        }
                    }
                }

                //check code
                if (!fiCode.equalsIgnoreCase(contentCode)) {
                    throw new IllegalFiException("Fi code in File Name and Fi Code in file doesn't match!!!");
                }


                //set Fi
                Fi fi = fiService.findByCode(fiCode);
                uf.setFi(fi);

                //set Template
                Template template = templateService.findByCode(templateCode);

                if (template.getSchedule().equals(Schedule.DAILY.toString())) {
                    if (!fileDate.equals(scheduleDate.replace("/", "")))
                        throw new ScheduleException("Date in file Name and Date in file are not compatible with each Other!!");
                }

                uf.setTemplate(template);

                //set Schedule
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy");
                Date scDate = format.parse(scheduleDate.trim());
                uf.setScheduleDate(scDate);

                //call EJB Save Method
                fileServiceLocal.upload(uf);

                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().write("done");

            }
        } catch (IllegalFiException e) {
            log.info(e);
            e.printStackTrace();
            response.getWriter().write("Code_Error");
        } catch (ScheduleException ex) {
            log.info(ex);
            ex.printStackTrace();
            response.getWriter().write("Schedule_Error");
        } catch (Exception ex) {
            log.info(ex);
            ex.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An Error occurred while saving file");
        }
    }
}
