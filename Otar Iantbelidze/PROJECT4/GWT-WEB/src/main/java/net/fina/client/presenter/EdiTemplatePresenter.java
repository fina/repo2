package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.ValueBaseField;
import com.sencha.gxt.widget.core.client.info.Info;
import net.fina.client.service.TemplateService;
import net.fina.shared.model.Exception.FieldUniqueException;
import net.fina.shared.model.ScheduleModel;
import net.fina.shared.model.TemplateModel;

/**
 * Created by oto on 11/14/2014.
 */
public class EdiTemplatePresenter implements Presenter {
    private Display display;
    private HandlerManager eventBus;
    private TemplateModel model = new TemplateModel();

    public interface Display {
        SelectEvent.HasSelectHandlers getSaveButton();

        SelectEvent.HasSelectHandlers getCancelbutton();

        ValueBaseField<String> name();

        ValueBaseField<String> code();

        HasValue<String> getValueFromCombo();

        HasValue<String> getName();

        HasValue<String> getCode();

        HasValue<ScheduleModel> getSch();

        SimpleComboBox<ScheduleModel> getCombo();

        HasWidgets getWindow();

        boolean isValid();

        void hideWindow();

        Widget asWidget();
    }


    public EdiTemplatePresenter(HandlerManager eventBus, Display display) {
        this.eventBus = eventBus;
        this.display = display;
        bind();
    }

    public EdiTemplatePresenter(Display display, TemplateModel model) {
        this.display = display;
        this.model = model;
        display.getName().setValue(model.getName());
        display.getCode().setValue(model.getCode());
        display.getSch().setValue(model.getSchedule());
        bind();
    }

    public void bind() {
        display.getSaveButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                if (!display.isValid()) {
                    new AlertMessageBox("INFO", "please fill all Fields").show();
                    return;
                }
                model.setCode(display.code().getValue().trim());
                model.setName(display.name().getValue().trim());
                model.setScheduleModel(display.getCombo().getCurrentValue().toString());

                TemplateService.Util.getInstance().saveTemplate(model, new AsyncCallback<Boolean>() {
                    public void onFailure(Throwable caught) {
                        if(caught instanceof FieldUniqueException) {
                            new AlertMessageBox("ERROR", caught.getMessage()).show();
                            return;
                        }else {
                            new AlertMessageBox("ERROR", caught.getMessage()).show();
                            return;
                        }
                    }

                    public void onSuccess(Boolean result) {
                        display.hideWindow();
                        TemplateTabPresenter.loadGrid();
                    }
                });

            }
        });
        display.getCancelbutton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                display.hideWindow();
            }
        });

    }


    public void go(HasWidgets container) {
        display.asWidget();
    }
}
