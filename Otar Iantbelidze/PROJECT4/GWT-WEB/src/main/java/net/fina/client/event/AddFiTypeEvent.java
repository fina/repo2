package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 10/24/2014.
 */
public class AddFiTypeEvent extends GwtEvent<AddFiTypeEventHandler> {
    public static final Type<AddFiTypeEventHandler> TYPE=new Type<AddFiTypeEventHandler>();

    @Override
    public Type<AddFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddFiTypeEventHandler handler) {
        handler.onFiTypeClickEvent(this);
    }
}
