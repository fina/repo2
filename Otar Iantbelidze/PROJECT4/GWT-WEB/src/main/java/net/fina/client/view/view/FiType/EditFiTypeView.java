package net.fina.client.view.view.FiType;


import com.google.gwt.user.client.ui.*;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.ValueBaseField;
import net.fina.client.presenter.EditFiTypePresenter;

/**
 * Created by oto on 10/27/2014.
 */
public class EditFiTypeView extends Composite implements EditFiTypePresenter.Display, IsWidget {
    private VerticalPanel vp;
    private Window window;

    private TextField nameField;
    private TextField codeField;
    private TextButton saveButton;
    private TextButton cancelButton;
    private boolean valid;

    public EditFiTypeView() {
        createWindow();
    }

    private void createWindow() {
        if (vp == null) {
            vp = new VerticalPanel();
            vp.setSpacing(10);

            FramedPanel form = new FramedPanel();
            form.setHeadingText("Create Fi Type Window");
            form.setBodyStyle("background: none; padding: 10px");
            form.setWidth(350);

            FieldSet fieldSet = new FieldSet();
            fieldSet.setHeadingText("User Information");
            fieldSet.setCollapsible(true);
            form.add(fieldSet);

            VerticalLayoutContainer p = new VerticalLayoutContainer();
            fieldSet.add(p);

            nameField = new TextField();
            nameField.setAllowBlank(false);
            p.add(new FieldLabel(nameField, "Name"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            codeField = new TextField();
            codeField.setAllowBlank(false);
            p.add(new FieldLabel(codeField, "Code"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            saveButton = new TextButton("Save");
            cancelButton = new TextButton("Cancel");

            form.addButton(saveButton);
            form.addButton(cancelButton);

            this.window = new Window();

            vp.add(form);
            window.add(vp);
            window.setModal(true);
        }
    }


    public SelectEvent.HasSelectHandlers getSaveButton() {
        return saveButton;
    }


    public SelectEvent.HasSelectHandlers getCancelButton() {
        return cancelButton;
    }

    public ValueBaseField<String> code() {
        return codeField;
    }

    public ValueBaseField<String> name() {
        return nameField;
    }


    public HasWidgets getWindow() {
        return window;
    }

    @Override
    public boolean isValid() {
        valid = true;
        valid &= nameField.isValid();
        valid &= codeField.isValid();
        return valid;
    }

    public Widget asWidget() {
        this.window.show();
        return this;
    }
}
