package net.fina.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import net.fina.shared.model.UploadFileModel;

import java.util.List;

/**
 * Created by oto on 12/10/2014.
 */
@RemoteServiceRelativePath("UploadService")
public interface UploadService extends RemoteService {

    public static class Util {
        private static UploadServiceAsync instance;

        public static UploadServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(UploadService.class);
            }
            return instance;
        }
    }

    public List<UploadFileModel> getData();

    public PagingLoadResult<UploadFileModel> getPagingData(PagingLoadConfig config);

    public boolean deleteModels(List<UploadFileModel> fileModels);

    public void test();
}
