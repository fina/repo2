package net.fina.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.event.BeforeCloseEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.event.CreateTemplateEvent;
import net.fina.client.event.EditTemplateEvent;
import net.fina.client.service.TemplateService;
import net.fina.client.view.view.template.TemplateGridView;
import net.fina.client.view.view.template.TemplateTabView;
import net.fina.shared.model.TemplateModel;

import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public class TemplateTabPresenter implements Presenter {
    private static Display display;
    private HandlerManager eventBus;
    private TemplateModel templateModel;
    private static boolean confirm;

    public interface Display {
        SelectEvent.HasSelectHandlers getCreateButton();

        SelectEvent.HasSelectHandlers getRefreshButton();

        SelectEvent.HasSelectHandlers getEditButton();

        SelectEvent.HasSelectHandlers getDeleteButton();

        TemplateModel getSelectedRow();

        List<TemplateModel> getSelectedRows();

        void setDataToGrid(List<TemplateModel> data);

        Widget asWidget();
    }

    public TemplateTabPresenter(Display display, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.display = display;
        addTab("Template", display.asWidget());
        bind();
    }


    public void bind() {
        loadGrid();
        display.getCreateButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                eventBus.fireEvent(new CreateTemplateEvent());
            }
        });
        display.getRefreshButton().addSelectHandler(
                new SelectEvent.SelectHandler() {
                    public void onSelect(SelectEvent event) {
                        loadGrid();
                    }
                }
        );
        display.getDeleteButton().addSelectHandler(new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                List<TemplateModel> list = display.getSelectedRows();
                confirmDelete(list);
            }
        });
        display.getEditButton().addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                templateModel = display.getSelectedRow();
                eventBus.fireEvent(new EditTemplateEvent(templateModel));
            }
        });
    }


    public void deleteTempList(final List<TemplateModel> templateModels) {
        TemplateService.Util.getInstance().deleteTempList(templateModels, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage()).show();
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    for (TemplateModel model : templateModels) {
                        TemplateGridView.getGrid().getStore().remove(model);
                    }
                } else {
                    new AlertMessageBox("INFO", "Has Dependencies").show();
                }
            }
        });
    }

    public void confirmDelete(final List<TemplateModel> modelList) {
        ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete?");
        box.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
            @Override
            public void onDialogHide(DialogHideEvent event) {
                confirm = event.getHideButton().name().equalsIgnoreCase("yes");
                if (!confirm) return;
                deleteTempList(modelList);
            }
        });
        box.show();
    }

    public void addTab(String name, Widget widget) {

        ((TabPanel) MainPresenter.display.getTabPanel()).add(widget, name);
        TabItemConfig config = ((TabPanel) MainPresenter.display.getTabPanel()).getConfig(widget);
        config.setClosable(true);
        ((TabPanel) MainPresenter.display.getTabPanel()).update(widget, config);
        ((TabPanel) MainPresenter.display.getTabPanel()).setActiveWidget(widget);
        ((TabPanel) MainPresenter.display.getTabPanel()).addBeforeCloseHandler(new BeforeCloseEvent.BeforeCloseHandler<Widget>() {
            @Override
            public void onBeforeClose(BeforeCloseEvent<Widget> event) {
                TemplateTabView.level = 0;
            }
        });
    }

    public static void loadGrid() {
        TemplateService.Util.getInstance().getData(new AsyncCallback<List<TemplateModel>>() {
            public void onFailure(Throwable caught) {
                new AlertMessageBox("ERROR", caught.getMessage());
            }

            public void onSuccess(List<TemplateModel> result) {
                display.setDataToGrid(result);
            }
        });
    }

    public void go(HasWidgets container) {
        container.add(display.asWidget());
    }
}
