package net.fina.client.view.view.Fi;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.grid.*;
import net.fina.shared.model.FiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 10/30/2014.
 */
@Deprecated
public class FiGridView implements IsWidget {
    private static final FiProperties props = GWT.create(FiProperties.class);
    private ContentPanel panel;
    private ListStore<FiModel> store;
    private static Grid<FiModel> grid;

    class KeyProvider implements ModelKeyProvider<FiModel> {
        @Override
        public String getKey(FiModel item) {
            return item.getType().getName();
        }
    }

    public Widget asWidget() {
        if (panel == null) {

            store = new ListStore<>(props.key());
            IdentityValueProvider<FiModel> identity = new IdentityValueProvider<>();
            CheckBoxSelectionModel<FiModel> selectionModel = new CheckBoxSelectionModel<>(identity);


            List<ColumnConfig<FiModel, ?>> cfgs = new ArrayList<ColumnConfig<FiModel, ?>>();
            cfgs.add(selectionModel.getColumn());

            ColumnConfig<FiModel, String> nameCol = new ColumnConfig<FiModel, String>(props.name(), 150, "Name");
            nameCol.setCell(new TextCell());
            cfgs.add(nameCol);

            ColumnConfig<FiModel, String> codeCol = new ColumnConfig<FiModel, String>(props.code(), 200, "Code");
            codeCol.setCell(new TextCell());
            cfgs.add(codeCol);

            ColumnConfig<FiModel, String> tpcol = new ColumnConfig<FiModel, String>(props.typeString(), 200);
            tpcol.setHeader("type");
            tpcol.setCell(new TextCell());
            cfgs.add(tpcol);


            ColumnModel<FiModel> cModel = new ColumnModel<>(cfgs);


            GroupingView<FiModel> view = new GroupingView<FiModel>();
            view.setShowGroupedColumn(false);
            view.setForceFit(true);

            grid = new Grid<>(store, cModel);
            grid.setSelectionModel(selectionModel);
            grid.setLoadMask(true);
            grid.setView(view);
            view.groupBy(tpcol);


            panel = new ContentPanel();
            panel.setHeadingText("CheckBox Grid");
            panel.setPixelSize(600, 320);


            VerticalLayoutContainer con = new VerticalLayoutContainer();
            con.add(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
            panel.add(con);


        }
        return panel;
    }

    public static List<FiModel> getSelectedRows() {
        return grid.getSelectionModel().getSelectedItems();
    }

    public void initGrid(List<FiModel> list) {
        grid.getStore().clear();
        grid.getStore().addAll(list);
    }

    public static Grid<FiModel> getGrid() {
        return grid;
    }
}

