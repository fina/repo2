package net.fina.server.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import net.fina.client.service.UploadService;
import net.fina.session.api.UploadFileServiceLocal;
import net.fina.session.entity.Fi;
import net.fina.session.entity.UploadedFile;
import net.fina.shared.model.FiModel;
import net.fina.shared.model.UploadFileModel;

import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by oto on 12/10/2014.
 */
public class UploadServiceImpl extends RemoteServiceServlet implements UploadService {

    private static ModelConverter<UploadFileModel, UploadedFile> converter = new ModelConverter<>();
    private static ModelConverter<FiModel, Fi> fiConverter = new ModelConverter<>();

    @EJB
    private UploadFileServiceLocal ejbService;

    @Override
    public List<UploadFileModel> getData() {
        try {
            List<UploadFileModel> modelList = new ArrayList<>();
            List<UploadedFile> uploadedFiles = ejbService.getData();

            UploadedFile uploadedFile = null;
            UploadFileModel uploadFileModel = null;
            Fi fi = new Fi();
            FiModel fiModel = null;

            for (UploadedFile uf : uploadedFiles) {
                uploadFileModel = new UploadFileModel();
                fiModel = new FiModel();
                converter.toModel(uploadFileModel, uf);
                convertFiToModel(fiModel, uf.getFi());
                uploadFileModel.setFiModel(fiModel);
                modelList.add(uploadFileModel);
            }
            return modelList;

        } catch (Exception ex) {
            GWT.log(ex.getMessage());
        }
        return null;
    }

    @Override
    public PagingLoadResult<UploadFileModel> getPagingData(PagingLoadConfig config) {
        List<UploadFileModel> models = null;
        ArrayList<UploadFileModel> sublist = null;
        try {

            List<UploadedFile> uploadedFiles = new ArrayList<>(ejbService.getData());
            models = new ArrayList<>();
            UploadFileModel uploadFileModel;
            for (int i = 0; i < uploadedFiles.size(); i++) {
                uploadFileModel = new UploadFileModel();
                converter.toModel(uploadFileModel, uploadedFiles.get(i));
                models.add(uploadFileModel);
            }

            if (config.getSortInfo().size() > 0) {
                SortInfo sort = config.getSortInfo().get(0);
                final String sortField = sort.getSortField();
                if (sortField != null) {
                    Collections.sort(models, sort.getSortDir().comparator(new Comparator<UploadFileModel>() {
                        public int compare(UploadFileModel p1, UploadFileModel p2) {
                            if (sortField.equalsIgnoreCase("File Name")) {
                                return p1.getFileName().compareTo(p2.getFileName());
                            } else if (sortField.equalsIgnoreCase("Upload Date")) {
                                return p1.getUploadDate().compareTo(p2.getUploadDate());
                            }
                            return 0;
                        }
                    }));
                }
            }

            sublist = new ArrayList<>();
            int start = config.getOffset();
            int limit = models.size();
            if (config.getLimit() > 0) {
                limit = Math.min(start + config.getLimit(), limit);
            }
            for (int i = config.getOffset(); i < limit; i++) {
                sublist.add(models.get(i));
            }

            return new PagingLoadResultBean<UploadFileModel>(sublist, models.size(), config.getOffset());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new PagingLoadResultBean<UploadFileModel>(sublist, models.size(), config.getOffset());
    }

    @Override
    public boolean deleteModels(List<UploadFileModel> fileModels) {
        try {
            List<Integer> idList = new ArrayList<>();

            for (UploadFileModel model : fileModels)
                idList.add(model.getId());

            ejbService.delete(idList);
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }


    public void convertFiToModel(FiModel model, Fi fi) {
        fiConverter.toModel(model, fi);
    }

    @Override
    public void test() {
        ejbService.test();
    }
}
