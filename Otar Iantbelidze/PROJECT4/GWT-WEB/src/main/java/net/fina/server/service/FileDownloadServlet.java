package net.fina.server.service;

import net.fina.session.api.UploadFileServiceLocal;
import net.fina.session.entity.UploadedFile;
import net.fina.shared.model.UploadFileModel;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by oto on 12/17/2014.
 */
@WebServlet(name = "FileDownloadServlet", urlPatterns = {"/FileDownloadServlet"})
public class FileDownloadServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(FileDownloadServlet.class);
    private static ModelConverter<UploadFileModel, UploadedFile> converter = new ModelConverter<>();

    @EJB
    private UploadFileServiceLocal fileServiceLocal;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        log.info("//================== calling download Servlet doGet()============//");
        String fileModelId = request.getParameter("id");
        int fileID = Integer.parseInt(fileModelId);
        log.info("//===========starting download file ID = "+fileID);

        try {
            UploadedFile uploadedFile = fileServiceLocal.getFile(fileID);

            UploadFileModel model = new UploadFileModel();

            converter.toModel(model, uploadedFile);

            OutputStream outputStream = response.getOutputStream();

            String mimetype = "application/octet-stream";

            response.setContentType(mimetype);

            response.setContentLength(model.getUploadedFile().length);

            response.setHeader("Content-Disposition", "attachment; filename=\"" + model.getFileName() + "\"");
            response.setHeader("received-id-parameter", fileModelId);

            byte[] bufferSize = new byte[4096];
            ByteArrayInputStream byteStream = new ByteArrayInputStream(model.getUploadedFile());

            DataInputStream fIn = new DataInputStream(byteStream);
            int length = 0;
            while ((length = fIn.read(bufferSize)) > -1) {
                outputStream.write(bufferSize, 0, length);
            }
            fIn.close();
            outputStream.close();

            log.info("//============= END SUCCESSFULLY =============//");

        } catch (Exception ex) {
            log.info(ex.getMessage());
            ex.printStackTrace();
        }
    }

}
