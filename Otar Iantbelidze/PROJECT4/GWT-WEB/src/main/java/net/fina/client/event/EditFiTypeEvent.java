package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;
import net.fina.shared.model.FiTypeModel;

/**
 * Created by oto on 10/27/2014.
 */
public class EditFiTypeEvent extends GwtEvent<EditFiTypeEventHandler
        > {

    public static Type<EditFiTypeEventHandler> TYPE = new Type<EditFiTypeEventHandler>();


    private FiTypeModel fitypeModel;

    public EditFiTypeEvent() {

    }
    public EditFiTypeEvent(FiTypeModel model){
        this.fitypeModel=model;
    }

    @Override
    public Type<EditFiTypeEventHandler> getAssociatedType() {
        return TYPE;
    }

    public FiTypeModel getFitypeModel() {
        return fitypeModel;
    }


    protected void dispatch(EditFiTypeEventHandler handler) {
        handler.onEditEvent(this);
    }

}
