package net.fina.client.view.view.Images;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Created by oto on 10/30/2014.
 */
public interface Images extends ClientBundle {

    public Images INSTANCE = GWT.create(Images.class);

    @Source("bank.png")
    ImageResource bnkICon();

    @Source("Refresh.png")
    ImageResource refreshIcon();

    @Source("add.png")
    ImageResource addIcon();

    @Source("remove.png")
    ImageResource removeIcon();

    @Source("delete.png")
    ImageResource deleteIcon();

    @Source("type.png")
    ImageResource typeIcon();

    @Source("template.png")
    ImageResource templateIcon();

    @Source("create.png")
    ImageResource createIcon();

    @Source("upload.png")
    ImageResource uploadIcon();

    @Source("edit.png")
    ImageResource editIcon();

    @Source("cancel.png")
    ImageResource cancelIcon();

    @Source("next.png")
    ImageResource nextIcon();

    @Source("back.png")
    ImageResource backIcon();
}
