package net.fina.client.view;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import net.fina.client.view.view.Images.Images;

/**
 * Created by oto on 10/24/2014.
 */
public class MenuView implements IsWidget {

    private TextButton fiTypeButton;
    private TextButton fiButton;
    private TextButton templateButton;
    private TextButton uploadTabButton;
    private ContentPanel panel;


    public Widget asWidget() {
        if (panel == null) {
            panel = new ContentPanel();

            VBoxLayoutContainer container = new VBoxLayoutContainer();
            container.setVBoxLayoutAlign(VBoxLayoutContainer.VBoxLayoutAlign.STRETCH);
            BoxLayoutContainer.BoxLayoutData layoutData = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 5));


            fiTypeButton = new TextButton("Fi Type",Images.INSTANCE.typeIcon());
            fiButton = new TextButton("FI", Images.INSTANCE.bnkICon());
            templateButton = new TextButton("Template",Images.INSTANCE.templateIcon());
            uploadTabButton = new TextButton("File Upload",Images.INSTANCE.uploadIcon());

            container.add(fiTypeButton, layoutData);
            container.add(fiButton, layoutData);
            container.add(templateButton, layoutData);
            container.add(uploadTabButton, layoutData);

            panel.setWidget(container);
        }
        return panel;
    }

    public TextButton getFiTypeButton() {
        return fiTypeButton;
    }

    public TextButton getFiButton() {
        return fiButton;
    }

    public TextButton getTemplateButton() {
        return templateButton;
    }

    public TextButton getUploadTabButton() {
        return uploadTabButton;
    }
}
