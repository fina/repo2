package net.fina.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import net.fina.shared.model.UploadFileModel;

import java.util.List;

public interface UploadServiceAsync {
    void test(AsyncCallback<Void> async);

    void getData(AsyncCallback<List<UploadFileModel>> async);

    void getPagingData(PagingLoadConfig config, AsyncCallback<PagingLoadResult<UploadFileModel>> async);

    void deleteModels(List<UploadFileModel> fileModels, AsyncCallback<Boolean> async);
}
