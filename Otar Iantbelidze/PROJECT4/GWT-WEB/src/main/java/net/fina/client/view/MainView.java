package net.fina.client.view;

import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.container.*;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import net.fina.client.presenter.MainPresenter;

/**
 * Created by oto on 10/16/2014.
 */
public class MainView extends Composite implements MainPresenter.Display {
    private SimpleContainer simpleContainer;
    BoxLayoutContainer.BoxLayoutPack pack = BoxLayoutContainer.BoxLayoutPack.CENTER;
    private Viewport viewport;
    private TabPanel tabPanel;
    private MenuView menu;

    public MainView() {
        init();
    }

    public void init() {
        if (simpleContainer == null) {
            simpleContainer = new SimpleContainer();

            tabPanel = new TabPanel();
            tabPanel.setCloseContextMenu(true);
            tabPanel.setTabScroll(true);
            tabPanel.setAnimScroll(true);


            final BorderLayoutContainer con = new BorderLayoutContainer();
            con.setBorders(true);

            ContentPanel north = new ContentPanel();
            ContentPanel west = new ContentPanel();
            ContentPanel center = new ContentPanel();

            center.setResize(false);

            BorderLayoutContainer.BorderLayoutData northData = new BorderLayoutContainer.BorderLayoutData(50);
            northData.setMargins(new Margins(0, 8, 0, 5));
            northData.setCollapsible(false);
            northData.setSplit(true);

            BorderLayoutContainer.BorderLayoutData westData = new BorderLayoutContainer.BorderLayoutData();
            westData.setCollapsible(true);

            westData.setSplit(true);
            westData.setCollapseMini(false);
            westData.setMargins(new Margins(0, 8, 0, 5));

            menu = new MenuView();
            west.add(menu);

            MarginData centerData = new MarginData();
            center.add(tabPanel);

            con.setNorthWidget(north, northData);
            con.setWestWidget(west, westData);
            con.setCenterWidget(center, centerData);

            simpleContainer.add(con, new MarginData(0));
        }
        viewport = new Viewport();
        viewport.setLayoutData(new FitLayout());
        viewport.add(simpleContainer);
        initWidget(viewport);

    }

    public HasWidgets getTabPanel() {
        return tabPanel;
    }


    public SelectEvent.HasSelectHandlers getFiTypeButton() {
        return menu.getFiTypeButton();
    }


    public SelectEvent.HasSelectHandlers getFiButton() {
        return menu.getFiButton();
    }

    public SelectEvent.HasSelectHandlers getTemplateButton() {
        return menu.getTemplateButton();
    }

    @Override
    public SelectEvent.HasSelectHandlers getUploadButton() {
        return menu.getUploadTabButton();
    }

    public Widget asWidget() {
        return this;
    }
}
