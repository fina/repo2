package net.fina.shared.model.Exception;

/**
 * Created by oto on 12/16/2014.
 */
public class ScheduleException extends Exception {
    public ScheduleException() {
    }

    public ScheduleException(String message) {
        super(message);
    }
}
