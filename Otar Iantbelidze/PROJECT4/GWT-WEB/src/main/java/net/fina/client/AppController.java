package net.fina.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import net.fina.client.event.*;
import net.fina.client.presenter.*;
import net.fina.client.view.MainView;
import net.fina.client.view.view.Fi.EditFiView;
import net.fina.client.view.view.Fi.FiTabView;
import net.fina.client.view.view.FiType.EditFiTypeView;
import net.fina.client.view.view.FiType.FiTypeTabView;
import net.fina.client.view.view.FileUpload.FileUploadTabView;
import net.fina.client.view.view.template.EditTemplateView;
import net.fina.client.view.view.template.TemplateTabView;

/**
 * Created by oto on 11/6/2014.
 */
public class AppController implements Presenter, ValueChangeHandler<String> {
    private HandlerManager eventBus;
    private HasWidgets container;
    Presenter presenter = null;

    public AppController(HandlerManager eventBus) {
        this.eventBus = eventBus;
        bind();
    }

    public void bind() {
        History.addValueChangeHandler(this);
        eventBus.addHandler(CreateFiEvent.TYPE, new CreateFiEventHandler() {

            public void onCreateEvent(CreateFiEvent event) {
                presenter = new EditFiPresenter(new EditFiView(), eventBus);
                presenter.go(null);
            }
        });
        eventBus.addHandler(AddFiTabEvent.TYPE, new AddFiTabEventHandler() {

            public void onAddTab(AddFiTabEvent event) {

                presenter = new FiTabPresenter(new FiTabView(), eventBus);
            }
        });
        eventBus.addHandler(AddTemplateTabEvent.TYPE, new AddTemplateTabEventHandler() {
            public void onAddTab(AddTemplateTabEvent event) {
                presenter = new TemplateTabPresenter(new TemplateTabView(), eventBus);
            }
        });
        eventBus.addHandler(AddFiTypeEvent.TYPE, new AddFiTypeEventHandler() {

            public void onFiTypeClickEvent(AddFiTypeEvent event) {
                presenter = new FiTypeTabPresenter(new FiTypeTabView(), eventBus);
            }
        });
        eventBus.addHandler(AddUploadTabEvent.TYPE, new AddUploadTabEventHandler() {
            @Override
            public void onAddTab(AddUploadTabEvent event) {
                presenter = new FileUploadTabPresenter(new FileUploadTabView(), eventBus);
            }
        });
        eventBus.addHandler(CreateFiTypeEvent.TYPE, new CreateFiTypeEventHandler() {

            public void onCreate(CreateFiTypeEvent event) {
                presenter = new EditFiTypePresenter(new EditFiTypeView());
                presenter.go(null);
            }
        });
        eventBus.addHandler(EditFiTypeEvent.TYPE, new EditFiTypeEventHandler() {
            public void onEditEvent(EditFiTypeEvent event) {
                presenter = new EditFiTypePresenter(new EditFiTypeView(), event.getFitypeModel());
                presenter.go(null);
            }
        });
        eventBus.addHandler(CreateTemplateEvent.TYPE, new CreateTemplateEventHandler() {
            public void onCreate(CreateTemplateEvent event) {
                presenter = new EdiTemplatePresenter(eventBus, new EditTemplateView());
                presenter.go(null);
            }
        });
        eventBus.addHandler(EditFiEvent.TYPE, new EditFiEventHandler() {
            @Override
            public void onEditFiEvent(EditFiEvent event) {
                presenter = new EditFiPresenter(new EditFiView(), event.getFiModel());
                presenter.go(null);
            }
        });
        eventBus.addHandler(EditTemplateEvent.TYPE, new EditTemplateEventHandler() {
            @Override
            public void onEditEvent(EditTemplateEvent event) {
                presenter = new EdiTemplatePresenter(new EditTemplateView(), event.getTemplateModel());
                presenter.go(null);
            }
        });

    }

    public void go(HasWidgets container) {
        this.container = container;

        if ("".equals(History.getToken())) {
            History.newItem("list");
        } else {
            History.fireCurrentHistoryState();
        }
    }

    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        if (token != null) {

            if (token.equals("list")) {
                presenter = new MainPresenter(new MainView(), eventBus);
            }
            if (presenter != null)
                presenter.go(container);
        }

    }
}
