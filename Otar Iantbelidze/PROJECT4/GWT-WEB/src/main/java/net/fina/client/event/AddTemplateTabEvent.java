package net.fina.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by oto on 11/14/2014.
 */
public class AddTemplateTabEvent extends GwtEvent<AddTemplateTabEventHandler> {
    public static Type<AddTemplateTabEventHandler> TYPE = new Type<AddTemplateTabEventHandler>();


    @Override
    public Type<AddTemplateTabEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddTemplateTabEventHandler handler) {
        handler.onAddTab(this);
    }
}
