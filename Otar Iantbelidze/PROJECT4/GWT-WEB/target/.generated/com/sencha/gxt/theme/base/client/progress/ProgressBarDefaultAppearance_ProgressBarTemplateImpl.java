package com.sencha.gxt.theme.base.client.progress;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class ProgressBarDefaultAppearance_ProgressBarTemplateImpl implements com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarTemplate {
  public com.google.gwt.safehtml.shared.SafeHtml render(com.google.gwt.safehtml.shared.SafeHtml text, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle style, com.google.gwt.safecss.shared.SafeStyles wrapStyles, com.google.gwt.safecss.shared.SafeStyles progressBarStyles, com.google.gwt.safecss.shared.SafeStyles progressTextStyles, com.google.gwt.safecss.shared.SafeStyles widthStyles){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    
    /**
     * safehtml content:
       * <div class="{0}" style="{1}"><div class="{2}"><div class="{3}" style="{4}"><div class="{5}" style="{6}"><div style="{7}">{8}</div></div><div class="{9} {10}"><div style="{11}">{12}</div></div></div></div></div>
       * 
     * params:
       * com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressWrap_ValueProviderImpl.INSTANCE.getValue(style), wrapStyles, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressInner_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressBar_ValueProviderImpl.INSTANCE.getValue(style), progressBarStyles, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressText_ValueProviderImpl.INSTANCE.getValue(style), progressTextStyles, widthStyles, text, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressText_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressTextBack_ValueProviderImpl.INSTANCE.getValue(style), widthStyles, text
     */
    outer = GWT.<com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarTemplate_render_SafeHtml__SafeHtml_text__ProgressBarStyle_style__SafeStyles_wrapStyles__SafeStyles_progressBarStyles__SafeStyles_progressTextStyles__SafeStyles_widthStyles___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarTemplate_render_SafeHtml__SafeHtml_text__ProgressBarStyle_style__SafeStyles_wrapStyles__SafeStyles_progressBarStyles__SafeStyles_progressTextStyles__SafeStyles_widthStyles___SafeHtmlTemplates.class).render0(com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressWrap_ValueProviderImpl.INSTANCE.getValue(style), wrapStyles, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressInner_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressBar_ValueProviderImpl.INSTANCE.getValue(style), progressBarStyles, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressText_ValueProviderImpl.INSTANCE.getValue(style), progressTextStyles, widthStyles, text, com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressText_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance_ProgressBarStyle_progressTextBack_ValueProviderImpl.INSTANCE.getValue(style), widthStyles, text);
    return outer;
  }
}
