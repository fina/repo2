package com.sencha.gxt.widget.core.client.grid;

public class GroupingView_GroupingData_value_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.sencha.gxt.widget.core.client.grid.GroupingView.GroupingData<java.lang.Object>, java.lang.Object> {
  public static final GroupingView_GroupingData_value_ValueProviderImpl INSTANCE = new GroupingView_GroupingData_value_ValueProviderImpl();
  public java.lang.Object getValue(com.sencha.gxt.widget.core.client.grid.GroupingView.GroupingData<java.lang.Object> object) {
    return object.getValue();
  }
  public void setValue(com.sencha.gxt.widget.core.client.grid.GroupingView.GroupingData<java.lang.Object> object, java.lang.Object value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "value";
  }
}
