package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class GroupingViewDefaultAppearance_GroupingViewResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewResources {
  private static GroupingViewDefaultAppearance_GroupingViewResources_default_InlineClientBundleGenerator _instance0 = new GroupingViewDefaultAppearance_GroupingViewResources_default_InlineClientBundleGenerator();
  private void groupByInitializer() {
    groupBy = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "groupBy",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 16, 16, false, false
    );
  }
  private static class groupByInitializer {
    static {
      _instance0.groupByInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return groupBy;
    }
  }
  public com.google.gwt.resources.client.ImageResource groupBy() {
    return groupByInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDF5,.GCGCW0WDH5{zoom:" + ("1")  + ";}.GCGCW0WDF5 .GCGCW0WDJFC{border-bottom:" + ("2px"+ " " +"solid")  + ";cursor:" + ("pointer")  + ";padding-top:" + ("4px")  + ";border-bottom-color:" + ("#99bbe8")  + ";}.GCGCW0WDF5 .GCGCW0WDJFC .GCGCW0WDKFC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"3px"+ " " +"-47px")  + ";padding:" + ("4px"+ " " +"17px"+ " " +"4px"+ " " +"4px")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/group-expand-sprite.gif"))  + ";color:" + ("#3764a0")  + ";font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDG5 .GCGCW0WDJFC .GCGCW0WDKFC{background-position:") + (("3px"+ " " +"3px")  + ";}.GCGCW0WDE5{display:" + ("none")  + ";}")) : ((".GCGCW0WDF5,.GCGCW0WDH5{zoom:" + ("1")  + ";}.GCGCW0WDF5 .GCGCW0WDJFC{border-bottom:" + ("2px"+ " " +"solid")  + ";cursor:" + ("pointer")  + ";padding-top:" + ("4px")  + ";border-bottom-color:" + ("#99bbe8")  + ";}.GCGCW0WDF5 .GCGCW0WDJFC .GCGCW0WDKFC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"3px"+ " " +"-47px")  + ";padding:" + ("4px"+ " " +"4px"+ " " +"4px"+ " " +"17px")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/group-expand-sprite.gif"))  + ";color:" + ("#3764a0")  + ";font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GCGCW0WDG5 .GCGCW0WDJFC .GCGCW0WDKFC{background-position:") + (("3px"+ " " +"3px")  + ";}.GCGCW0WDE5{display:" + ("none")  + ";}"));
      }
      public java.lang.String bodyCollapsed() {
        return "GCGCW0WDE5";
      }
      public java.lang.String group() {
        return "GCGCW0WDF5";
      }
      public java.lang.String groupCollapsed() {
        return "GCGCW0WDG5";
      }
      public java.lang.String groupHead() {
        return "GCGCW0WDH5";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAkUlEQVR42mNgGBSge9Wr/6TisLan/1EMWHTo///5B/79n7X37/9pO//8n7D19//uDb/+t6z++b92+Y//5Yu+/S+c+/V/1syv/1OnfsY0gCIXgGwnBnz4AcUf/mM3gFjbH7z4QJoLkG0GacZpwIEDB8AYmY1N7MGDD6NhQHQYINn84QPEZpDmC9gMAAmQiqmSEQEAyG1RAgbUgQAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource groupBy;
  private static com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      groupBy(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("groupBy", groupBy());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'groupBy': return this.@com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewResources::groupBy()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.GroupingViewResources::style()();
    }
    return null;
  }-*/;
}
