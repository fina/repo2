package com.sencha.gxt.theme.base.client.info;

public class InfoDefaultAppearance_InfoStyle_info_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoStyle, java.lang.String> {
  public static final InfoDefaultAppearance_InfoStyle_info_ValueProviderImpl INSTANCE = new InfoDefaultAppearance_InfoStyle_info_ValueProviderImpl();
  public java.lang.String getValue(com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoStyle object) {
    return object.info();
  }
  public void setValue(com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoStyle object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "info";
  }
}
