package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListLoadConfigBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getSortInfo(com.sencha.gxt.data.shared.loader.ListLoadConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.ListLoadConfigBean::sortInfo;
  }-*/;
  
  private static native void setSortInfo(com.sencha.gxt.data.shared.loader.ListLoadConfigBean instance, java.util.List value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.ListLoadConfigBean::sortInfo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.ListLoadConfigBean instance) throws SerializationException {
    setSortInfo(instance, (java.util.List) streamReader.readObject());
    
  }
  
  public static com.sencha.gxt.data.shared.loader.ListLoadConfigBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.ListLoadConfigBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.ListLoadConfigBean instance) throws SerializationException {
    streamWriter.writeObject(getSortInfo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.ListLoadConfigBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.ListLoadConfigBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.ListLoadConfigBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.ListLoadConfigBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.ListLoadConfigBean)object);
  }
  
}
