package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderCell_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("<div class='{0}'>&#160;</div>")
  SafeHtml renderCell0(java.lang.String arg0);
}
