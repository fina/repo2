package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface GroupingViewDefaultAppearance_DefaultHeaderTemplate_renderGroupHeader_SafeHtml__GroupingData_groupInfo___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("{0}")
  SafeHtml renderGroupHeader0(java.lang.Object arg0);
}
