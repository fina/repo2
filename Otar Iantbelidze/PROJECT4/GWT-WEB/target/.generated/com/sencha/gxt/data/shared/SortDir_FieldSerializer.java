package com.sencha.gxt.data.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SortDir_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.SortDir instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static com.sencha.gxt.data.shared.SortDir instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    com.sencha.gxt.data.shared.SortDir[] values = com.sencha.gxt.data.shared.SortDir.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.SortDir instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.SortDir_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.SortDir_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.SortDir)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.SortDir_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.SortDir)object);
  }
  
}
