package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplatesImpl implements com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnTemplates {
  public com.google.gwt.safehtml.shared.SafeHtml renderCell(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle style){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    
    /**
     * safehtml content:
       * <div class='{0}'>&#160;</div>
     * params:
       * com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnStyle_rowChecker_ValueProviderImpl.INSTANCE.getValue(style)
     */
    outer = GWT.<com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderCell_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderCell_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates.class).renderCell0(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnStyle_rowChecker_ValueProviderImpl.INSTANCE.getValue(style));
    return outer;
  }
  public com.google.gwt.safehtml.shared.SafeHtml renderHeader(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle style){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    
    /**
     * safehtml content:
       * <div class='{0}'></div>
     * params:
       * com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnStyle_hdChecker_ValueProviderImpl.INSTANCE.getValue(style)
     */
    outer = GWT.<com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderHeader_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderHeader_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates.class).renderHeader0(com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnStyle_hdChecker_ValueProviderImpl.INSTANCE.getValue(style));
    return outer;
  }
}
