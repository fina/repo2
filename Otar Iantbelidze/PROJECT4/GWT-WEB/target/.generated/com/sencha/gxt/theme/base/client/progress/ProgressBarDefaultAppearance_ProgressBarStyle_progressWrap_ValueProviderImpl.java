package com.sencha.gxt.theme.base.client.progress;

public class ProgressBarDefaultAppearance_ProgressBarStyle_progressWrap_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle, java.lang.String> {
  public static final ProgressBarDefaultAppearance_ProgressBarStyle_progressWrap_ValueProviderImpl INSTANCE = new ProgressBarDefaultAppearance_ProgressBarStyle_progressWrap_ValueProviderImpl();
  public java.lang.String getValue(com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle object) {
    return object.progressWrap();
  }
  public void setValue(com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "progressWrap";
  }
}
