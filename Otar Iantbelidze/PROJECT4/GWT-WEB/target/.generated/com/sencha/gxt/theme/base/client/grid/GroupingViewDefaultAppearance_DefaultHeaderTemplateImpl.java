package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class GroupingViewDefaultAppearance_DefaultHeaderTemplateImpl implements com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance.DefaultHeaderTemplate {
  public com.google.gwt.safehtml.shared.SafeHtml renderGroupHeader(com.sencha.gxt.widget.core.client.grid.GroupingView.GroupingData<java.lang.Object> groupInfo){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    
    /**
     * safehtml content:
       * {0}
     * params:
       * com.sencha.gxt.widget.core.client.grid.GroupingView_GroupingData_value_ValueProviderImpl.INSTANCE.getValue(groupInfo)
     */
    outer = GWT.<com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance_DefaultHeaderTemplate_renderGroupHeader_SafeHtml__GroupingData_groupInfo___SafeHtmlTemplates>create(com.sencha.gxt.theme.base.client.grid.GroupingViewDefaultAppearance_DefaultHeaderTemplate_renderGroupHeader_SafeHtml__GroupingData_groupInfo___SafeHtmlTemplates.class).renderGroupHeader0(com.sencha.gxt.widget.core.client.grid.GroupingView_GroupingData_value_ValueProviderImpl.INSTANCE.getValue(groupInfo));
    return outer;
  }
}
