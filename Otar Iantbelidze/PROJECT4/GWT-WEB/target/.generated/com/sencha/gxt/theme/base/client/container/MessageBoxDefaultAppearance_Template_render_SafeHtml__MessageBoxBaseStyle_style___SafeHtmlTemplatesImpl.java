package com.sencha.gxt.theme.base.client.container;

public class MessageBoxDefaultAppearance_Template_render_SafeHtml__MessageBoxBaseStyle_style___SafeHtmlTemplatesImpl implements com.sencha.gxt.theme.base.client.container.MessageBoxDefaultAppearance_Template_render_SafeHtml__MessageBoxBaseStyle_style___SafeHtmlTemplates {
  
  public com.google.gwt.safehtml.shared.SafeHtml render0(java.lang.String arg0,java.lang.String arg1,java.lang.String arg2) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("\"></div><div class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("\"></div><div class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg2));
    sb.append("\"></div>\n");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
