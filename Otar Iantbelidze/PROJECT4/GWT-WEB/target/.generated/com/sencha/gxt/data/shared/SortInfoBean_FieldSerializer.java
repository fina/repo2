package com.sencha.gxt.data.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SortInfoBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.sencha.gxt.data.shared.SortDir getSortDir(com.sencha.gxt.data.shared.SortInfoBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.SortInfoBean::sortDir;
  }-*/;
  
  private static native void setSortDir(com.sencha.gxt.data.shared.SortInfoBean instance, com.sencha.gxt.data.shared.SortDir value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.SortInfoBean::sortDir = value;
  }-*/;
  
  private static native java.lang.String getSortField(com.sencha.gxt.data.shared.SortInfoBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.SortInfoBean::sortField;
  }-*/;
  
  private static native void setSortField(com.sencha.gxt.data.shared.SortInfoBean instance, java.lang.String value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.SortInfoBean::sortField = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.SortInfoBean instance) throws SerializationException {
    setSortDir(instance, (com.sencha.gxt.data.shared.SortDir) streamReader.readObject());
    setSortField(instance, streamReader.readString());
    
  }
  
  public static com.sencha.gxt.data.shared.SortInfoBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.SortInfoBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.SortInfoBean instance) throws SerializationException {
    streamWriter.writeObject(getSortDir(instance));
    streamWriter.writeString(getSortField(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.SortInfoBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.SortInfoBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.SortInfoBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.SortInfoBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.SortInfoBean)object);
  }
  
}
