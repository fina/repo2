package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderHeader_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("<div class='{0}'></div>")
  SafeHtml renderHeader0(java.lang.String arg0);
}
