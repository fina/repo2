package com.sencha.gxt.theme.blue.client.toolbar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BluePagingToolBarAppearance_BluePagingToolBarResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources {
  private static BluePagingToolBarAppearance_BluePagingToolBarResources_default_InlineClientBundleGenerator _instance0 = new BluePagingToolBarAppearance_BluePagingToolBarResources_default_InlineClientBundleGenerator();
  private void firstInitializer() {
    first = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "first",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 16, 16, false, false
    );
  }
  private static class firstInitializer {
    static {
      _instance0.firstInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return first;
    }
  }
  public com.google.gwt.resources.client.ImageResource first() {
    return firstInitializer.get();
  }
  private void lastInitializer() {
    last = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "last",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 16, 16, false, false
    );
  }
  private static class lastInitializer {
    static {
      _instance0.lastInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return last;
    }
  }
  public com.google.gwt.resources.client.ImageResource last() {
    return lastInitializer.get();
  }
  private void loadingInitializer() {
    loading = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "loading",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 18, 18, true, false
    );
  }
  private static class loadingInitializer {
    static {
      _instance0.loadingInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return loading;
    }
  }
  public com.google.gwt.resources.client.ImageResource loading() {
    return loadingInitializer.get();
  }
  private void nextInitializer() {
    next = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "next",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 16, 16, false, false
    );
  }
  private static class nextInitializer {
    static {
      _instance0.nextInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return next;
    }
  }
  public com.google.gwt.resources.client.ImageResource next() {
    return nextInitializer.get();
  }
  private void prevInitializer() {
    prev = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "prev",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 16, 16, false, false
    );
  }
  private static class prevInitializer {
    static {
      _instance0.prevInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return prev;
    }
  }
  public com.google.gwt.resources.client.ImageResource prev() {
    return prevInitializer.get();
  }
  private void refreshInitializer() {
    refresh = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "refresh",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 16, 16, false, false
    );
  }
  private static class refreshInitializer {
    static {
      _instance0.refreshInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return refresh;
    }
  }
  public com.google.gwt.resources.client.ImageResource refresh() {
    return refreshInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABKElEQVR42mNgGFSgcNbz/00Lnv/PnPj4Py414e0PcMox5M589n/Vkff/cSmK737wP7jhGm4DMqY8/b9w3wesiuJ7Hv5fuv//f//qC7gNSOh58n/alvcYiiLa74M1Z876/9+r9ARuAyK7Hv3vXf0BRVFo6z2w5tZ1QNs7//53yT+A24DA5of/6+a/gysKbrn7f9He//87gJoz5vz8b1f38799xnbcBnhU3f+fP+0NiiK74tv/0+f8/u/f9e2/TuG7/xYJ63EbYF9693/25NcYivRTzv83KXrxXyXr03/jyGW4DbDIu/M/tf81VkUGSSf+y6Z9/a8bPA+3AYZZt/+HtbzEqUg/9fJ/Dd9puA3QSbvx36/6IV5FKh4TcBvglHvlP0gBXkWDDgAA7aHAqyyQNxAAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABAUlEQVR42mNgGFSgcNbz//jkmhY8/5858TFONQy5M5/9j+9+8B+X3Koj7/+Htz/AbUDGlKf/F+39j9UQkNzCfR/+Bzdcw21AQs8TsAEd6/7/d8q98h9dbtqW9//9qy/gNiCy6xFYUcacn//T5/xGMQQk17v6w3+v0hO4DQhsfghWBNKsU/juv07ajf/Gkcv+w+Tq5r/775J/ALcBHlX3/4NCG6ZZxWPCf2S5/Glv/ttnbMdtgH3p3f8gm9A1w+SyJ7/+b5GwHrcBFnl3wP5G1wyTS+1/DfcSVmCYdRurZphcWMvL/7rB83AbAHI6Pjm/6of/NXyn/ScrmcO8hsuFAwcAZcfAErjadG0AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage1 = "data:image/gif;base64,R0lGODlhEgASAMQaAHl5d66urMXFw3l5dpSUk5WVlKOjoq+vrsbGw6Sko7u7uaWlpbm5t3h4doiIhtLSz4aGhJaWlsbGxNHRzrCwr5SUkqKiobq6uNHRz4eHhf///wAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFAAAaACwAAAAAEgASAAAFaaAmjmRplstyrkmbrCNFaUZtaFF0HvyhWRZNYVgwBY4BEmFJOB1NlYpJoYBpHI7RZXtZZb4ZEbd7AodFDIYVAjFJJCYA4ISoI0hyuUnAF2geDxoDgwMnfBoYiRgaDQ1WiIqPJBMTkpYaIQAh+QQFAAAaACwBAAEAEAAQAAAFY6AmjhpFkSh5rEc6KooWzIG2LOilX3Kd/AnSjjcyGA0oBiNlsZAkEtcoEtEgrghpYVsQeAVSgpig8UpFlQrp8Ug5HCiMHEPK2DOkOR0A0NzxJBMTGnx8GhAQZwOLA2ckDQ0uIQAh+QQFAAAaACwBAAEAEAAQAAAFZKAmjpqikCh5rVc6SpLGthSFIjiiMYx2/AeSYCggBY4B1DB1JD0ertFiocFYMdGENnHFugxgg2YyiYosFhIAkIpEUOs1qUAvkAb4gcbh0BD+BCgNDRoZhhkaFRVmh4hmIxAQLiEAIfkEBQAAGgAsAQABABAAEAAABWOgJo6aJJEoiaxIOj6PJsyCpigopmNyff0X0o43AgZJk0mKwSABAK4RhaJ5PqOH7GHAHUQD4ICm0YiKwCSHI7VYoDLwDClBT5Di8khEY+gbUBAQGgWEBRoWFmYEiwRmJBUVLiEAIfkEBQAAGgAsAQABABAAEAAABWSgJo7a85Aoia1YOgKAxraShMKwNk0a4iOkgXBAEhgFqEYjZSQ5HK6RQqHJWDPRi/Zyxbq2Fw0EEhUxGKRIJEWhoArwAulAP5AIeIJmsdAE/gEoFRUaCYYJfoFRBowGZSQWFi4hACH5BAUAABoALAEAAQAQABAAAAVloCaOGgCQKGma6eg42iAP2vOgWZ5pTaNhQAxJtxsFhSQIJDWZkCKR1kgi0RSuBSliiyB4CVKBWKCpVKQiMWmxSCkUqIQ8QbrYLySD3qChUDR3eCQWFhoHhwcaDAxoAY4BaCSOLSEAIfkEBQAAGgAsAQABABAAEAAABWOgJo6a45Aoma1ZOkaRxrYAgBZ4oUGQVtckgpBAGhgHqEol1WiQFgvX6PHQJK4JKWaLMXgNWq7GYpGKJhMShZKSSFCH+IGEqCNIgXxAo1BoBIACKHkaF4YXf4JSh4hmIwwMLiEAIfkEBQAAGgAsAQABABAAEAAABWSgJo5aFJEoWaxFOi6LRsyE5jhooidaVWmZYIZkKBpIwiHJYklBICQKxTUCADSH7IFqtQa+AepgPNB8qaJGg6RQpB4P1GV+IWHuGBK9LpFo8HkkDAwaCIYIGhMTaAKNAmgkjS4hADs=";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAj0lEQVR42mNgGJSgcNbz/xQZkDvz2f/47gfkG5Ix5en/RXv/k29IQs8TsAEd6/7/d8q9QrohkV2P/k/b8v5/xpyf/9Pn/CbdkMDmh/97V38Aa9YpfPdfJ+3Gf+PIZcQb4lF1/z8oJmCaVTwmkOYC+9K7/0GuIEszCFjk3QH7myzNIGCYdZt8zSAAcjrDyAEAxeJf/+mIwdgAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAsUlEQVR42mNgGBIgvP3Bf7I1x3c/+B/ccI08A+J7Hv5fuv//f//qC6QbENF+H6w5c9b//16lJ0gzILT1Hlhz6zqg7Z1//7vkHyDegOCWu/8X7f3/vwOoOWPOz/92dT//22dsJ80FdsW3/6fP+f3fv+vbf53Cd/8tEtaTHgb6Kef/mxS9+K+S9em/ceQy8mLBIOnEf9m0r/91g+eRnw70Uy//1/Cd9p+ilKjiMeE/w/AFAFKOYJgptZ8qAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB9klEQVR42rWT3WvSURjH9yd00cUYXUREgewyCEa7WV0MCwYStaKLILoauAK3ssWSyrKtLYv1Qq6ce0mSFroKYaQtZ+0Fp7nN32oq6nQupyCkLAfWp3MnMipvduDwXHx5Ppzv83xPVdV2HL1zjSNPo+zRSMg088jvennrWaei5pY3WXbpC8geFqkzwWEztH2A/R0BnjmT/4aYZrPU6ELoXFmsiznabWvUdq9y0grqjyBTf0GK5TlxP4z8VnArrNWSIJmnTHCHNzjQvUKbE3SzsKPZhsHzm4OXZqh4Jj1jMdrtBfo88MQH1oiwpp6sDJD+UUQ7IlF3bY6BBRj0gy0Kik5XCWDxZDhqiqEYitE4mOaYMYXCGMeb+ElLf4CBb/B+FSYSoq7AuKgXeqZKAKU5xGoOPgnRHgLTInROgtISwRvOcd4QpumOhPx2QNxpGq5OMepKlADHDUv4UzA8D0bhsfamn6538b96LGwWyzX5AwmH8PVcND8Sgzr7IsO98e+VT/lQl4TW9Yudqs/0zoBKrEx2fRl3MF8Giac30b6MbgU36COcHl5HSm6w77KPc3Y4NQZ7O76iGgrxyp3ihjlI9RkHIxP/SaNDyonYLtD0GhpHoV5EWfYYajQZWvuXK7O1LF7S3DvH7os+qpVe6q9M02cLsy0/9w91ZXn2MctncwAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource first;
  private static com.google.gwt.resources.client.ImageResource last;
  private static com.google.gwt.resources.client.ImageResource loading;
  private static com.google.gwt.resources.client.ImageResource next;
  private static com.google.gwt.resources.client.ImageResource prev;
  private static com.google.gwt.resources.client.ImageResource refresh;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      first(), 
      last(), 
      loading(), 
      next(), 
      prev(), 
      refresh(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("first", first());
        resourceMap.put("last", last());
        resourceMap.put("loading", loading());
        resourceMap.put("next", next());
        resourceMap.put("prev", prev());
        resourceMap.put("refresh", refresh());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'first': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::first()();
      case 'last': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::last()();
      case 'loading': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::loading()();
      case 'next': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::next()();
      case 'prev': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::prev()();
      case 'refresh': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::refresh()();
    }
    return null;
  }-*/;
}
