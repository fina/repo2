package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FilterConfigBean_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.FilterConfigBean[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.sencha.gxt.data.shared.loader.FilterConfigBean[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new com.sencha.gxt.data.shared.loader.FilterConfigBean[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.FilterConfigBean[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.FilterConfigBean_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterConfigBean_Array_Rank_1_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.FilterConfigBean[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterConfigBean_Array_Rank_1_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.FilterConfigBean[])object);
  }
  
}
