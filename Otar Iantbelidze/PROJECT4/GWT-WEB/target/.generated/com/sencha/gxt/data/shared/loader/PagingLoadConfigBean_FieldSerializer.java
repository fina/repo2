package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class PagingLoadConfigBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getLimit(com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.PagingLoadConfigBean::limit;
  }-*/;
  
  private static native void setLimit(com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance, int value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.PagingLoadConfigBean::limit = value;
  }-*/;
  
  private static native int getOffset(com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.PagingLoadConfigBean::offset;
  }-*/;
  
  private static native void setOffset(com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance, int value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.PagingLoadConfigBean::offset = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance) throws SerializationException {
    setLimit(instance, streamReader.readInt());
    setOffset(instance, streamReader.readInt());
    
    com.sencha.gxt.data.shared.loader.ListLoadConfigBean_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.PagingLoadConfigBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.PagingLoadConfigBean instance) throws SerializationException {
    streamWriter.writeInt(getLimit(instance));
    streamWriter.writeInt(getOffset(instance));
    
    com.sencha.gxt.data.shared.loader.ListLoadConfigBean_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.PagingLoadConfigBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.PagingLoadConfigBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.PagingLoadConfigBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.PagingLoadConfigBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.PagingLoadConfigBean)object);
  }
  
}
