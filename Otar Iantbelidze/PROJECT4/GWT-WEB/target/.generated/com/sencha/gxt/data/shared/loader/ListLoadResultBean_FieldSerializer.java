package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListLoadResultBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.ListLoadResultBean instance) throws SerializationException {
    instance.list = (java.util.List) streamReader.readObject();
    
  }
  
  public static com.sencha.gxt.data.shared.loader.ListLoadResultBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.ListLoadResultBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.ListLoadResultBean instance) throws SerializationException {
    streamWriter.writeObject(instance.list);
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.ListLoadResultBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.ListLoadResultBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.ListLoadResultBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.ListLoadResultBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.ListLoadResultBean)object);
  }
  
}
