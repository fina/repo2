package com.sencha.gxt.widget.core.client.grid;

public class ColumnHeader_ColumnHeaderStyles_headerInner_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.sencha.gxt.widget.core.client.grid.ColumnHeader.ColumnHeaderStyles, java.lang.String> {
  public static final ColumnHeader_ColumnHeaderStyles_headerInner_ValueProviderImpl INSTANCE = new ColumnHeader_ColumnHeaderStyles_headerInner_ValueProviderImpl();
  public java.lang.String getValue(com.sencha.gxt.widget.core.client.grid.ColumnHeader.ColumnHeaderStyles object) {
    return object.headerInner();
  }
  public void setValue(com.sencha.gxt.widget.core.client.grid.ColumnHeader.ColumnHeaderStyles object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "headerInner";
  }
}
