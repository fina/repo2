package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface FieldSetDefaultAppearance_Template_render_SafeHtml__FieldSetStyle_style__boolean_isGecko___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("<span class=\"{0}\"></span>")
  SafeHtml render0(java.lang.String arg0);
  @Template("<div class=\"{0}\"></div>")
  SafeHtml render1(java.lang.String arg0);
  @Template("<fieldset class=\"{0}\"><legend class=\"{1}\">\n  {2}\n  {3}\n  <span class=\"{4}\"></span></legend><div class=\"{5}\"></div></fieldset>")
  SafeHtml render2(java.lang.String arg0, java.lang.String arg1, com.google.gwt.safehtml.shared.SafeHtml arg2, com.google.gwt.safehtml.shared.SafeHtml arg3, java.lang.String arg4, java.lang.String arg5);
}
