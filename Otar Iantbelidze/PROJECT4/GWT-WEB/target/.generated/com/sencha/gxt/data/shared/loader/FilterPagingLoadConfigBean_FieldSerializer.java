package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FilterPagingLoadConfigBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getFilterConfigs(com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean::filterConfigs;
  }-*/;
  
  private static native void setFilterConfigs(com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean instance, java.util.List value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean::filterConfigs = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean instance) throws SerializationException {
    setFilterConfigs(instance, (java.util.List) streamReader.readObject());
    
    com.sencha.gxt.data.shared.loader.PagingLoadConfigBean_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean instance) throws SerializationException {
    streamWriter.writeObject(getFilterConfigs(instance));
    
    com.sencha.gxt.data.shared.loader.PagingLoadConfigBean_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean)object);
  }
  
}
