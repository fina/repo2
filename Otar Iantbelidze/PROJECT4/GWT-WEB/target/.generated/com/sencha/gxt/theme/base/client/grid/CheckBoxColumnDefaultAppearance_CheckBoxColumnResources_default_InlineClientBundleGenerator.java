package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources {
  private static CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator _instance0 = new CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator();
  private void checkedInitializer() {
    checked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "checked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 16, 16, false, false
    );
  }
  private static class checkedInitializer {
    static {
      _instance0.checkedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return checked;
    }
  }
  public com.google.gwt.resources.client.ImageResource checked() {
    return checkedInitializer.get();
  }
  private void specialColumnInitializer() {
    specialColumn = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumn",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnInitializer {
    static {
      _instance0.specialColumnInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumn;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumn() {
    return specialColumnInitializer.get();
  }
  private void specialColumnSelectedInitializer() {
    specialColumnSelected = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumnSelected",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnSelectedInitializer {
    static {
      _instance0.specialColumnSelectedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumnSelected;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumnSelected() {
    return specialColumnSelectedInitializer.get();
  }
  private void uncheckedInitializer() {
    unchecked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "unchecked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 16, 16, false, false
    );
  }
  private static class uncheckedInitializer {
    static {
      _instance0.uncheckedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return unchecked;
    }
  }
  public com.google.gwt.resources.client.ImageResource unchecked() {
    return uncheckedInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDI2{padding:" + ("2px"+ " " +"2px"+ " " +"0"+ " " +"0")  + " !important;}.GCGCW0WDJ2{padding:" + ("0")  + " !important;}.GCGCW0WDMFC .GCGCW0WDI2{width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getTop() + "px  repeat-y")  + ";vertical-align:" + ("top")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDPFC .GCGCW0WDI2{width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDM2,.GCGCW0WDK2{height:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getHeight() + "px")  + ";width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getTop() + "px  no-repeat")  + ";height:" + ("18px")  + ";}.GCGCW0WDPFC .GCGCW0WDM2,.GCGCW0WDL2 .GCGCW0WDK2{height:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getHeight() + "px")  + ";width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getTop() + "px  no-repeat")  + ";height:" + ("18px")  + ";}.GCGCW0WDM2{background-repeat:" + ("no-repeat")  + ";background-color:" + ("transparent")  + ";}.GCGCW0WDK2{background-repeat:" + ("no-repeat")  + ";background-color:" + ("transparent")  + ";padding-bottom:" + ("0")  + " !important;}")) : ((".GCGCW0WDI2{padding:" + ("2px"+ " " +"0"+ " " +"0"+ " " +"2px")  + " !important;}.GCGCW0WDJ2{padding:" + ("0")  + " !important;}.GCGCW0WDMFC .GCGCW0WDI2{width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumn()).getTop() + "px  repeat-y")  + ";vertical-align:" + ("top")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDPFC .GCGCW0WDI2{width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GCGCW0WDM2,.GCGCW0WDK2{height:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getHeight() + "px")  + ";width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.unchecked()).getTop() + "px  no-repeat")  + ";height:" + ("18px")  + ";}.GCGCW0WDPFC .GCGCW0WDM2,.GCGCW0WDL2 .GCGCW0WDK2{height:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getHeight() + "px")  + ";width:" + ((CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getSafeUri().asString() + "\") -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getLeft() + "px -" + (CheckBoxColumnDefaultAppearance_CheckBoxColumnResources_default_InlineClientBundleGenerator.this.checked()).getTop() + "px  no-repeat")  + ";height:" + ("18px")  + ";}.GCGCW0WDM2{background-repeat:" + ("no-repeat")  + ";background-color:" + ("transparent")  + ";}.GCGCW0WDK2{background-repeat:" + ("no-repeat")  + ";background-color:" + ("transparent")  + ";padding-bottom:" + ("0")  + " !important;}"));
      }
      public java.lang.String cell() {
        return "GCGCW0WDI2";
      }
      public java.lang.String cellInner() {
        return "GCGCW0WDJ2";
      }
      public java.lang.String hdChecker() {
        return "GCGCW0WDK2";
      }
      public java.lang.String headerChecked() {
        return "GCGCW0WDL2";
      }
      public java.lang.String rowChecker() {
        return "GCGCW0WDM2";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABW0lEQVR42s2Ty0sCURTG/d+KstoHUZpQBC2CCLJdyzZFRpTmwkWUCAUWtKixwiIKn4xCdygdX0ONjS96jMNMm6+ZC7nInAo3HfjgXi7nd875ONdi+Rex6nLhr2oDyLLcUaqqQtM0KIpC7x0BwSOmTdFECr7ABZxL++ALD2g2m+aAOEtaSqQ41BsvGJ72oWdkHUw4Rbv4FSCZ5lAURBycsOjVkyedfpTLTz+PQO5zKAiPIHc8Xt9k2Ga30Te6gbMrDlmeNwdcR5OIsjnsBCN66w2ELgmsNjcm5v3USEI4c0C+WMLM4h6tyN4KmFoIYHDcg+MwobPzubw5IMmmkS9J6B/bhNXupsmOuV2o2jt9LwmCOeAmEkOl9oxl7ykG7B4MObZwyKRbOyCKojkgFk8gk8nqJkpY8Yaw5jvXjVRQq9dRrdYgSRVzwKeMZTGqGsYZ56+b+S2gq7/QTXwA3ngd6xuLHCkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAANklEQVR42rXNqREAIAwEQPqvLn2QH0xUmDmBxiBW76iqfhG5wWNd5glq0aIOLNaTFYiox+/gACe+uBEe1wRCAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAAXUlEQVR42rXNvQ6CMBSA0b7/o/AsDgSiRmIwKNTW8iOaMPTe5qOzu8OZjwnfxC//Sbg1Yd/KsCh9dp+VblJuo9IG5foSGi9cnHB+CqfsaIV6iFR9pHxEisOG+XewA9F9tJvfRlttAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABBElEQVR42s2T3UrDQBBG+/4X9k0UtKC11dKqVWyDrW7a2E0aN39NdhMSQpLbz3QvAqJuK7lx4LAwMGeGYbbT+Rdx0u3ir3wTFEVxNL8KTi/6P3LWu0avf4PB7fiwYPaif2HxZkA3TFDLxvju4XiBtlzhVd/AoFtYNoPjepg+Ph0WaMs1FuQdZE1lV+a48IMAYRhirmlqwfnlUI67MW18MFcWxXGMJEkkRF+pBVeDkRzX831wzpGmKfI8bzCtrVowHE3gej6iKJId90X7fFmW8mWOoxZM7qdNcZZlMldVVUNQ70IpeJ7NQYgOSs16BwzBbgcuBES9ByHiWs7VglaX2OovtIlPX7c5S7fSRyEAAAAASUVORK5CYII=";
  private static com.google.gwt.resources.client.ImageResource checked;
  private static com.google.gwt.resources.client.ImageResource specialColumn;
  private static com.google.gwt.resources.client.ImageResource specialColumnSelected;
  private static com.google.gwt.resources.client.ImageResource unchecked;
  private static com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      checked(), 
      specialColumn(), 
      specialColumnSelected(), 
      unchecked(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("checked", checked());
        resourceMap.put("specialColumn", specialColumn());
        resourceMap.put("specialColumnSelected", specialColumnSelected());
        resourceMap.put("unchecked", unchecked());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'checked': return this.@com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources::checked()();
      case 'specialColumn': return this.@com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources::specialColumn()();
      case 'specialColumnSelected': return this.@com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources::specialColumnSelected()();
      case 'unchecked': return this.@com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources::unchecked()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance.CheckBoxColumnResources::style()();
    }
    return null;
  }-*/;
}
