package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FilterConfigBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getComparison(com.sencha.gxt.data.shared.loader.FilterConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::comparison;
  }-*/;
  
  private static native void setComparison(com.sencha.gxt.data.shared.loader.FilterConfigBean instance, java.lang.String value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::comparison = value;
  }-*/;
  
  private static native java.lang.String getField(com.sencha.gxt.data.shared.loader.FilterConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::field;
  }-*/;
  
  private static native void setField(com.sencha.gxt.data.shared.loader.FilterConfigBean instance, java.lang.String value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::field = value;
  }-*/;
  
  private static native java.lang.String getType(com.sencha.gxt.data.shared.loader.FilterConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::type;
  }-*/;
  
  private static native void setType(com.sencha.gxt.data.shared.loader.FilterConfigBean instance, java.lang.String value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::type = value;
  }-*/;
  
  private static native java.lang.String getValue(com.sencha.gxt.data.shared.loader.FilterConfigBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::value;
  }-*/;
  
  private static native void setValue(com.sencha.gxt.data.shared.loader.FilterConfigBean instance, java.lang.String value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.FilterConfigBean::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.FilterConfigBean instance) throws SerializationException {
    setComparison(instance, streamReader.readString());
    setField(instance, streamReader.readString());
    setType(instance, streamReader.readString());
    setValue(instance, streamReader.readString());
    
  }
  
  public static com.sencha.gxt.data.shared.loader.FilterConfigBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.FilterConfigBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.FilterConfigBean instance) throws SerializationException {
    streamWriter.writeString(getComparison(instance));
    streamWriter.writeString(getField(instance));
    streamWriter.writeString(getType(instance));
    streamWriter.writeString(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.FilterConfigBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterConfigBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.FilterConfigBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.FilterConfigBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.FilterConfigBean)object);
  }
  
}
