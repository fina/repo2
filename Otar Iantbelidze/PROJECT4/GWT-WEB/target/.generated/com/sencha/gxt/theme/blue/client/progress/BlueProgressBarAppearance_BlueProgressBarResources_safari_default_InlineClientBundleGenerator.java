package com.sencha.gxt.theme.blue.client.progress;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.progress.BlueProgressBarAppearance.BlueProgressBarResources {
  private static BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator _instance0 = new BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator();
  private void barInitializer() {
    bar = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bar",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 1, 96, false, false
    );
  }
  private static class barInitializer {
    static {
      _instance0.barInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bar;
    }
  }
  public com.google.gwt.resources.client.ImageResource bar() {
    return barInitializer.get();
  }
  private void innerBarInitializer() {
    innerBar = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "innerBar",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 9, 78, false, false
    );
  }
  private static class innerBarInitializer {
    static {
      _instance0.innerBarInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return innerBar;
    }
  }
  public com.google.gwt.resources.client.ImageResource innerBar() {
    return innerBarInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCGCW0WDNDB{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";border-color:" + ("#6593cf")  + ";border-color:" + ("#6593cf")  + ";}.GCGCW0WDKDB{height:" + ("18px")  + ";background:" + ("repeat-x")  + ";position:" + ("relative")  + ";}.GCGCW0WDJDB{height:" + ("18px")  + ";width:" + ("0")  + ";border-top:" + ("1px"+ " " +"solid")  + ";border-bottom:") + (("1px"+ " " +"solid")  + ";border-left:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDLDB{padding:" + ("1px"+ " " +"5px")  + ";overflow:" + ("hidden")  + ";position:" + ("absolute")  + ";right:" + ("0")  + ";text-align:" + ("center")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";color:" + ("#fff")  + ";}.GCGCW0WDJDB .GCGCW0WDLDB{z-index:" + ("99") ) + (";}.GCGCW0WDMDB{z-index:" + ("9")  + " !important;color:" + ("#396095")  + " !important;}.GCGCW0WDKDB{height:" + ((BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getSafeUri().asString() + "\") -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getLeft() + "px -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getTop() + "px  repeat-x")  + ";background-color:" + ("#e0e8f3")  + ";height:" + ("auto")  + ";}.GCGCW0WDJDB{height:" + ((BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getSafeUri().asString() + "\") -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getLeft() + "px -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getTop() + "px  repeat-x")  + ";background-color:") + (("#9cbfee")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("right"+ " " +"center")  + ";height:" + ("18px")  + ";border-top-color:" + ("#d1e4fd")  + ";border-bottom-color:" + ("#7fa9e4")  + ";border-left-color:" + ("#7fa9e4")  + ";}")) : ((".GCGCW0WDNDB{border:" + ("1px"+ " " +"solid")  + ";overflow:" + ("hidden")  + ";border-color:" + ("#6593cf")  + ";border-color:" + ("#6593cf")  + ";}.GCGCW0WDKDB{height:" + ("18px")  + ";background:" + ("repeat-x")  + ";position:" + ("relative")  + ";}.GCGCW0WDJDB{height:" + ("18px")  + ";width:" + ("0")  + ";border-top:" + ("1px"+ " " +"solid")  + ";border-bottom:") + (("1px"+ " " +"solid")  + ";border-right:" + ("1px"+ " " +"solid")  + ";}.GCGCW0WDLDB{padding:" + ("1px"+ " " +"5px")  + ";overflow:" + ("hidden")  + ";position:" + ("absolute")  + ";left:" + ("0")  + ";text-align:" + ("center")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";color:" + ("#fff")  + ";}.GCGCW0WDJDB .GCGCW0WDLDB{z-index:" + ("99") ) + (";}.GCGCW0WDMDB{z-index:" + ("9")  + " !important;color:" + ("#396095")  + " !important;}.GCGCW0WDKDB{height:" + ((BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getSafeUri().asString() + "\") -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getLeft() + "px -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.innerBar()).getTop() + "px  repeat-x")  + ";background-color:" + ("#e0e8f3")  + ";height:" + ("auto")  + ";}.GCGCW0WDJDB{height:" + ((BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getSafeUri().asString() + "\") -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getLeft() + "px -" + (BlueProgressBarAppearance_BlueProgressBarResources_safari_default_InlineClientBundleGenerator.this.bar()).getTop() + "px  repeat-x")  + ";background-color:") + (("#9cbfee")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("left"+ " " +"center")  + ";height:" + ("18px")  + ";border-top-color:" + ("#d1e4fd")  + ";border-bottom-color:" + ("#7fa9e4")  + ";border-right-color:" + ("#7fa9e4")  + ";}"));
      }
      public java.lang.String progressBar() {
        return "GCGCW0WDJDB";
      }
      public java.lang.String progressInner() {
        return "GCGCW0WDKDB";
      }
      public java.lang.String progressText() {
        return "GCGCW0WDLDB";
      }
      public java.lang.String progressTextBack() {
        return "GCGCW0WDMDB";
      }
      public java.lang.String progressWrap() {
        return "GCGCW0WDNDB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABgCAYAAADcvRh2AAAAYElEQVR42mPYfuX3f4bhQmw4//M/w4qT3/8zLDjy9T/DzP2f/zNM2PnxP0Pbpnf/GWpWv/nPULzwwX+GyqWP/jPUr3zyn6F13bP/DF2bXgDVbXv1n2HaLqCSOfvfDRsCAGJuQ3fITtlJAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAABOCAYAAADyzDzEAAABNUlEQVR42qXWcW+CQAwF8Pf9v+TcdBNFUYEDDu7oKssS9we8l8zk1zSxGhSuLUx44VC2xuDzEozRir6uwRgcPTAoqs4Yreh064zB+d4bg9IDg/LhCYGLBwbXejAGlQcGVeMJgVsTjcG9jcbgEaIxqMNoDOrOEwKNBwZtPxqD0E/GoBs8IdDFyZ6FW9DHZAyGMRmD6CHSoinbj7QKoxcwmFI2BslDokV5NgbZA4N5nu1XXvGnaO0DYN+SlWtaLlz6ddKfufbG+EK7d1t3fzGqD51UJD3j7KQspyUo5046wVIvkLqK1J+kTif1TKn7Sn1cmgjSbJGmlDTv6ORUZvAyqDcnuY/7J20v+NcacnyhbT3S/iRtYoeLL3cE9r79MdifPSHw4YHB+6kxBjsPDHaFJwTeitqYb/0jlN71rHJRAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource bar;
  private static com.google.gwt.resources.client.ImageResource innerBar;
  private static com.sencha.gxt.theme.base.client.progress.ProgressBarDefaultAppearance.ProgressBarStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      bar(), 
      innerBar(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("bar", bar());
        resourceMap.put("innerBar", innerBar());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'bar': return this.@com.sencha.gxt.theme.blue.client.progress.BlueProgressBarAppearance.BlueProgressBarResources::bar()();
      case 'innerBar': return this.@com.sencha.gxt.theme.blue.client.progress.BlueProgressBarAppearance.BlueProgressBarResources::innerBar()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.progress.BlueProgressBarAppearance.BlueProgressBarResources::style()();
    }
    return null;
  }-*/;
}
