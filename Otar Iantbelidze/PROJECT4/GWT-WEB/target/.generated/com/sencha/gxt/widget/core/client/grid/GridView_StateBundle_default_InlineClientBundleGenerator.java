package com.sencha.gxt.widget.core.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class GridView_StateBundle_default_InlineClientBundleGenerator implements com.sencha.gxt.widget.core.client.grid.GridView.StateBundle {
  private static GridView_StateBundle_default_InlineClientBundleGenerator _instance0 = new GridView_StateBundle_default_InlineClientBundleGenerator();
  private void stylesInitializer() {
    styles = new com.sencha.gxt.widget.core.client.grid.GridView.GridStateStyles() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "styles";
      }
      public String getText() {
        return ("");
      }
      public java.lang.String cell() {
        return "GCGCW0WDJFC";
      }
      public java.lang.String cellInner() {
        return "GCGCW0WDKFC";
      }
      public java.lang.String cellSelected() {
        return "GCGCW0WDLFC";
      }
      public java.lang.String row() {
        return "GCGCW0WDMFC";
      }
      public java.lang.String rowBody() {
        return "GCGCW0WDNFC";
      }
      public java.lang.String rowBodyRow() {
        return "GCGCW0WDOFC";
      }
      public java.lang.String rowSelected() {
        return "GCGCW0WDPFC";
      }
      public java.lang.String rowWrap() {
        return "GCGCW0WDAGC";
      }
    }
    ;
  }
  private static class stylesInitializer {
    static {
      _instance0.stylesInitializer();
    }
    static com.sencha.gxt.widget.core.client.grid.GridView.GridStateStyles get() {
      return styles;
    }
  }
  public com.sencha.gxt.widget.core.client.grid.GridView.GridStateStyles styles() {
    return stylesInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.widget.core.client.grid.GridView.GridStateStyles styles;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      styles(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("styles", styles());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'styles': return this.@com.sencha.gxt.widget.core.client.grid.GridView.StateBundle::styles()();
    }
    return null;
  }-*/;
}
