package com.sencha.gxt.theme.base.client.grid;

public class CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderCell_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplatesImpl implements com.sencha.gxt.theme.base.client.grid.CheckBoxColumnDefaultAppearance_CheckBoxColumnTemplates_renderCell_SafeHtml__CheckBoxColumnStyle_style___SafeHtmlTemplates {
  
  public com.google.gwt.safehtml.shared.SafeHtml renderCell0(java.lang.String arg0) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class='");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("'>&#160;</div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
