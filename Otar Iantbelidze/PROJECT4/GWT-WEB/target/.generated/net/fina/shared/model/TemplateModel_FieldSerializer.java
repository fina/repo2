package net.fina.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TemplateModel_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCode(net.fina.shared.model.TemplateModel instance) /*-{
    return instance.@net.fina.shared.model.TemplateModel::code;
  }-*/;
  
  private static native void setCode(net.fina.shared.model.TemplateModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.TemplateModel::code = value;
  }-*/;
  
  private static native int getId(net.fina.shared.model.TemplateModel instance) /*-{
    return instance.@net.fina.shared.model.TemplateModel::id;
  }-*/;
  
  private static native void setId(net.fina.shared.model.TemplateModel instance, int value) 
  /*-{
    instance.@net.fina.shared.model.TemplateModel::id = value;
  }-*/;
  
  private static native java.lang.String getName(net.fina.shared.model.TemplateModel instance) /*-{
    return instance.@net.fina.shared.model.TemplateModel::name;
  }-*/;
  
  private static native void setName(net.fina.shared.model.TemplateModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.TemplateModel::name = value;
  }-*/;
  
  private static native net.fina.shared.model.ScheduleModel getScheduleModel(net.fina.shared.model.TemplateModel instance) /*-{
    return instance.@net.fina.shared.model.TemplateModel::scheduleModel;
  }-*/;
  
  private static native void setScheduleModel(net.fina.shared.model.TemplateModel instance, net.fina.shared.model.ScheduleModel value) 
  /*-{
    instance.@net.fina.shared.model.TemplateModel::scheduleModel = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.TemplateModel instance) throws SerializationException {
    setCode(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setScheduleModel(instance, (net.fina.shared.model.ScheduleModel) streamReader.readObject());
    
  }
  
  public static net.fina.shared.model.TemplateModel instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.TemplateModel();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.TemplateModel instance) throws SerializationException {
    streamWriter.writeString(getCode(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getScheduleModel(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.TemplateModel_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.TemplateModel_FieldSerializer.deserialize(reader, (net.fina.shared.model.TemplateModel)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.TemplateModel_FieldSerializer.serialize(writer, (net.fina.shared.model.TemplateModel)object);
  }
  
}
