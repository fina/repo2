package net.fina.shared.model;

public class UploadFileModel_fileName_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.UploadFileModel, java.lang.String> {
  public static final UploadFileModel_fileName_ValueProviderImpl INSTANCE = new UploadFileModel_fileName_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.UploadFileModel object) {
    return object.getFileName();
  }
  public void setValue(net.fina.shared.model.UploadFileModel object, java.lang.String value) {
    object.setFileName(value);
  }
  public String getPath() {
    return "fileName";
  }
}
