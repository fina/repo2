package net.fina.shared.model;

public class UploadFileModel_id_ModelKeyProviderImpl implements com.sencha.gxt.data.shared.ModelKeyProvider<net.fina.shared.model.UploadFileModel> {
  public static final UploadFileModel_id_ModelKeyProviderImpl INSTANCE = new UploadFileModel_id_ModelKeyProviderImpl();
  public String getKey(net.fina.shared.model.UploadFileModel item) {
    return "" + item.getId();
  }
}
