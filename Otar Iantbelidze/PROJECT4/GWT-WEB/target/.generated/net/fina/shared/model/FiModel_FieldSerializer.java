package net.fina.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FiModel_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAddress(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::address;
  }-*/;
  
  private static native void setAddress(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::address = value;
  }-*/;
  
  private static native java.lang.String getCode(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::code;
  }-*/;
  
  private static native void setCode(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::code = value;
  }-*/;
  
  private static native java.lang.String getFax(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::fax;
  }-*/;
  
  private static native void setFax(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::fax = value;
  }-*/;
  
  private static native int getId(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::id;
  }-*/;
  
  private static native void setId(net.fina.shared.model.FiModel instance, int value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::id = value;
  }-*/;
  
  private static native java.lang.String getMail(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::mail;
  }-*/;
  
  private static native void setMail(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::mail = value;
  }-*/;
  
  private static native java.lang.String getName(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::name;
  }-*/;
  
  private static native void setName(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::name = value;
  }-*/;
  
  private static native java.lang.String getPhone(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::phone;
  }-*/;
  
  private static native void setPhone(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::phone = value;
  }-*/;
  
  private static native java.util.Date getReg_date(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::reg_date;
  }-*/;
  
  private static native void setReg_date(net.fina.shared.model.FiModel instance, java.util.Date value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::reg_date = value;
  }-*/;
  
  private static native java.util.List getTemplateModelList(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::templateModelList;
  }-*/;
  
  private static native void setTemplateModelList(net.fina.shared.model.FiModel instance, java.util.List value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::templateModelList = value;
  }-*/;
  
  private static native net.fina.shared.model.FiTypeModel getType(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::type;
  }-*/;
  
  private static native void setType(net.fina.shared.model.FiModel instance, net.fina.shared.model.FiTypeModel value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::type = value;
  }-*/;
  
  private static native java.lang.String getTypeString(net.fina.shared.model.FiModel instance) /*-{
    return instance.@net.fina.shared.model.FiModel::typeString;
  }-*/;
  
  private static native void setTypeString(net.fina.shared.model.FiModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiModel::typeString = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.FiModel instance) throws SerializationException {
    setAddress(instance, streamReader.readString());
    setCode(instance, streamReader.readString());
    setFax(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setMail(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setPhone(instance, streamReader.readString());
    setReg_date(instance, (java.util.Date) streamReader.readObject());
    setTemplateModelList(instance, (java.util.List) streamReader.readObject());
    setType(instance, (net.fina.shared.model.FiTypeModel) streamReader.readObject());
    setTypeString(instance, streamReader.readString());
    
  }
  
  public static net.fina.shared.model.FiModel instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.FiModel();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.FiModel instance) throws SerializationException {
    streamWriter.writeString(getAddress(instance));
    streamWriter.writeString(getCode(instance));
    streamWriter.writeString(getFax(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getMail(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getPhone(instance));
    streamWriter.writeObject(getReg_date(instance));
    streamWriter.writeObject(getTemplateModelList(instance));
    streamWriter.writeObject(getType(instance));
    streamWriter.writeString(getTypeString(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.FiModel_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.FiModel_FieldSerializer.deserialize(reader, (net.fina.shared.model.FiModel)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.FiModel_FieldSerializer.serialize(writer, (net.fina.shared.model.FiModel)object);
  }
  
}
