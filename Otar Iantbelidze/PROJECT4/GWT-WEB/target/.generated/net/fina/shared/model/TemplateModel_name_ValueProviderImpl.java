package net.fina.shared.model;

public class TemplateModel_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.TemplateModel, java.lang.String> {
  public static final TemplateModel_name_ValueProviderImpl INSTANCE = new TemplateModel_name_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.TemplateModel object) {
    return object.getName();
  }
  public void setValue(net.fina.shared.model.TemplateModel object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
