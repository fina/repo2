package net.fina.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UploadFileModel_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getScheduleDate(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::ScheduleDate;
  }-*/;
  
  private static native void setScheduleDate(net.fina.shared.model.UploadFileModel instance, java.util.Date value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::ScheduleDate = value;
  }-*/;
  
  private static native net.fina.shared.model.FiModel getFiModel(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::fiModel;
  }-*/;
  
  private static native void setFiModel(net.fina.shared.model.UploadFileModel instance, net.fina.shared.model.FiModel value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::fiModel = value;
  }-*/;
  
  private static native java.lang.String getFileName(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::fileName;
  }-*/;
  
  private static native void setFileName(net.fina.shared.model.UploadFileModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::fileName = value;
  }-*/;
  
  private static native int getId(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::id;
  }-*/;
  
  private static native void setId(net.fina.shared.model.UploadFileModel instance, int value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::id = value;
  }-*/;
  
  private static native net.fina.shared.model.TemplateModel getTemplateModel(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::templateModel;
  }-*/;
  
  private static native void setTemplateModel(net.fina.shared.model.UploadFileModel instance, net.fina.shared.model.TemplateModel value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::templateModel = value;
  }-*/;
  
  private static native java.util.Date getUploadDate(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::uploadDate;
  }-*/;
  
  private static native void setUploadDate(net.fina.shared.model.UploadFileModel instance, java.util.Date value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::uploadDate = value;
  }-*/;
  
  private static native byte[] getUploadedFile(net.fina.shared.model.UploadFileModel instance) /*-{
    return instance.@net.fina.shared.model.UploadFileModel::uploadedFile;
  }-*/;
  
  private static native void setUploadedFile(net.fina.shared.model.UploadFileModel instance, byte[] value) 
  /*-{
    instance.@net.fina.shared.model.UploadFileModel::uploadedFile = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.UploadFileModel instance) throws SerializationException {
    setScheduleDate(instance, (java.util.Date) streamReader.readObject());
    setFiModel(instance, (net.fina.shared.model.FiModel) streamReader.readObject());
    setFileName(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setTemplateModel(instance, (net.fina.shared.model.TemplateModel) streamReader.readObject());
    setUploadDate(instance, (java.util.Date) streamReader.readObject());
    setUploadedFile(instance, (byte[]) streamReader.readObject());
    
  }
  
  public static net.fina.shared.model.UploadFileModel instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.UploadFileModel();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.UploadFileModel instance) throws SerializationException {
    streamWriter.writeObject(getScheduleDate(instance));
    streamWriter.writeObject(getFiModel(instance));
    streamWriter.writeString(getFileName(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeObject(getTemplateModel(instance));
    streamWriter.writeObject(getUploadDate(instance));
    streamWriter.writeObject(getUploadedFile(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.UploadFileModel_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.UploadFileModel_FieldSerializer.deserialize(reader, (net.fina.shared.model.UploadFileModel)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.UploadFileModel_FieldSerializer.serialize(writer, (net.fina.shared.model.UploadFileModel)object);
  }
  
}
