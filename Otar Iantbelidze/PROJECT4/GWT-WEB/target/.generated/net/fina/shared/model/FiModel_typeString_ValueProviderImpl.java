package net.fina.shared.model;

public class FiModel_typeString_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_typeString_ValueProviderImpl INSTANCE = new FiModel_typeString_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getTypeString();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setTypeString(value);
  }
  public String getPath() {
    return "typeString";
  }
}
