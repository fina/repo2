package net.fina.shared.model;

public class UploadFileModel_uploadDate_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.UploadFileModel, java.util.Date> {
  public static final UploadFileModel_uploadDate_ValueProviderImpl INSTANCE = new UploadFileModel_uploadDate_ValueProviderImpl();
  public java.util.Date getValue(net.fina.shared.model.UploadFileModel object) {
    return object.getUploadDate();
  }
  public void setValue(net.fina.shared.model.UploadFileModel object, java.util.Date value) {
    object.setUploadDate(value);
  }
  public String getPath() {
    return "uploadDate";
  }
}
