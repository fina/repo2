package net.fina.shared.model;

public class FiModel_phone_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_phone_ValueProviderImpl INSTANCE = new FiModel_phone_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getPhone();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setPhone(value);
  }
  public String getPath() {
    return "phone";
  }
}
