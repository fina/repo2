package net.fina.shared.model;

public class FiModel_mail_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_mail_ValueProviderImpl INSTANCE = new FiModel_mail_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getMail();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setMail(value);
  }
  public String getPath() {
    return "mail";
  }
}
