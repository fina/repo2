package net.fina.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TemplateModel_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.TemplateModel[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static net.fina.shared.model.TemplateModel[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new net.fina.shared.model.TemplateModel[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.TemplateModel[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.TemplateModel_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.TemplateModel_Array_Rank_1_FieldSerializer.deserialize(reader, (net.fina.shared.model.TemplateModel[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.TemplateModel_Array_Rank_1_FieldSerializer.serialize(writer, (net.fina.shared.model.TemplateModel[])object);
  }
  
}
