package net.fina.shared.model;

public class FiTypeModel_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiTypeModel, java.lang.String> {
  public static final FiTypeModel_name_ValueProviderImpl INSTANCE = new FiTypeModel_name_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiTypeModel object) {
    return object.getName();
  }
  public void setValue(net.fina.shared.model.FiTypeModel object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
