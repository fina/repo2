package net.fina.shared.model.Exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FieldUniqueException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.Exception.FieldUniqueException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static net.fina.shared.model.Exception.FieldUniqueException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.Exception.FieldUniqueException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.Exception.FieldUniqueException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.Exception.FieldUniqueException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.Exception.FieldUniqueException_FieldSerializer.deserialize(reader, (net.fina.shared.model.Exception.FieldUniqueException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.Exception.FieldUniqueException_FieldSerializer.serialize(writer, (net.fina.shared.model.Exception.FieldUniqueException)object);
  }
  
}
