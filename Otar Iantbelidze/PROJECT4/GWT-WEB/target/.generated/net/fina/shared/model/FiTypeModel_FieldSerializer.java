package net.fina.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FiTypeModel_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCode(net.fina.shared.model.FiTypeModel instance) /*-{
    return instance.@net.fina.shared.model.FiTypeModel::code;
  }-*/;
  
  private static native void setCode(net.fina.shared.model.FiTypeModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiTypeModel::code = value;
  }-*/;
  
  private static native int getId(net.fina.shared.model.FiTypeModel instance) /*-{
    return instance.@net.fina.shared.model.FiTypeModel::id;
  }-*/;
  
  private static native void setId(net.fina.shared.model.FiTypeModel instance, int value) 
  /*-{
    instance.@net.fina.shared.model.FiTypeModel::id = value;
  }-*/;
  
  private static native java.lang.String getName(net.fina.shared.model.FiTypeModel instance) /*-{
    return instance.@net.fina.shared.model.FiTypeModel::name;
  }-*/;
  
  private static native void setName(net.fina.shared.model.FiTypeModel instance, java.lang.String value) 
  /*-{
    instance.@net.fina.shared.model.FiTypeModel::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.FiTypeModel instance) throws SerializationException {
    setCode(instance, streamReader.readString());
    setId(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    
  }
  
  public static net.fina.shared.model.FiTypeModel instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.FiTypeModel();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.FiTypeModel instance) throws SerializationException {
    streamWriter.writeString(getCode(instance));
    streamWriter.writeInt(getId(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.FiTypeModel_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.FiTypeModel_FieldSerializer.deserialize(reader, (net.fina.shared.model.FiTypeModel)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.FiTypeModel_FieldSerializer.serialize(writer, (net.fina.shared.model.FiTypeModel)object);
  }
  
}
