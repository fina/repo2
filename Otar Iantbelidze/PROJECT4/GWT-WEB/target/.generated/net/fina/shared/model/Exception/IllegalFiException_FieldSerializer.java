package net.fina.shared.model.Exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class IllegalFiException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, net.fina.shared.model.Exception.IllegalFiException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.io.IOException_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static net.fina.shared.model.Exception.IllegalFiException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new net.fina.shared.model.Exception.IllegalFiException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, net.fina.shared.model.Exception.IllegalFiException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.io.IOException_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return net.fina.shared.model.Exception.IllegalFiException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    net.fina.shared.model.Exception.IllegalFiException_FieldSerializer.deserialize(reader, (net.fina.shared.model.Exception.IllegalFiException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    net.fina.shared.model.Exception.IllegalFiException_FieldSerializer.serialize(writer, (net.fina.shared.model.Exception.IllegalFiException)object);
  }
  
}
