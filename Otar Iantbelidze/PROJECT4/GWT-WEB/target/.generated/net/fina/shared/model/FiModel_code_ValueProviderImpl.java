package net.fina.shared.model;

public class FiModel_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_code_ValueProviderImpl INSTANCE = new FiModel_code_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getCode();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
