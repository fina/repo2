package net.fina.shared.model;

public class UploadFileModel_ScheduleDate_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.UploadFileModel, java.util.Date> {
  public static final UploadFileModel_ScheduleDate_ValueProviderImpl INSTANCE = new UploadFileModel_ScheduleDate_ValueProviderImpl();
  public java.util.Date getValue(net.fina.shared.model.UploadFileModel object) {
    return object.getScheduleDate();
  }
  public void setValue(net.fina.shared.model.UploadFileModel object, java.util.Date value) {
    object.setScheduleDate(value);
  }
  public String getPath() {
    return "ScheduleDate";
  }
}
