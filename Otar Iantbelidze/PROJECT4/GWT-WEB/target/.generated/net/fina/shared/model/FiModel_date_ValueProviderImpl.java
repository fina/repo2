package net.fina.shared.model;

public class FiModel_date_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.util.Date> {
  public static final FiModel_date_ValueProviderImpl INSTANCE = new FiModel_date_ValueProviderImpl();
  public java.util.Date getValue(net.fina.shared.model.FiModel object) {
    com.google.gwt.core.client.GWT.log("Getter was called on ValueProvider, but no getter exists.", new RuntimeException());
    return null;
  }
  public void setValue(net.fina.shared.model.FiModel object, java.util.Date value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "date";
  }
}
