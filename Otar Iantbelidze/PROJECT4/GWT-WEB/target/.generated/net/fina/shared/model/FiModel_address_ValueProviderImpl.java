package net.fina.shared.model;

public class FiModel_address_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_address_ValueProviderImpl INSTANCE = new FiModel_address_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getAddress();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setAddress(value);
  }
  public String getPath() {
    return "address";
  }
}
