package net.fina.shared.model;

public class FiModel_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_name_ValueProviderImpl INSTANCE = new FiModel_name_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getName();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
