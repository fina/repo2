package net.fina.shared.model;

public class TemplateModel_scheduleModel_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.TemplateModel, java.lang.String> {
  public static final TemplateModel_scheduleModel_ValueProviderImpl INSTANCE = new TemplateModel_scheduleModel_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.TemplateModel object) {
    return object.getScheduleModel();
  }
  public void setValue(net.fina.shared.model.TemplateModel object, java.lang.String value) {
    object.setScheduleModel(value);
  }
  public String getPath() {
    return "scheduleModel";
  }
}
