package net.fina.shared.model;

public class FiTypeModel_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiTypeModel, java.lang.String> {
  public static final FiTypeModel_code_ValueProviderImpl INSTANCE = new FiTypeModel_code_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiTypeModel object) {
    return object.getCode();
  }
  public void setValue(net.fina.shared.model.FiTypeModel object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
