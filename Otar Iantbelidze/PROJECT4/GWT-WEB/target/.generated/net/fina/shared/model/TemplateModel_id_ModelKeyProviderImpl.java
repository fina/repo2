package net.fina.shared.model;

public class TemplateModel_id_ModelKeyProviderImpl implements com.sencha.gxt.data.shared.ModelKeyProvider<net.fina.shared.model.TemplateModel> {
  public static final TemplateModel_id_ModelKeyProviderImpl INSTANCE = new TemplateModel_id_ModelKeyProviderImpl();
  public String getKey(net.fina.shared.model.TemplateModel item) {
    return "" + item.getId();
  }
}
