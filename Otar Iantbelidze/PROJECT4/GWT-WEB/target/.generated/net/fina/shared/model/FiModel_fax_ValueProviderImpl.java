package net.fina.shared.model;

public class FiModel_fax_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.FiModel, java.lang.String> {
  public static final FiModel_fax_ValueProviderImpl INSTANCE = new FiModel_fax_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.FiModel object) {
    return object.getFax();
  }
  public void setValue(net.fina.shared.model.FiModel object, java.lang.String value) {
    object.setFax(value);
  }
  public String getPath() {
    return "fax";
  }
}
