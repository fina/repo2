package net.fina.shared.model;

public class TemplateModel_code_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<net.fina.shared.model.TemplateModel, java.lang.String> {
  public static final TemplateModel_code_ValueProviderImpl INSTANCE = new TemplateModel_code_ValueProviderImpl();
  public java.lang.String getValue(net.fina.shared.model.TemplateModel object) {
    return object.getCode();
  }
  public void setValue(net.fina.shared.model.TemplateModel object, java.lang.String value) {
    object.setCode(value);
  }
  public String getPath() {
    return "code";
  }
}
