package net.fina.client.view.view.Images;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class Images_default_InlineClientBundleGenerator implements net.fina.client.view.view.Images.Images {
  private static Images_default_InlineClientBundleGenerator _instance0 = new Images_default_InlineClientBundleGenerator();
  private void addIconInitializer() {
    addIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "addIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 16, 16, false, false
    );
  }
  private static class addIconInitializer {
    static {
      _instance0.addIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return addIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource addIcon() {
    return addIconInitializer.get();
  }
  private void backIconInitializer() {
    backIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 16, 16, false, false
    );
  }
  private static class backIconInitializer {
    static {
      _instance0.backIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource backIcon() {
    return backIconInitializer.get();
  }
  private void bnkIConInitializer() {
    bnkICon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bnkICon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 16, 16, false, false
    );
  }
  private static class bnkIConInitializer {
    static {
      _instance0.bnkIConInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bnkICon;
    }
  }
  public com.google.gwt.resources.client.ImageResource bnkICon() {
    return bnkIConInitializer.get();
  }
  private void cancelIconInitializer() {
    cancelIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "cancelIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 16, 16, false, false
    );
  }
  private static class cancelIconInitializer {
    static {
      _instance0.cancelIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return cancelIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource cancelIcon() {
    return cancelIconInitializer.get();
  }
  private void createIconInitializer() {
    createIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "createIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 16, 16, false, false
    );
  }
  private static class createIconInitializer {
    static {
      _instance0.createIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return createIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource createIcon() {
    return createIconInitializer.get();
  }
  private void deleteIconInitializer() {
    deleteIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "deleteIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 16, 16, false, false
    );
  }
  private static class deleteIconInitializer {
    static {
      _instance0.deleteIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return deleteIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource deleteIcon() {
    return deleteIconInitializer.get();
  }
  private void editIconInitializer() {
    editIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "editIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 16, 16, false, false
    );
  }
  private static class editIconInitializer {
    static {
      _instance0.editIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return editIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource editIcon() {
    return editIconInitializer.get();
  }
  private void nextIconInitializer() {
    nextIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "nextIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 16, 16, false, false
    );
  }
  private static class nextIconInitializer {
    static {
      _instance0.nextIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return nextIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource nextIcon() {
    return nextIconInitializer.get();
  }
  private void refreshIconInitializer() {
    refreshIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "refreshIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 16, 16, false, false
    );
  }
  private static class refreshIconInitializer {
    static {
      _instance0.refreshIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return refreshIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource refreshIcon() {
    return refreshIconInitializer.get();
  }
  private void removeIconInitializer() {
    removeIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "removeIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage8),
      0, 0, 16, 16, false, false
    );
  }
  private static class removeIconInitializer {
    static {
      _instance0.removeIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return removeIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource removeIcon() {
    return removeIconInitializer.get();
  }
  private void templateIconInitializer() {
    templateIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "templateIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage9),
      0, 0, 16, 16, false, false
    );
  }
  private static class templateIconInitializer {
    static {
      _instance0.templateIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return templateIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource templateIcon() {
    return templateIconInitializer.get();
  }
  private void typeIconInitializer() {
    typeIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "typeIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage10),
      0, 0, 16, 16, false, false
    );
  }
  private static class typeIconInitializer {
    static {
      _instance0.typeIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return typeIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource typeIcon() {
    return typeIconInitializer.get();
  }
  private void uploadIconInitializer() {
    uploadIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "uploadIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage11),
      0, 0, 16, 16, false, false
    );
  }
  private static class uploadIconInitializer {
    static {
      _instance0.uploadIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return uploadIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource uploadIcon() {
    return uploadIconInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACZklEQVR42q2T60+ScRTH/Vugtl7EukhmLbbEXOXmxnqWeZ2WoCEhaDrFeB7EQAS8bKSPl9SxFcyAxpKL4IWlzsyJWkpLfWS6FqzW1lYw336DNjG39JXf7bz8fH6/c3ZOWtpJxzDdwNZPyEmNV8q0uKpjyjdVscbXFUydvZyUjpaxj4cn6widrzb6cqUHS9E5hH6sYuX7InxhJ1R+KcSWgqjITBDHwPL45LYTu7+3Mf3VCwvzHOZNGs4dC+a/vYVhVoGyEUG8ZDDvsEQ3KWcl4Mj4lg3bvzYwvGHCs+V2dAU1oENG9IYM6Fp7ivFdJ0i/BPkDtyJ36GxWStDuk1F9C1p8/rmWeLEXPesG6N+rsB/tsgLKOQmoBRk8Ow7cG8lBnimLSgk0binjDTsSX7WiY0mNtkUlFNM1KUG1oxiNU+V4PFGCgY9GmOZbcLOLx6QE1Jh4L/DFi75PxgTcjKMi9xWCmhHDFhoG35i5lxI0O0V7rvArGFZJqIMNxwrIQBXsoSHw2rkHgnrHfaY/aAC9rkfzOwnkMxUQ2e8eApNV5y9GX1CLzsATXNGeP2hBZiulHtoJuMM2KGaEqA2U/e35X0GNpwD14yVwb1mR3Z2JS62cgyGKLUWsyhf5EdJfjTFmNDWwJCjzJCsJF8O1acEjWxG4rZwIR3WadWgXHpgFROlQXrzJLYQnIekP6kBOVUI5IUT/UhvcCVhiKwBXfTZ+gTrz/20sHMwlCDonKhjIQvcsCesHGtZVGvpAE/idGUk4eiS8H4GJz77dzSNvdFxlruszYtd0F2OXNeeYdDWHTFeeYp/49f4BuZPQBvduJy4AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABhklEQVR42qWTS0tCURDHx030EVq2i1oEMd8qyLKHqfTUCAo1H73osWgRRCu1xB6WpVdNLYyiRdB3mWbmSl69ZUYDh8Occ+Y3/5lzDkC7jacIXOcEkzym0jynTX/ijKCjOVMVPTidIXBfEniueFybY5b9mYwJlQT2rMmabrovCLxZgvkcwcIdicmsvi+r4KH1+28grkawHJIAv0FWg4BBsJQnmLul4UiJMFyg1ppFtrcRHGgNVsBalWClpBDcqhJGSzSwmmtApDlSM9NhuWAPjr0ShOsmhOG480gYK1tUSO3SJK6zW8N4hXDDsAC4438x3OQyIkUCZ8LzPwWLpz2AoTwNBos/9gBPPgiP3wmP3ggPnu09cHATUa5GZO0+6SEbRIIPXwj36l+3MBLMN68SQwVd1E2B7LdCNLMEb9ea2UfTfZZnnNRFhYgSkckgDRag+Jq5rLU7vv0XAhElUo4cjD+YzZIhWaPmC3T89ql6+QcqSBRxNgwb6vf7b9pkd2NjCZ9eVQf7BMkIKjNBTzDqAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAaklEQVR42mNgwA0UgHg9FCswkAgKgPg9EP+HYhA7nxiNBkB8HkmjARTD+OehfAwgAMT9SApxGQDD/VA9YBAAxPexKCKE70P1Ug4ckEx1IIMPBjABcvmjBlDDAIrAAjJSIQyD9DI8oMCA+wDJo31HmX7otAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABzUlEQVR42rVT2WoCQRD0XEXWK3hEweCteOOFiq4KoovgtajkA/KQD/ZbBB9Cp6vJhkRN8pSCZtfZqZqqntZiucUL157L+FL7j/U/YbRaLdJ1nZbLJS0WC5pMJtTr9Qjr/H3Jpf9Ink6nQt7tdmQYBq3Xa5rP5zQcDkWg0+lARLPb7c/X5P1oNKLxeEyr1YruodvtUrVaNUW6n0ybzfbabDbf8BF2gcPhQMfjUZzAkaZpsl6r1SidTptxhqaGXiwWRR0AYbPZ0Ha7FTez2UwiwAGQzWbJ7/dDIG8KaKFQiCqVimyAC+RGA0FGrMFgQO12+1MgGAxC4MkU6EExn8/Lhn6/f7cH9XpdnplMhgKBwDeBhsfjoWQyKRu4H2IXhYY1Gg2Jh5hAIpEgVVUh8GAKVLxeL8XjcdmAKCDgWSqVqFAoyKmpVEq+R6NRcjgcONAtbP4xQFeRy3SRy+UkK0g4MRaLyXokEiGfz0flcplOp5PydQ4e0aRwOCyEewAR1nGNDNv1ICGPD07QIAjhZvCO/iiKQk6nU9b4ltTz+fx0M8dMUpEI+Xm4yOVyCZHHlqxWq7wzlMvlUvv138SbHLDIJ7n5akM8lSqIXEnLf+Ad+yZHrnr94jUAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAqUlEQVR42q1T2w3EIAwL0A8+O0Fn4YMROkZ3YPJT+hAgw/lAd7pIqJLlxI6VSghhkVwxxu36qHNu77DVGKMd1pb3/iHAQM2YogjhvYGKzVn5eUUZ3VQ7s2bmJqVkBUCd2MaB1bVYa49+ZxIYXQWLKnfrfeTJQFkGq9z1YhZpM+OVENdfQqw5gJ1vQ9QmRUYcHdP/L3Fim/JKiDL7WcpAdiNN1YlwYARreCfSiJrqbyopqQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAcklEQVR42mNgQAISZXv+gzAuPlEApgkdE6+5dM9+fAbi03yYZBsJOZtor+BTjM7GqRldAUkGYONjo4kKG5oaQHSMDF0DxIBYgUAiUoKqQwEWQBwGxElAnALEGXhwGlRdBBBb4XMNExBzADEfEHNB+VgBAAFvvrXI3zRKAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAf0lEQVR42mNgQAJOTUf+E8IMuAAxinDKEWsLTE5DP3u+pm5OAE5nM+ABGno5/2FYyyDLgCQDkDWDsW5uAgMhG3FqBmL8AUOMZmIMwKuZkAEENZNqAEmJA6YBWTNWtfgMQLeVaANwOZckF5BkAAg7Nh3Zjw/jTaXICnBhkBpsegEHY+Ieq2dk8AAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABgElEQVR42pVSy07CUBAdXbjUrd/h/StjQPFBJSpG3LigSgU1aqIL48KVVEFR4gMJCGgwkYWJHzPOubdopQ2BkzSTmTlz5tFL1IOR2StW6Qor++XvE39U4tQXEbeh0kLeqbHK1Fk58u2+GgsfcclTJB8iFM23dKeMkLJNVnstVvtvDMBqH3Hk7RAR3RnJnJAO3lkdttkP+Dqe80QwSRfYTY+X9YqPP1iddLgXOo48eMIfjxfZ171uRj0S0mmH1dkXq/PvgAhtVJlWH3kK/O4Uenc51qCgtScm644pdu0JbFdFoMHDgKx7pvkC08ylJRMMJ2AmEIGYCCQvxswNHHMDSsmOW00mu83kfAaLU+YGFC8xzXkrTFi3v7+Q1itMmzUtEihGHPlEmWnxhsn/Ms0jqjOtPBiSdAp0Rhz5pZI54HRh0vcSXcYx9WjogDGTz6ZYrPYRRx7HE37Ic3aNMsYDEYdaLhsLH3HkQ4v9wG4gotNC0Vj4iP8bexBE3YT+VX3wA+pbKaOhVxKzAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA8klEQVR42mNgAILTDAz/QfgMlMaGz6BhoFgOA7JmfAa8Wbjw/xVtbQxDiDbg379//0EAiysmYxjw5fRpsAZ0DANohsxnQLcJF4aBf3//ohoA41zV1QUreJibi2LLny9f/qODy2pqmAb8+/0bw48gDANfzpzBJo8wABe+amCATx67ATfs7MC23o+P/0/AAoQB53h4/v/98QPFr+garhkZoYsjDPj14sV/YgAosHFGIzia8ETjk5oawmEAAw/S0/9/3Lnz//vNm//f8vAgPhAvSEhg9T82fJKBQQ3khRx0iTeLFhFlAAMyAApMBDmJGAy2GQoAtYY6J6hgbAQAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACUklEQVR42q2T709SYRTH/VugtpYvbNVctVZbrjVF5QoIESARxOSmjdZI03GZabYVoyHNF00stZ9S+caMnLFSyzbImjotr5VB3IIVgfeCQLpvREtzqa882/fd8/k8O2fn5ORsdPmrq/mzJGl+T1bSMzotO63TsO+0KnpSpzSPq2X8deFAVZX4I0ky3x02JIc8SI37kPIOg+vtht+ox1iFmBlVEuI14VkDyc3d78bizFuk+x8g3WVH2nkJP++2YWGkH6F6I3yKYs4rP7RS8slg4GXgIHu7C4v0BNJXLyBqtyBmq0PyigWpFgpJ6xkk++4gcFKLEdnB4JCogPevgAo3mrEw8Sr747yDQuSiCYmmqhVhz2qR7LmBN8J9GCzfTy0JPpB6Ot5zE2mXE1FrDb6pJatmrkYLztaAkKUWT8V76CUBbdAlUo97kHZYED1vxFoVkpcgQlYg5myFR5ifWBJM69SJeVcHks1GJM5VrisI65WItbVigNi+LJg6pqQj1gbM2ymwp1XZh6tGIUS0sR7+ehP6BXnLLUyoZRQtFSDh6kRUJ0VEL/oPZg4XgzkiQrzrGoaJnXALcpeHOKYR8V5rJMHACQ3i9zqyAwsriT+gvBSMNANLCXCd7Zg8KoG7cGuwV7CZt2IXRuUlYq+8kJs9rkT8Vgd+NFMIaRX4opIj0mQGd70dU6oyPCrK5R4WbVl9G19mNuxFeQHjE+3F1zoTIi2Xs/lcewrPSnb8hpk14b/1XHaAP0jsNnvEu+gnZfnsgGAb6y7NozM9m/sKN/E3/Hp/AXKB05tGS6e8AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACRUlEQVQ4EW1Sv4sTQRT+ZmY3m2Qvel4S5SCH3HmgYKFoJVhZioIWltfY2tgIipUW14lgc/+BhYjYWoha2CoWio0K2hxkk9PL5sf+mvGbNRm3uBcm8/bNe9/M970n8M9Wbtx5MYjHU2hdoCgKtJo+RvEUWZ7h9+43fHr1qMvUaJ7vNm/uyUa9ht7RFvrDGEuhB11oaGOQZwr+6nH0ad1ut838oaumswCApxR2h1PebrBz/0o1B2ma8mUaURQNOp3OYR7uLxIcQJIbTJMCm2vL+PI9QjxJ8GDnDdIsJ2iOwc+PUCbB+rlrn398eHmCAKkFcQBKeZAqh5QKd5+8owYebExpU152bP18qUu0N+oRYIPBr/ZAlqf8EyxsL4cEkCxUEELizMlVbF09W35DKJtUgjK9uahzL2g1A8zSDGEjwNPtuQa83PB3/dKpRT627j1zvnUcQKZJKgfsfvPha4R1gTF1SBi0rYUp0KwrTKhT1RwFbSRyFhsjoCF4r0JBhob+5lqbLbU+F8+r5gCkFBQpIE1ZCimpg5JWWMXFmN2pk11VcxQ8T+FIy6dIAp5vOyC5K97MGM9qjNkcz/er9f81aAQ+9seWr8ahMEDNE5BCIA9y9PcStJbqqBHcTmfVHIVpqjGaFLBAly/0cHpjBfGsKGOa3LdvXYQdtnhCpSvmKAiKZflHf1I8f/uLXaAG5F1wkOyE3n78HmFg9ThYg/JlFoQqgW3gINHn4CxEMzZWiljeWY6xfUi1J3V+O0qVVx7kzhgkJPAXBePOf1wJukcAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage10 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAkElEQVR42mNgwAPunt/wn4FcoG2g+19TTxuMSdYM06ihq5VGsiHYFJPtEuSwINuQ22fXwTWTbMj1k6sxNK1Z3EOcIReOLMdpI0GXnNizALdmfe2jeDXv3jgdr19BAWpgYCCA5Byd3ej+w6VZU1czAENOU1drLkiQoqjS0tNeSnFi0dDT3kiRAdDwOEixIaQCAAvoZeDtkVmqAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage11 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAUUlEQVR42mNgQAJOTUf+E8IMuAAxinDKEWsLVjlSnEmxAaT5CwnE7En/D8IjyQCYBkKYYgOi96TPHy6BCMKOTUf2Y8MEDQABkEJC2RccYFgAAIlH4h/xuB5GAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource addIcon;
  private static com.google.gwt.resources.client.ImageResource backIcon;
  private static com.google.gwt.resources.client.ImageResource bnkICon;
  private static com.google.gwt.resources.client.ImageResource cancelIcon;
  private static com.google.gwt.resources.client.ImageResource createIcon;
  private static com.google.gwt.resources.client.ImageResource deleteIcon;
  private static com.google.gwt.resources.client.ImageResource editIcon;
  private static com.google.gwt.resources.client.ImageResource nextIcon;
  private static com.google.gwt.resources.client.ImageResource refreshIcon;
  private static com.google.gwt.resources.client.ImageResource removeIcon;
  private static com.google.gwt.resources.client.ImageResource templateIcon;
  private static com.google.gwt.resources.client.ImageResource typeIcon;
  private static com.google.gwt.resources.client.ImageResource uploadIcon;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      addIcon(), 
      backIcon(), 
      bnkICon(), 
      cancelIcon(), 
      createIcon(), 
      deleteIcon(), 
      editIcon(), 
      nextIcon(), 
      refreshIcon(), 
      removeIcon(), 
      templateIcon(), 
      typeIcon(), 
      uploadIcon(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("addIcon", addIcon());
        resourceMap.put("backIcon", backIcon());
        resourceMap.put("bnkICon", bnkICon());
        resourceMap.put("cancelIcon", cancelIcon());
        resourceMap.put("createIcon", createIcon());
        resourceMap.put("deleteIcon", deleteIcon());
        resourceMap.put("editIcon", editIcon());
        resourceMap.put("nextIcon", nextIcon());
        resourceMap.put("refreshIcon", refreshIcon());
        resourceMap.put("removeIcon", removeIcon());
        resourceMap.put("templateIcon", templateIcon());
        resourceMap.put("typeIcon", typeIcon());
        resourceMap.put("uploadIcon", uploadIcon());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'addIcon': return this.@net.fina.client.view.view.Images.Images::addIcon()();
      case 'backIcon': return this.@net.fina.client.view.view.Images.Images::backIcon()();
      case 'bnkICon': return this.@net.fina.client.view.view.Images.Images::bnkICon()();
      case 'cancelIcon': return this.@net.fina.client.view.view.Images.Images::cancelIcon()();
      case 'createIcon': return this.@net.fina.client.view.view.Images.Images::createIcon()();
      case 'deleteIcon': return this.@net.fina.client.view.view.Images.Images::deleteIcon()();
      case 'editIcon': return this.@net.fina.client.view.view.Images.Images::editIcon()();
      case 'nextIcon': return this.@net.fina.client.view.view.Images.Images::nextIcon()();
      case 'refreshIcon': return this.@net.fina.client.view.view.Images.Images::refreshIcon()();
      case 'removeIcon': return this.@net.fina.client.view.view.Images.Images::removeIcon()();
      case 'templateIcon': return this.@net.fina.client.view.view.Images.Images::templateIcon()();
      case 'typeIcon': return this.@net.fina.client.view.view.Images.Images::typeIcon()();
      case 'uploadIcon': return this.@net.fina.client.view.view.Images.Images::uploadIcon()();
    }
    return null;
  }-*/;
}
