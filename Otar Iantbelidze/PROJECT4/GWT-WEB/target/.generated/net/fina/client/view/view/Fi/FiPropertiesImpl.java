package net.fina.client.view.view.Fi;

public class FiPropertiesImpl implements net.fina.client.view.view.Fi.FiProperties {
  public com.sencha.gxt.core.client.ValueProvider address() {
    return net.fina.shared.model.FiModel_address_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider code() {
    return net.fina.shared.model.FiModel_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider date() {
    return net.fina.shared.model.FiModel_date_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider fax() {
    return net.fina.shared.model.FiModel_fax_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.ModelKeyProvider key() {
    return net.fina.shared.model.FiModel_id_ModelKeyProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider mail() {
    return net.fina.shared.model.FiModel_mail_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return net.fina.shared.model.FiModel_name_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider phone() {
    return net.fina.shared.model.FiModel_phone_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider typeString() {
    return net.fina.shared.model.FiModel_typeString_ValueProviderImpl.INSTANCE;
  }
}
