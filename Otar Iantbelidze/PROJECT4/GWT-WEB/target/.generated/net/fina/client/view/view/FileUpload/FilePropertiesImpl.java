package net.fina.client.view.view.FileUpload;

public class FilePropertiesImpl implements net.fina.client.view.view.FileUpload.FileProperties {
  public com.sencha.gxt.core.client.ValueProvider ScheduleDate() {
    return net.fina.shared.model.UploadFileModel_ScheduleDate_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider fileName() {
    return net.fina.shared.model.UploadFileModel_fileName_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.ModelKeyProvider key() {
    return net.fina.shared.model.UploadFileModel_id_ModelKeyProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider uploadDate() {
    return net.fina.shared.model.UploadFileModel_uploadDate_ValueProviderImpl.INSTANCE;
  }
}
