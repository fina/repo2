package net.fina.client.view.view.FiType;

public class FiTypePropertiesImpl implements net.fina.client.view.view.FiType.FiTypeProperties {
  public com.sencha.gxt.core.client.ValueProvider code() {
    return net.fina.shared.model.FiTypeModel_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.ModelKeyProvider key() {
    return net.fina.shared.model.FiTypeModel_id_ModelKeyProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return net.fina.shared.model.FiTypeModel_name_ValueProviderImpl.INSTANCE;
  }
}
