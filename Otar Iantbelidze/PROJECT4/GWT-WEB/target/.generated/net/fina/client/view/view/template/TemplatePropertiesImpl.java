package net.fina.client.view.view.template;

public class TemplatePropertiesImpl implements net.fina.client.view.view.template.TemplateProperties {
  public com.sencha.gxt.core.client.ValueProvider code() {
    return net.fina.shared.model.TemplateModel_code_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.data.shared.ModelKeyProvider key() {
    return net.fina.shared.model.TemplateModel_id_ModelKeyProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return net.fina.shared.model.TemplateModel_name_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider scheduleModel() {
    return net.fina.shared.model.TemplateModel_scheduleModel_ValueProviderImpl.INSTANCE;
  }
}
