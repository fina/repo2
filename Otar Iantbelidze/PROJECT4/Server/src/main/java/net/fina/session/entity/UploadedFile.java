package net.fina.session.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by oto on 12/7/2014.
 */
@Entity
@Table(name = "Imported_Returns")
public class UploadedFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "file_id_generator")
    @SequenceGenerator(name = "file_id_generator", sequenceName = "file_id_sequence", allocationSize = 1, initialValue = 1)
    private int id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Template_Id")
    private Template template;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Fi_ID")
    private Fi fi;
    private String fileName;
    @Column(name = "Uploaded_File")
    @Basic(fetch = FetchType.LAZY)
    private byte[] uploadedFile;
    private Date uploadDate;
    private Date ScheduleDate;

    public UploadedFile() {
    }

    public UploadedFile(int id, Template template, Fi fi, byte[] uploadedFile, Date uploadDate, Date scheduleDate) {
        this.id = id;
        this.template = template;
        this.fi = fi;
        this.uploadedFile = uploadedFile;
        this.uploadDate = uploadDate;
        ScheduleDate = scheduleDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Fi getFi() {
        return fi;
    }

    public void setFi(Fi fi) {
        this.fi = fi;
    }

    public byte[] getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(byte[] uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getScheduleDate() {
        return ScheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        ScheduleDate = scheduleDate;
    }


    public void setFileName(String fileName) {

        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
