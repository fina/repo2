package net.fina.session.api;

import net.fina.session.entity.Template;

import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
public interface TemplateServiceLocal {
    public List<Template> getTemplateData() throws Exception;

    public void saveTemplate(Template template) throws Exception;

    public void deleteTemplate(int id) throws Exception;

    public List<Template> getFiTemplates(int id) throws Exception;

    public boolean deleteTempList(List<Integer> idList) throws Exception;

    public Template findByCode(String code) throws Exception;
}
