package net.fina.session.impl;

import net.fina.session.api.UploadFileServiceLocal;
import net.fina.session.entity.UploadedFile;
import org.jboss.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by oto on 12/7/2014.
 */
@Stateless
@Local(UploadFileServiceLocal.class)
public class UploadFileSessionBean implements UploadFileServiceLocal, Serializable {
    private static Logger log = Logger.getLogger(UploadFileSessionBean.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UploadedFile upload(UploadedFile file) throws Exception {
        log.info("=========== STARTING PERSISTING DATABASE========================");
        entityManager.persist(file);
        log.info("=============== FILE SUCCESSFULLY SAVED================");
        log.info("// =========================================//");
        log.info("// == conetnt size is = " + file.getUploadedFile().length + " ===== //");
        log.info("//===========END======================//");
        return file;
    }

    @Override
    public List<UploadedFile> getData() throws Exception {
        return entityManager.createQuery("from UploadedFile").getResultList();
    }

    @Override
    public UploadedFile getFile(int id) throws Exception {
        log.info("//==========================================================//");
        log.info("//===== getting file from DataBase =====");
        return entityManager.find(UploadedFile.class, id);
    }

    @Override
    public void delete(List<Integer> uploadedFiles) throws Exception {
        log.info("//===============================================================//");
        log.info("//====================== TRYING TO DELETE FILE(S)==============//");
        for (Integer i : uploadedFiles)
            entityManager.remove(entityManager.find(UploadedFile.class,i));
    }

    @Override
    public void test() {
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
        log.info("======== servet calling ======");
    }
}
