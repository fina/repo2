package net.fina.session.api;

import net.fina.session.entity.Fi;

import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
public interface FiLocalService {
    public Fi findFi(int id) throws Exception;

    public void saveFi(Fi fi) throws Exception;

    public List<Fi> getFiList();

    public void deleteFi(int id) throws Exception;

    public boolean deleteFiList(List<Integer> idList) throws Exception;

    public boolean setTemplateListToFi(Fi fi) throws Exception;

    public Fi findByCode(String code) throws Exception;
}
