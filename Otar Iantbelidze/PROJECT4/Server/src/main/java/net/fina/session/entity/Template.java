package net.fina.session.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by oto on 10/16/2014.
 */
@Entity
public class Template implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "template_id_generator")
    @SequenceGenerator(name = "template_id_generator",sequenceName = "template_id_sequence",allocationSize = 1,initialValue = 1)
    private int id;
    @Column(unique = true)
    private String code;
    private String name;
    @Enumerated(EnumType.STRING)
    private Schedule schedule;

    public Template() {
    }

    public Template(String code, String name, Schedule schedule) {
        this.code = code;
        this.name = name;
        this.schedule = schedule;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchedule() {
        return schedule.toString();
    }

    public void setSchedule(String schedule) {
        this.schedule =Schedule.valueOf(schedule);
    }
}
