package net.fina.session.entity;

/**
 * Created by oto on 10/16/2014.
 */
public enum Schedule {
    DAILY,
    MONTHLY,
    Quarterly,
    Yearly
}
