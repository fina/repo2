package net.fina.session.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
@Entity
public class Fi implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
    @SequenceGenerator(name = "id_generator", sequenceName = "id_sequence", allocationSize = 1, initialValue = 1)
    private int id;
    private String code;
    @Column(unique = true)
    private String name;
    private String address;
    private String phone;
    private String fax;
    private String mail;
    @Temporal(TemporalType.DATE)
    @Column(name = "Registration_Date")
    private Date reg_date;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "Type_ID")
    private FiType fiType;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Fi_Template",    joinColumns = @JoinColumn(name = "Fi_ID"),    inverseJoinColumns = @JoinColumn(name = "Temp_ID"))
    private List<Template> templateList;


    public Fi(String code, String name, String address, String phone, String fax, String mail, Date Date, FiType fiType, List<Template> template) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.mail = mail;
        this.reg_date = Date;
        this.fiType = fiType;
        this.templateList = template;
    }

    public Fi() {
        templateList=new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getDate() {
        return reg_date;
    }

    public void setDate(Date regDate) {
        this.reg_date = regDate;
    }

    public FiType getFiType() {
        return fiType;
    }

    public void setFiType(FiType type) {
        this.fiType = type;
    }

    public List<Template> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<Template> templateList) {
        this.templateList = templateList;
    }


}
