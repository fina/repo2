package net.fina.session.impl;

import net.fina.session.api.FiLocalService;
import net.fina.session.entity.Fi;
import org.jboss.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
@Stateless(name = "fiService")
@Local(FiLocalService.class)
public class FiLocalServiceBean implements FiLocalService {
    private static Logger log = Logger.getLogger(FiLocalServiceBean.class);

    @PersistenceContext(unitName = "project4")
    private EntityManager entityManger;

    @Override
    public Fi findFi(int id) throws Exception {
        return null;
    }

    @Override
    public void saveFi(Fi fi) throws Exception {
        if (fi.getId() == 0) {
            if (!isNameUnique(fi.getName()))
                throw new SQLException("Name is Not Unique!!");
            else if (!isCodeUnique(fi.getCode())) {
                throw new SQLException("Code Is Not Unique!!");
            }
            entityManger.merge(fi);
        } else
            entityManger.merge(fi);
    }


    @Override
    public List<Fi> getFiList() {
        List<Fi> list = new ArrayList<Fi>(entityManger.createQuery("from Fi").getResultList());
        return list;
    }

    @Override
    public void deleteFi(int id) throws Exception {
        entityManger.remove(entityManger.find(Fi.class, id));
    }

    @Override
    public boolean deleteFiList(List<Integer> idList) throws Exception {
        log.info("======= deleting Fi Entity ======");
        for (int i = 0; i < idList.size(); i++) {
            entityManger.remove(entityManger.find(Fi.class, idList.get(i)));
        }
        return true;
    }

    @Override
    public boolean setTemplateListToFi(Fi fi) throws Exception {
        log.info("============= trying to set Template list to Fi ===========");
        entityManger.merge(fi);
        return true;
    }

    @Override
    public Fi findByCode(String code) throws Exception {
        List<Fi> fiList = entityManger.createQuery("select f from Fi f where f.code=:code").setParameter("code", code).getResultList();
        return fiList.size() != 0 ? fiList.get(0) : null;
    }

    public boolean isNameUnique(String name) {
        Query query = entityManger.createQuery("select f from Fi f where f.name=:name").setParameter("name", name.trim())
                .setParameter("name", name);
        return query.getResultList().size() > 0 ? false : true;
    }

    public boolean isCodeUnique(String code) {
        Query query = entityManger.createQuery("select f from Fi f where f.code=:code").setParameter("code", code.trim())
                .setParameter("code", code);
        return query.getResultList().size() > 0 ? false : true;
    }
}
