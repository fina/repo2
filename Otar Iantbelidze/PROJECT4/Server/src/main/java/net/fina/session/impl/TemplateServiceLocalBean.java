package net.fina.session.impl;

import net.fina.session.api.TemplateServiceLocal;
import net.fina.session.entity.Template;
import org.jboss.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/14/2014.
 */
@Stateless(name = "templateService")
@Local(TemplateServiceLocal.class)
public class TemplateServiceLocalBean implements TemplateServiceLocal {
    private static Logger log = Logger.getLogger(TemplateServiceLocalBean.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Template> getTemplateData() throws Exception {
        List<Template> templates = new ArrayList<Template>(entityManager.createQuery("from Template").getResultList());
        return templates;
    }

    @Override
    public void saveTemplate(Template template) throws Exception {
        if (template.getId() == 0) {
            if (!isCodeUnique(template.getCode()))
                throw new SQLException("Code is not Unique!!");
            entityManager.persist(template);
        } else
            entityManager.merge(template);
    }

    @Override
    public void deleteTemplate(int id) throws Exception {
        entityManager.remove(entityManager.find(Template.class, id));
    }

    @Override
    public List<Template> getFiTemplates(int id) throws Exception {
        List<Template> templateList = new ArrayList<>();
        Query query = entityManager.createQuery("select f.templateList from Fi f where f.id=:id ");
        templateList = query.setParameter("id", id).getResultList();
        return templateList;
    }

    @Override
    public boolean deleteTempList(List<Integer> idList) throws Exception {
        for (int i = 0; i < idList.size(); i++) {
            entityManager.remove(entityManager.find(Template.class, idList.get(i)));
        }
        return true;
    }

    @Override
    public Template findByCode(String code) throws Exception {
        List<Template> templateList = entityManager.createQuery("select t from Template t where t.code=:code").setParameter("code", code).getResultList();

        return templateList.size() != 0 ? templateList.get(0) : null;
    }

    public boolean isCodeUnique(String code) {
        Query query = entityManager.createQuery("select t from Template t where t.code=:code").setParameter("code", code.trim())
                .setParameter("code", code);
        return query.getResultList().size() > 0 ? false : true;
    }
}
