package net.fina.session.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by oto on 10/16/2014.
 */
@Entity
public class FiType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "type_id_generator")
    @SequenceGenerator(name = "type_id_generator", sequenceName = "type_id_sequence", allocationSize = 1, initialValue = 1)
    @Column(name = "ID")
    private int id;
    @Column(unique = true)
    private String code;
    @Column(unique = true)
    private String name;

    public FiType() {
    }

    public FiType(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
