package net.fina.session.impl;

import net.fina.session.api.FiTypeLocal;
import net.fina.session.entity.FiType;
import org.jboss.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
@Stateless(name = "fiTypeService")
@Local(FiTypeLocal.class)
public class FiTypeLocalServiceBean implements FiTypeLocal {
    private static Logger log = Logger.getLogger(FiTypeLocalServiceBean.class);

    @PersistenceContext(unitName = "project4")
    private EntityManager entityManager;

    @Override
    public void saveFiType(FiType type) throws Exception {
        log.info("================== saving FiType ==================================");
        if (type.getId() == 0) {
            if (!isNameUnique(type.getName())) {
                throw new SQLException("Name is not unique!!!");
            }
            entityManager.persist(type);
        } else
            entityManager.merge(type);

    }

    @Override
    public List<FiType> getFiTypeList() throws Exception {
        log.info("============ returning fi type list form database===============");
        List<FiType> typeList = new ArrayList<FiType>(entityManager.createQuery("from FiType").getResultList());
        return typeList;
    }

    @Override
    public boolean deleteFiTypeByID(int id) throws Exception {
        log.info("============== trying to delte fi by id ========================");
        entityManager.remove(entityManager.find(FiType.class, id));
        return true;
    }

    @Override
    public boolean deleteFiTypeList(List<Integer> idList) throws Exception {
        log.info("============= deleting fi type =======================");
        for (int i = 0; i < idList.size(); i++) {
            entityManager.remove(entityManager.find(FiType.class, idList.get(i)));
        }
        return true;
    }

    public boolean isCodeUnique(String code) {
        Query query = entityManager.createQuery("select t from FiType t where t.code=:code").setParameter("code", code.trim())
                .setParameter("code", code);
        return query.getResultList().size() > 0 ? false : true;
    }

    public boolean isNameUnique(String name) {
        Query query = entityManager.createQuery("select t from FiType t where t.name=:name").setParameter("name", name.trim())
                .setParameter("name", name);
        return query.getResultList().size() > 0 ? false : true;
    }

}
