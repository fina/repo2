package net.fina.session.api;

import net.fina.session.entity.UploadedFile;

import java.util.List;

/**
 * Created by oto on 12/7/2014.
 */
public interface UploadFileServiceLocal {

    public UploadedFile upload(UploadedFile file) throws Exception;

    public List<UploadedFile> getData() throws Exception;

    public UploadedFile getFile(int id) throws Exception;

    public void delete(List<Integer> uploadedFiles) throws Exception;

    public void test();
}
