package net.fina.session.api;

import net.fina.session.entity.FiType;

import java.util.List;

/**
 * Created by oto on 11/6/2014.
 */
public interface FiTypeLocal {
    public void saveFiType(FiType type)throws Exception;
    public List<FiType> getFiTypeList()throws Exception;
    public boolean deleteFiTypeByID(int id)throws Exception;
    public boolean deleteFiTypeList(List<Integer> idList) throws Exception;
}
