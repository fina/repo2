package com.oto.shared;

import javax.persistence.*;
import java.io.Serializable;

//import org.hibernate.annotations.GenericGenerator;

@Entity
public class Template implements Serializable {

	private static final long serialVersionUID = 1155628L;

	public enum Schedule {
		DAILY, WEEKLY, MONTHLY
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	

	private String code;

	@Column(name = "NAME")
	private String name;

	
	@Column(name = "SCHEDULE")
	private String schedule;

	@Transient
	private Schedule sch;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

}
