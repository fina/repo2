package com.oto.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddTemplateEventHandler extends EventHandler{

	public void onAddTemplate(AddtemplateEvent event);
}
