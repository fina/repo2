package com.oto.client.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
import com.oto.client.Presenter.AddTemplatePresenter;
import com.oto.shared.Template;
import com.oto.shared.Template.Schedule;

public class AddTemplateView extends Composite implements
        AddTemplatePresenter.Display {

    private Button saveButton;
    private Button cancelButton;
    private TextBox codeBox;
    private TextBox nameBox;
    private ListBoxView listBox;
    private FlexTable table;

    public AddTemplateView() {
        DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("30em");
        initWidget(contentDetailsDecorator);

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");
        table = new FlexTable();
        table.setWidth("100%");
        codeBox = new TextBox();
        nameBox = new TextBox();
        listBox = new ListBoxView();
        initTableForFirst();
        contentDetailsPanel.add(table);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");
        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);

    }

    public void initTableForFirst() {
        Schedule[] mass = Template.Schedule.values();
        listBox.addItem("");
        for (int i = 0; i < Template.Schedule.values().length; i++) {
            listBox.addItem(mass[i].toString());
        }
        table.setWidget(0, 0, new Label("Code"));
        table.setWidget(0, 1, codeBox);
        table.setWidget(1, 0, new Label("choose "));
        table.setWidget(1, 1, listBox);
        table.setWidget(2, 0, new Label("description "));
        table.setWidget(2, 1, nameBox);

    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public HasClickHandlers getCancalButton() {
        return cancelButton;
    }

    public HasValue<String> getCode() {
        return codeBox;
    }

    public HasValue<String> getName() {
        return nameBox;
    }

    public HasValue<String> getListBox() {
        return listBox;
    }

    public HasValue<Schedule> getSchedule() {
        return getSchedule();
    }

    public Widget asWidget() {
        return this;
    }

}
