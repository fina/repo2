package com.oto.client.Presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.oto.client.TemplateServiceAsync;
import com.oto.client.event.CancelEvent;
import com.oto.client.event.TemplateSavedEvent;
import com.oto.shared.FieldVerifier;
import com.oto.shared.Template;

public class AddTemplatePresenter implements Presenter {

    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancalButton();

        HasValue<String> getCode();

        HasValue<String> getName();

        HasValue<String> getListBox();

        Widget asWidget();
    }

    private Template template;
    private TemplateServiceAsync rpcService;
    private HandlerManager eventBus;
    private Display display;

    public AddTemplatePresenter(TemplateServiceAsync rpc, Display display,
                                HandlerManager handler) {
        this.display = display;
        this.rpcService = rpc;
        this.eventBus = handler;
        addListeners();
    }

    public void addListeners() {
        display.getSaveButton().addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                if (FieldVerifier.isValidName(display.getCode().getValue())
                        && FieldVerifier.isValidName(display.getName()
                        .getValue())) {
                    template = new Template();
                    template.setCode(display.getCode().getValue());
                    template.setName(display.getName().getValue());
                    template.setSchedule(display.getListBox().getValue());

                    rpcService.addTemplate(template, new AsyncCallback<Void>() {

                        public void onSuccess(Void result) {

                            eventBus.fireEvent(new TemplateSavedEvent(template));
                        }

                        public void onFailure(Throwable caught) {
                            Window.alert("error adding template to base");

                        }
                    });
                } else {
                    Window.alert("please fill all data!");

                }
            }
        });

        display.getCancalButton().addClickHandler(new ClickHandler() {


            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new CancelEvent());
            }
        });
    }

    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

}
