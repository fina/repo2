package com.oto.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.oto.shared.Template;

import java.util.ArrayList;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface TemplateService extends RemoteService {
  public ArrayList<Template> getTemplateInfo();
  public void addTemplate(Template template);
  public ArrayList<Template> deleteTemplates(ArrayList<Integer> list);

}
