package com.oto.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.oto.client.Presenter.AddTemplatePresenter;
import com.oto.client.Presenter.Presenter;
import com.oto.client.Presenter.TemplatePresenter;
import com.oto.client.event.*;
import com.oto.client.view.AddTemplateView;
import com.oto.client.view.TemplateView;

public class AppController implements Presenter, ValueChangeHandler<String> {
	private final HandlerManager eventBus;
	private final TemplateServiceAsync rpcService;
	private HasWidgets container;

	public AppController(TemplateServiceAsync rpcService,
			HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.rpcService = rpcService;
		bind();
	}

	public void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(AddtemplateEvent.TYPE,
				new AddTemplateEventHandler() {

					public void onAddTemplate(AddtemplateEvent event) {
						History.newItem("add");

					}
				});
		eventBus.addHandler(TemplateSavedEvent.TYPE,
				new TemplateSavedEventHandler() {

					public void onSaveEvent(TemplateSavedEvent event) {
						History.newItem("list");
					}

				});
		eventBus.addHandler(CancelEvent.TYPE, new CancelEventHandler() {

			public void onCancelEvent(CancelEvent event) {
				History.newItem("list");
			}

		});

	}

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;
			if (token.equals("list")) {
				presenter = new TemplatePresenter(rpcService, eventBus,
						new TemplateView());
			} else if (token.equals("add")) {
				presenter = new AddTemplatePresenter(rpcService,
						new AddTemplateView(), eventBus);
			}

			if (presenter != null) {
				presenter.go(container);
			}
		}
	}

	public void go(HasWidgets container) {
		this.container = container;

		if ("".equals(History.getToken())) {
			History.newItem("list");
		} else {
			History.fireCurrentHistoryState();
		}
	}

}
