package com.oto.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddtemplateEvent extends GwtEvent<AddTemplateEventHandler>{

	public static Type<AddTemplateEventHandler> TYPE=new Type<AddTemplateEventHandler>();
	
	@Override
	public Type<AddTemplateEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AddTemplateEventHandler handler) {
		handler.onAddTemplate(this);
		
	}

}
