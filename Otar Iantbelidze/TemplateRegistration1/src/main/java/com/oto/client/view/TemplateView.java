package com.oto.client.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
import com.oto.client.Presenter.TemplatePresenter;
import com.oto.shared.Template;

import java.util.ArrayList;
import java.util.List;

public class TemplateView extends Composite implements
        TemplatePresenter.Display {

    private Button addButton;
    private Button deleteButton;
    private FlexTable container;
    private FlexTable templateTable;

    public TemplateView() {
        DecoratorPanel contentTableDecorator = new DecoratorPanel();
        contentTableDecorator.addStyleName("panelStyle");
        initWidget(contentTableDecorator);
        contentTableDecorator.setWidth("100%");
        contentTableDecorator.setWidth("50em");

        container = new FlexTable();
        container.setWidth("100%");
        container.getFlexCellFormatter().setVerticalAlignment(0, 0,
                DockPanel.ALIGN_MIDDLE);

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.addStyleName("panelStyle");
        hPanel.setBorderWidth(0);
        hPanel.setSpacing(0);
        hPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
        addButton = new Button("Add");
        deleteButton = new Button("Delete");
        hPanel.add(addButton);
        hPanel.add(deleteButton);
        container.setWidget(0, 0, hPanel);

        templateTable = new FlexTable();
        templateTable.setBorderWidth(10);
        templateTable.setWidth("100%");

        container.setWidget(1, 0, templateTable);

        contentTableDecorator.add(container);

    }

    public HasClickHandlers getAddButton() {
        return addButton;
    }

    public HasClickHandlers getDeleteButton() {
        return deleteButton;
    }

    public int getClickedRow() {

        return 0;
    }

    public void setData(List<Template> data) {
        templateTable.removeAllRows();
        templateTable.getRowFormatter().addStyleName(0, "tableHeader");
        templateTable.setText(0, 1, "ID");
        templateTable.setText(0, 2, "CODE");
        templateTable.setText(0, 3, "DESCRIPTION");
        templateTable.setText(0, 4, "SCHEDULE");

        for (int i = 0; i < data.size(); ++i) {

            templateTable.setWidget(i + 1, 0, new CheckBox());
            templateTable.setText(i + 1, 1, "" + data.get(i).getId());
            templateTable.setText(i + 1, 2, data.get(i).getCode());
            templateTable.setText(i + 1, 3, data.get(i).getName());
            templateTable.setText(i + 1, 4, data.get(i).getSchedule());
        }
        applyDataRowStyles();
    }

    public List<Integer> getSelectedRows() {
        ArrayList<Integer> rowList = new ArrayList<Integer>();

        for (int i = 1; i < templateTable.getRowCount(); i++) {
            CheckBox chk = (CheckBox) templateTable.getWidget(i, 0);
            if (chk.getValue()) {
                rowList.add(Integer.parseInt(templateTable.getText(i, 1)));
            }

        }
        return rowList;
    }

    public Widget asWidget() {
        return this;
    }

    private void applyDataRowStyles() {
        HTMLTable.RowFormatter rf = templateTable.getRowFormatter();

        for (int row = 1; row < templateTable.getRowCount(); ++row) {
            if ((row % 2) != 0) {
                rf.addStyleName(row, "Table-OddRow");
            } else {
                rf.addStyleName(row, "Table-EvenRow");
            }
        }
    }

}
