package com.oto.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class CancelEvent extends GwtEvent<CancelEventHandler> {

	public static Type<CancelEventHandler> TYPE = new Type<CancelEventHandler>();

	@Override
	public Type<CancelEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(CancelEventHandler handler) {
		handler.onCancelEvent(this);
	}

}
