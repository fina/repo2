package com.oto.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface CancelEventHandler extends EventHandler {
	void onCancelEvent(CancelEvent event);
}
