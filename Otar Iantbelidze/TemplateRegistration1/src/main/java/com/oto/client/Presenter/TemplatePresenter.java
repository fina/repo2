package com.oto.client.Presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.oto.client.TemplateServiceAsync;
import com.oto.client.event.AddtemplateEvent;
import com.oto.shared.Template;

import java.util.ArrayList;
import java.util.List;

public class TemplatePresenter implements Presenter {

	public interface Display {
		HasClickHandlers getAddButton();

		HasClickHandlers getDeleteButton();

		int getClickedRow();

		void setData(List<Template> data);

		List<Integer> getSelectedRows();

		Widget asWidget();
	}

	ArrayList<Template> tempList;

	TemplateServiceAsync rpcService;
	HandlerManager eventBus;
	Display display;

	public TemplatePresenter(TemplateServiceAsync rpc, HandlerManager eventBus,
			Display display) {
		this.rpcService = rpc;
		this.eventBus = eventBus;
		this.display = display;
	}

	public void addListeners() {
		display.getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new AddtemplateEvent());

			}
		});

		display.getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				ArrayList<Integer> rows = (ArrayList<Integer>) display
						.getSelectedRows();
				if (rows.size() > 0) {
					rpcService.deleteTemplates(rows,
							new AsyncCallback<ArrayList<Template>>() {

								public void onSuccess(ArrayList<Template> result) {
									display.setData(result);
									Window.alert("its done!!");
								}

								public void onFailure(Throwable caught) {
									Window.alert("error");
								}
							});
				}
			}
		});

	}

	public void addDataToDiswplayView() {

		rpcService.getTemplateInfo(new AsyncCallback<ArrayList<Template>>() {

			public void onSuccess(ArrayList<Template> result) {
				tempList = result;
				display.setData(tempList);

			}

			public void onFailure(Throwable caught) {
				Window.alert("Failed when retrieving Data!!!");

			}
		});

	}

	public void go(HasWidgets container) {
		addListeners();
		container.clear();
		container.add(display.asWidget());
		addDataToDiswplayView();
	}

}
