package com.oto.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TemplateSavedEventHandler extends EventHandler{
    public void onSaveEvent(TemplateSavedEvent event);
}
