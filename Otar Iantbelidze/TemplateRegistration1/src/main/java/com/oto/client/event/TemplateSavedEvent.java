package com.oto.client.event;



import com.google.gwt.event.shared.GwtEvent;
import com.oto.shared.Template;

public class TemplateSavedEvent extends GwtEvent<TemplateSavedEventHandler> {

	public static Type<TemplateSavedEventHandler> TYPE = new Type<TemplateSavedEventHandler>();
	private Template template;

	public TemplateSavedEvent(Template temp) {
		this.template = temp;
	}

	public Template getSavedTemplate() {
		return template;
	}

	@Override
	public Type<TemplateSavedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TemplateSavedEventHandler handler) {
		handler.onSaveEvent(this);

	}

}
