package com.oto.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.oto.client.TemplateService;
import com.oto.shared.Template;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class TemplateServiceImpl extends RemoteServiceServlet implements
        TemplateService {

    Template temp;
    ArrayList<Template> tempList = new ArrayList<Template>();

    public TemplateServiceImpl() {

    }

    @SuppressWarnings("deprecation")
    static SessionFactory sf = new Configuration().configure()
            .buildSessionFactory();

    @SuppressWarnings("unchecked")
    public ArrayList<Template> getTemplateInfo() {
        Session sesion = sf.openSession();
        sesion.beginTransaction();

        tempList = (ArrayList<Template>) sesion.createCriteria(Template.class)
                .list();

        sesion.close();

        return tempList;
    }


    public void addTemplate(Template template) {

        Session sesion = sf.openSession();
        sesion.beginTransaction();
        sesion.save(template);
        sesion.getTransaction().commit();
        sesion.close();

    }

    Session session;
    Transaction tr;

    public ArrayList<Template> deleteTemplates(ArrayList<Integer> list) {

        try {
            session = sf.openSession();
            tr = session.beginTransaction();
            for (int i = 0; i < list.size(); i++) {
                temp = (Template) session.get(Template.class, list.get(i));
                session.delete(temp);

            }
            tr.commit();
            return (ArrayList<Template>) session.createCriteria(Template.class).list();
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
        } finally {

            session.close();
        }
        return null;

    }
}
