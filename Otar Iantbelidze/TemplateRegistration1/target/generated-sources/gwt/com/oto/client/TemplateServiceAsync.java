package com.oto.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TemplateServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.oto.client.TemplateService
     */
    void getTemplateInfo( AsyncCallback<java.util.ArrayList<com.oto.shared.Template>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.oto.client.TemplateService
     */
    void addTemplate( com.oto.shared.Template template, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.oto.client.TemplateService
     */
    void deleteTemplates( java.util.ArrayList<java.lang.Integer> list, AsyncCallback<java.util.ArrayList<com.oto.shared.Template>> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static TemplateServiceAsync instance;

        public static final TemplateServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (TemplateServiceAsync) GWT.create( TemplateService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
