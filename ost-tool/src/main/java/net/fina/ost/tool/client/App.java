
package net.fina.ost.tool.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;

public class App implements EntryPoint {

	public void onModuleLoad () {
		CenterWidget centerWidget = new CenterWidget ();
		RootLayoutPanel.get ().add (centerWidget.asWidget ());
	}
}
