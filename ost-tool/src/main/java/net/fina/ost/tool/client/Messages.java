package net.fina.ost.tool.client;

/**
 * Interface to represent the messages contained in resource bundle:
 * D:/Dev/sources
 * /ost-tool/src/main/resources/net/fina/ost/tool/client/Messages.properties'.
 */
public interface Messages extends com.google.gwt.i18n.client.Messages {

	@DefaultMessage("Upload File")
	@Key("info")
	String info();

	@DefaultMessage("Identity")
	@Key("username")
	String username();

	@DefaultMessage("Password")
	@Key("password")
	String password();

	@DefaultMessage("Show")
	@Key("show")
	String show();

	@DefaultMessage("Decrypt")
	@Key("decrypt")
	String decrypt();

	@DefaultMessage("download")
	@Key("download")
	String download();

	@DefaultMessage("Success, click to ")
	@Key("downloadLink")
	String downloadLink();

	@DefaultMessage("error")
	@Key("R2")
	String R2();

	@DefaultMessage("no error")
	@Key("noR2")
	String noR2();

	@DefaultMessage("Select Fina File")
	@Key("finaFileNotSeleced")
	String finaFileNotSeleced();

	@DefaultMessage("Select Certificate")
	@Key("certificateNotSeleced")
	String certificateNotSeleced();

	@DefaultMessage("Select Identity")
	@Key("identityNotSelected")
	String identityNotSelected();

	@DefaultMessage("Select Password")
	@Key("passwordNotSeleted")
	String passwordNotSeleted();

}
