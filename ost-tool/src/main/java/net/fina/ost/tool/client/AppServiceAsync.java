package net.fina.ost.tool.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AppServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see net.fina.ost.tool.client.AppService
     */
    void greetServer( java.lang.String name, AsyncCallback<java.lang.String> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static AppServiceAsync instance;

        public static final AppServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (AppServiceAsync) GWT.create( AppService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
