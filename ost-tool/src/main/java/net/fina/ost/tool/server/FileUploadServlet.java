package net.fina.ost.tool.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadServlet
 */
public class FileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 17L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FileUploadServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// process only multipart requests
		if (ServletFileUpload.isMultipartContent(request)) {
			// Create a factory for disk-based file items
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			try {
				List<FileItem> items = upload.parseRequest(request);

				String finaFileName = "";
				byte[] finaFileContent = null;

				String password = "";
				byte[] certificateFileContent = null;

				for (FileItem item : items) {
					if (item.getFieldName().equalsIgnoreCase("uploadFinaFile")) {
						try (InputStream in = item.getInputStream();) {
							finaFileContent = new byte[in.available()];
							in.read(finaFileContent);
							finaFileName = item.getName();
						}
					}
				}

				for (FileItem item : items) {
					if (item.getFieldName().equalsIgnoreCase("uploadPFSFile")) {
						try (InputStream in = item.getInputStream();) {
							certificateFileContent = new byte[in.available()];
							in.read(certificateFileContent);
						}
					}
				}

				for (FileItem item : items) {
					if (item.getFieldName().equalsIgnoreCase("password")) {
						password = item.getString();
					}
				}

				/**
				 * Decrypt FinA File
				 */
				finaFileContent = AppUtil.decryptFinaFile(certificateFileContent, password.toCharArray(), finaFileContent);

				request.getSession().setAttribute("fileContent", finaFileContent);
				request.getSession().setAttribute("fileName", finaFileName.replace("fina", "zip"));
				response.setStatus(HttpServletResponse.SC_CREATED);
				response.getWriter().print("SUCCESS");
				response.flushBuffer();

			} catch (Exception e) {
				e.printStackTrace();
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An error occurred while creating the file : " + e.getMessage());
			}

		} else {
			response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Request contents type is not supported by the servlet.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
