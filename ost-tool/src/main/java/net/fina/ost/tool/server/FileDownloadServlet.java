
package net.fina.ost.tool.server;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FileDownloadServlet
 */
public class FileDownloadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	static final int BUFSIZE = 4096;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Object fileName = request.getSession ().getAttribute ("fileName");
		byte[] fileContent = (byte[]) request.getSession ().getAttribute ("fileContent");

		ServletOutputStream outStream = response.getOutputStream ();

		String mimetype = "application/octet-stream";

		response.setContentType (mimetype);

		response.setContentLength (fileContent.length);

		// sets HTTP header
		response.setHeader ("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		byte[] byteBuffer = new byte[BUFSIZE];
		ByteArrayInputStream bais = new ByteArrayInputStream (fileContent);
		DataInputStream in = new DataInputStream (bais);

		int lengthOfDataInBuffer = 0;
		// reads the file's bytes and writes them to the response stream
		while ( (in != null) && ( (lengthOfDataInBuffer = in.read (byteBuffer)) != -1))
			{
				outStream.write (byteBuffer, 0, lengthOfDataInBuffer);
			}
		in.close ();

		outStream.close ();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet (request, response);
	}

}
