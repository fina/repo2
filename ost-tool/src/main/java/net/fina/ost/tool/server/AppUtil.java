package net.fina.ost.tool.server;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.fina.common.server.util.ConfigurationUtil;
import net.fina.server.dcs.impl.ConverterUtil;

public class AppUtil {

	public static byte[] decryptFinaFile(byte[] certificate, char[] password, byte[] finaFile) throws Exception {

		String alias = ConfigurationUtil.get().get("KeyStoreAlias");

		Map<String, byte[]> files = ConverterUtil.extractZip(finaFile);

		net.fina.server.security.crypto.SecurityManager securityManager = new net.fina.server.security.crypto.SecurityManager(certificate, alias, password, files.get("stamp"));

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (ZipOutputStream zipOut = new ZipOutputStream(out)) {
			for (Map.Entry<String, byte[]> entry : files.entrySet()) {
				String fileName = entry.getKey();
				byte[] content = entry.getValue();
				if (ConverterUtil.isExtension(fileName, "xml")) {
					content = securityManager.decrypt(content);
					ZipEntry zipEntry = new ZipEntry(fileName);
					zipOut.putNextEntry(zipEntry);
					zipOut.write(content);
				}
			}
		}
		return out.toByteArray();
	}

}
