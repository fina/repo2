package net.fina.ost.tool.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.HtmlLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;

public class ResultWindow extends Window {

	private final Messages messages = GWT.create(Messages.class);

	private String errorMessage;
	private String link;

	public ResultWindow(String errorMessage, String link) {
		this.errorMessage = errorMessage;
		this.link = link;
		setHeadingText("Message");
		this.setSize("500px", "300px");
		initComponents();
	}

	private void initComponents() {
		VerticalLayoutContainer verticalLayoutContainer = new VerticalLayoutContainer();

		if (this.errorMessage == null || this.errorMessage.trim().isEmpty()) {
			Anchor downloadLink = new Anchor(messages.download(), link);
			downloadLink.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					hide();
				}
			});
			verticalLayoutContainer.add(new FieldLabel(downloadLink, messages.downloadLink()));
		} else {
			HtmlLayoutContainer htmlLayoutContainer = new HtmlLayoutContainer(errorMessage);
			verticalLayoutContainer.add(htmlLayoutContainer);
		}

		this.add(verticalLayoutContainer);
	}

}
