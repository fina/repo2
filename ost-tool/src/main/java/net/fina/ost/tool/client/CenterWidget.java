package net.fina.ost.tool.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.CenterLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent.SubmitCompleteHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.PasswordField;

public class CenterWidget implements IsWidget {

	private final Messages messages = GWT.create(Messages.class);

	@Override
	public Widget asWidget() {
		GWT.log(GWT.getHostPageBaseURL());

		CenterLayoutContainer centerLayoutContainer = new CenterLayoutContainer();
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeadingHtml("FinA file decryptor");
		contentPanel.setWidth(350);

		// Create a FormPanel and point it at a service.
		final com.sencha.gxt.widget.core.client.form.FormPanel formPanel = new com.sencha.gxt.widget.core.client.form.FormPanel();
		formPanel.setAction(GWT.getHostPageBaseURL() + "FileUploadServlet");
		formPanel.setEncoding(com.sencha.gxt.widget.core.client.form.FormPanel.Encoding.MULTIPART);
		formPanel.setMethod(com.sencha.gxt.widget.core.client.form.FormPanel.Method.POST);

		VerticalLayoutContainer panel = new VerticalLayoutContainer();

		final PasswordField passwordField = new PasswordField();
		passwordField.setName("password");
		passwordField.setWidth(200);

		final FileUploadField finaFileUploader = new FileUploadField();
		finaFileUploader.setName("uploadFinaFile");
		finaFileUploader.setWidth(240);
		finaFileUploader.getElement().setPropertyString("accept", ".fina");

		final FileUploadField certificate = new FileUploadField();
		certificate.setName("uploadPFSFile");
		certificate.setWidth(240);
		certificate.getElement().setPropertyString("accept", ".pfx");

		final AutoProgressMessageBox box = new AutoProgressMessageBox("Progress", "Please wait...");
		box.setProgressText("Uploading...");
		box.auto();

		final TextButton submit = new TextButton(messages.decrypt());
		submit.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				boolean error = false;
				String errorMessage = "";
				if (finaFileUploader.getValue().isEmpty()) {
					errorMessage = errorMessage.concat(messages.finaFileNotSeleced() + "\n");
					error = true;
				}
				if (certificate.getValue().isEmpty()) {
					errorMessage = errorMessage.concat(messages.certificateNotSeleced() + "\n");
					error = true;
				}
				if (passwordField.getText().trim().isEmpty()) {
					errorMessage = errorMessage.concat(messages.passwordNotSeleted());
					error = true;
				}
				if (error) {
					new ResultWindow(errorMessage, null).show();
					return;
				}

				box.show();
				formPanel.submit();
				formPanel.clearSizeCache();
				submit.disable();

			}
		});

		final FieldLabel forIdentitytb = new FieldLabel();
		forIdentitytb.setText(messages.username());
		forIdentitytb.setWidth(50);
		final FieldLabel forPasswordtb = new FieldLabel();
		forPasswordtb.setText(messages.password());

		// VerticalLayoutData verticalData = new VerticalLayoutData (1, -1);
		panel.add(new HTML("<br />"));
		panel.add(new FieldLabel(finaFileUploader, "fina"), new VerticalLayoutData(1, -1));
		panel.add(new FieldLabel(certificate, "certficate"), new VerticalLayoutData(1, -1));
		panel.add(new HTML("<br />"));
		panel.add(new FieldLabel(passwordField, "password"), new VerticalLayoutData(1, -1));
		panel.add(new HTML("<br />"));
		panel.add(submit, new VerticalLayoutData(1, -1));

		FieldSet set = new FieldSet();
		set.setCollapsible(false);
		set.setBorders(false);
		set.add(panel);

		formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				box.hide();
				String result = event.getResults();
				if (result.contains("SUCCESS")) {
					ResultWindow resultWindow = new ResultWindow(null, GWT.getHostPageBaseURL() + "FileDownloadServlet");
					resultWindow.setModal(true);
					resultWindow.show();
				} else {
					ResultWindow resultWindow = new ResultWindow(result, null);
					resultWindow.setModal(true);
					resultWindow.show();
				}
				submit.setEnabled(true);
			}
		});

		formPanel.add(set);
		contentPanel.add(formPanel);
		centerLayoutContainer.add(contentPanel);
		return centerLayoutContainer;

	}
}
